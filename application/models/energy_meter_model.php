<?php 
class energy_meter_model extends CI_Model {

		public static function fetchEnergyMeterDataByUnitTypeByFromDateByToDateByField( $strUnitType, $strFromDateTime , $strToDateTime , $strField ) {

				switch( $strField ) {
					case 'hour':
							$strGroupBy	= ' GROUP By HOUR( created_on )';
							break;
						
					case 'month':
							$strGroupBy	= ' GROUP By MONTH( created_on )';
							break;
							
					case 'date':
							$strGroupBy	= ' GROUP By DATE( created_on )';
							break;
							
					default:
							$strGroupBy = '';
				}
			
				$CI = get_instance();
		     	$sql="	SELECT 
			   							". $strUnitType . ", created_on
								FROM
											energy_meter_data 
								WHERE
											created_on >= '$strFromDateTime' AND created_on <= '$strToDateTime' "; 
			 
			  	$arrobjDbResult = $CI->db->query($sql);

			return $arrobjDbResult->result();
				
		}
		
		public static function fetchEnergyMeterDataByUnitTypeByFromDateByToDate( $strUnitType, $strFromDateTime , $strToDateTime  ) {
		
			// 				switch( $strField ) {
			// 					case 'hour':
			// 							$strGroupBy	= ' GROUP By HOUR( created_on )';
			// 							break;
		
			// 					case 'month':
			// 							$strGroupBy	= ' GROUP By MONTH( created_on )';
			// 							break;
				
			// 					case 'date':
			// 							$strGroupBy	= ' GROUP By DATE( created_on )';
			// 							break;
				
			// 					default:
			// 							$strGroupBy = '';
			// 				}
				
			$CI = get_instance();
			$sql="	SELECT
			   							". $strUnitType . ", created_on
					   							FROM
					   							energy_meter_data
					   							WHERE
					   							created_on >= '$strFromDateTime' AND created_on <= '$strToDateTime' ";
		
			$arrobjDbResult = $CI->db->query($sql);
		
			return $arrobjDbResult->result();
		
		}
		
	    public static function getLastHourEnergyMeterData( $strUnitType )
	    {
	    	$CI = get_instance();
	    	  $sql="SELECT
	    	". $strUnitType . ", created_on
	    	FROM
	    	energy_meter_data where
	    	`created_on` > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 1 HOUR)";
	    	$arrobjDbResult = $CI->db->query($sql);
	    	return $arrobjDbResult->result();
	    }
	    
	    public static function getLastHourEnergyMeterDataByUnitTypeByDeviceId(  $strUnitType,  $intDeviceId ) {
	
	    	$CI = get_instance();
	    	 $sql="SELECT
	    	". $strUnitType . ", created_on
	    	FROM
	    	energy_meter_data where
	    	`created_on` > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 1 HOUR) and unit_number = " . $intDeviceId;
	    	$arrobjDbResult = $CI->db->query($sql);
	    	return $arrobjDbResult->result();
	    }
	  	  
	    public static function getLastHourAcesAtmByUnitTypeByDeviceId(  $strUnitType,  $intDeviceId, $intAcesId = NULL ) {
	    
	    	$CI = get_instance();
	    	
	    	//for webservice
	    	if( is_array($strUnitType) ) {
	    		$strUnitType = implode(',',$strUnitType);
	    		$sql="SELECT ". $strUnitType . ", created_on
		    	FROM
		    	aces_atm where
		    	`created_on` > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 1 HOUR) and serial_no = " . $intDeviceId . " and id > " . $intAcesId;
	    	} else {
		     	$sql="SELECT
		    	". $strUnitType . ", created_on
		    	FROM
		    	aces_atm where
		    	`created_on` > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 1 HOUR) and serial_no = " . $intDeviceId;
	    	}
	    	
	    	$arrobjDbResult = $CI->db->query($sql);
	    	return $arrobjDbResult->result();
	    }
	    
	    public static function getHourlyWatt()
	    {
	    	$CI = get_instance();
	    	$sql="SELECT
	    		watts,  created_on
	    	FROM
	    	energy_meter_data where
	    	`created_on` > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 1 HOUR)";
	    	$arrobjDbResult = $CI->db->query($sql);
	    	return $arrobjDbResult->result();
	    }
	    
	    public static function getHourlyWattBydeviceId( $intDeviceId )
	    {
	    	$CI = get_instance();
	    	$sql="SELECT
	    		watts,  created_on
	    	FROM
	    	energy_meter_data where
	    	`created_on` > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 1 HOUR) and unit_number = " . $intDeviceId;
	    	$arrobjDbResult = $CI->db->query($sql);
	    	return $arrobjDbResult->result();
	    }
		
			// admin functions.
			public static function fetchTableFieldsByTableName( $strTableName ) {
			
					$CI = get_instance();
					return  ( array ) $CI->db->list_fields( $strTableName );
			}

			// This function will require in future, if functionality for adding column changed.
			public static function alterTableFieldByColumnNameByTableField( $stColumnName, $strTableField ) {
					
					$CI = get_instance();

					$arrstrFieldData = array( 	
							$strTableField => array(		
									'type' => 'smallint',
									'null' => 'true',
							)
					);
				
					return $CI->dbforge->add_column('energy_meter_data', $arrstrFieldData );
			
			}
			
			public static function alterTableColumnByColumnName( $strColumnName ) {
				
					$CI = get_instance();

					$arrstrColumnDetails = array(
							$strColumnName => array(
									'type' => 'mediumint',
									'null' => TRUE,
							)
					);
					
					return $CI->dbforge->add_column( 'energy_meter_data', $arrstrColumnDetails );
					
			}
			
			public static function getColumnCountByColumnName( $strColumnName =  NULL ) {
				
				if( NULL == $strColumnName ) return;
			
					$CI = get_instance();
					$CI->db->where( 'name', $strColumnName );
					$CI->db->from( 'mysql.help_keyword' );
					
					return $CI->db->count_all_results();
			}
			
			public static function getlatestStats( $fieldsarr  )
			{
				$CI = get_instance();
				//	echo "SELECT " . $fieldsarr . " FROM energy_meter_data WHERE created_on >= '$strFromDate' AND created_on <= '$strToDate' order by id DESC";
			$boolQueryResult = $CI->db->query("SELECT ".implode(',',$fieldsarr)." FROM energy_meter_data order by id DESC");
				return $energymeter =  $boolQueryResult->row();
				//return $energymeter->$strFieldValue;
			}
			
			public static function getlatestStatsByDeviceId( $fieldsarr, $intDeviceId ) {
				$CI = get_instance();
				//	echo "SELECT " . $fieldsarr . " FROM energy_meter_data WHERE created_on >= '$strFromDate' AND created_on <= '$strToDate' order by id DESC";
				$boolQueryResult = $CI->db->query("SELECT ".implode(',',$fieldsarr)." FROM energy_meter_data where unit_number=" . $intDeviceId . " order by id DESC");
				return $energymeter =  $boolQueryResult->row();
				//return $energymeter->$strFieldValue;
			}
			
			public static function getAcesAtmStatsByDeviceId( $fieldsarr, $intDeviceId ) {
				$CI = get_instance();
				//echo "SELECT ".implode(',',$fieldsarr)." FROM aces_atm where serial_no =" . $intDeviceId . " and created_on =NOW() order by id DESC"; die;
				$boolQueryResult = $CI->db->query("SELECT ".implode(',',$fieldsarr)." FROM aces_atm where serial_no =" . $intDeviceId . " and created_on > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 1 HOUR) order by id DESC");
				return $energymeter =  $boolQueryResult->row();
				//return $energymeter->$strFieldValue;
			}
			
			public static function getlatestStatsByFromDateByToDate( $fieldsarr, $strFromDate, $strToDate  )
			{
				$CI = get_instance();
			//	echo "SELECT " . $fieldsarr . " FROM energy_meter_data WHERE created_on >= '$strFromDate' AND created_on <= '$strToDate' order by id DESC";
				$boolQueryResult = $CI->db->query("SELECT " . $fieldsarr . ", created_on FROM energy_meter_data WHERE created_on >= '$strFromDate' AND created_on <= '$strToDate' order by id DESC");
				return $energymeter =  $boolQueryResult->row();
				//return $energymeter->$strFieldValue;
			}
			
			public static function getVoltageVesesCurrentData()
			{
				
				$CI = get_instance();
			$sql="SELECT voltage,current FROM `energy_meter_data` WHERE `created_on` > DATE_SUB( CURDATE( ) ,INTERVAL 1 DAY)";
				$arrobjDbResult = $CI->db->query($sql);
				return $arrobjDbResult->result();
				
			}
			
			public static function getVoltageVesesCurrentDataByDeviceId( $intDeviceId )
			{
			
				$CI = get_instance();
				$sql="SELECT voltage,current FROM `energy_meter_data` WHERE `created_on` > DATE_SUB( CURDATE( ) ,INTERVAL 1 DAY) AND unit_number=" . $intDeviceId;
				$arrobjDbResult = $CI->db->query($sql);
				return $arrobjDbResult->result();
			
			}
}
