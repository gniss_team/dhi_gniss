<?php
class Users_model extends CI_Model {
	
	// please check this function is used somewhere or not. If not remove this function...
	public static function fetchAdminUser( $strUserName, $strPassword ) {
	
		$CI = get_instance();
		$CI->db->where( 'username', $strUserName );
		//$CI->db->where( 'password',md5($strPassword));
		$CI->db->where( 'status', '1' );
		$CI->db->where( 'deleted_by', NULL );
		$objDbResult = $CI->db->get( 'users' );
		$rowResult = $objDbResult->row();
		return ( 1 == $objDbResult->num_rows && ( false != password_verify( $strPassword, $rowResult->password ) ) ) ? $rowResult: false;
	}
	
	public static function getTotalUserCnt()
	{
		$CI = get_instance();
		$CI->db->select('id');
		$CI->db->where( 'deleted_by', NULL );
	
		$objDbResult = $CI->db->get( 'users' );
		return $objDbResult->num_rows;
		
	}
		
	public static function fetchUserByUsernameByPasswordByUserType( $strUserName, $strPassword, $intUserType ) {

		$CI = get_instance();
		
		$strSql			= 'SELECT * FROM users WHERE ( account_number = ? OR username = ? OR email_address = ? OR mobile_number = ? ) and user_type_id = ? and deleted_by IS ?';	 
		$objDbResult 	= $CI->db->query ( $strSql, array($strUserName, $strUserName, $strUserName, $strUserName, $intUserType, NULL ));
		
		return ( 1 == $objDbResult->num_rows && ( false != password_verify( $strPassword, $objDbResult->row( 'password' ) ) ) ) ? $objDbResult->row(): false;
	}
	
	public static function fetchIdByAccountNumber( $strAccountNumber ) {
		
		//Used for webservices
		$CI = get_instance();
		$CI->db->select('id');
		$CI->db->where( 'account_number', $strAccountNumber );
		$CI->db->where( 'deleted_by', NULL );
		$query = $CI->db->get( 'users' );
		
		if ($query->num_rows() > 0) {
			$row = $query->row();		
			return $row->id;
		} else {
			return NULL;
		}
    }
    
    public static function fetchUsersByAccountNumber( $strAccountNumber ) {
    
    	//Used for webservices
    	$CI = get_instance();
    	$CI->db->select( '*' );
    	$CI->db->where( 'account_number', $strAccountNumber );
    	$CI->db->where( 'deleted_by', NULL );
    	$arrobjUser = $CI->db->get( 'users' );
   		
    	if( $arrobjUser->num_rows() > 0 ) {
    		return $arrobjUser->row();
    	} else {
    		return false;
    	}
    	
    }
	
    public static function fetchShareUsersByName( $strSearchName ) {
    	
    	$CI = get_instance();

    	$objDbResult = $CI->db->query(
			   'SELECT 
    				* 
    			FROM (
    					SELECT
			                id, account_number, CONCAT( first_name, \' \', last_name ) as name                                                    
			            FROM
			    			users
			    		WHERE
			    		    deleted_by IS NULL
    				)  gniss_users
    			WHERE
    				gniss_users.name LIKE "'.$strSearchName.'%"' );

    	if( 0 < $objDbResult->num_rows() ) {
    		return $objDbResult->result();
    	} else {
    		return false;
    	}

    }
    
	public static function fetchUserById( $intId ) {
	
		$CI = get_instance();
		$arrmixData = array();
	
		$CI->db->where( 'id', $intId );
		$arrobjUser = $CI->db->get('users');
	
		return $arrobjUser->row();
	}
	
	public static function fetchUserAddressesById( $intId ) {
	
			$CI = get_instance();
						
			$CI->db->select( 'ua.id, ua.user_id, ua.address, ua.zipcode, ua.country_id, ua.state_id, ua.city_id, lct.name as city, ls.name as state, lc.name as country' );
			$CI->db->from( 'user_addresses ua' );
			$CI->db->join( 'locations lc', 'ua.country_id = lc.id and lc.type = 0' , 'LEFT' );
			$CI->db->join( 'locations ls', 'ua.state_id = ls.id and ls.type = 1', 'LEFT' );
			$CI->db->join( 'locations lct', 'ua.city_id = lct.id and lct.type = 2', 'LEFT' );
			$CI->db->where( 'ua.user_id', $intId );
			$arrobjUser = $CI->db->get();
		
			return $arrobjUser->result();
	}

	public static function fetchUserNameById( $intUserId ) {

		$CI = get_instance();
		$CI->db->select('username');
		$CI->db->where('id', $intUserId);
		$arrUserNameObj = $CI->db->get('users');
	
		return $arrUserNameObj->row();
	}
	
	public static function fetchMobileNumbersById( $intId ) {
		
		//also used for webservices
		$CI = get_instance();
		$arrmixData = array();
	
		$CI->db->where( 'user_id', $intId );
		$arrobjUser = $CI->db->get( 'user_mobile_numbers' );
	
		return $arrobjUser->result();
	}
	
	public static function fetchSocialWebsitesById( $intId ) {
	
			$CI = get_instance();
			$arrmixData = array();
		
			$CI->db->where( 'user_id', $intId );
			$arrobjUser = $CI->db->get( 'user_social' );
		
			return $arrobjUser->result();
	}
		
	public static function fetchEmailAddressesById( $intId ) {
	
		$CI = get_instance();
		$arrmixData = array();
	
		$CI->db->where( 'user_id', $intId );
		$arrobjUser = $CI->db->get( 'user_emails' );
	
		return $arrobjUser->result();
	}
	
	public static function fetchUserAddressIdsByUserId( $inUsertId ) {
	
		// Fetch ids from user addresses.
		$CI = get_instance();
		$arrmixData = array();
		$CI->db->select( 'id' );
		$CI->db->where( 'user_id', $inUsertId );
		$arrobjUsers = $CI->db->get( 'user_addresses' );
	
	  	$arrintIds = array();
	   	foreach(  $arrobjUsers->result() as $intIndex => $objUser ) {
	   		$arrintIds[]	= $objUser->id;
	  	}
		return $arrintIds;
	}
	
	public static function fetchUserEmailIdsByUserId( $inUsertId ) {
	
			$CI = get_instance();
			$arrmixData = array();
			$CI->db->select( 'id, email_address' );
			$CI->db->where( 'user_id', $inUsertId );
			$arrobjUsers = $CI->db->get( 'user_emails' );
		
			$arrintIds = array();
			foreach(  $arrobjUsers->result() as $intIndex => $objUser ) {
				$arrintIds[]	= $objUser->id;
			}
			//return $arrintIds;
			return $arrobjUsers->result();
	}
	
	public static function fetchUserSocialWebsitesByUserId( $inUsertId ) {
	
		$CI = get_instance();
		$arrmixData = array();
		$CI->db->select( 'id, website' );
		$CI->db->where( 'user_id', $inUsertId );
		$arrobjUsers = $CI->db->get( 'user_social' );
	
		$arrintIds = array();
		foreach(  $arrobjUsers->result() as $intIndex => $objUser ) {
			$arrintIds[]	= $objUser->id;
		}
		//return $arrintIds;
		return $arrobjUsers->result();
	}
	
	public static function fetchUserMobileNumberIdsByUserId( $inUsertId ) {

			// Fetch ids from related to user.  
			$CI = get_instance();
			$arrmixData = array();
			$CI->db->select( 'id' );
			$CI->db->where( 'user_id', $inUsertId );
			$arrobjUsers = $CI->db->get( 'user_mobile_numbers' );
		
			$arrintIds = array();
			foreach(  $arrobjUsers->result() as $intIndex => $objUser ) {
				$arrintIds[]	= $objUser->id;
			}
			return $arrintIds;
	}
	
	public static function fetchOneFieldByUserId( $intId, $intFeild ) {
	
			$CI = get_instance();
			$arrmixData = array();
			$CI->db->select( $intFeild );
			$CI->db->where( 'id', $intId );
			$arrobjUser = $CI->db->get('users');
			return $arrobjUser->row();
	}
	
	public static function fetchEmailAddressByForgotPasswordFieldByUserType( $strForgotPasswordField , $strUserType ) {
	
			$CI = get_instance();
				
			$CI->db->or_where( 'username', $strForgotPasswordField );
			$CI->db->or_where( 'email_address', $strForgotPasswordField );
			
			if( user_type_model::USER  == $strUserType ) {
					$CI->db->or_where( 'account_number', $strForgotPasswordField );
			}
			$objDbResult = $CI->db->get('users');
	
			return ( 1 == $objDbResult->num_rows ) ? $objDbResult->row() : NULL;
	}
	
	public static function fetchAllUsers( ) {
		
			$CI = get_instance();
			$CI->db->select( '*' );
			$CI->db->from( 'users' );
			$arrobjUserInfo = $CI->db->get();
			
			return $arrobjUserInfo->result();
	}
	public static function fetchLatestUsers( ) {
	
		$CI = get_instance();
		$CI->db->select( 'first_name, last_name' );
		$CI->db->limit( 5,'DESC' );
		$CI->db->from( 'users' );
		$arrobjUserInfo = $CI->db->get();
			
		return $arrobjUserInfo->result();
	}

	public static function fetchUsers( ) {
		
			$CI = get_instance();
			$CI->db->select( 'id, account_number, role, first_name, last_name, username, email_address, is_active' );
			$CI->db->where( 'deleted_by', NULL );
			//$CI->db->where( 'created_by !=', $CI->session->userdata( 'admin_user_id' ) );
			$CI->db->where( 'id !=', $CI->session->userdata( 'admin_user_id' ) );
		//	$CI->db->where( 'user_type_id', 1 );
			$role_id = $CI->session->userdata( 'role_id' );
		
			if( true == isset( $role_id ) && false == empty( $role_id ) ) {
					$CI->db->where( 'role', $role_id );
			} else {
					$CI->db->where( 'role !=', 0 );
			}
		
			$CI->db->from( 'users' );
				
			$arrobjUserInfo = $CI->db->get();
		
			return $arrobjUserInfo->result();
		
	}
	
	public static function fetchUsersByUserIdByRoleIdByUserTypeId( $intUserId, $intRoleId ) {
		
			$CI = get_instance();
			$CI->db->select( 'id, account_number, role, first_name, last_name, username,email_address' );
			$CI->db->where( 'deleted_by', NULL );
			$CI->db->where( 'id !=', $intUserId );
			
			if( false == is_null( $intRoleId ) ) {
					$CI->db->where( 'role', $intRoleId );
			}
			
			if( 1 != $intUserId ) {
					$CI->db->where( 'created_by', $intUserId );
			} else {
					$CI->db->where( 'user_type_id', '1' );
			}
		
			$CI->db->from( 'users' );
			
			$arrobjUserInfo = $CI->db->get();

			return $arrobjUserInfo->result();
	}

	public static function fetchCustomers() {
		$CI = get_instance();
		$CI->db->select( 'id, account_number, first_name, last_name, username, email_address, is_active' );
		$CI->db->where( 'deleted_by', NULL );
		$CI->db->where( 'role', 0 );
		$CI->db->where( 'id !=', $CI->session->userdata( 'admin_user_id' ) );
		$CI->db->from( 'users' );
		$arrobjUserInfo = $CI->db->get();

		return $arrobjUserInfo->result();
	}

	public static function fetchRoles() {

		$CI = get_instance();
		$CI->db->select('id, name');
		$CI->db->from('user_role');
		$arrObjUserRole = $CI->db->get();

		return $arrObjUserRole->result();
	}

	public static function prefernces( ) {
	
		$CI = get_instance();
		$CI->db->select( '*' );
		$CI->db->from( 'prefernces' );
		$arrobjUserInfo = $CI->db->get();
			
		return $arrobjUserInfo->result();
	}
	public static function insert( $strToken , $account_number, $autoPassword = NULL ) {

			$CI = get_instance();
			if( $autoPassword ) {
				$strPassword = $autoPassword;
			} else {
				$strPassword = $CI->input->post('password');	
			}
			
			$options = array('cost' => 11, 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),);
			$strPassword = password_hash($strPassword, PASSWORD_BCRYPT, $options);
		
			$arrmixUserData = array(
	    			'first_name' 							=> $CI->input->post('first_name'),
	     			'last_name' 							=> $CI->input->post('last_name'),
	     			'email_address'					=> $CI->input->post( 'email_address' ),
	    			'username'							=> $CI->input->post( 'username' ),
	    			'password' 							=> $strPassword,
	    			'mobile_number'				=> $CI->input->post( 'mobile_number' ),
	     			'created_by' 						=> $CI->session->userdata( 'user_id' ),
	     			'created_on' 						=> date( 'Y-m-d H:i:s' ),
	    			'reset_password_code'	=> $strToken ,
	    			'account_number'	=>			$account_number
	    	);
			if( $autoPassword ) {
				$role['role'] = $CI->input->post('role');
				$arrmixUserData['created_by'] = $CI->session->userdata( 'admin_user_id' );
				$arrmixUserData['user_type_id'] = 1;
				$arrmixUserData = array_merge($arrmixUserData, $role);
			}
		
	      	return $CI->db->insert( 'users', $arrmixUserData );
	}

	//Admin panel user
	public static function update_user( $intUserId, $userData ) {
		$CI = get_instance();

		$arrmixUserData = array(
    			'first_name' 		=> $userData['first_name'],
     			'last_name' 		=> $userData['last_name'],
     			'email_address'		=> $userData['email_address'],
    			'username'			=> $userData['username'],
    			'mobile_number'		=> $userData['mobile_number'],
     			'updated_by'		=> $CI->session->userdata( 'admin_user_id' ),
     			'updated_on'		=> 'NOW()',
	    			
	    	);
		$CI->db->where( 'id', $intUserId );
    	
    	return $CI->db->update( 'users', $arrmixUserData );
	}

	public static function updatePrimaryEmail( $arrmixData, $intUserId ) {
	
		$CI = get_instance();
		if( true == isset( $arrmixData['user_email'] ) ) {
			$arrmixData['email_address'] =  $arrmixData['user_email'];
			unset( $arrmixData['user_email'] );
		}
		$CI->db->where( 'id', $intUserId );
	
		return $CI->db->update( 'users', $arrmixData );
	
	}
	
	public static function deleteUserById( $intUserId ) {
		$CI = get_instance();
           
        $arrmixUserData = array(
                'deleted_on'    => date( 'Y-m-d h:i:s' )
        );
       
        $arrmixUserData['deleted_by']    = $CI->session->userdata( 'admin_user_id' );
           
        $CI->db->where('id', $intUserId );
       
        return $CI->db->update( 'users', $arrmixUserData );
	}

    public static function update( $arrPost, $strImageName = NULL, $intUserId ) {  
    	
    	$CI = get_instance();
    	//$intUserId = $CI->session->userdata( 'user_id');
    	$arrmixData = array(
    			'updated_by'		=> $intUserId,
    			'updated_on'		=> 'NOW()'
    	);
    	if( true == isset( $arrPost['user_email'] )) {
    		unset( $arrPost['user_email'] );
    	}
    	if( true == isset( $arrPost['user_country_code'] ) ) {
    		$arrmixData['country_code']	= $arrPost['user_country_code'];
    		unset( $arrPost['user_country_code'] );
    	}
    	if( true == isset( $arrPost['user_mobile_number'] ) ) {
    		$arrmixData['mobile_number']	= $arrPost['user_mobile_number'];
    		unset( $arrPost['user_mobile_number'] );
    	}
    	unset( $arrPost['website'] );
    	unset( $arrPost['website_value'] );
    	unset( $arrPost['user_website_id'] );
    	unset( $arrPost['user_mobile_id'] );
    	unset( $arrPost['mobile_value'] );
    	unset( $arrPost['mobile_number'] );
    	unset( $arrPost['user_email_id'] );
    	unset( $arrPost['email_address'] );
    	unset( $arrPost['email_value'] );
    	unset( $arrPost['country_code'] );
    	
    	$arrmixData = array_merge( $arrPost, $arrmixData);
    	
    	if( isset( $strImageName ) ) {
    		$arrImageData = array( 'profile_picture'	=>	$strImageName );
    		$arrmixData = array_merge( $arrmixData, $arrImageData);
    	}
    	
    	$CI->db->where( 'id', $intUserId );
    	
    	return $CI->db->update( 'users', $arrmixData );
    //	echo $sql = $ci->db->last_query();
    }
    public static function insertAddress( $arrmixData, $intUserId ) {
    	 
	    	$CI = get_instance();
	    	
	    	$arrmixData['user_id']	= $intUserId;
	    	//$CI->db->where( 'id', $intUserId );
	    	// print_r( $arrmixData ); die;
	    	return $CI->db->insert( 'user_addresses', $arrmixData );
    	
    }
    
    public static function insertEmail( $arrmixData, $intUserId  ) {
    
		    	$CI = get_instance();
		    	//$intUserId = $CI->session->userdata( 'user_id');
		    	$arrmixData['user_id']	= $intUserId;
		    	//$CI->db->where( 'id', $intUserId );
		    	// print_r( $arrmixData ); die;
		    	return $CI->db->insert( 'user_emails', $arrmixData );
    	 
   }
   
   public static function insertSocial( $arrmixData, $intUserId ) {
   
   	$CI = get_instance();
   
   	$arrmixData['user_id']	= $intUserId;
   		unset( $arrmixData['facebook_link'] );
   		unset( $arrmixData['twitter_link'] );
   		unset( $arrmixData['linkin_link'] );
   	//$CI->db->where( 'id', $intUserId );
   	// print_r( $arrmixData ); die;
   	return $CI->db->insert( 'user_social', $arrmixData );
   
   }
   
   public static function insertMobile( $arrmixData,  $intUserId  ) {
   
   	$CI = get_instance();
   
   	$arrmixData['user_id']	= $intUserId;
   	//$CI->db->where( 'id', $intUserId );
   	// print_r( $arrmixData ); die;
   	return $CI->db->insert( 'user_mobile_numbers', $arrmixData );
   
   }
    
    public static function updateAddress( $arrmixData  ) {
    
	    	$CI = get_instance();
	    	    	    
	    	$CI->db->where( 'id', $arrmixData['user_addr_id'] );
	    	unset( $arrmixData['user_addr_id'] );

	    	return $CI->db->update( 'user_addresses', $arrmixData );
    	 
    }
    
    public static function updateEmail( $arrmixData  ) {
    
	    	$CI = get_instance();
	    	
	    	$CI->db->where( 'id', $arrmixData['user_email_id'] );
	    	unset( $arrmixData['user_email_id'] );
	    
	    	return $CI->db->update( 'user_emails', $arrmixData );
    
    }
    
	public static function updateMobileVerificationStatus( $strField, $intPid  ) {
    
	    	$CI = get_instance();
	    	$arrmixData = array();
	    	if( 'user_mobile_number' == $strField ) {
	    			$arrmixData['mobile_number_status']	= '1';
	    			$CI->db->where( 'id', $CI->session->userdata( 'user_id') );
	    			return $CI->db->update( 'users', $arrmixData );
	    			
	    	} else {
	    			$arrmixData['status']	= '1';
	    			$CI->db->where( 'user_id', $CI->session->userdata( 'user_id') );
	    			$CI->db->where( 'id', $intPid );
	    			return $CI->db->update( 'user_mobile_numbers', $arrmixData );
	    	}
    }
    public static function updateSocial( $arrmixData  ) {
    
	    	$CI = get_instance();
	    
	    	$CI->db->where( 'id', $arrmixData['user_website_id'] );
	    	unset( $arrmixData['user_website_id'] );
	    	 
	    	return $CI->db->update( 'user_social', $arrmixData );
    
    }
    
    public static function updateMobile( $arrmixData  ) {
    
    	$CI = get_instance();
    
    	$CI->db->where( 'id', $arrmixData['user_mobile_id'] );
    	unset( $arrmixData['user_mobile_id'] );
    	 
    	return $CI->db->update( 'user_mobile_numbers', $arrmixData );
    
    }
    
    public static function deleteAddress( $arrintIds  ) {
    
    	$CI = get_instance();
    	 
      $CI->db->where_in('id', $arrintIds );

    	return $CI->db->delete( 'user_addresses' );
    
    }
    
    public static function deleteEmail( $arrintIds  ) {
    
    	$CI = get_instance();
    
    	$CI->db->where_in('id', $arrintIds );
    
    	return $CI->db->delete( 'user_emails' );
    
    }
    
    public static function deleteWebsite( $arrintIds  ) {
    
    	$CI = get_instance();
    
    	$CI->db->where_in('id', $arrintIds );
    
    	return $CI->db->delete( 'user_social' );
    
    }
    
    public static function deleteMobile( $arrintIds  ) {
    
	    	$CI = get_instance();
	    
	    	$CI->db->where_in('id', $arrintIds );
	    
	    	return $CI->db->delete( 'user_mobile_numbers' );
   }
    
	public static function sendTempPassword($strUsername, $strLoginAccountType ) {

    	$CI = get_instance();
    	$CI->load->library('parser');
    	$CI->db->where( 'username', $strUsername );
    	$objDbResult = $CI->db->get( 'users' );
    	return $objDbResult;
    
    }
    
    public static function checkForVerifiationLink( $strVerificationString, $strAccountType ){
    	
    	$CI = get_instance();
    	$CI->db->select('*');
    	$CI->db->where( 'reset_password_code', $strVerfictionString );
    	if('Individual' == $strAccountType ) {
    			$objDbResult = $CI->db->get( 'users' );
    	} else {
    				$objDbResult = $CI->db->get( 'companies' );
    	}		

    	return $objDbResult;
    }
    
    public static function fetchUserStatusByResetPasswordCode( $strToken = NULL ) {
    	
    	if( NULL == $strToken ) return;
    	
	    	$CI = get_instance();
	    	$CI->db->select( 'status' );
	    	$CI->db->where( 'reset_password_code', $strToken );
	    	
	    	$objDbResult = $CI->db->get( 'users' );
	    	    
	    	return ( 1 == $objDbResult->num_rows ) ? $objDbResult->row(): NULL;
   }
   
   public static function checkforaccountNumber(  ) {   	
   	$intAccountNumber=10;
  // 	$intAccountNumber .= mt_rand(100000000000, 999999999999);
   	$num1 = date("ymds");
   	$num2 = (rand(1000,9999));
   	//$num3 = time();
   	   $intAccountNumber .= $num1 . $num2 ;
   	$CI = get_instance();
   	$CI->db->select( 'id' );
   	$CI->db->where( 'account_number', $intAccountNumber );
   	$objDbResult = $CI->db->get( 'users' );
   	 return ( 1 == $objDbResult->num_rows ) ? self::checkforaccountNumber(): $intAccountNumber;
   }
   
   public static function fetchTokenExpirationTimeByResetPasswordCode( $strToken = NULL ) {
   
   		if( NULL == $strToken ) return;
   		
	   	$CI = get_instance();
	   	$CI->db->select( 'token_expiration_time' );
	   	$CI->db->where( 'reset_password_code', $strToken );
	   
	   	$objDbResult = $CI->db->get( 'users' );
	   
	  	 	return ( 1 == $objDbResult->num_rows ) ? $objDbResult->row(): NULL;
   }
    
	public static function checkPasswordAlreadyExists( $strPassword, $strUsertype ){
    	
    	$CI = get_instance();
    	if( $strUsertype == user_type_model::USER ) {
    		$CI->db->where( 'id', $CI->session->userdata( 'user_id' ) );
    	} else if ( $strUsertype == user_type_model::ADMIN ) {
    		$CI->db->where( 'id', $CI->session->userdata( 'admin_user_id' ) );
    	}
    	
    	$objDbResult = $CI->db->get( 'users' );
    	$rowResult = $objDbResult->row();
    	return ( 1 == $objDbResult->num_rows && ( false != password_verify( $strPassword, $rowResult->password ) ) ) ? $rowResult: false;
    }
    
    public static function changePassword( $strUsertype ) {
    
    	$CI = get_instance();
    	$options = array('cost' => 11, 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),);
    	$strPassword = $CI->input->post('new_password');
    	$strPassword = password_hash($strPassword, PASSWORD_BCRYPT, $options);
    	$arrmixData = array(
    			'password' => $strPassword
    	);
    	if( $strUsertype == user_type_model::USER ) {
    		$CI->db->where( 'id', $CI->session->userdata( 'user_id' ) );
    	} else {
    		$CI->db->where( 'id', $CI->session->userdata( 'admin_user_id' ) );
    	}
    	return $CI->db->update( 'users', $arrmixData );
		
    }
    
	public static function updateResetPasswordCode( $strEmailAddress, $StrRandomTempPass ) {
         $arrmixUserData = array();
         $CI = get_instance();
         $arrmixUserData['reset_password_code']    = $StrRandomTempPass;
         $arrmixUserData['token_expiration_time']	= time() + 24*60*60;
         
         $CI->db->where( 'email_address', $strEmailAddress );
                  
         return  $CI->db->update( 'users', $arrmixUserData);
     
    }
    
    public static function updatePassword( $strToken  ) {
    
              $CI = get_instance();
              $options = array('cost' => 11, 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),);
              $strPassword = $CI->input->post('password');
              $strPassword = password_hash($strPassword, PASSWORD_BCRYPT, $options);
              $arrmixUserData = array(
                        'password' 	=> $strPassword,
                        'reset_password_code' 	=> ""
              );
               
              $CI->db->where( 'reset_password_code', $strToken );
            
              return $CI->db->update( 'users', $arrmixUserData );

    
    }
    
    public static function updateStatus( $strToken ) {
    
	    	$CI = get_instance();
	    	$arrmixUserData = array(
	    			'status' 	=> '2',
	    			'reset_password_code' 	=> ""
	    	);
	    	 
	    	$CI->db->where( 'reset_password_code', $strToken  );
	    
	    	return $CI->db->update( 'users', $arrmixUserData );
    
    }
    
    public static function updateActiveStatus( $intUserId, $intStatus ) {
    
    	$CI = get_instance();
    	$arrmixUserData = array(
    			'is_active' 	=> $intStatus
    	);
    	 
    	$CI->db->where( 'id', $intUserId  );
    	 
    	return $CI->db->update( 'users', $arrmixUserData );
    
    }
    
    public static function checkUserAvailable( $strAccountNumber ){
    
    	$CI = get_instance();
    	$CI->db->where( 'account_number', $strAccountNumber );
    	$arrobjUserInfo = $CI->db->get( 'users' );
    
    	return $arrobjUserInfo->num_rows;
    }
   
    public static function fetchUserByUsername( $strUserName ) {
    
    	$CI = get_instance();
    	$CI->db->select('*');
    	$CI->db->where( 'username', $strUserName );
    	$CI->db->where( 'deleted_by', NULL );
    
    	$arrobjUser = $CI->db->get( 'users' );
    	
       	return ( 1 == $arrobjUser->num_rows ) ? $arrobjUser->row(): false;
    }
    
    public static function checkEmailAlreadyExists( $email ) {
    	
    	$CI = get_instance();
    	$CI->db->where( 'email_address', $email );
    	$arrobjUserInfo = $CI->db->get( 'users' );
    	
    	return $arrobjUserInfo->num_rows;
    	
    }
    
    public static function webserviceinsert( $firstname, $lastname, $email, $mobile, $username, $password , $account_number, $intGcmRegisterId, $strToken) {
    	
	    	$CI = get_instance();
	    	$options = array('cost' => 11, 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),);
	    	$strPassword = $password;
	    	$strPassword = password_hash($strPassword, PASSWORD_BCRYPT, $options);
	    
	    
	    	$arrmixUserData = array(
	    					'first_name' 							=> $firstname,
	    					'last_name' 							=> $lastname,
	    					'email_address'					=> $email,
	    					'username'							=> $username,
	    					'password' 							=> $strPassword,
	    					'mobile_number'				=> $mobile,
	    					'gcm_register_id'	=>	$intGcmRegisterId,
	    					'created_on' 						=> date( 'Y-m-d H:i:s' ),
	    					'reset_password_code'	=> $strToken ,
	    					'account_number'	=>			$account_number
	    	);
	    
	    	$CI->db->insert( 'users', $arrmixUserData );
	    	return $CI->db->insert_id();
    	
    }
    
    public static function ServiceProfile( $intuserid ) {
	    	$arrprofie=array();
	    	$CI = get_instance();
	    	$CI->db->select('*');
	    	$CI->db->where( 'id', $intuserid );
	    
	    	 
	    	$arrobjUser = $CI->db->get( 'users' );
	    	//print_r($arrobjUser->row());
	    	 
	    	$arrprofie['user_details'] =  $arrobjUser->row();
	    	$arrprofie['user_address']= self::fetchUserAddressesById( $intuserid );
	    	$arrprofie['user_phonenumbers']= self::fetchMobileNumbersById( $intuserid );
	    	$arrprofie['user_email']= self::fetchEmailAddressesById( $intuserid );
	    	$arrprofie['user_social']= self::fetchSocialWebsitesById( $intuserid );
	    	return $arrprofie;
    }
    
    public static function webserviceChangePasswrod( $accountNumber, $oldPassword, $newPasssword) {   	
    	$CI = get_instance();
    	
    	$CI->db->select('password');
    	$CI->db->where( 'account_number', $accountNumber );
 		$arrObjUsr = $CI->db->get( 'users' );
 		$user_detail = $arrObjUsr->row();
 		$saved_password = @$user_detail->password;
 		
 		if( password_verify( $oldPassword, $saved_password ) ) {
 			
 			$options = array('cost' => 11, 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),);
 			$newPasssword = password_hash($newPasssword, PASSWORD_BCRYPT, $options);
 			$arrPassword = array( 'password' => $newPasssword );
 			
 			$CI->db->where( 'account_number', $accountNumber );
 			return $CI->db->update( 'users', $arrPassword );
 			
 		} else {
 			
 			return false;
 			
 		}
 	 	
    }
    
    public static function webserviceUpdateGeneralProfile( $arrUserGeneralData ) {
    	
    	$CI = get_instance();
    	$account_number = $arrUserGeneralData['account_number'];
    	$emailIDS = $arrUserGeneralData['email_address'];
    	$intUserID = $arrUserGeneralData['user_id'];
    	$arrGeneralProfileData = array( 'first_name' => $arrUserGeneralData['first_name'],
    									'last_name' => $arrUserGeneralData['last_name'],
    									'email_address' => $emailIDS[0],
    									'updated_by'		=> $arrUserGeneralData['user_id'],
    									'updated_on'		=> 'NOW()'
    	 );
    	$CI->db->where( 'account_number', $account_number );
    	$is_update = $CI->db->update( 'users', $arrGeneralProfileData );
    	
    	if( count($emailIDS) == 1 && $is_update ) {
    		
    		$CI->db->where('user_id', $intUserID );
    		$is_deleted = $CI->db->delete( 'user_emails' );
    	
    		return true;
    		
    	}
    	
    	if( count($emailIDS) > 1 && $is_update ) {
    		
    		array_shift( $emailIDS );
    		
    		$CI->db->where( 'user_id', $intUserID );
    		$arrmixUserEmails = $CI->db->get( 'user_emails' );
    	
    		$arrmixUserEmails = $arrmixUserEmails->result();
    		$arrintUserEmails	= array();
    		foreach(  $arrmixUserEmails as $intIndex => $objUser ) {
    			$arrintUserEmails[]		= $objUser->email_address;
    		}
    		$arrintDeletedEmails = array_diff( $arrintUserEmails, $emailIDS);
    		
    		if( !empty( $arrintDeletedEmails ) ) {
    			
    			$CI->db->where_in('email_address', $arrintDeletedEmails );
    			
    			$is_deleted = $CI->db->delete( 'user_emails' );
    			if( false == $is_deleted ) {
    				return false;
    			}
    			
    		}
    		
    		foreach( $emailIDS as $key => $email ) {
    			
    			$CI->db->where( 'email_address', $email );
    			$arrObjEmails = $CI->db->get( 'user_emails' );
    			
    			if( $arrObjEmails->num_rows() > 0 ) {
    					
    				continue;
    				
    			} else {
    				
    				$arrEmail = array( 'email_address' => $email,
    									'user_id' =>  $intUserID );
    				$is_inserted = $CI->db->insert( 'user_emails', $arrEmail );
    				if( false == $is_inserted ) {
    					
    					return false;
    					
    				}
    			}
    			
    		}
    		return true;
    		
    	} else {
    		
    		return false;
    	}
    	
    }
    
    public static function webserviceDeleteEmailsByUserID( $intUserID ) {
    	 
    	$CI = get_instance();
    
    	$CI->db->where('user_id', $intUserID );
    	return $CI->db->delete( 'user_emails' );
    	 
    }
    
    public static function webserviceUpdateContactDetail( $arrUserContactData ) {
    	
    	$CI = get_instance();
    	$account_number = $arrUserContactData['account_number'];
    	$mobileNumbers = $arrUserContactData['mobile_number'];
    	$countryCodes = $arrUserContactData['country_code'];
    	
    	$arrContactData = array( 'mobile_number' => $mobileNumbers[0],
    					'country_code' => $countryCodes[0],
    					'office_country_code' => $arrUserContactData['office_country_code'],
    					'office_ext' => $arrUserContactData['office_ext'],
    	                'office_contact' => $arrUserContactData['office_contact'],
    					'home_country_code' => $arrUserContactData['home_country_code'],
    					'home_ext' => $arrUserContactData['home_ext'],
    					'home_contact' => $arrUserContactData['home_contact'],
    					'fax_country_code' => $arrUserContactData['fax_country_code'],
    					'fax_ext' => $arrUserContactData['fax_ext'],
    					'fax' => $arrUserContactData['fax'],
    					'updated_by'		=> $arrUserContactData['user_id'],
    					'updated_on'		=> 'NOW()'
    	 				);
    	$CI->db->where( 'account_number', $account_number );
    	return $CI->db->update( 'users', $arrContactData );
    	 
    }
    
    public static function webserviceDeleteMobilesByUserID( $intUserID ) {
    	
    	$CI = get_instance();
    	 
    	$CI->db->where('user_id', $intUserID );
    	return $CI->db->delete( 'user_mobile_numbers' );
    	
    }
    
    public static function webserviceAddMobiles( $arrInsertMobiles, $countryCodes, $intUserID) {
    	
    	$CI = get_instance();
    		
    	foreach( $arrInsertMobiles as $intKey => $mobileNo ) { 
    		
    		$arrMobileData = array( 'mobile_number' => $mobileNo,
    							'country_code' => $countryCodes[$intKey],
    							'user_id' => $intUserID);
    		$is_added = $CI->db->insert( 'user_mobile_numbers', $arrMobileData );
    		if( false == $is_added ) {
    		
    			return false;
    		
    		}
    			 
    	}
    		 
    	return true;
    		
    }
    
    public static function webserviceUpdateAddress( $arrUserAddressData ) {
    	$CI = get_instance();
    	
    	$userAddressData = $arrUserAddressData['address_details'];
    	$intUserID = $arrUserAddressData['user_id'];
    	if( !empty($userAddressData) ) {
    		
	    	foreach( $userAddressData as $intIndex => $addressData ) {
	    		
	    		$arrAddressData = array( 'user_id' => $intUserID,
	    						'address' => $addressData['address'],
	    						'zipcode' => $addressData['zipcode'],
	    						'country_id' => $addressData['country_id'],
	    						'state_id' => $addressData['state_id'],
	    						'city_id' => $addressData['city_id'],
	    		);
	    		$userAddressID = @$addressData['user_addr_id'];
	    		
	    		if( $userAddressID ) {
	    			$arrUpdatedData = array( 'updated_by'		=> $intUserID,
	    									'updated_on'		=> 'NOW()'
	    			);
	    			$arrAddressData = array_merge( $arrAddressData, $arrUpdatedData );
	    			$CI->db->where( 'id', $userAddressID );
	    			$is_updated = $CI->db->update( 'user_addresses', $arrAddressData );
	    			
	    			if( false == $is_updated ) {
	    				return false;
	    			}
	    			
	    		} else {
	    			
	    			$is_inserted = $CI->db->insert( 'user_addresses', $arrAddressData );
	    			
	    			if( false == $is_inserted ) {
	    				return false;
	    			}
	    			
	    		}
	    		$userAddressID = '';
	    		
	    	}
	    	
	    	return true;
	    	
    	} else {
    		
    		return false;
    		
    	}
    
    }
    
    public static function webserviceUpdatePreferences( $arrPreferencesData ) {
    	$CI = get_instance();
    	
    	$arrPreferenceData = array( 'fault_notify' => $arrPreferencesData['fault_notify'],
    					'newsletter_notify' => $arrPreferencesData['newsletter_notify'],
    					'alert_notify' => $arrPreferencesData['alert_notify'],
    					'updated_by'		=> $arrPreferencesData['user_id'],
    					'updated_on'		=> 'NOW()'
    	);
    	
    	$CI->db->where( 'id', $intUserID);
    	return $CI->db->update( 'users', $arrPreferenceData);
    	
    }
    
    public static function webserviceUploadProfilePicture( $strProfileImg, $intUserID ) {
    	
    	$CI = get_instance();
    	$arrProfileData = array( 'profile_picture' => $strProfileImg,
    							 'updated_by'		=> $intUserID,
    							 'updated_on'		=> 'NOW()' 
    	);
    	
    	$CI->db->where( 'id', $intUserID );
    	return $CI->db->update( 'users', $arrProfileData );
    }
    
 public static function webserviceSavePlace( $strProfileImg, $gniss_id ,$place_name,$lat,$lang ,$address  , $date ) {
    	
    	$CI = get_instance();
    	$arrProfileData = array( 'place_image' => $strProfileImg,
    							 'gniss_id'		=> $intUserID,
    							 'place_name'		=> $place_name,
    							 'lat'		=> $lat,
    							 'lang'		=> $lang,
    							 'address'		=> $address,
    							 'place_image'		=> 'NOW()' 
    	);
    	
    	
    	return $CI->db->insert( 'place_details', $arrProfileData );
    }

    public static function serviceUpdateSocial( $arrUpdateData  ) {
    
	    	$CI = get_instance();
	    	foreach ($arrUpdateData as $key => $value) {
	    		$CI->db->where( 'id', $key );
	    		$is_update = $CI->db->update( 'user_social',  $value);

	    		if( false == $is_update ) {
	    			return false;
	    		}
	    	}

	    	return true;
    }

    public static function webserviceInsertSocial( $socialData, $intuserid ) {

    	$CI = get_instance();
	   	$arrmixData['user_id']	= $intuserid;
	   	
	   	foreach ($socialData as $key => $value) {
	   		$arrmixData['website'] = $value;
	   		$is_insert = $CI->db->insert( 'user_social', $arrmixData );
	   		if( false == $is_insert ) {
	   			return false;
	   		}
	   	}

	   	return true;
	}

	public static function webserviceUpdateMobileVerificationStatus( $strField, $intUserId, $intMobileNumber, $intMobileId ) {
    
    	$CI = get_instance();
    	$arrmixData = array();
    	if( 'user_mobile_number' == $strField ) {
    			$arrmixData['mobile_number_status']	= '1';
    			$CI->db->where( 'id', $intUserId );
    			return $CI->db->update( 'users', $arrmixData );
    			
    	} else {
    			$arrmixData['status']	= '1';
    			$CI->db->where( 'user_id', $intUserId );
    			$CI->db->where( 'id', $intMobileId );
    			return $CI->db->update( 'user_mobile_numbers', $arrmixData );
    	}
    }

}
