<?php

class Asset_types_model extends CI_Model {

	public static function getCountAssetTypes() {
		
		$CI = get_instance();
		$CI->db->where( 'deleted_by', NULL );
   		$objDbResult = $CI->db->get( 'asset_types' );
		return $objDbResult->num_rows();  
	}
	
	public static function fetchAssetTypes() {
		$CI = get_instance();
		$CI->db->where( 'deleted_by', NULL );
		$objDbResult = $CI->db->get( 'asset_types' );
		return $objDbResult->result();
	}
	
    public static function getAssetTypes( $intAssetTypeId = null ) {   
    	   	
    	$CI = get_instance();
    	$CI->db->select( 'at.*, u.first_name, u.last_name, u.email_address' );
      	$CI->db->from( 'asset_types at' );
      	$CI->db->join( 'users u', 'u.id = at.created_by' );
      	
      	if( true == isset( $intAssetTypeId ) ) {

      		$CI->db->where( 'at.id', $intAssetTypeId );
      	} else {

      		$CI->db->where( 'at.deleted_by', NULL );
      	}
      		
		$objDbResult = $CI->db->get();
		$arrmixAssetTypes = array();
		
		foreach ( $objDbResult->result() as $assetType ) {
			
		    $arrmixAssetTypes[] = $assetType;
		}
		return $arrmixAssetTypes;
	}
	
	public static function insert() {
		
		$CI = get_instance();
		$arrmixData = array('name' 	=> $CI->input->post('typename'),
				'description'		=> $CI->input->post('typedescription'),
				'created_by'		=> $CI->session->userdata('admin_user_id'),
				'created_on' 		=> date('Y-m-d H:i:s') );

		return $CI->db->insert( 'asset_types', $arrmixData );		
	}  
	
	public static function update( $intAssetTypeId ) {
		
		$CI = get_instance();
		$arrmixData = array(
               	'name' 			=> $CI->input->post('typename'),
               	'description' 	=> $CI->input->post('typedescription'),
	 		   	'updated_by' 	=> $CI->session->userdata('admin_user_id'),
	 		   	'updated_on' 	=> date('Y-m-d H:i:s')
            						);
		$CI->db->where( 'id', $intAssetTypeId );
		
		return $CI->db->update( 'asset_types', $arrmixData);
		
	}
	
	public static function delete( $intAssetTypeId ) {
		
		$CI = get_instance();
		$arrmixData = array(
				'deleted_by' => $CI->session->userdata( 'admin_user_id' ),
				'deleted_on' => date( 'Y-m-d H:i:s' ) );
		$CI->db->where( 'id', $intAssetTypeId );
		
		return $CI->db->update( 'asset_types', $arrmixData );
	}
}