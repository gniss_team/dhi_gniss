<?php
class Devices_model extends CI_Model {
   
    public static function insert( $arrmixFormInputData ) {
         
            $CI = get_instance();
       
            $arrmixFormInputData['created_on']        = date( 'Y-m-d H:i:s' );
            $arrmixFormInputData['status']        = '1';

            $arrmixFormInputData[ 'created_by']    = $CI->session->userdata( 'user_id' );
            //print_r( $arrmixFormInputData ); die;
            return $CI->db->insert( 'devices', $arrmixFormInputData );
    }
   
    public static function update( $intDeviceId, $arrmixFormInputData ) {
    	 
    		$CI = get_instance();
	    	 
	    	$arrmixFormInputData['updated_on']        = date( 'Y-m-d H:i:s' );
	    	$arrmixFormInputData[ 'created_by']    		= $CI->session->userdata( 'user_id' );

    		$CI->db->where( 'id', $intDeviceId );

            return $CI->db->update(  'devices', $arrmixFormInputData );
    }
    
    public static function updatecomparameters( $intSerrialNumer, $arrmixFormInputData ) {
    	$CI = get_instance();
    	$CI->db->where( 'unit_id', $intSerrialNumer );
    	return $CI->db->update( 'resources', $arrmixFormInputData );
    }
    
    public static function insertcomparameters( $arrmixFormInputData ) {
    	$CI = get_instance();
    	return $CI->db->insert( 'resources', $arrmixFormInputData );
    }
    
    
     
    public static function fetchDevicesByUserId( $intUserId,  $intDeviceType = NULL, $strSearch = NULL, $strStatus = NULL ) {
    
            $CI = get_instance();
           
            $CI->db->select( 'd.*,dt.name as device_type, dt.unit_type,
										 (CASE	
													WHEN d.status = \'0\' THEN \'red\'
		            							WHEN d.status = \'1\' THEN \'green\'
	 										END ) as status' );
            $CI->db->from( 'devices d' ) ;
          
            	if( false == is_null( $intDeviceType ) ) {
            		$CI->db->join( 'device_types dt', 'dt.unit_type = d.device_type_id and d.device_type_id ='.  ( int ) $intDeviceType . ' and d.deleted_by IS NULL' );
            		
           } else {
      	      	$CI->db->join( 'device_types dt', 'dt.unit_type = d.device_type_id' );
          	  		$CI->db->where( 'd.deleted_by', NULL );
            }
            
           		if( false == is_null( $strStatus )) {
            	$CI->db->where( 'd.status', $strStatus );
            }
            
        	    if( false == is_null( $strSearch )) {
            	$CI->db->where( "( dt.name LIKE '%$strSearch%' OR d.sim_number  LIKE '%$strSearch%' OR d.device_number LIKE '%$strSearch%'
            			OR d.name LIKE '%$strSearch%' OR d.address LIKE '%$strSearch%' OR d.status LIKE '%$strSearch%' )" );
            }
            
            $CI->db->where( 'd.created_by', ( int ) $intUserId );

            $objDbResult = $CI->db->get();
        //  echo $sql = $CI->db->last_query();
        
            return $objDbResult;
           
    }
   
     public static function fetchDevicesByUserIdByLimit($intUserId,  $intDeviceType = NULL,  $start, $limit,  $strSearch = NULL, $strStatus = NULL ) {
     		
     	  		$arrmixDevicesData    = array();
	     	 
	     		$CI = get_instance();
	     	 
	     		$CI->db->select( 'd.*,dt.name as device_type, dt.unit_type,
											 (CASE
														WHEN d.status = \'0\' THEN \'red\'
			            							WHEN d.status = \'1\' THEN \'green\'
		 									END ) as status' );
	    	 $CI->db->from( 'devices d' ) ;
	  		  if( false == is_null( $intDeviceType ) ) {
	            	$CI->db->join( 'device_types dt', 'dt.unit_type = d.device_type_id and d.device_type_id =' .  ( int ) $intDeviceType  .' and d.deleted_by IS NULL' );
	            	
	        } else {
	            	$CI->db->join( 'device_types dt', 'dt.unit_type = d.device_type_id' );
	            	$CI->db->where( 'd.device_type_id', ( int ) $intDeviceType );
	            	$CI->db->where( 'd.deleted_by', NULL );
	        }
	    
	     	$CI->db->where( 'd.created_by', ( int ) $intUserId );
	     	
	     	if( false == is_null( $strStatus )) {
	     			$CI->db->where( 'd.status', $strStatus );
	     	}
	     	
	     	if( false == is_null( $strSearch )) {
	     			$CI->db->where( "( dt.name LIKE '%$strSearch%' OR d.sim_number  LIKE '%$strSearch%' OR d.device_number LIKE '%$strSearch%'
	     													OR d.name LIKE '%$strSearch%' OR d.address LIKE '%$strSearch%' OR d.status LIKE '%$strSearch%' )" );
	     	}
	     	   	 
	     	$CI->db->limit( $limit, $start );
	     	
	     	$arrobjDbResult = $CI->db->get();
	         	 
	     	return $arrobjDbResult;
   }
   
    public static function fetchDevicesByUserIdWebservice( $intUserId,  $intDeviceType = NULL ) {
   
        $arrmixDevicesData    = array();
           
        $CI = get_instance();
           
        $CI->db->select( 'd.device_type_id,dt.name as device_type' );
        $CI->db->from( 'devices d' ) ;
        $CI->db->join( 'device_types dt', 'dt.unit_type = d.device_type_id' );
        $CI->db->where( 'd.deleted_by', NULL );
        $CI->db->where( 'd.created_by', ( int ) $intUserId );
        $CI->db->group_by( 'd.device_type_id' );
           
        if( false== is_null( $intDeviceType ) ) {
            $CI->db->where( 'd.device_type_id', ( int ) $intDeviceType );
        }
   
        $objDbResult = $CI->db->get();
   
        return $objDbResult->result() ;
           
    }
   
    public static function fetchDevicesByDeviceTypeIdWebservice( $intUserId,  $intDeviceTypeId ) {
   
        $arrmixDevicesData    = array();
           
        $CI = get_instance();
           
        $CI->db->select( 'd.*');
        $CI->db->from( 'devices d' ) ;
        //$CI->db->join( 'device_types dt', 'dt.id = d.device_type_id' );
        $CI->db->where( 'd.deleted_by', NULL );
        $CI->db->where( 'd.created_by', ( int ) $intUserId );
        $CI->db->where( 'd.device_type_id', ( int ) $intDeviceTypeId );
           
        $objDbResult = $CI->db->get();
        return $objDbResult->result() ;
           
    }
    
    public static function checkforunitid( $intSerrialNumer ) { 
    	$CI = get_instance();
    	$CI->db->select( 'unit_id');
    	$CI->db->from( 'resources' ) ;
    	$CI->db->where( 'unit_id', ( int ) $intSerrialNumer );
       	$objDbResult = $CI->db->get(); 
    	return $objDbResult->num_rows() ;
    	 
    }
   
    public static function fetchDeviceNameByUserIdAndDeviceId( $intDeviceId ) {
        
    	$arrmixDevicesData    = array();
           
        $CI = get_instance();
           
        $CI->db->select( 'dt.name' );
        $CI->db->from( 'devices d' ) ;
        $CI->db->join( 'device_types dt', 'dt.id = d.device_type_id' );
        $CI->db->where( 'd.deleted_by', NULL );
        $CI->db->where( 'd.id', ( int ) $intDeviceId );
        $objDbResult = $CI->db->get();
        //$objDbResult = $CI->db->row();
       
        return $objDbResult->row()->name;
    }
   
    public static function fetchDeviceDataByUserIdAndDeviceTypeIdWebservice( $intDeviceNumber, $intDeviceId, $fromDate, $toDate ) {
   
       
        $device_name = self::fetchDeviceNameByUserIdAndDeviceId( $intDeviceId );
        //print_r($device_name);die;
       
        $arrmixDevicesData    = array();
        $fromDate = $fromDate." 00:00:00";
        $toDate = $toDate." 23:59:59";
        $CI = get_instance();
           
        $CI->db->select( '*');
        $CI->db->from( "$device_name") ;
        $CI->db->where( 'created_on >' , $fromDate );
        $CI->db->where( 'created_on <' , $toDate );
        $CI->db->where( 'unit_number' , $intDeviceNumber );
           
        $objDbResult = $CI->db->get();
   
        return $objDbResult->result() ;
           
    }
   
    public static function fetchDevicesByDeviceTypeIdByUserId( $intDeviceTypeId, $intUserId ) {
   
            $arrmixDevicesData    = array();
               
            $CI = get_instance();
               
            $CI->db->select( 'd.*');
            $CI->db->from( 'devices d' ) ;
            //$CI->db->join( 'device_types dt', 'dt.id = d.device_type_id' );
            $CI->db->where( 'd.deleted_by', NULL );
            $CI->db->where( 'd.created_by', ( int ) $intUserId );
            $CI->db->where( 'd.device_type_id', ( int ) $intDeviceTypeId );
               
            $objDbResult = $CI->db->get();
       
            return $objDbResult->result() ;
           
    }
    public static function fetchDeviceTypeByUserId( $intUserId ) {
                       
            $CI = get_instance();
               
            $objDbResult = $CI->db->query(
                                                    'SELECT
                                                                 dt.id, REPLACE( dt.name, \'_\', \' \' ) as name, dt.image, dt.unit_type
                                                    FROM
                                                                devices  d
                                                                JOIN device_types dt ON ( dt.unit_type = d.device_type_id )
                                                    WHERE
                                                                d.deleted_by IS NULL
                                                                AND d.created_by = ' . ( int ) $intUserId . '
                                                    GROUP BY
                                                                dt.id'
                                    );
       
           
            return $objDbResult->result() ;
    }
   
    public static function fetchDeviceTypesWebservice() {
    	
    	$arrmixDevicesData    = array();
    	 
    	$CI = get_instance();
    	 
    	$CI->db->select( 'id, name, image,unit_type');
    	$CI->db->from( 'device_types' ) ;
    	 
    	$objDbResult = $CI->db->get();
    	 
    	return $objDbResult->result() ;
    }
    
    public static function fetchDeviceTypeNameByDeviceTypeId( $intDeviceTypeId ) {
    	
    	$arrmixDevicesData    = array();
    	 
    	$CI = get_instance();
    	 
    	$CI->db->select( 'name' );
    	$CI->db->from( 'device_types' ) ;
    	$CI->db->where( 'id', ( int ) $intDeviceTypeId );
    	$objDbResult = $CI->db->get();
    	 
    	return $objDbResult->row()->name;
    }
    
    public static function fetchVTSDeviceList( $intUserId ) {
    	$arrmixDevicesData    = array();
    	$intDeviceTypeId=3;
    	$CI = get_instance();
    	 
    	$CI->db->select( 'd.*');
    	$CI->db->from( 'devices d' ) ;
    	//$CI->db->join( 'device_types dt', 'dt.id = d.device_type_id' );
    	$CI->db->where( 'd.deleted_by', NULL );
    	$CI->db->where( 'd.created_by', ( int ) $intUserId );
    	$CI->db->where( 'd.device_type_id', ( int ) $intDeviceTypeId );
    	 
    	$objDbResult = $CI->db->get();
    	return $objDbResult->result() ;
    }
    
    public static function fetchLatLongByPhoneNumberAndDeviceId( $intPhoneNumber, $intDeviceId ) {
    	
    	$arrmixDevicesData    = array();
    	 
    	$CI = get_instance();
    	 
    	$CI->db->select( 'latitude, longitude');
    	$CI->db->from( 'tracker' );
    	$CI->db->where('phoneno', $intPhoneNumber );
    	$CI->db->where('deviceid', $intDeviceId );
    	$CI->db->order_by('id', 'DESC' );
    	$CI->db->limit( 1, 0 );
    	 
    	$objDbResult = $CI->db->get();
    	 
    	return $objDbResult->result() ;
    }
    
    public static function fetchDeviceTableColumnsByDeviceTypeId( $intDeviceTypeId ) {
    	 
    	$deviceTypeName = self::fetchDeviceTypeNameByDeviceTypeId( $intDeviceTypeId );
    	$deviceTypeName = trim($deviceTypeName);
    	$arrmixDevicesData    = array();
    	$CI = get_instance();

    	$query = $CI->db->query("SHOW COLUMNS FROM $deviceTypeName ");
    	$fields = $query->field_data();
    	
    	return $fields;
    }
    
    public static function insertDeviceInformation( $intUserId=NULL, $intDeviceTypeId=NULL, $intDeviceName=NULL, $intDeviceNumber=NULL, $intSimNumber, $intIMEINumber, $strAddress , $strCity=NULL ) {
    	if( true == is_null( $intUserId ) || true == is_null( $intDeviceTypeId ) || true == is_null( $intDeviceName ) || true == is_null( $intDeviceNumber ) ) return 0;
    	$CI = get_instance();
    	$arrDeviceInformation = array(
    					'device_type_id'	=> $intDeviceTypeId,
    					'name'				=> $intDeviceName,
    					'device_number'		=> $intDeviceNumber,
    					'sim_number'		=> $intSimNumber,
    					'imei_number'		=> $intIMEINumber,
    					'address'			=> $strAddress,
    				     'status'			=> '1',
    					'created_by'		=> $intUserId,
    					'created_on'		=> date('Y-m-d h:i:s')
    	);
    	if( $intUserId && $intDeviceTypeId && $intDeviceName && $intDeviceNumber ) {
    		$CI->db->insert( 'devices', $arrDeviceInformation );
    		$intDeviceId = $CI->db->insert_id();
    		return $intDeviceId;
    	}
    }
    
    public static function deleteDeviceByIdByUserId( $intDeviceId, $intUserId ) {
       
            $CI = get_instance();
           
            $arrmixDeviceData = array(
                    'deleted_on'    => date( 'Y-m-d h:i:s' )
            );
           
            $arrmixDeviceData['deleted_by']    = $intUserId;
               
            $CI->db->where('created_by', $intUserId );
            $CI->db->where('id', $intDeviceId );
           
            return $CI->db->update( 'devices', $arrmixDeviceData );
    }
   
    /* delete if not needed ...
       public static function fetchAllDevices() {
        $CI = get_instance();
          $CI->db->select('d.*,dt.name');
          $CI->db->from('devices d');
          $CI->db->where('d.deleted_by', NULL );
          $CI->db->join('device_types dt', 'dt.id = d.device_type_id');
         
          if( $CI->session->userdata( 'customer_id' ) )  {
               
              $CI->db->select('d.*,dt.name,s.is_enable');
              $CI->db->join('sales s', 's.devices_id = d.id');
              $CI->db->where('s.customer_id', $this->session->userdata( 'customer_id' ) );
             
          }
         
      $boolQueryResult = $CI->db->get();
        $arrDevicesInfo = array();
        foreach ( $boolQueryResult->result() as $row ) {
            $arrDevicesInfo[]=$row;
        }
        return $arrDevicesInfo;
    }
   
     */
     
     function updateColumnName( $strDevice ) {
       
    //    print_r($this->input->post());exit;
        if(TRUE==is_array($this->input->post())) {
   
            foreach($this->input->post() as $key => $value) {
                if($key != "addassettype" && $key != "para" && $value!="" ) {
                $fields = array( $key => array('name' => str_replace(' ', '_', $value ),'type' => 'VARCHAR','constraint' => '100' ));
                $is_modify_column =  $this->dbforge->modify_column( $strDevice, $fields );
                }
            }
            foreach($this->input->post('para') as $column) {
       
                if($column!="") {
                $fields1 = array(str_replace(' ', '_', $column ) => array('type' => 'VARCHAR','constraint' => '100'));
                $this->dbforge->add_column($strDevice, $fields1);
                }
            }
        }
        return $is_modify_column;
   
    }
    function createbleByDeviceName(){
       
         $table_name = strtolower( preg_replace('/\s/', ' ', $this->input->post('device_name') ) );
         $table_name    = str_replace( ' ','_', $table_name );
            $this->dbforge->add_field('id');
             $is_table_created = $this->dbforge->create_table($table_name, TRUE);
             if(TRUE==$is_table_created)
             {
                 // after table create add it into device masters
                 $arrAddDevicetype = array(
                         'name'         => $table_name,
                         'created_on'         => date('Y-m-d H:i:s')
                 );
                     $this->db->insert('device_types', $arrAddDevicetype);
                 // end here
                 $fields = array(
                          
                         strtolower(str_replace(' ', '_', $this->input->post('para1') )) => array(
                                 'type' => 'VARCHAR',
                                 'constraint' => '100',
                         ),
                         strtolower(str_replace(' ', '_', $this->input->post('para2') )) => array(
                                 'type' =>'VARCHAR',
                                 'constraint' => '100'
                         )
                 );
                 $this->dbforge->add_column($table_name, $fields);
                 foreach($this->input->post('para') as $column) {
                     if($column!="") {
                             $fields1 = array( strtolower(str_replace(' ', '_', $column )) => array('type' => 'VARCHAR','constraint' => '100'));
                             $this->dbforge->add_column($table_name, $fields1);
                     }
                 }
            }
           
            return $is_table_created;
    }
   
    function delete( $strDeviceName ) {
       
        $this->db->where('name', $strDeviceName);
        $is_deleted = $this->db->delete('device_types');
        if(TRUE==$is_deleted) {
        $this->dbforge->drop_table($strDeviceName);
        }
        return $is_deleted;
       
    }
   
    function fetchDeviceByName( $strDeviceName ) {
        $strSql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='".$this->db->database."' AND
        TABLE_NAME = '".$strDeviceName."'
        AND COLUMN_NAME NOT IN( 'id' )";
        $objResult = $this->db->query($strSql);
        return $objResult->result();
       
    }

    public static function fetchDeviceTypes() {

            $CI = get_instance();
           
            $objResult = $CI->db->query('SELECT id,unit_type, REPLACE(name,\'_\',\' \') as name, created_on FROM device_types WHERE deleted_by IS NULL');
       
            return $objResult->result();
       
    }
    public function fetchDeviceTypeName( $intDeviceTypeId ) {       
       
        $objResult = $this->db->query("SELECT  name FROM device_types WHERE id='".$intDeviceTypeId."'");
       
        return $objResult->row()->name;
    }
      
    public  static function fetchDeviceByDeviceTypeIdByDeviceId( $intDeviceTypeId, $intDeviceId ) {
    	
    	  $CI = get_instance();
	   
	    	$CI->db->select( 'd.id, d.device_type_id, d.device_number, d.name, dt.unit_type ' );
	    	$CI->db->from( 'devices d' );
	    	$CI->db->join( 'device_types dt', 'dt.id = d.device_type_id' );
	    	$CI->db->where( 'd.device_type_id', (int) $intDeviceTypeId);
	    	$CI->db->where( 'd.id', ( int ) $intDeviceId );
	    	
	    	$objDbResult = $CI->db->get();
	    	
	    	return $objDbResult->row() ;
		
    }
    
    public static function fetchDeviceTypeById( $intDeviceTypeId = NULL )  {
    	
	    	$CI = get_instance();
	    	
	     	$sql	= 'SELECT unit_type, REPLACE(name,\'_\',\' \') as name, name as tbl_name FROM device_types';
// 	    	$CI->db->select( 'unit_type, REPLACE(name,"_"," ") as name' );
// 	    	$CI->db->from( 'device_types' ) ;
	    	
	    	if( false == is_null( $intDeviceTypeId ) ) {
	    			$sql	.= ' WHERE unit_type = ' . ( int ) $intDeviceTypeId;
	    	}		
	    
	    	$objDbResult	= $CI->db->query( $sql );
	   // 	$objDbResult = $CI->db->get();
	    	
    	return $objDbResult->row();
   }
   
   public static function fetchCountryWebservice( $type , $parent_id ) {
   	 
   	$arrmixDevicesData    = array();
   	 
   	$CI = get_instance();
   	 
   	$CI->db->select( '*');
   	$CI->db->from( 'locations' ) ;
   	$CI->db->where( 'parent_id', $parent_id );
   	$CI->db->where( 'type',  $type );
   	 
   	$objDbResult = $CI->db->get();
   	 // echo $sql = $CI->db->last_query();
   	return $objDbResult->result() ;
   }
   
   public static function getAcesAtmStatsByDeviceId( $fieldsarr, $intDeviceId ) {
   	$CI = get_instance();
   	//echo "SELECT ".implode(',',$fieldsarr)." FROM aces_atm where serial_no =" . $intDeviceId . " and created_on =NOW() order by id DESC"; die;
   	$boolQueryResult = $CI->db->query("SELECT ".implode(',',$fieldsarr)." FROM aces_atm where serial_no =" . $intDeviceId . " and created_on > SUBDATE( CURRENT_TIMESTAMP, INTERVAL 1 HOUR) order by id DESC");
   	//echo $CI->db->last_query();
   	return $energymeter =  $boolQueryResult->row();
   	//return $energymeter->$strFieldValue;
   }
   
    public static function updatedevicefault( $intUserid ) {
   	
    	$arrmixDevicesData    = array();
   	$CI = get_instance();
   	$CI->db->select( 'device_number');
   	$CI->db->from( 'devices' ) ;
   	$CI->db->where( 'created_by', $intUserid );
   	//$CI->db->where( 'device_number!=', "" );
   
   	$objDbResult = $CI->db->get();
   	//echo $sql = $CI->db->last_query();
   	
   	 foreach($objDbResult->result() as $rs)
   	 {
		   	 	if($rs->device_number!=""){
		   	 	$intDeviceId		=  $rs->device_number;
		   	 	$fieldsarr 			= array( 'ambient_temperature', 'set_temperature','grid_voltage', 'cyclic_period', 'ac_switch_duration', 'cum_kwh' , 'cum_watt_hour', 'ac1_stat', 'ac2_stat', 'compressor_status' );
		   	 	
		   	 	$getStatData = self::getAcesAtmStatsByDeviceId( $fieldsarr, $intDeviceId );
		   	 
			   	 	if( true == is_object( $getStatData ) ) {
			   	 			
			   	 		  $intAc1Fault		= ( ( 1 == $getStatData->ac1_stat ) && ( 0 == $getStatData->compressor_status && $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10 ) ) ? 0 : 1;
			   	 	 	 $intAc2Fault		= ( ( 1 == $getStatData->ac2_stat ) && ( 0 == $getStatData->compressor_status && $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10 ) ) ? 0 : 1;
						 $intAcFault		= ( 0 == $getStatData->ac1_stat && 0 == $getStatData->ac1_stat && 0 == $getStatData->compressor_status ) ? 0 : 1;
			   	 	  	 $intDeviceFault	= ( 0 == $getStatData->ac1_stat && 0 == $getStatData->ac2_stat && $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10) ? 0 : 1;
			   	 	 
			   	 			if($intAc1Fault == 0 || $intAc2Fault == 0 || $intDeviceFault == 0|| $intAcFault == 0 ) {
			   	 		    $upfault = self::updatefault( $intDeviceId ,'0' );
			   	 		}	else {  
			   	 			$upfault = self::updatefault( $intDeviceId ,'1' );
			   	 		}
			   	 			
			   	 	} else { 
			   	 			$upfault = self::updatefault( $intDeviceId ,'0');
			   	 	}
			   	 	
			   }
   	 }
  }
   
   public static function updatefault( $intDeviceId, $fault ) {
   	$CI = get_instance();
   	$arrmixDeviceData = array(
   			'status'    => $fault
   	);
   	$CI->db->where('device_number', $intDeviceId );
   	return $CI->db->update( 'devices', $arrmixDeviceData );
   }
   
   public static function fetchDevicesById( $intDevicesId ) {
   
   	$arrmixAssetsInfo         = array();
   
   	$CI = get_instance();
   
   	$CI->db->select( '*' );
   	$CI->db->from( 'devices') ;
   	 
   	$CI->db->where( 'id', $intDevicesId );
   	$CI->db->where( 'device_type_id', '3' );
   	
   	$CI->db->where( 'deleted_by', NULL );
   	 
   	$objDbResult = $CI->db->get();
   	 
   	return $objDbResult->row() ;
   }
   
   public static function getparalist( $intUnitId )
   {
   	$arrmixAssetsInfo  = array();
   	$CI = get_instance();
   	$CI->db->select( '*' );
   	$CI->db->from( 'resources') ;
   	$CI->db->where('unit_id', $intUnitId );
   	$CI->db->order_by("id", "desc");
   	$CI->db->limit(10);
   	$objDbResult = $CI->db->get();
   	return $objDbResult->row() ;
   
   
   }
   
}
