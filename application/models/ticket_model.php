<?php
class Ticket_model extends CI_Model {

	function fetchAllSubjects() {
		
		$this->db->select( '*' );
		$this->db->from( 'support_subjects' );
		$objQueryResult = $this->db->get();
		$arrSubjectsInfo = array();
		
		foreach ( $objQueryResult->result() as $objSubjects ) {

			$arrSubjectsInfo[] = $objSubjects;
		}
		return $arrSubjectsInfo;
	}
	

    
    function fetchAlltickets() {
    	
    	$this->db->select( '( concat( u.first_name,("  ") , u.last_name ) ) as name, s.subject, s.id, s.status, s.created_date, s.created_by, s.unread_user, s.unread_admin' );
    	$this->db->from( 'support s' );
    	$this->db->join( 'users u', 'u.id = s.created_by' );
    
    	$this->db->order_by( 'created_date','id' );
    	$objQueryResult = $this->db->get();
    	$arrmixTicketsInfo = array();
    	
    	foreach( $objQueryResult->result() as $objTickets ) {
    		
    		$arrmixTicketsInfo[] = $objTickets;
    	}
    	return $arrmixTicketsInfo;
    }
    
    function fetchFullticket( $intTicketId ) {
    	
    	$this->db->select( '*' );
    	$this->db->from( 'support' );
    	$this->db->where( 'id', $intTicketId );
    	
    	$objQueryResult = $this->db->get();
    	$arrmixTicketsInfo = $objQueryResult->result();
   // echo	$this->db->last_query();
    	$intRole = $this->session->userdata( 'role' );
    	if( 1 == $intRole ) {
    		
    		$arrmixUserData = array( 'unread_admin'	=> '0' );
    	}
    	else {
    		$arrmixUserData = array( 'unread_user'	=> '0' );
    	} 
    	$this->db->where( 'id', $intTicketId );
    	$this->db->update( 'support', $arrmixUserData );
    	// print_r($arrmixTicketsInfo);exit;
    	return $arrmixTicketsInfo[0];
    }
    
    function fetchTicketReplies( $intTicketId ) {
    	
    	$this->db->select( '( concat( u.first_name,("  ") , u.last_name ) ) as name, st.id, st.reply_from_id, st.reply_message, st.created_on' );
    	$this->db->from( 'support_thread st' );
    	$this->db->join( 'users u', 'u.id = st.reply_from_id' );
    	$this->db->where( 'st.ticket_id', $intTicketId );
    	$this->db->order_by( 'created_on','desc' );
    	$objQueryResult = $this->db->get();
    	$arrmixTicketReplies = array();
    	
    	foreach( $objQueryResult->result() as $objTicketReplies ) {
    		
    		$arrmixTicketReplies[] = $objTicketReplies;
    	}
    	return $arrmixTicketReplies;
    }
    
    function addNewTicket( $strSubject,$strTxtMessage,$intUserId ) {
    	
    	$arrmixData = array(
    			'created_date'	=>date( 'Y-m-d : H:i:s' ),
    			'subject'		=> $strSubject,
    			'message' 		=> $strTxtMessage,
    			'status' 		=> 1,
    			'unread_admin' 	=> 1,
    			'created_by' 	=> $intUserId
    	);
    	 
    	return $this->db->insert( 'support', $arrmixData );
    }
    
    function replyTicketnow( ) {
    	
    	$intReplyerId = $this->session->userdata( 'admin_user_id' );
    	$intRole = $this->session->userdata( 'role' );
    	$intTicketId = $this->input->post( 'ticketid' );
    	$strDataMsg = $this->input->post( 'reply-message' );
    	$arrmixData = array(
    			'created_on'	=> date( 'Y-m-d : H:i:s' ),
    			'reply_from_id'	=> $intReplyerId,
    			'ticket_id'		=> $intTicketId,
    			'status'		=> 1,
    			'reply_message'	=> $strDataMsg
    	);
    	$query =  $this->db->insert( 'support_thread', $arrmixData );
    	
    	if( 1 == $intRole ) {
    		
    		$sql = 'update support set unread_user=unread_user+1 where id = ?';
    	} else {
    		
    		$sql = 'update support set unread_admin=unread_admin+1 where id = ?';
    	}
    	$this->db->query( $sql, array( $intTicketId ) );
 	 
    	return $query;
    }
    
    
    function delete( $intTicketId ) {
    	
    	$arrmixData = array( 'status' => 0 );
    	$this->db->where( 'id', $intTicketId );
    	return $this->db->update( 'support', $arrmixData );
    }
}
