<?php 
class asset_model extends CI_Model {

		public static function insert( $arrmixFormInputData ) {
    	
 				$CI = get_instance();

				$arrmixFormInputData['created_on']		= date( 'Y-m-d H:i:s' );
				$arrmixFormInputData[ 'created_by']	= $CI->session->userdata( 'user_id' );

				return $CI->db->insert( 'assets', $arrmixFormInputData );
		}
          
    public static function fetchAssetsByUserId( $intUserId ) {
     
		$CI = get_instance();
   
		$strSql = 'SELECT 
											a.id, 
										 (CASE	
											WHEN a.status = \'0\' THEN  \'Disabled\'
	 										WHEN a.status = \'1\' THEN \'Enabled\'
	 									END ) as asset_status, 
											a.sim_number,
											a.name,
											a.sim_number, 
									 		
											a.imei_number,  
											a.phone_number,
											a.frequency_rate,
											a.over_speed_limit,
											a.status,
											a.created_by,
     									 ( case when a.phone_number=t.phoneno THEN 1 ELSE 0 END ) as live_flag
							FROM
										devices a
										LEFT JOIN tracker t ON ( t.phoneno = a.phone_number )
							WHERE		a.device_type_id	= 3 AND
										a.created_by	=	'.  $intUserId .' AND 
										a.deleted_by IS NULL
							GROUP BY
										a.id
							ORDER BY
										a.id desc
				';
				$arrmixAssetData	=		$CI->db->query( $strSql );
 
				return $arrmixAssetData->result();
     }
     
    public static function fetchAssetById( $intAssetsId ) {
    
               $arrmixAssetsInfo         = array();
                
               $CI = get_instance();
                
               $CI->db->select( '*' );
               $CI->db->from( 'assets') ;
                   
               $CI->db->where( 'id', $intAssetsId );
               $CI->db->where( 'deleted_by', NULL );
               
               $objDbResult = $CI->db->get();
                     
               return $objDbResult->row() ;
     }

    public static function update( $intTrackerId, $arrmixFormInputData ) {
               $CI = get_instance();

                 $arrmixFormInputData['updated_on']		= date( 'Y-m-d H:i:s' );
                 $arrmixFormInputData[ 'updated_by']	= $CI->session->userdata( 'user_id' );
                 
               $CI->db->where( 'id', $intTrackerId );

               return $CI->db->update('assets', $arrmixFormInputData);
     }
     
    public static function delete( $intAssetsId ) {
          
     
        $CI = get_instance();
        
 		$arrmixAssetData = array(
 					'deleted_on'	=> date( 'Y-m-d h:i:s' )
 		);
    
 		$arrmixAssetData['deleted_by']       = $CI->session->userdata( 'user_id' );
 		
 		$CI->db->where('id', $intAssetsId );
    	  	return $CI->db->update('devices', $arrmixAssetData );
  
     } 
     
     public static function fetchAssetByPhoneNumber( $intPhoneNumber ) {
     
     	$arrmixAssetsInfo         = array();
     
     	$CI = get_instance();
     
     	$CI->db->select( '*' );
     	$CI->db->from( 'devices') ;
     	 
     	$CI->db->where( 'phone_number', $intPhoneNumber );
     	$CI->db->where( 'deleted_by', NULL );
     	 
     	$objDbResult = $CI->db->get();
     	 
     	return $objDbResult->row() ;
     }
}