<?php
class Modules_Model extends CI_Model {
    
		const SIDEBAR				= 1;
		const PROFILE				= 2;
		const BREADCRUM		= 3;
		const HIDE						= 4;
		
		const ADMIN						= 0;
		const CUSTOMER				= 1;
		
		public static $arrstrModuleUrl	= array(
				'users'				=> 'admin/user/action/view',
				'devices'			=>'admin/devices/action/viewdevices',
				'customers'	=>'admin/user/action/viewCustomers',
				'roles'				=>'admin/roles/action/view',
				'modules'		=>	'admin/module/action/view',
				'dashboard'	=>	'admin/action/viewdashboard',
				'support'			=> 'admin/ticket/action/view'
		);
		
		public static function fetchModules() {
		
				$CI = get_instance();
				
		      $CI->db->select( 'id, name, description, (CASE WHEN is_published = \'0\' THEN  \'No\' ELSE \'Yes\' END ) as is_published,
		      						(CASE	WHEN is_admin = \'0\' THEN  \'Admin\'
		      								WHEN is_admin = \'1\' THEN \'Customer\' 
		      								WHEN is_admin = \'2\' THEN \'Both\'
		      						 END ) as is_admin,
		      		(CASE	WHEN position = \'1\' THEN  \'Sidebar\'
		      								WHEN position = \'2\' THEN \'Profile\' 
		      								WHEN position = \'3\' THEN \'Breadcrum\'
		      								WHEN position = \'4\' THEN \'HIde\'
		      						 END ) as position,
		      		(CASE	WHEN is_default = \'0\' THEN  \'No\'
		      								WHEN is_default = \'1\' THEN \'Yes\' 
		      						 END ) as is_default,
		      		order_num' );
		      $CI->db->from( 'modules' );
		      $CI->db->where( 'deleted_by', NULL );
		      $CI->db->order_by( 'id','desc' );
		      $objDbResult = $CI->db->get();
	
				return $objDbResult->result();
		}
		
		public static function fetchModulesCount() {
				$intTotalModuleCount		= $this->db->count_all( 'modules' );
		}
	    
	   public static function getOrderNum() {
	    	
		  		$CI = get_instance();
		   	$arrModulesInfo	= array();
		    	$CI->db->select_max( 'order_num' );
		    	$CI->db->from( 'modules' );
		    	$objDbResult = $CI->db->get();
	    		return $objDbResult->result();
	    }
	    
	    public static function insert( $intOrderNum, $strModuleImg ) {
	    	
	    	$CI = get_instance();
	    	$arrmixData = array(
	    		'name' 			=> $CI->input->post('name'),
	    		'parent_module_id' 			=> $CI->input->post('parent_module'),
	    		'description' 	=> $CI->input->post('description'),
	    		'internal_url' 	=> $CI->input->post('internal_url'),
	    		'is_admin' 		=> $CI->input->post('is_admin'),
	    		'position' 		=> $CI->input->post('position'),
	    		'is_published' 	=> $CI->input->post('is_published'),
	    		'order_num'		=> $intOrderNum,
	    		'image'			=> $strModuleImg,
// 	    		'is_default'	=> $CI->input->post('is_default'),
	    		'created_by' 	=> $CI->session->userdata('user_id'),
	    		'created_on' 	=> date('Y-m-d H:i:s')
	    	);
	    	
	    	return $CI->db->insert( 'modules', $arrmixData );
	    }
	
	    public static function update( $intModuleId, $strModuleImg ) {
	    	
			    	$CI = get_instance();
			    	$arrmixData = array(
				    		'name' 			=> $CI->input->post('name'),
				    		'description' 	=> $CI->input->post('description'),
	// 			    		'internal_url' 	=> $CI->input->post('internal_url'),
				    		'is_admin' 		=> $CI->input->post('is_admin'),
				    		'position' 		=> $CI->input->post('position'),
				    		'is_published' 	=> $CI->input->post('is_published'),
				    		'order_num'		=> $CI->input->post('order_num'),
				    		'image'			=> $strModuleImg,
				    		'is_default'	=> $CI->input->post('is_default'),
				    		'updated_by' 	=> $CI->session->userdata('user_id'),
				    		'updated_on' 	=> date('Y-m-d H:i:s')
			    	);
			    	$CI->db->where('id', $intModuleId );
			    	return $CI->db->update( 'modules', $arrmixData );
	    }
	    
	    public static function delete( $intModuleId ) {
	    	
	    	$CI = get_instance();
	    	
	    	$CI->db->where('id', $intModuleId );
	    	return $CI->db->delete('modules', $arrmixData );
	    }
	    
	    public static function fetchModuleById( $intModuleId ) {
    	
    	$CI = get_instance();
    	$arrmixModule = array();
    	
		$CI->db->where( 'id', $intModuleId );
		$objDbResult = $CI->db->get('modules');
		
		foreach( $objDbResult->result() as $objModule ) {
	    	$arrmixModule[]	= $objModule;
		}
		return $arrmixModule;
    } 

}