<?php
class Permission_model extends CI_Model {
	
	public static function fetchAllpermission() {
		
		$CI = get_instance();
	  	$CI->db->select( '*' );
	  	$CI->db->from( 'user_permission' );
	  	$CI->db->order_by( 'id','desc' );
	  	$strSql = $CI->db->get();
		$arrPermissionInfo = array();
		foreach( $strSql->result() as $row ) {
			$arrPermissionInfo[]=$row;
		}
		return $arrPermissionInfo;
	}
	
	public static function fetchAllpermissionByUserId($intUserId) {
		
			$CI = get_instance();
			$CI->db->select( '*' );
			$CI->db->from( 'user_permission' );
			$CI->db->where( 'user_id', $intUserId );
			$CI->db->order_by( 'id','desc' );
			$objDbResult = $CI->db->get();
			return $objDbResult->num_rows();
	}
	
	public static function fetchAllpermissionByRole( $intRoleId ) {
	
		$CI = get_instance();
		$CI->db->select( '*' );
		$CI->db->from( 'role_rights' );
		$CI->db->where( 'role_id', $intRoleId );
		$CI->db->order_by( 'id','desc' );
		$objDbResult = $CI->db->get();
		return $objDbResult->num_rows();
	}
		
	public static function insert( $module_id, $intUserId ) {
		
			$CI = get_instance();
			$arrmixData = array(
				'user_id'		=> $intUserId,
				'module_id' 	=> $module_id,
				'created_by' 	=> $CI->session->userdata('admin_user_id'),
				'created_on' 	=> date('Y-m-d H:i:s'),
			);
			return $CI->db->insert( 'user_permission', $arrmixData );
	}
	
	public static function insertRole( $intModuleId, $intRoleId ) {
	
			$CI = get_instance();
			$arrmixData = array(
					'role_id'				=> $intRoleId,
					'module_id' 		=> $intModuleId,
					'created_by' 	=> $CI->session->userdata('admin_user_id'),
					'created_on' 	=> date('Y-m-d H:i:s'),
			);
			return $CI->db->insert( 'role_rights', $arrmixData );
	}
	
	public static function fetchModulesByUserId( $intUserId ) {
		
		$CI = get_instance();
		$arrmixPermission = array();
		
		$CI->db->where( 'user_id', $intUserId );
		$objDbResult = $CI->db->get('user_permission');
		
		foreach ($objDbResult->result() as $objPermission) {
			$arrmixPermission[]=$objPermission;
		}
		return $arrmixPermission;
		
	}
	
	public static function fetchModulesByRoleId( $intRoleId ) {
		
			$CI = get_instance();
			$arrmixPermission = array();
				
			$CI->db->select('module_id,');
			$CI->db->from('role_rights');
			$CI->db->where('role_id', $intRoleId );

			$objDbResult = $CI->db->get();
			
			return $objDbResult->result();
	}
	
	public static function fetchPermissionByRoleId( $intRoleId, $boolIsAdmin ) {
	
			$CI = get_instance();
			$arrmixPermission = array();
			
			$CI->db->select('m.*,rr.role_id,');
			$CI->db->from('modules m');
		  	$CI->db->join('role_rights rr', 'm.id = rr.module_id and rr.role_id = ' .$intRoleId , 'left' );
			$CI->db->where('m.deleted_by', NULL );
			$CI->db->where('m.is_admin', $boolIsAdmin );
			$CI->db->or_where('m.is_admin','2');
	
			$CI->db->group_by('m.id');
			$objDbResult = $CI->db->get();
	
			return $objDbResult->result();
	}
	
	public static function fetchPermissionById( $intUserId, $boolIsAdmin ) {
	
			$CI = get_instance();
			$arrmixPermission = array();
			 
			$CI->db->select('m.*,up.user_id,');
			$CI->db->from('modules m');
			$CI->db->join('user_permission up', 'm.id = up.module_id and up.user_id = ' .$intUserId , 'left' );
			$CI->db->where('m.deleted_by', NULL );
			$CI->db->where('m.is_admin', $boolIsAdmin );
		
			$CI->db->group_by('m.id');
			$objDbResult = $CI->db->get('');
			 
			return $objDbResult->result();
	}
	public static function fetchModuleIdsByUserId( $intUserId ) {
	
		$CI = get_instance();
		$arrmixPermission = array();
		$CI->db->select( 'module_id' );
		$CI->db->where( 'user_id', $intUserId );
		$objDbResult = $CI->db->get('user_permission');
	
		foreach ($objDbResult->result() as $objPermission) {
			$arrmixPermission[]=$objPermission->module_id;
		}
		return $arrmixPermission;
	}
    
    public static function fetchModulesByModuleIdsByUserId( $arrintModuleIds, $intUserId, $boolIsAdmin ) {
    	
    	$CI = get_instance();
    	$arrmixPermission = array();

    	$strSqlCond = '';
    	if(  false == empty( $arrintModuleIds ) ) {
    			$strSqlCond	.= ' AND m.id IN ( '. implode( ', ' , $arrintModuleIds ) . ' ) ';
       }
    	
	   $strQuery = '(SELECT
   							m.id, m.internal_url,m.order_num, m.position, m.name, m.is_default, m.image
   						 FROM 
   							modules m join user_permission up on ( up.module_id = m.id )
   						 WHERE
   							m.is_admin = \'' . $boolIsAdmin . '\' 
   							AND m.is_published = \'1\'
   							AND up.user_id  = \'' . $intUserId .'\''. $strSqlCond . '
   						 )
   						UNION
   						(SELECT 
   							m.id, m.internal_url,m.order_num, m.position, m.name, m.is_default, m.image
   						FROM
   							modules m
   						WHERE
   							m.is_admin = \'2\'
   							AND m.is_published = \'1\'
   							AND m.is_default = \'1\')
   						ORDER BY 
   							order_num ASC';
	  //  echo $strQuery;die;
	   	$objDbResult   = $CI->db->query( $strQuery );
	
	   		return $objDbResult->result();
    }
    
    public static function fetchModulesByUserTypeId( $intUserTypeId ) {
	    	$CI = get_instance();
	     	
	     	$strQuery = '	SELECT
	   												m.id, m.parent_module_id,m.is_published, m.internal_url,m.order_num, m.position, m.name, m.is_default, m.image
	   						 			FROM
	   												modules m
	   									WHERE
	   												m.is_admin IN ( \'0\', \'2\' )
	     												AND m.is_published = \'1\'
	    									ORDER BY
	   												order_num ASC';
	    	 
	    	$objDbResult   = $CI->db->query( $strQuery );
	        	 
	    	return $objDbResult->result();
    }
    
    public static function update( $intModuleId,$intUserId, $booleanEnableStatus ) {
    	
    	$CI = get_instance();
    	$arrUpdatePermissionInfo = array(
    			//'status' 			=> $CI->input->post('status'),
    			'is_enabled' 		=> $booleanEnableStatus,
    			'updated_by' 		=> $CI->session->userdata('user_id'),
    			'updated_on' 		=> date('Y-m-d H:i:s')
    	);
    	$CI->db->where('user_id', $intUserId );
    	$CI->db->where('module_id', $intModuleId );
    	return $CI->db->update('user_permission', $arrUpdatePermissionInfo);
    }
    
    public static function delete( $intUserId ) {
    	
	    	$CI = get_instance();
	    	$CI->db->where('user_id', $intUserId );
	    	return $CI->db->delete( 'user_permission' );
	    	
    }
    
    public static function deleteRole( $intRoleId ) {
    	 
	    	$CI = get_instance();
	    	$CI->db->where('role_id', $intRoleId );
	    	return $CI->db->delete( 'role_rights' );
    }
}