<?php
class Messaging_model extends CI_Model {
	
    public static function getAllMessages( $strval ) {
    	
    	$CI = get_instance();
    	$CI->db->select('mt.*,m.message,m.sent_from as sent_from,m.date,m.subject,m.parent_id
    			 ,ufrom.first_name as ufromfname,ufrom.last_name as ufromlname 
    			,uto.first_name as utofname,uto.last_name as utolname');
    	$CI->db->from('msg_dtl mt');
    	$CI->db->join('messages as m', 'm.id = mt.msg_id');
    	$CI->db->join('users as ufrom', 'm.sent_from = ufrom.id');
    	$CI->db->join('users as uto', 'mt.sent_to = uto.id');
    	
    	if( $strval=='inbox' ) {
		   $CI->db->where( 'mt.sent_to', $CI->session->userdata('user_id') );
		   $CI->db->where( 'mt.status', 'inbox');
		 //  $this->db->where( 'm.parent_id', '0');
		  $CI->db->group_by("m.subject");
		  $CI->db->order_by("m.date", "DESC");
    	}
    	else if( $strval=='trash' ) {
    	
    		 $CI->db->where( 'mt.sent_to', $CI->session->userdata('user_id') );
    		
    		 $CI->db->where( 'mt.status', 'trash' ); 
    		// $this->db->or_where( 'sent_from', $this->session->userdata('user_id') );
    	}
    	else if( $strval=='sent' ) { 
    		
    		$CI->db->where( 'mt.sent_to', $CI->session->userdata('user_id') );
    		$CI->db->where( 'mt.status', 'sent' );
    		$CI->db->group_by("m.subject"); 
    		
    	}
    	else {
    		$CI->db->where( 'sent_to', $CI->session->userdata('user_id') );
    		 $CI->db->where( 'mt.status !=', 'trash' ); 
    	//	$CI->db->where( 'is_delete', '0' );
    	}
			
			$CI->db->order_by("m.id", "desc");
			$strSql = $CI->db->get();
			$arrmixData = array();
			//print_r($this->db->last_query());
			$i=0;
			foreach ( $strSql->result() as $row ) {
				if($row->status=='sent' || $row->status=='trash' )
				{
					$row->tousers= Messaging_model::getAllReciever( $row->msg_id );	
				}
				if($row->parent_id==0) {
					$row->totmsg= Messaging_model::getAllmsgTotal( $row->msg_id );
				} else {
					//$row->totmsg="0"; 	
					$row->totmsg= Messaging_model::getAllmsgTotal( $row->parent_id );
				}
			    $arrmixData[] = $row;
			    $i++;
			}
			return $arrmixData;
    }
    
    public static function getAllmsgTotal( $intMsgID ) {
    	
    	$CI = get_instance();
    	$CI->db->select('id');
    	$CI->db->from('msg_dtl');
    	$CI->db->where( 'parent_id', $intMsgID);
    	$CI->db->where( 'status', "inbox");
    	//$this->db->where( 'sent_to',  $this->session->userdata('user_id') );
    	$strSql = $CI->db->get();
    	//print_r($this->db->last_query());
    	return $strSql->num_rows();
    }
    
    public static function getAllReciever( $intMsgID ) {
    	
    	$CI = get_instance();
    	$CI->db->select('uto.first_name,uto.last_name');
    	$CI->db->from('msg_dtl mt');
    	$CI->db->join('users as uto', 'mt.sent_to = uto.id');
    	$CI->db->where( 'mt.msg_id', $intMsgID); 
    	$CI->db->where( 'mt.sent_to !=',  $CI->session->userdata('user_id'));
    	
    	$strSql = $CI->db->get();
    	$recTousers="";
    	//print_r($this->db->last_query());
    	
    	foreach ( $strSql->result() as $row ) {
    		$recTousers .= $row->first_name." ".$row->last_name.",";
    	}
    	return $recTousers;
    }
    
    public static function getAllCustomerByCustId( $intCustId ) {
    	
    	$CI = get_instance();
    	$CI->db->select('users.id,users.email_address,users.first_name,users.last_name,users.is_login');
    	$CI->db->from('users');
    	$CI->db->where( 'customer_id', $intCustId );
    	$CI->db->where( 'id !=', $CI->session->userdata('user_id') );
    	$CI->db->order_by('users.is_login desc, users.first_name asc');
    	$CI->db->where( 'deleted_by', NULL );
    	$CI->db->or_where( 'id', "1" );
    	
    	$strSql = $CI->db->get(); 
    	$arrmixData = array();
    	//print_r($this->db->last_query()); 
    	foreach ( $strSql->result() as $row ) {
    		$arrmixData[] = $row;
    	} 
    	return $arrmixData;
    }
    
    public static function getAllUsersByCustId( ) {
    	
    	$CI = get_instance();
    	$CI->db->select('users.id,users.email_address,users.first_name,users.last_name,users.is_login');
    	$CI->db->from('users');
    	// $this->db->where( 'customer_id', $intCustId );
    	$CI->db->where( 'id !=', $CI->session->userdata('user_id') );
    	$CI->db->where( 'users.deleted_by',NULL);
    	//$this->db->or_where( 'id', "1" );
    	// $this->order_by('users.is_login','DESC');
    	$CI->db->order_by('users.is_login desc, users.first_name asc');
    	$strSql = $CI->db->get();
    	$arrmixData = array();
    	//print_r($this->db->last_query());
    	foreach ( $strSql->result() as $row ) {
    		$arrmixData[] = $row;
    	}
    	return $arrmixData;
    }

    public static function sendMessage() {
    	
    	$CI = get_instance();
    	$arrmixData = array(
    			'sent_from'	=> $CI->session->userdata('user_id'),
    			'date'		=> date('Y-m-d : H:i:s'),
    			'subject'	=> $CI->input->post('subject'),
    			'message'	=> $CI->input->post('message')
    	);
    	$query =  $CI->db->insert( 'messages', $arrmixData );
    	$msg_id = $CI->db->insert_id();
    	
    	$arrmixData = array(
    			'sent_to'	=> $CI->session->userdata('user_id'),
    			'status'	=> 'sent',
    			'msg_id'	=> $msg_id,
    	);
     $CI->db->insert( 'msg_dtl', $arrmixData );
    	
    	if( $CI->input->post('msg_to') ) {
    		$name = $CI->input->post('msg_to');
    		foreach( $name as $key => $to ) {
    			$arrmixData2 = array(
    					'sent_to'	=> $to,
    					'status'	=> 'inbox',
    					'msg_id'	=> $msg_id
    			);
    		$query2 =  $CI->db->insert( 'msg_dtl', $arrmixData2 );
	    }
    	}
    	return $query;
    }
    
    public static function replyMessage() {
    	
    	$CI = get_instance();
    	$arrmixData = array(
    			'sent_from'	=> $CI->session->userdata('user_id'),
    			'date'		=> date('Y-m-d : H:i:s'),
    			'parent_id'	=> $CI->input->post('parent_id'),
    			'subject'	=> $CI->input->post('subject'),
    			'message'	=> $CI->input->post('message')
    	);
    	$query =  $CI->db->insert( 'messages', $arrmixData );
    	$msg_id = $CI->db->insert_id();
    	
    	$arrmixData = array(
    			'sent_to'	=> $CI->session->userdata('user_id'),
    			'status'	=> 'sent',
    			'parent_id'	=> $CI->input->post('parent_id'),
    			'msg_id'	=> $msg_id,
    	);
     	$CI->db->insert( 'msg_dtl', $arrmixData );
    	
    	if( $CI->input->post('message_to') ) {
    			$arrmixData2 = array(
    					'sent_to'	=> $CI->input->post('message_to'),
    					'status'	=> 'inbox',
    					'parent_id'	=> $CI->input->post('parent_id'),
    					'msg_id'	=> $msg_id
    			);
    			$query2 =  $CI->db->insert( 'msg_dtl', $arrmixData2 );
	 
    	}
    	return $query;
    	
    }
      
    public static function deleteMails( ) {
    
    	$CI = get_instance();
    	if(count($CI->input->post('ids')) > 0) {
    		foreach ( $CI->input->post('ids') as $users ) {
    			if( $CI->input->post('del_type')=='dl' ) {	
    				$CI->db->delete('msg_dtl', array('id' => $users));
		    		$CI->session->set_userdata("suc_msg","Your message deleted");
    			}else {
    				
    			$arrmixUserData = array('status' 	=> "trash");
    			$CI->db->where( 'id',$users );
    			$CI->db->update( 'msg_dtl', $arrmixUserData );
    			$CI->session->set_userdata("suc_msg","Your message moved to trash");
    			
    			}
    		
		    	
		    	 //print_r($this->db->last_query());
 			}
 			//exit;
    	}
     
    }
     
    public static function getMessagesById($id) {
     	
     	$CI = get_instance();
     	$CI->db->select('mg.*,m.message,m.date,m.subject,m.sent_from,m.parent_id,ufrom.first_name as ufromfname,ufrom.last_name as ufromlname');
     	$CI->db->from('msg_dtl mg');
     	$CI->db->join('messages as m', 'm.id = mg.msg_id');
     	$CI->db->join('users as ufrom', 'm.sent_from = ufrom.id');
     	$CI->db->where( 'm.id', $id);
     	$CI->db->or_where( 'm.parent_id', $id);
     	$CI->db->group_by( 'mg.msg_id');
     	$strSql = $CI->db->get();
     //	print_r($this->db->last_query());
     	//return $strSql->result(); 
     	foreach ( $strSql->result() as $row ) {
     		$arrmixData[] = $row;
     	}
     	return $arrmixData;
	} 
     
    public static function getInboxCount()   {
     	
    	$CI = get_instance();
    	$CI->db->select('id');
     	$CI->db->from('msg_dtl');
     	$CI->db->where( 'sent_to', $CI->session->userdata('user_id') );
     	$CI->db->where( 'is_read', "0");
     	$CI->db->where( 'status !=', "trash");
     	$CI->db->where( 'status !=', "sent");
     	$CI->db->where( 'status !=', "delete");
     	
     	$query = $CI->db->get();
     	return $query->num_rows(); 
    }
     
    public static  function getnotifycount() {
     
    	$CI = get_instance();
    	$CI->db->select('st.ticket_id');
    	$CI->db->from('support s');
    	$CI->db->join('support_thread as st', 's.id = st.ticket_id');
    	$CI->db->where( 'st.reply_from_id != ', $CI->session->userdata('user_id') );
    	$CI->db->where( 's.created_by = ', $CI->session->userdata('user_id') );

    	$query = $CI->db->get();
   //  print_r($this->db->last_query());
     	return $query->num_rows();
	} 

    public static function makeRead( $intMsgId ) {
    	
     	$CI = get_instance();
     	$arrmixUserData = array('is_read' 	=> "1");
     	$CI->db->where( 'msg_id', $intMsgId );
     	$CI->db->where( 'sent_to',$CI->session->userdata('user_id') );
     	$CI->db->update( 'msg_dtl', $arrmixUserData );
     	
     	$arrmixUserData = array('is_read' 	=> "1");
     	$CI->db->where_in('parent_id',$intMsgId);
     	$CI->db->update( 'msg_dtl', $arrmixUserData );
     	//print_r($this->db->last_query());
    }
     
	public static function getNotificationCount() {
     	
     	$CI = get_instance();
     	$role = $CI->session->userdata('role');
     	$userInSession = $CI->session->userdata('user_id');
 	
     	if(1==$role)
     	{
     		$CI->db->select_sum('unread_admin','countit');
     		 
     	}else
     	{
     		$CI->db->select_sum('unread_user','countit');
     		$CI->db->where( 'created_by', $userInSession );
     	}
     	$countNotifications = $CI->db->get( 'support' );
     	
     	$countset = $countNotifications->result();
     	if(empty($countset[0]->countit))
     	{
     		return '0';
     	}else{
     		return $countset[0]->countit;
     	}
    }
    
}
