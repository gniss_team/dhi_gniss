<?php
class Subscription_plan_Model extends CI_Model {
    
		
		public static function fetchSubscriptionPlans() {
		
				$CI = get_instance();
				
		      $CI->db->select( 'id, name, description, (CASE WHEN is_published = \'0\' THEN  \'No\' ELSE \'Yes\' END ) as is_published,
		      										thumbnail_image, price, subscription_length, subscription_period,
		      										(CASE WHEN is_lifetime_plan = \'0\' THEN  \'No\' ELSE \'Yes\' END ) as is_lifetime_plan, created_on' );
		      $CI->db->from( 'subscription_plan' );
		      $CI->db->where( 'deleted_by', NULL );
		      $CI->db->order_by( 'id','desc' );
		      $objDbResult = $CI->db->get();
	
				return $objDbResult->result();
		}
		
		public static function fetchModulesCount() {
				$intTotalModuleCount		= $this->db->count_all( 'modules' );
		}
	    
	   public static function getOrderNum() {
	    	
		  		$CI = get_instance();
		   	$arrModulesInfo	= array();
		    	$CI->db->select_max( 'order_num' );
		    	$CI->db->from( 'modules' );
		    	$objDbResult = $CI->db->get();
	    		return $objDbResult->result();
	    }
	    
	    public static function insert( $strPlanImg ) {
	    	
	    	$CI = get_instance();
	    	$arrmixData = array(
	    		'name' 								=> $CI->input->post('name'),
	    		'description' 					=> $CI->input->post('description'),
	    		'subscription_length'	=> $CI->input->post('subscription_length'),
	    		'subscription_period'	=> $CI->input->post('subscription_period'),
	    		'is_lifetime_plan' 			=> $CI->input->post('is_lifetime_plan'),
	    		'price'									=> $CI->input->post('price'),
	    		'is_published' 					=> $CI->input->post('is_published'),
	    		'thumbnail_image'		=> $strPlanImg,
	    		'created_by' 					=> $CI->session->userdata('admin_user_id'),
	    		'created_on' 					=> date('Y-m-d H:i:s')
	    	);
	    	
	    	return $CI->db->insert( 'subscription_plan', $arrmixData );
	    }
	
	    public static function update( $intSubscriptionPlanId, $strPlanImg ) {
	    	
			    	$CI = get_instance();
			    	$arrmixData = array(
				    		'name' 								=> $CI->input->post('name'),
				    		'description' 					=> $CI->input->post('description'),
				    		'subscription_length'	=> $CI->input->post('subscription_length'),
				    		'subscription_period'	=> $CI->input->post('subscription_period'),
				    		'is_lifetime_plan' 			=> $CI->input->post('is_lifetime_plan'),
				    		'price'									=> $CI->input->post('price'),
				    		'is_published' 					=> $CI->input->post('is_published'),
				    		'thumbnail_image'		=> $strPlanImg,
				    		'updated_by' 	=> $CI->session->userdata('admin_user_id'),
				    		'updated_on' 	=> date('Y-m-d H:i:s')
			    	);
			    	$CI->db->where('id', $intSubscriptionPlanId );
			    	return $CI->db->update( 'subscription_plan', $arrmixData );
	    }
	    
	    public static function delete( $intSubscriptionPlanId ) {
	    	
	    	$CI = get_instance();
	    	
	    	$CI->db->where('id', $intSubscriptionPlanId );
	    	return $CI->db->update('subscription_plan', $arrmixData );
	    }
	    
	    public static function fetchSubscriptionPlanById( $intSubscriptionPlanId ) {
    	
		    	$CI = get_instance();
		    	    	
				$CI->db->where( 'id', $intSubscriptionPlanId );
				$objDbResult = $CI->db->get('subscription_plan');
				
				return $objDbResult->result();
    } 

}