<?php 
class User_role_model extends CI_Model {

		public static function fetchUserRoles( ) {
		
				$CI = get_instance();
				$arrmixData = array();
				
				$arrobjUser = $CI->db->get('user_role');
			
				return $arrobjUser->result();

		}
		
		public static function fetchUserRoleById( $intUserRoleId ) {
			
				$CI = get_instance();
				$arrmixData = array();
				
				$CI->db->where( 'id', $intUserRoleId );
				$arrobjUserRole = $CI->db->get('user_role');
				
				return $arrobjUserRole->row();
			
		}
		
		public static function insert(){
				
				$CI = get_instance();
			
				$arrmixData = array(
						'name' 				=> $CI->input->post( 'name' ),
						'description'		=> $CI->input->post( 'description' )
				);
					
				return $CI->db->insert( 'user_role', $arrmixData );
		}
  		
		public static function update( $intUserRoleId ) {
			
			$CI = get_instance();
			
			$arrmixData = array(
					'name' 				=> $CI->input->post( 'name' ),
					'description'		=> $CI->input->post( 'description' )
			);
			$CI->db->where( 'id', $intUserRoleId );
			 
			return $CI->db->update( 'user_role', $arrmixData );
		}
		
		public static function delete( $intUserRoleId  ) {
		
				$CI = get_instance();
			
				$CI->db->where('id', $intUserRoleId );
			
				return $CI->db->delete( 'user_role' );
		
		}
}