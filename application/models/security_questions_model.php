<?php
class Security_questions_model extends CI_Model {
   
    public static function fetchSecurityQuestions() {
    
    	$CI = get_instance();
    	$arrmixSecurityQuestions	= array();
    		
    	$objDbResult	= $CI->db->get('security_questions');
    	    
    	return  $objDbResult->result();
    }
    public static function fetchSecurityQuestionsByUserTypeIdByUserId( $strLoginAccountType, $intUserId ) {
    
    	$CI = get_instance();
    	$arrmixSecurityQuestions	= array();
    	$CI->db->select('sq.*');
    	$CI->db->from('security_questions sq');
    	$CI->db->join('security_answers sa', 'sq.id = sa.question_id ');
    	$CI->db->where( 'sa.user_id', $intUserId );
       if( 'Individual' == $strLoginAccountType ) {
       			$CI->db->where( 'sa.user_type_id', 1 );
      }
      if( 'Company' == $strLoginAccountType ) {
      	$CI->db->where( 'sa.user_type_id', 2 );
      }
    	$objDbResult	= $CI->db->get();
    
    	   	return $objDbResult->row();
    }
    public static function insert( $intSecurityQuestionId, $intUserId, $intUsertypeId, $strAnswer ) {
    	$CI = get_instance();
    	$arrmixData = array(
    			'question_id' 	=> $intSecurityQuestionId,
    			'user_id' 		=> $intUserId,
    			'user_type_id'	=> $intUsertypeId,
    			'answer'		=> $strAnswer,
    			'created_by'	=> $intUserId,
    			'created_on' 	=> 'NOW()'
    	);
    	return $CI->db->insert( 'security_answers', $arrmixData );
    }
    
    public static function fetchSecurityQuestionByUserIdByUserTypeIdByQuestionIdByAnswer( $intUserId , $intUserTypeId, $intQuestionId, $strAnswer ) {
              
              $CI = get_instance();
              
              $CI->db->where( 'user_id', $intUserId );
              $CI->db->where( 'question_id', $intQuestionId );
              $CI->db->where( 'user_type_id', $intUserTypeId );
              $CI->db->where( 'LOWER( answer ) = ', $strAnswer );
		       // $CI->db->where( 'deleted_by', NULL );
              
              $objDbResult	= $CI->db->get('security_answers');
                  
                   return $objDbResult->row();
    }
    
    
}