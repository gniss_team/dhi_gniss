<?php
class Locations_model extends CI_Model {
	
			const COUNTRY		=	0;
			const STATE			=	1;
			const CITY			= 	2;
   
		    public static function fetchLocationsByType( $intType ) {
		    	
				    	$CI = get_instance();
				    	$arrmixLocations = array();
				    	
				    	$CI->db->select('id, name');
				    	$CI->db->where('type', $intType );
				    	$CI->db->from('locations');
				    	$CI->db->order_by('name', 'asc');
				    	
				    	$objDbResult	= $CI->db->get();
				    
				    	foreach ( $objDbResult->result() as $objLocation ) {
				    		$arrmixLocations[] = $objLocation;
				    	}
				    
				    	return $arrmixLocations;
		    }
    
		    public static function fetchLocationsById( $intLocationId ) {
		    
				    	$CI = get_instance();
				    	$arrmixLocations	= array();
				    		
				    	$CI->db->where('id', $intLocationId );
				    	$objDbResult	= $CI->db->get('locations');
				    
				    	foreach ( $objDbResult->result() as $objLocation ) {
				    		$arrmixLocations[] = $objLocation;
				    	}
				    	return $arrmixLocations;
		    }
    
		    public static function fetchLocationsByCountryIdByType( $intCountryId, $intType ) {
		    
				    	$CI = get_instance();
				    	$arrmixTypes	= array();
				        	
				    	$CI->db->where('parent_id', ( int ) $intCountryId );
				    	$CI->db->where('type', $intType );
				    	$objDbResult = $CI->db->get('locations');
				    
				    	foreach ( $objDbResult->result() as $objType ) {
				    		$arrmixTypes[] = $objType;
				    	}
				    	return $arrmixTypes;
		    }
    
		    public static function fetchLocationsByCountryIdByStateIdByType( $intCountryId, $intStateId, $intType ) {
		    
				    	$CI = get_instance();
				    	$arrmixLocations	= array();
				    	
				    	$CI->db->select('l.id, l.name');
				    	$CI->db->from('locations l');
				    	$CI->db->join('locations ls', 'ls.id = l.parent_id and ls.type = 1' );
				    	$CI->db->join('locations lc', 'lc.id = ls.parent_id and lc.type = 0' );
				    	$CI->db->where('lc.id', $intCountryId );
				    	$CI->db->where('ls.id', $intStateId );
				    	$CI->db->where('l.type', $intType );
				    	$objDbResult = $CI->db->get();
				    
				    	foreach ( $objDbResult->result() as $objLocation ) {
				    		$arrmixLocations[] = $objLocation;
				    	}
				    	return $arrmixLocations;
		    }
    
		    public static function fetchLocationsData( $strLoadType, $intLoadId ) {
		    
				    	$CI = get_instance();
				    	if( 'country_id' == $strLoadType && true == empty( $intLoadId ) ) {
				    		
				    		$strFieldList		= 'id, name';
				    		$strTableName		= 'locations';
				    		$strFieldName		= 'parent_id';
				    		$strOrderByField	= 'name';
				    		
				    		$CI->db->where( 'type', locations_model::COUNTRY );
				    	} elseif( 'country_id' == $strLoadType && false == empty( $intLoadId ) ) {
				    		
				    		$strFieldList		= 'id, name';
				    		$strTableName		= 'locations';
				    		$strFieldName		= 'parent_id';
				    		$strOrderByField	= 'name';
				    		
				    		$CI->db->where( 'type', locations_model::STATE );
				    	} else {
				    		
				    		$strFieldList		= 'id, name';
				    		$strTableName		= 'locations';
				    		$strFieldName		= 'parent_id';
				    		$strOrderByField	= 'name';
				    		
				    		$CI->db->where( 'type', locations_model::CITY );
				    	}
				    	
				    	$CI->db->select( $strFieldList );
				    	$CI->db->from( $strTableName );
				    	$CI->db->where( $strFieldName, $intLoadId );
				    	$CI->db->order_by( $strOrderByField, 'asc' );
				    	
				    	return $CI->db->get();
		    }
		    
		    public static function fetchLoadLocationsData( $strLoadType, $intLoadId, $intRow ) {
		    
		    	$CI = get_instance();
		    	if( 'country_id'.$intRow == $strLoadType && true == empty( $intLoadId ) ) {
		    
		    		$strFieldList		= 'id, name';
		    		$strTableName		= 'locations';
		    		$strFieldName		= 'parent_id';
		    		$strOrderByField	= 'name';
		    
		    		$CI->db->where( 'type', locations_model::COUNTRY );
		    	} elseif( 'country_id'.$intRow == $strLoadType && false == empty( $intLoadId ) ) {
		    
		    		$strFieldList		= 'id, name';
		    		$strTableName		= 'locations';
		    		$strFieldName		= 'parent_id';
		    		$strOrderByField	= 'name';
		    
		    		$CI->db->where( 'type', locations_model::STATE );
		    	} else {
		    
		    		$strFieldList		= 'id, name';
		    		$strTableName		= 'locations';
		    		$strFieldName		= 'parent_id';
		    		$strOrderByField	= 'name';
		    
		    		$CI->db->where( 'type', locations_model::CITY );
		    	}
		    	 
		    	$CI->db->select( $strFieldList );
		    	$CI->db->from( $strTableName );
		    	$CI->db->where( $strFieldName, $intLoadId );
		    	$CI->db->order_by( $strOrderByField, 'asc' );
		    	 
		    	return $CI->db->get();
		    }

		    public static function getLocationsData( $id, $type ) {
		    
		    	$CI = get_instance();
		    	$strFieldList		= 'id, name';
	    		$strTableName		= 'locations';
	    		$strFieldName		= 'parent_id';
	    		$strOrderByField	= 'name';
		    
		    	if( !empty($id) && $type == 'state' ) {
		    		$CI->db->where( 'type', locations_model::STATE );
		    	} else {
		    		$CI->db->where( 'type', locations_model::CITY );
		    	}
		    	 
		    	$CI->db->select( $strFieldList );
		    	$CI->db->from( $strTableName );
		    	$CI->db->where( $strFieldName, $id );
		    	$CI->db->order_by( $strOrderByField, 'asc' );
		    	 
		    	return $CI->db->get();
		    }

		    public static function getLocations() {

		    	$CI = get_instance();
		    	$CI->db->from('locations');
		    	return $CI->db->get();
		    }
}