<?php
class Company_model extends CI_Model {
	
	public static function fetchAllCompaniesByUserId( $intUserId ) {
	
		$CI = get_instance();
		$CI->db->select( '*' );
		$CI->db->from( 'companies' );
		$CI->db->where( 'created_by', $intUserId );
		$arrobjUserOwnedCompanies = $CI->db->get();
		
		return $arrobjUserOwnedCompanies->result();
	}
	
	public static function fetchCompanyCountByUserId( $intUserId ) {
		
		$CI = get_instance();
		$query	=	 $CI->db->query(
										'SELECT
       												count(*)  as record_count
					     					FROM
     												companies 
											WHERE
                    								created_by= ' .( int ) $intUserId . '
    													AND deleted_by IS NULL
     			');
		 
		return $query->row();
	}
	
	public static function fetchCompanyById( $intCompanyId ) {
	
		$CI = get_instance();
		$CI->db->select( '*' );
		$CI->db->from( 'companies' );
		$CI->db->where( 'id', $intCompanyId );
		$arrobjCompany = $CI->db->get();
	
		return $arrobjCompany->row();
	}
	public static function fetchCompanyDetailById( $intCompanyId ) {
		 
		$CI = get_instance();
		$CI->db->select( 'c.*,ct.name as city_name,st.name as state_name,cn.name as country_name,u.first_name,u.last_name' );
		$CI->db->from( 'companies c' );
		$CI->db->join('locations as ct', 'ct.id = c.city_id','left');
		$CI->db->join('locations as st', 'st.id = c.state_id','left');
		$CI->db->join('locations as cn', 'cn.id = c.country_id','left');
		$CI->db->join('users as u', 'u.id = c.created_by','left');
		$CI->db->where( 'c.id', $intCompanyId );
		$objQueryResult = $CI->db->get();
		
		return ( 1 == $objQueryResult->num_rows ) ? $objQueryResult->row(): false;
	}
	
	public static function fetchCompanyNameByCompanyId( $intCompanyId ) {
	
		$CI = get_instance();
		$CI->db->select('company_name');
		$CI->db->where( 'id', $intCompanyId );
		$CI->db->where( 'deleted_by', NULL );
		$query = $CI->db->get( 'companies' );
	
		if ($query->num_rows() > 0) {
			$row = $query->row();
			return $row->company_name;
		} else {
			return NULL;
		}
	}
	
	public static function fetchAllCompanies( ) {
		$CI = get_instance();
		$CI->db->select( '*' );
		$CI->db->from( 'companies' );
		//$CI->db->where( 'created_by', $CI->session->userdata('user_id') );
		$arrobjCompanies = $CI->db->get();

		return $arrobjCompanies->result();
	}
	
	public static function fetchAllUserCompanies( $intUserId ) {
			
		$CI = get_instance();
		$CI->db->select( 'id' );
		$CI->db->where( 'created_by', (int) $intUserId );
		$arrobjUserCompanies = $CI->db->get( 'companies' );
		$arrCompanyId = array();
	
		foreach ($arrobjUserCompanies->result() as $key) {
			$arrCompanyId[] = $key -> id;  
		}
		return $arrCompanyId;
	}
	
	public static function fetchBooleanUserCompanies( $intUserId, $intCompanyId ) {
			
		$CI = get_instance();
		$CI->db->select( 'id' );
		$CI->db->where( 'created_by', (int) $intUserId );
		$CI->db->where( 'id', (int) $intCompanyId );
		$arrobjUserCompanies = $CI->db->get( 'companies' );
		if ($arrobjUserCompanies->num_rows() > 0){
	        return TRUE;
	    }
	    else{
	        return FALSE;
	    }
	}
	
    public static function insertCompany( ) {
    	$CI = get_instance();

    	$arrmixData = array(
    			'company_name'		=>	$CI->input->post( 'company_name' ),
    			'username'			=>	$CI->input->post( 'username' ),
    			'password'			=>	md5( $CI->input->post( 'password' ) ),
    			'mobile_number'		=>	$CI->input->post( 'mobile_number' ),
    			'email_address'		=>	$CI->input->post( 'email_address' ),
    			'company_address'	=>	$CI->input->post( 'company_address' ),
    			'country_id'		=>	$CI->input->post( 'country_id' ),
    			'state_id'			=>	$CI->input->post( 'state_id' ),
    			'city_id'			=>	$CI->input->post( 'city_id' ),
    			'zip_code'			=>	$CI->input->post( 'zip_code' ),
    			'created_by'		=>	$CI->session->userdata( 'user_id' ),
    			'created_on'		=>	date( 'Y-m-d H:i:s' )
    	);
    	$CI->db->insert( 'companies', $arrmixData );
    	$intCompanyId = $CI->db->insert_id();
    	$CI->company_user_model->insert( $CI->session->userdata( 'user_id' ), $intCompanyId, '1', '1' );
    	return $intCompanyId;
    }
    
    public static function updateResetPassword( $strEmailAddress, $StrRandomTempPass  ) {
    	
		 $arrmixUserData = array();
		 
		 $CI = get_instance();
		 $arrmixCompanyData['reset_password_code']    = $StrRandomTempPass;
		 $arrmixUserData['token_expiration_time']			= time() + 24*60*60;
		 $CI->db->where( 'email_address', $strEmailAddress );

		 
		 return  $CI->db->update( 'companies', $arrmixCompanyData);
	}
	public static function updateResetPasswordToken( $strVerfictionString ,$strAccountType ) {
	
		$CI = get_instance();
		$arrmixUserData = array(
					'password' 	=> md5($CI->input->post('password')),
					'reset_password_code' 	=> ""
		);
		 
		$CI->db->where( 'reset_password_code', $strVerfictionString  );
	
		return $CI->db->update( 'companies', $arrmixUserData );
	
	}
    
    public static function updateCompanyStatus( $status , $intCompanyId ) {
    	$CI = get_instance();
    	$arrmixCompanyStatus = array(
    			'is_approved' => $status
    	);
    	
    	$CI->db->where('id', $intCompanyId);
    	return $CI->db->update('companies', $arrmixCompanyStatus);
    }
    
    public static function update( $arrPost, $intCompanyId, $strImageName = NULL ) {
    	//print_r($arrPost);die;
    
    	$CI = get_instance();
    	//$intCompanyId = $CI->session->userdata( 'company_id');
    	$arrmixData = array(
    			'updated_by'		=> $CI->session->userdata( 'user_id' ),
    			'updated_on'		=> 'NOW()'
    	);
    	$arrmixData = array_merge( $arrPost, $arrmixData);
    	
    	if( isset( $strImageName ) )
    	{
    		$arrImageData = array( 'profile_picture'	=>	$strImageName );
    		$arrmixData = array_merge( $arrmixData, $arrImageData);
    	}
    	
    	//print_r($arrmixData);die;
    	
    /*	( false == is_null( $strCompanyName ) ) ? $arrmixData['company_name']		= $strCompanyName : '';
    	( false == is_null( $intMobileNumber ) ) ? $arrmixData['mobile_number']		= $intMobileNumber : '';
    	( false == is_null( $strEmailAddress ) ) ? $arrmixData['email_address']		= $strEmailAddress : '';
    
    	( false == is_null( $intCountryId ) ) ? $arrmixData['country_id']			= $intCountryId : '';
    	( false == is_null( $intStateId ) ) ? $arrmixData['state_id']				= $intStateId : '';
    	( false == is_null( $intCityId ) ) ? $arrmixData['city_id']					= $intCityId : '';
    	( false == is_null( $strZipCode ) ) ? $arrmixData['zip_code']				= $strZipCode : '';
    	( false == is_null( $strProfileImg ) ) ? $arrmixData['profile_picture']		= $strProfileImg : '';
    	( false == is_null( $strCompanyAddress ) ) ? $arrmixData['company_address']	= $strCompanyAddress : '';
  */  
    	$CI->db->where( 'id', $intCompanyId );
    	return $CI->db->update( 'companies', $arrmixData );
    }

}
