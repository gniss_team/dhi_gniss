<?php
class Company_user_model extends CI_Model {
	
	public static function fetchAllCompanyUsers( $intCompanyId ) {
			
		$CI = get_instance();
		$CI->db->select( '*' );
		$CI->db->from( 'company_user cu' );
		$CI->db->join( 'users u', 'cu.uid = u.id' );
		$CI->db->where( 'cu.cid', (int) $intCompanyId );
		$CI->db->where( 'cu.user_type', '0' );
		$CI->db->where( 'cu.status', '1' );
		$arrobjCompanyUsers = $CI->db->get();
	
		return $arrobjCompanyUsers->result();
	}
	
	public static function fetchAllCompaniesByUserId( $intUserId ) {
	
		$CI = get_instance();
		$CI->db->select( 'c.id, c.company_name, cu.user_type, c.email_address' );
		$CI->db->from( 'companies as c ' );
		$CI->db->join('company_user cu', 'c.id = cu.cid');
		$CI->db->where( 'cu.uid', $intUserId );
		$CI->db->order_by( 'cu.user_type','asc' );
		$arrobjUserOwnedCompanies = $CI->db->get();
	
		return $arrobjUserOwnedCompanies->result();
	}
	
	public static function fetchAllCompanyRequests( $intUserId ) {
	
		$CI = get_instance();
		$CI->db->select( 'c.id,c.company_name' );
		$CI->db->from( 'companies as c ' );
		$CI->db->join('company_user cu', 'c.id = cu.cid');
		$CI->db->where( 'cu.uid', $intUserId );
		$CI->db->where( 'cu.status', '0' );
		$CI->db->where( 'cu.user_type', '0' );
		$CI->db->limit( 5 );
		$arrobjUserOwnedCompanies = $CI->db->get();
	
		return $arrobjUserOwnedCompanies->result();
	}
	
	public static function insert( $intId, $intCompanyId, $enumUserType, $enumStatus ) {
		
		$CI = get_instance();
		$arrmixCompanyUserData = array(
				'cid'		=>	$intCompanyId,
				'uid'		=>	$intId,
				'user_type'	=>	$enumUserType,
				'status'	=>	$enumStatus,
				'created_on'=>	date( 'Y-m-d H:i:s' )
		);
		return $CI->db->insert( 'company_user', $arrmixCompanyUserData );
	}
	
	public static function checkUserRegisteredWithCompany( $strTableName, $intUserId, $intCompanyId ){
	
		$CI = get_instance();
		$CI->db->select( 'id' );
		$CI->db->where( 'uid', $intUserId );
		$CI->db->where( 'cid', $intCompanyId );
		
		return $arrobjCompanyUserInfo = $CI->db->get( $strTableName );
	}
	
	public static function changeUserRequest( $intUserId, $intCompanyId, $enumRequestStatus ) {
		$CI = get_instance();
		$arrmixRequestStatus = array(
				'status' => $enumRequestStatus
		);
		$CI->db->where('uid', $intUserId);
		$CI->db->where('cid', $intCompanyId);
		return $CI->db->update('company_user', $arrmixRequestStatus);
	}

}