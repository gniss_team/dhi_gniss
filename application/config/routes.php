<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|  
*/   
  
$route['default_controller'] = "welcome";
$route['404_override'] = ''; 

$route['admin/action/(:any)'] 	= "admin/CManageAdminController/action/$1";
$route['admin/action'] 			= "admin/CManageAdminController/action";

$route['users/action/(:any)'] 	= "CManageUsersController/action/$1";
$route['users/action'] 			= "CManageUsersController/action";

$route['company/action/(:any)'] = "CManageCompanyController/action/$1";
$route['company/action'] 	    = "CManageCompanyController/action";

$route['profile/action/(:any)'] = "CManageProfileController/action/$1";
$route['profile/action'] 	    = "CManageProfileController/action";

$route['assets/action/(:any)'] 	= "CManageAssetController/action/$1";
$route['assets/action'] 		= "CManageAssetController/action";

$route['webservice/action/(:any)'] 	= "CManageWebServicesController/action/$1";
$route['webservice/action'] 		= "CManageWebServicesController/action";

$route['assettype/action/(:any)'] 	= "admin/CManageAssetTypeController/action/$1";
$route['assettype/action'] 		= "admin/CManageAssetTypeController/action";

$route['admin/energymeter/action/(:any)'] 	= "admin/CManageAdminEnergyMeterController/action/$1";
$route['admin/energymeter/action'] 		= "admin/CManageAdminEnergyMeterController/action";

$route['apps/action/(:any)'] 	= "CManageAppsController/action/$1"; 
$route['apps/action'] 		= "CManageAppsController/action";

$route['livetracking'] 			= "CManageLiveTrackingController";
$route['livetracking/getLatlong'] = "CManageLiveTrackingController/getLatlong";
$route['livetracking/demogetLatlong'] = "CManageLiveTrackingController/demogetLatlong";
$route['livetracking/demogetLatlong/(:any)'] = "CManageLiveTrackingController/demogetLatlong/$1";
$route['livetracking/getLatlong/(:any)'] = "CManageLiveTrackingController/getLatlong/$1";

$route['livetracking/geofench/(:any)'] = "CManageLiveTrackingController/geofench/$1";
$route['livetracking/getSavedFench/(:any)'] = "CManageLiveTrackingController/getSavedFench/$1";
$route['livetracking/fenching'] = "CManageLiveTrackingController/getlatlangBYid";
$route['livetracking/fenching/(:any)'] = "CManageLiveTrackingController/getlatlangBYid/$1"; 
$route['livetracking/savefench/(:any)'] = "CManageLiveTrackingController/savefench/$1";

$route['livetracking/routewiselocation/(:any)'] = "CManageLiveTrackingController/routeWiseLocations/$1";
$route['livetracking/routewisemap/(:any)'] = "CManageLiveTrackingController/viewLocationsmap/$1";
$route['livetracking/routedatewisemap/(:any)'] = "CManageLiveTrackingController/viewDateWiseLocationsmap/$1";
$route['livetracking/insertdata/(:any)'] = "CManageLiveTrackingController/insertdata/$1";
$route['livetracking/insertdata'] 			= "CManageLiveTrackingController/insertdata";
$route['livetracking/checkimminumber/(:any)'] = "CManageLiveTrackingController/checkImeiNumber/$1";
$route['livetracking/viewmap/(:any)'] = "CManageLiveTrackingController/viewmap/$1"; 
$route['livetracking/sendsms/(:any)'] = "CManageLiveTrackingController/sendsms/$1";
$route['livetracking/livelocation/(:any)'] = "CManageLiveTrackingController/livelocation/$1";
$route['livetracking/share/(:any)'] = "CManageLiveTrackingController/share/$1";
$route['livetracking/getshareLatlong/(:any)'] = "CManageLiveTrackingController/getshareLatlong/$1";
$route['livetracking/shareroute/(:any)'] = "CManageLiveTrackingController/viewmarkwers/$1";
$route['livetracking/sharedby'] = "CManageLiveTrackingController/getSharedByUsers";

$route['footermenu/action/(:any)'] 	            = "CManageFooterMenuController/action/$1";
$route['footermenu/action'] 	                     = "CManageFooterMenuController/action";

$route['energymeter/action/(:any)'] 	= "CManageEnergyMeterController/action/$1";
$route['energymeter/action'] 				= "CManageEnergyMeterController/action";

$route['welcome/action/(:any)'] 	= "welcome/action/$1";
$route['welcome/action'] 					= "welcome/action";

$route['webservice'] 							= "CManageGnissWebservice";
$route['webservice/login/(:any)'] = "CManageGnissWebservice/energymeterLogin/$1";

$route['admin/devices/action/(:any)'] 	= "admin/CManageDeviceController/action/$1";
$route['admin/devices/action'] 			= "admin/CManageDeviceController/action";
$route['admin/user/action/(:any)'] 	= "admin/CManageAdminUserController/action/$1";
$route['admin/user/action'] 			= "admin/CManageAdminUserController/action";
$route['admin/roles/action/(:any)'] 	= "admin/CUserRoleController/action/$1";
$route['admin/roles/action'] 				= "admin/CUserRoleController/action";
$route['admin/module/action/(:any)'] 	= "admin/CManageModulesController/action/$1";
$route['admin/module/action'] 			= "admin/CManageModulesController/action";

$route['devices/action/(:any)'] 	= "CManageDeviceController/action/$1";
$route['devices/action'] 				= "CManageDeviceController/action";

$route['admin/ticket/action/(:any)'] 		= "admin/CManageTicketController/action/$1";
$route['admin/ticket/action'] 			= "admin/CManageTicketController/action";

$route['admin/plan/action/(:any)'] 	= "admin/CManageSubscriptionPlanController/action/$1";
$route['admin/plan/action'] 				= "admin/CManageSubscriptionPlanController/action";

$route['admin/profile/action/(:any)'] = "admin/CManageAdminProfileController/action/$1";
$route['admin/profile/action'] 				= "admin/CManageAdminProfileController/action";

$route['vahakapi/action/(:any)'] 	= "CManageVahakApiController/action/$1";
$route['vahakapi/action'] 		= "CManageVahakApiController/action";


$route['webservice/login/(:any)'] = "CManageGnissWebservice/energymeterLogin/$1";
/* End of file routes.php */
/* Location: ./application/config/routes.php */
