<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 function sendMail( $strEmailFrom, $strEmailTo, $strSubject, $arrstrEmailFormat ) {
	
	$this->load->library( 'email' );
	$this->load->library( 'parser' );
	$this->email->mailtype	= 'html';
		
	$this->email->from( $strEmailFrom, 'Rectus Energy pvt ltd' );
	$this->email->to( $strEmailTo );
	$this->email->subject( $strSubject );
		
	$strEmailContent = $this->parser->parse( 'email_template',$arrstrEmailFormat, TRUE );
	$this->email->message( $strEmailContent );
		
	return $this->email->send();
}


?>