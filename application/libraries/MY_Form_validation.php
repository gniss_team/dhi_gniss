<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Form_validation extends CI_Form_validation {
    /**
     * MY_Form_validation::alpha_extra().
     * Alpha-numeric with periods, underscores, spaces and dashes.
     */
	protected $CI;
	
	function __construct() {
		parent::__construct();
		
		// reference to the CodeIgniter super object
		$this->CI =& get_instance();
	}
	
	function valid_url_format($str){
		if( false == empty( $str) ) {
			
		//$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
		//$pattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
			$pattern =	"@^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$@i";
			if (!preg_match( $pattern, $str)){
				
				$this->CI->form_validation->set_message('valid_url_format', 'The URL you entered is not correctly formatted.');
				return FALSE;
			}
		}
		return TRUE;
	}
	
    function website_url($url) {
    	if( false == empty( $url) ) {
		    $ch = curl_init($url);
		    curl_setopt($ch, CURLOPT_NOBODY, true);
		    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		    curl_exec($ch);
		    $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    curl_close($ch);
		    
		    if (200 == $retcode) {
		       return true;
		    } else {
		    	$this->CI->form_validation->set_message('website_url', 'The website does not exist.');
		       return false;
		    }
    	}    
    }
    // add more function to apply custom rules.
}