<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- responsive -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!-- responsive style-->
<title>GNISS-Global Network Information Security System</title>
<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>css/responsive-style.css" rel="stylesheet" />

<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">	
<script src="<?php echo base_url(); ?>js/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/custom.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>

<script>
$(document).ready(function(){
$(".profile-styled").click(function(){
    $("#login-panel").slideToggle(150);
    return false;
});
$(document).click(function(){
    $("#login-panel").slideUp(150);
});
});

</script>

</head>
<body>

<div class="main-wrapper">
		<div class="header">
    		<a href="<?php echo base_url(); ?>"><div class="logo"></div></a>
    	</div>
    </div>

<div class="welcome-heading">Welcome to GNISS</div>
       
		  <div class="login-txt"></div>
		<div class="content-welcome-wrapper">
				<div class="content-inner-wrapper clearfix">
   					<div>                    
		
									<div>
												<div> <img src="<?php echo base_url();?>images/welcome-gra.png">   </div>
												<div class="content-welcome-txt"><p>Hello, <?php echo ucwords( $this->session->userdata( 'name') ); ?></p></div>
				    							<div class="content-acc-txt"><p>Your Gniss ID is <?php echo $this->session->userdata('account_number');?>	<br/>	<br/>
        										<a class="signup1" href="<?php echo base_url();?>">Click here to get started</a></p></div>
								</div>
						</div>
				</div> 
		</div>
</div>
<style>
.content-welcome-wrapper{
		margin : 0 auto;
		padding : 0 0 34px;
		text-align : center;
}
.content-welcome-txt{
		text-align : center;
		font-size : 1.8em;
		line-height : 12px;
}
.content-acc-txt{
		text-align : center;
		font-size : 1.2em;
}
.signup1{
		text-decoration:none;
		padding : 5px 12px;;
		background:#252525;
		color:#fff;
		border-radius:3px;
}
.signup1:hover{
		text-decoration:none;
		padding : 5px 12px;
}
.welcome-heading {
	font-size : 2.3em;
	text-align : center;
	
}
.welcome-sub-txt {
	font-size : 0.8em;
	text-align : center;
	text-transform : capitalize;;
}
.error{text-indent:0;}
</style>
<div class="footer-wrapper">
       <div class="footer-menu">
			 <ul>
                <li><a href="<?php echo base_url();?>footermenu/action/about_us" target="_blank">About us</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/contact_us" target="_blank">Contact</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/disclaimer" target="_blank">Disclaimer</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/privacy_policy" target="_blank">Privacy Policy</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/terms-conditions" target="_blank">Terms & Conditions</a></li>
             </ul>
		</div>
		<div class="footer">Copyright © 2015 Rectus Energy Private Limited. All rights reserved.</div>
</div>
</body>
</html>



