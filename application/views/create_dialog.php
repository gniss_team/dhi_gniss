<style>
.form-grp label {
/*     width: 20%; */
}
.form-control {
/*     width: 10%; */
}
.input_name {
/* 	margin-left:15px; */
}
</style>
<div class="form-holder">
<?php 	
$attributes = array('class' => 'settings',  'id' => 'settings');	
echo form_open_multipart('',$attributes);	
?>
	<div>
		<div id='msg'></div>
		<div class="form-grp">
	    	<label>Device Name : </label><span class="input_name"><?php echo $device->name; ?></span>
      	</div>
		<div class="form-grp">
		   	<label>Set Frequency Data </label>
           	<select class="form-control styled" name="frequency_rate" id="frequency_rate">
				<option value="10" <?php if( '10' == $device->frequency_rate ) {?> selected="selected"<?php } ?> >10</option>
               	<option value="20" <?php if('20' == $device->frequency_rate ) {?> selected="selected"<?php } ?> >20</option>
               	<option value="30" <?php if( '30' == $device->frequency_rate ) {?> selected="selected"<?php } ?> >30</option>
               	<option value="40" <?php if( '40' == $device->frequency_rate ) {?> selected="selected"<?php } ?> >40</option>
               	<option value="50" <?php if( '50' == $device->frequency_rate ) {?> selected="selected"<?php } ?> >50</option>
               	<option value="60" <?php if( '60' == $device->frequency_rate ) {?> selected="selected"<?php } ?> >60</option>
               	<option value="70" <?php if( '70' == $device->frequency_rate ) {?> selected="selected"<?php } ?> >70</option>
               	<option value="80" <?php if( '80' == $device->frequency_rate ) {?> selected="selected"<?php } ?> >80</option>
               	<option value="90" <?php if( '90' == $device->frequency_rate ) {?> selected="selected"<?php } ?> >90</option>
            </select>
	        <span>Sec</span>
            <?php echo form_error('data_frequency'); ?>
      	  </div>
      	  <div class="form-grp">
		   	<label>Set Over-speed limit </label>
           	<select class="form-control styled" name="over_speed_limit" id="over_speed_limit">
				<option value="60" <?php if( '60' == $device->over_speed_limit ) {?> selected="selected"<?php } ?> >60</option>
               	<option value="70" <?php if( '70' == $device->over_speed_limit ) {?> selected="selected"<?php } ?> >70</option>
               	<option value="80" <?php if( '80' == $device->over_speed_limit ) {?> selected="selected"<?php } ?> >80</option>
               	<option value="90" <?php if( '90' == $device->over_speed_limit ) {?> selected="selected"<?php } ?> >90</option>
               	<option value="100" <?php if( '100' == $device->over_speed_limit ) {?> selected="selected"<?php } ?> >100</option>
               	<option value="110" <?php if( '110' == $device->over_speed_limit ) {?> selected="selected"<?php } ?> >110</option>
               	<option value="120" <?php if( '120' == $device->over_speed_limit ) {?> selected="selected"<?php } ?> >120</option>
               	<option value="130" <?php if( '130' == $device->over_speed_limit ) {?> selected="selected"<?php } ?> >130</option>
               	<option value="140" <?php if( '140' == $device->over_speed_limit ) {?> selected="selected"<?php } ?> >140</option>
            </select>
	        <span>Sec</span>
            <?php echo form_error('data_frequency'); ?>
      	  </div>
		</div>
		<div class="btn-grp">
			<button class="signup" id="btnUpdate" type="button" class="btn btn-primary">Update</button>
			<button class="signup" type="reset" class="btn btn-default">Cancel</button>
		</div>
		<?php  echo form_close(); ?>
</div>
<script type="text/javascript">

$("button[type='reset']").on("click", function(){
		
	if( 0 != $('#dialog').find( '.alert-success' ).text().length ){
		$( '#dialog' ).dialog( "close" );
		ajaxindicatorstart('loading data.. please wait..');
		window.location.reload();
		ajaxindicatorstop();
	} else {
		$('#dialog').find( '.success' ).text('');
		$('#dialog').find( '.error' ).text('');
		$( '#dialog' ).dialog( "close" );
	}

});

$("#btnUpdate").on("click", function(event) {
	event.preventDefault();
	ajaxindicatorstart('loading data.. please wait..');
	var device_id = <?php echo $device->id;?>;
    $.ajax({
    	url: strUrl + "devices/action/setting/" + device_id, // Url to which the request is send
    	type: "POST",             // Type of request to be send, called as method
    	data: new FormData($('form')[0]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    	contentType: false,       // The content type used when sending data to the server.
    	cache: false,             // To unable request pages to be cached
    	processData:false,        // To send DOMDocument or non processed data file it is set to false
    	success: function(d)   // A function to be called if request succeeds
    	{ 
    		ajaxindicatorstop();
			var d = JSON.parse(d);
	          
        	if( d.error=='true' ) {
        		
          		$('#msg').html( d.msg );
          		$('#msg').addClass( "error alert-error" );
        	} else 	{
        	
        		$('#msg').html( d.msg );
        		$('#msg').addClass( "success alert-success" );   
         	}

    	}
	});

});

</script>
