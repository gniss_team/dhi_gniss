<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<style>
.error {
	left:160px;
}
.alert-success {
 	padding:5px 0;
 	margin:5px 0;
 }
.breadcrums li a {
    background: none;
}
#frequency_data .form-grp{margin:7px 0 4px;padding:0;display:inline-block; font-size:1.1em;}
#frequency_data .btn-grp{margin:0 auto;}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}
.form-grp > span {
    color: #252525;
    font-size: 1em;
    margin-left: 11px;
    text-transform: capitalize;
}

.dataTables_length label {
	width: auto;
	display: inline-flex;
}
.dataTables_length select {
	margin: -6px 12px;
	padding: 5px 10px 5px 0px;
	/*-webkit-border-radius:4px;*/
   -moz-border-radius:4px;
   /* border-radius:4px;
  -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;*/
   background: url("../../images/select-arrow.png") no-repeat scroll 98% center #fff;
   color:#555;
   border:1px solid #e3e3e3;
   outline:none;
   display style="margin:0 18px;": inline-block;
   -webkit-appearance:none;
   -moz-appearance:none;
   appearance:none;
   cursor:pointer;
}
.dataTables_paginate{
    float: right;
    font-size: 13px;
    padding-top: 0px;
    padding-right: 12px;
    padding-bottom: 6px;
    padding-left: 12px;
    text-align: right;
}
.dataTables_wrapper .dataTables_filter{
	margin-top: 4px;
}
#myTable_info{
	font-size:13px;
}
table.dataTable {
	/*width:98% !important;*/
	margin-left:0px !important;	
}
.img_icon {
	margin-left:10px;
	cursor:pointer;
	vertical-align: middle;
}	
#img_livetrack {
	margin-right:10px;
	cursor:pointer;
	vertical-align: middle;
}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('.img_inactive_livetrack').click(function(){
			alert('Device not live!');
		});
	});
</script>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div style="min-height:450px;width:98%;">
<div class="content clearfix">
<div class="septr">
<?php if( 0 != count( $assets) ) {?>
<button class="button-back" type="button" onclick="window.location='<?php echo base_url(); ?>livetracking'">Back to Live Tracking</button>
	<?php } ?></div>
<div class="title-box">
	<div class="page-title"><h4>Mobile Tracker</h4></div>
<div class="track-btn-container">	
	<button class="track-but" type="button" onclick="window.location='<?php echo base_url(); ?>devices/action/add'">Add Tracker</button>
	</div>
	
	</div>
		<div class="content-wrap clearfix">
	<?php if(isset($msg) && $msg != '') { ?>
			<div class="alert alert-success" >
				 <?php echo $msg; ?>
			</div>
	<?php } ?>
	<?php if(isset($errmsg) && $errmsg != '') { ?>
		<div class="alert alert-error" >
		 <?php echo $errmsg; ?>
	</div>
	<?php } ?>
	<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	<thead>
    	<tr>
        	<th style="display:none">Id</th>
            <th>Name</th>
            <th>IMEI Number</th>
            <th>Phone Number</th>
            <th>Sim Number</th>
  			<th class="noheader" style="border-right:1px solid;">Action</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach( $assets as $asset ) { ?>
    	<tr class="active">
        	<td style="display:none"><?php echo $asset->id; ?></td>
            <td><?php echo $asset->name; ?></td>
             <td><?php echo $asset->imei_number; ?></td>
             <td><?php echo $asset->phone_number; ?></td>
             <?php if( false == empty( $asset->sim_number ) ) {?><td><?php echo $asset->sim_number; ?></td><?php } else { ?>
             <td>-</td><?php } ?>
			<td>
			<?php if( '1' == $asset->live_flag ){?>  
 				<img id="img_livetrack" src="<?php echo base_url(); ?>images/map_icons/tracking-button.png"  onclick="window.location='<?php echo base_url(); ?>livetracking'" alt="Live tracking" title="Live tracking" >
 			<?php  } else { ?>
 				<img id="img_livetrack" class="img_inactive_livetrack" src="<?php echo base_url(); ?>images/map_icons/tracking-button.png"  alt="Live tracking" title="Live tracking" >
 			<?php } ?>
			<button style="border:none;background:none; cursor:pointer;font-size:1em;"  title="Edit" onclick="window.location='<?php echo base_url(); ?>devices/action/update/<?php echo $asset->id; ?>'" ><i class="fa fa-pencil-square-o"></i>

</button>
 			<img class="img_icon" src="<?php echo base_url(); ?>images/delete.png"  onclick="deletedata(<?php echo $asset->id;?>,'assets/action/delete/');" height="15px" alt="Delete" title="Delete">
 			<img class="img_icon" onclick="loadDialog(<?php echo $asset->id;?>, <?php echo $asset->frequency_rate;?>, <?php echo $asset->over_speed_limit?>);" src="<?php echo base_url(); ?>images/set_frequency.png"  height="15px" alt="Device Setting" title="Set Device Settings">
 		</tr>
	<?php } ?>
    </tbody>
    </table>
</div>
	<div id="dialog" title="Settings" style="display:none;">	</div>
</div>
</div>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css" />

<script src="<?php echo base_url(); ?>js/external/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<?php $this->load->view('common/footer');?>
<script type="text/javascript">
var strUrl = "<?php echo base_url(); ?>";

$( document ).ready(function() {

	$( "#dialog" ).dialog({
		autoOpen: false,
		width: 500,
		modal: true,
		 open: function(event, ui) {
			device_id = $(this).data('device_id');
             $(this).load( strUrl + "devices/action/loadView/"+device_id, function() {
                 
             });
         },
		buttons: [	],
		beforeClose: function(event, ui) { 
			ajaxindicatorstart('loading data.. please wait..');
				if( 0 != $('#dialog').find( '.alert-success' ).text().length ){
					window.parent.location.reload();
				}
				ajaxindicatorstop();
            }
	});

		
});

function loadDialog( device_id ) {
	$( "#dialog" ).data('device_id', device_id ).dialog( "open" );
}
</script>
 <script src="<?php echo base_url(); ?>js/loader_image.js"></script>