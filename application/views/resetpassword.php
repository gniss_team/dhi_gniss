<?php $this->load->view('common/header');?>

<div class="content-login-wrapper">
<div class="forgot_txt">Reset Password</div>
	<?php
		$attributes = array('class' => 'form-signin');
		echo form_open('users/action/resetPass/'.$ver_code, $attributes);
		if( true == isset($msg)  && false == empty($msg)){
			echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>'.$msg;
            echo '</div>';
        }?>
	<div class="content-inner-wrapper clearfix">
	   <div>
            <label class="label-reset"> New Password <span class="red">*</span></label>
            <input class="user-ico-login" type="password" placeholder="Password" id="password" name="password"  maxlength="25" />
            <?php echo form_error('password'); ?>
       </div>
       <div>
            <label class="label-reset"> Confirm Password <span class="red">*</span></label>
            <input class="user-ico-login" type="password" placeholder="Confirm password" id="passconf" name="passconf"  maxlength="25"  />
            <span class="red"><?php echo form_error('passconf'); ?></span> 
       </div>
       <div style="float:right;clear:both;"><button type="submit" name="add" value="add"  class="signup">Submit</button>
       		<button type="reset" name="reset" value="reset"  class="signup" onclick="window.location='<?php echo base_url(); ?>welcome/login'">Cancel</button></div>
       </div>
     <?php  echo form_close(); ?>
</div>
<style>
.error {
	width:auto;
   	margin:6px 0px 6px 6px;
}	                	
</style>
<?php $this->load->view('common/footer');?>