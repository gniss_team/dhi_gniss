<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<script src="<?php echo base_url(); ?>js/simpletabs_1.3.js"></script>
<script>
var base_url = '<?php echo base_url();?>';
</script>
<script type="text/javascript">
$(window).load(function() {
	$(".loader").fadeOut("slow");
})
</script>
<style>

body{
		margin:0;
}
.pro-user-title {
		text-transform:capitalize;
		font-weight:normal;
} 	
.form-space-profile{float:left;width:70%;}
.demoHeaders {
		margin-top: 2em;
}
.profile-holder1 img{width:100px; height:100px;}

#dialog-link {
		padding: .4em 1em .4em 15px;
		text-decoration: none;
		position: relative;
}
#dialog-link span.ui-icon {
		margin: 0 5px 0 0;
		position: absolute;
		left: .2em;
		top: 50%;
		margin-top: -8px;
}
#icons {
		margin: 0;
		padding: 0;
}
#icons li {
		margin: 2px;
		position: relative;
		padding: 4px 0;
		cursor: pointer;
		float: left;
		list-style: none;
}
#icons span.ui-icon {
		float: left;
		margin: 0 4px;
}
.fakewindowcontain .ui-widget-overlay {
		position: absolute;
}
select {
		width: auto;
}
.pro-des{border:3px solid #f5f5f5;border-radius:8px;}
 .form-grp-profile-tabg{float:left;width:50%;}
 
 .styled{
		width:42.6%;
}
.form-holder1{
		float:left;
		width:50%;
}
.profile-holder1{
		float:right;
}
textarea {
		width:40%;
		font-family: inherit;
}
label {
		float:left;
		width:20%; 
}
.form-control-profile {
		/*width:auto !important;*/
/* 			width:39%!important; */
}
.error {
		margin: 0;
		text-indent:21%;
}
	
.alert-success {
		margin: 0;
/*		text-indent:19%; */
}
.pro-title{
		font-size:1.3em;
		text-transform:capitalize;
		margin:10px 0;
		padding:0;
		font-weight:normal;
}
	
input[name="office_ext"] {
		width:5%;
}
input[name="home_ext"] {
		width:5%;
}
input[name="fax_ext"] {
		width:5%;
}
input[name="home_country_code"] {
		width:5%;
}
input[name="user_country_code"] {
		width:5%;
}
input[name="office_country_code"] {
		width:5%;
}
input[name="fax_country_code"] {
		width:5%;
}
input[name="country_code[]"] {
		width:5%;
}
.btn-grp img{
		margin : 1% 0 0 95%;
}
.form-control-pref {
		 border: 1px solid #e5e5e5;
	    margin: 0 12px;
	    padding: 5px;
	    width: 9%;
}
.label-pref {
		width:30%;
}
.sel-style {
		width : 20%;
}
.remove-icon {
		background:url( ../../images/add-del-icon.png  );
}
.error-gen {
		color:red;
}
.success-gen {
		color:green;
}

.profile-name-sts{
		 float:left;
		 line-height:12px; 
		 padding:0 12px;
}

.edit-button-loc{
		 float:left;
		 margin-left:30px;
}

.general-block{
		 float:left; 
		 width:27%;
		 line-height:20px;
		 padding:10px;
}

.address-block, .pro-sts-block{
		 float:left; 
		 width:30%;
		 padding:10px;
		 line-height: 20px;
		 word-wrap:break-word;
}

.pro-sts-style{
		 width:60%;
		 background:#ea4949;
		 height:10px;
		 border-bottom:3px solid #ddd;
}

.social-block{
		 clear:both; 
		 margin-top:100px;
}

.remove_email_field{
	vertical-align:middle;
	margin-bottom:10px;
}
</style>
 
<link type="text/css" href="<?php echo base_url(); ?>css/simpletabs.css" rel="stylesheet" />
<div class="loader"></div>
<!-- content area-->             
<div class="content-blocks">
<?php 
	$profile_picture = $user_info->profile_picture;
	if( 'default.png' == $profile_picture ) {
			$profile_picture_path = 'images/'.$profile_picture;	
	} else {  $this->m_intProfileCont += 2;
			$profile_picture_path = 'uploads/'.$profile_picture;
	}?>
<div style="float:left;"><img class="pro-des"  width="90px" height="90px" src="<?php echo base_url().$profile_picture_path; ?>" /></div>
<div class="profile-name-sts"><h2 class="pro-user-title"><?php echo $user_info->first_name . ' ' . $user_info->last_name; ?></h2><span style="font-size:16px;">Gniss Id - <?php echo $user_info->account_number; ?></span></div>
<div class="edit-button-loc">
		<p><a href="#" id="dialog-link" class="ui-state-default ui-corner-all">Edit Profile</a></p></div>
<div style="clear:both;">
	<div class="general-block"><h2 class="pro-title">General</h2> 
	<div>Name - <?php  echo $user_info->first_name . ' ' . $user_info->last_name; ?></div>
	<div>Email - <?php  echo $user_info->email_address; ?></div>
	<?php 		
		$this->m_intProfileCont += 2;
	 	$this->m_intProfileCont += count( $user_social_websites ) * 2;
	 
		foreach( $user_emails as $intKey => $objEmail ) { $intEmailCount		= $intKey + 1; $this->m_intProfileCont += 2;?>
	<div>Alternate Email - <?php  echo $objEmail->email_address; ?></div>
	<?php } ?><br/>
    <h2 class="pro-title">Contact Details</h2>
  <div>Mobile - <?php  echo $user_info->mobile_number; ?></div>
    <?php 
    foreach( $user_mobile_numbers as $intKey => $objUserMobileNumber ) { $intMobileCount		= $intKey + 1; $this->m_intProfileCont += 2; 
    if ( false == empty( $objUserMobileNumber->mobile_number ) ) {
    ?>
     <div>Alternate Mobile - <?php  
     			 $strCountryCode	= ( false == empty( $objUserMobileNumber->country_code ) ) ? $objUserMobileNumber->country_code : '';
 				 echo $strMobileNo =   ( false == empty( $objUserMobileNumber->mobile_number ) ) ?  $strCountryCode  .' - '. $objUserMobileNumber->mobile_number : '' ;
 ?></div>
     <?php } } ?>
    <?php  $home = ( false == empty( $user_info->home_contact ) ) ? $user_info->home_country_code . ' - ' . $user_info->home_ext . ' - ' . $user_info->home_contact : ''; ?>
	<?php if ( false == empty( $home ) ) { ?><div>Home - <?php echo $home; ?></div> <?php } ?>
	<?php  $office = ( false == empty( $user_info->office_contact) ) ? $user_info->office_country_code . ' - ' . $user_info->office_ext . ' - ' . $user_info->office_contact : '';?>
	 <?php if ( false == empty( $office ) ) { ?><div>Office -  <?php echo $office;?> </div><br/>
    <?php } ?>
    </div>
    <div class="address-block">
    <?php
    		foreach( $user_addresses as $intKey => $objUserAddress ) {?>
    			<h2 class="pro-title"><?php if( 0 != $intKey ) {?>Alternate <?php } ?>Address</h2>
    	<?php 	$address =  ( false ==empty( $objUserAddress->address ) ) ? nl2br( $objUserAddress->address, '\n' ) : '';
    			$address .= ( false ==empty( $objUserAddress->city ) ) ? ' <br/>'. $objUserAddress->city : '';
    			$address .= ( false ==empty( $objUserAddress->state ) ) ? ', '. $objUserAddress->state : '';
    			$address .= ( false ==empty( $objUserAddress->country ) ) ? ', '. $objUserAddress->country : '';
    			$address .= ( false ==empty( $objUserAddress->zipcode ) ) ? ', '. $objUserAddress->zipcode : '';
    					
    			$this->m_intProfileCont +=  ( false ==empty( $objUserAddress->address ) ) ? 2 : 0;
    			$this->m_intProfileCont += ( false ==empty( $objUserAddress->city ) ) ? 2 : 0;
    			$this->m_intProfileCont += ( false ==empty( $objUserAddress->state ) ) ?  2 : 0;
    			$this->m_intProfileCont += ( false ==empty( $objUserAddress->country ) ) ? 2 : 0;
    			$this->m_intProfileCont += ( false ==empty( $objUserAddress->zipcode ) ) ? 2 : 0;
   		?>
    	   		<div><?php echo $address;  ?><br/><br/></div>
   		<?php
   		 	}  	
   		?>
   </div>
    <div class="pro-sts-block">
    <h2 class="pro-title">Profile Status</h2>
    <?php 
    $this->m_intProfileCont = ( 100 > ( (int ) $this->m_intProfileCont ) ) ? $this->m_intProfileCont : 100 ; ?>
<div class="pro-sts-style">
<div style="width:<?php echo $this->m_intProfileCont; ?>%;background:#99d91d;height:10px;"></div>
</div>
<span><?php echo $this->m_intProfileCont; ?>% Completed</span>
<div class="social-block"><h2 class="pro-title">Social Network</h2>
<ul style="margin:0;padding:0;">
<?php foreach( $user_social_websites as $intIndexKey => $objWebsite ) { 

	$strUrl = $objWebsite->website;
	if (!preg_match("@^[hf]tt?ps?://@", $objWebsite->website)) {
			$strUrl = "http://" . $strUrl;
	}
	$logo = ( true == empty( $objWebsite->logo ) ) ? 'images/website_icon.png' : $objWebsite->logo;
	if( false == empty( $objWebsite->website )) {
	?>
		<li style="list-style:none;float:left;margin-right:10px;"><a href="<?php echo $strUrl; ?>" target="_blank" ><img src="<?php echo base_url() . $logo; ?>" title="<?php echo $objWebsite->website; ?>" /></a></li>
<?php } }

?>
</ul>
</div>
</div>
</div>		
			 <div id="dialog" title="Edit Profile" style="display:none;">
						<!-- This contains the hidden content for inline calls -->

			  <div class="simpleTabs">
	   
		        <ul class="simpleTabsNavigation">
		            <li><a href="javascript:void(0)" >General </a></li>
		              <li><a href="javascript:void(0)" >Account</a></li>
		                <li><a href="javascript:void(0)" >Contact Details</a></li>
		                   <li><a href="javascript:void(0)" >Address</a></li>
		                    <li><a href="javascript:void(0)" >Social</a></li>
		                    <li><a href="javascript:void(0)" >Preferences</a></li>
		           </ul>

		        <div class="simpleTabsContent">
		        <div id='gen_err_msg' class="error alert-error"></div>
		        <div id='gen_suc_msg' class ="success alert-success"></div>
							<div class="form-holder">
									<div class="form-space-profile">
											<?php 	$attributes = array('class' => 'email',  'id' => 'myform');		echo form_open_multipart('',$attributes); ?>
											<div class="form-grp-profile">
														<label>Gniss ID </label>
														<div style="vertical-align:center;line-height:35px;font-size:13px;"  ><?php echo $user_info->account_number; ?></div>
											</div>
						       			<div class="form-grp-profile">
														<label>First name</label>
														<input type="text" class="form-control-profile" maxlength="20" placeholder="First name" name="first_name" id="first_name" value="<?= $user_info->first_name; ?>">
													<div id="firstn"></div>
											</div>
											<div class="form-grp-profile"> 
														<label>Last name </label>
														<input type="text" class="form-control-profile" maxlength="20" placeholder="Last name" id="last_name" value="<?= $user_info->last_name; ?>" name="last_name">
														<div id="lastn"></div>
											</div>
										
											<div class="email_fields_wrap">
											<div class="form-grp-profile">
											<label> Email </label>
												<input style="vertical-align:text-bottom;" type="text" class="form-control-profile" maxlength="40" placeholder="Email address" id="user_email" name="user_email"  value="<?= $user_info->email_address; ?>">
												<img id="add_email" src="<?php echo base_url(); ?>images/add-icon.png" title="Add email address" style="margin:0px 0px 9px 0px;">
												<div id="err_email"></div>		
												</div>
												<?php 
												foreach( $user_emails as $intKey => $objEmail ) { 
																	$intEmailCnt		= $intKey + 1;
													?>
														<div class="form-grp-profile">
															<label>Alternate Email </label>
															<input style="vertical-align:text00007-bottom;" type="text" class="form-control-profile" maxlength="40" placeholder="Email address" id="email_address<?php echo $intEmailCnt;?>" name="email_address[]"  value="<?= $objEmail->email_address; ?>">
															<img  class="remove_email_field" src="<?php echo base_url(); ?>images/remove-icon.png"  title="Remove Emaiil address" />
															<div class="error<?php echo $intEmailCnt;?>"></div>
															<div class="form-grp-profile">
																	<input type="hidden" id="user_email_id<?php echo $intEmailCnt;?>" name="user_email_id[]" value='<?php echo $objEmail->id; ?>'>
															</div>
														</div>
											<?php } ?>
											</div>
											<div class="form-grp-profile-tabg">
														<input type="hidden" id="email_value" name="email_value" value='<?php echo count( $user_emails ); ?>'>
											</div>		
											<div class="btn-grp">
														<button class="signup" type="submit" class="btn btn-primary">Update</button>
														<button class="signup" type="reset" class="btn btn-default" >Cancel</button>
											</div>
									</div>
									<div class="profile-holder1">
										<?php $profile_picture = $user_info->profile_picture;
										if( 'default.png' == $profile_picture ) {
												$profile_picture_path = 'images/'.$profile_picture;	
										} else {
												$profile_picture_path = 'uploads/'.$profile_picture;
										}?>
						        		<img width="100px" height="100px" src="<?php echo base_url() . $profile_picture_path; ?>" id="thumb" />
						        		<div class="pro-txt"></div>
										<label class="browse-button"><input maxlength="20" type="file"  id="profile_img" name="profile_img" onchange="readURL(this);">Browse</label>
									</div>
							<?php  echo form_close(); ?>
			        			</div>
			        </div>
		        <div class="simpleTabsContent">
		       <?php $attributes = array('class' => 'email', 'id' => 'changepassword');	echo form_open_multipart('',$attributes); ?>
								<div id='suc_msg' class="success alert-success"></div>
		       				 <div class="form-holder">
										<div class="form-grp-profile">
													<label>Old Password</label>
													 <input class="form-control-profile" type="password" placeholder="Old password" maxlength="20" name="old_password" id="old_password"  >
														<div id="old_pass"></div>
										</div>
						
										<div class="form-grp-profile">
													<label>New Password </label>
													<input class="form-control-profile" type="password" maxlength="50" placeholder="New password" name="new_password" id="new_password"  >
													<div id="new_pass"></div>
										</div>
							
										<div class="form-grp-profile">
													<label>Confirm Password </label>
													<input class="form-control-profile" type="password" maxlength="50" placeholder="Confirm password" id="confirm_password" name="confirm_password" >
													<div id="confirm_pass"></div>
										</div>
											<div class="btn-grp">
													<button class="signup"  type="submit" class="btn btn-primary">Update</button>
													<button class="signup" type="reset" class="btn btn-default">Cancel</button>
											</div>
								</div>
								    <?php echo form_close(); ?>
		          </div>
		            <div class="simpleTabsContent">
		          <?php 	$attributes = array('class' => 'email', 'id' => 'details');		
		          					echo form_open_multipart('',$attributes); ?>
								<div id='dtl_suc_msg'	class="success alert-success"></div>
								<div id='dtl_err_msg' class="error alert-error"></div>
								<div id='dtl_country_code_msg'></div>
								<div class="form-holder">
										<div class="mobile_fields_wrap">
										<div class="form-grp-profile">
													<label>Mobile number </label>
													<input type="text"  placeholder="Country Code"  maxlength="4"  id="user_country_code" name="user_country_code"" value="<?php echo $user_info->country_code; ?>">
													<input type="text" class="form-control-profile" maxlength="10" placeholder="Mobile number" id="user_mobile_number" name="user_mobile_number" value="<?php echo $user_info->mobile_number; ?>">
													<?php if( 0 == $user_info->mobile_number_status ) { ?>
													<button id="load-dialog-link" type="button" class="verify-btn btn btn-primary" onclick='verify_number( "user_mobile_number", "<?php echo $user_info->id;?>" );'>Verify</button>
													<?php } ?>
													<img style="vertical-align:text-bottom;" id="add_mobile" src="<?php echo base_url(); ?>/images/add-icon.png" title="Add Mobile Number">
													<div id="err_user_mobile" class="error alert-error"></div>
										</div>
											<?php foreach( $user_mobile_numbers as $intKey => $objUserMobileNumber ) { 
															$intMobNumberCount		= $intKey + 1;
													?>
													<div class="form-grp-profile">
																<label>Alternate Mobile</label>
																<input type="text"  placeholder="Country Code"  maxlength="4"  id="country_code<?php echo $intMobNumberCount;?>" name="country_code[]"" value="<?php echo $objUserMobileNumber->country_code; ?>">
																<input type="text" class="form-control-profile" maxlength="10" placeholder="Mobile number" id="mobile_number<?php echo $intMobNumberCount;?>" name="mobile_number[]" value="<?php echo $objUserMobileNumber->mobile_number; ?>">
																<?php if( 0 == $objUserMobileNumber->status ) { ?>
																	<button id="load-dialog-link" type="button" class="verify-btn btn btn-primary" onclick="verify_number('mobile_number<?php echo $intMobNumberCount;?>', '<?php echo $objUserMobileNumber->id;?>');">Verify</button>
													<?php } ?>
																<img style="vertical-align:text-bottom;" class="remove_mobile_field" src="<?php echo base_url(); ?>/images/remove-icon.png" title="Remove Mobile Number" >
																		<input type="hidden" id="user_mobile_id<?php echo $intMobNumberCount;?>" name="user_mobile_id[]" value='<?php echo $objUserMobileNumber->id; ?>'>
																	<div id="error_mobile<?php echo $intMobNumberCount;?>" class="error alert-error"></div>
													</div>
													<?php } ?>
										</div>
										<div class="form-grp-profile-tabg">
													<input type="hidden" id="mobile_value" name="mobile_value" value='<?php echo count( $user_mobile_numbers ); ?>'>
										</div>		
										<div class="form-grp-profile">
													<label>Office </label>
													<input type="text"  placeholder="Country Code" maxlength="4"  id="office_country_code" name="office_country_code" value="<?= $user_info->office_country_code; ?>">
													<input type="text"  placeholder="Ext"  value="<?= $user_info->office_ext; ?>" id="office_ext" name="office_ext" >
													<input type="text" class="form-control-profile" maxlength="10" placeholder="Office" id="office_contact" name="office_contact"  value="<?= $user_info->office_contact; ?>" >
													<div id="err_office" class="error alert-error"></div>
										</div>

										<div class="form-grp-profile">
													<label>Home </label>
													<input type="text"  placeholder="Country Code"  maxlength="4"  id="home_country_code" name="home_country_code" value="<?php echo $user_info->home_country_code; ?>">
													<input type="text"  placeholder="Ext" id="home_ext"  value="<?php echo $user_info->home_ext; ?>" name="home_ext" >
													<input type="text" class="form-control-profile" maxlength="10" placeholder="Home" id="home_contact" name="home_contact"  value="<?php echo $user_info->home_contact; ?>" >
										<div id="err_home" class="error alert-error"></div>
										</div>
												
										<div class="form-grp-profile">
													<label>Fax </label>
													<input type="text"  placeholder="Country Code"  maxlength="4"  id="fax_country_code" name="fax_country_code" value="<?= $user_info->fax_country_code; ?>">
													<input type="text"  placeholder="Ext"  value="<?php echo $user_info->office_ext; ?>" id="fax_ext" name="fax_ext" >
													<input type="text" class="form-control-profile" maxlength="10" placeholder="Fax Number" id="fax" name="fax"  value="<?= $user_info->fax; ?>">
										<div id="err_fax" class="error alert-error"></div>
										</div>
										
										<div class="btn-grp">
													<button class="signup" type="submit" class="btn btn-primary">Update</button>
													<button class="signup" type="reset" class="btn btn-default">Cancel</button>
										</div>
								</div>
								 <?php  echo form_close(); ?>
					</div>
					
					<div class="simpleTabsContent">
					 <?php 	$attributes = array('class' => 'email', 'id' => 'frm_address');		echo form_open_multipart('',$attributes); ?>
					 <div id='addr_suc_msg' class ='success alert-success'></div>
							<div class="form-holder">
							<div class="input_fields_wrap">
							<div class="btn-grp">
										<img id="add_addr" src="<?php echo base_url(); ?>/images/add-icon.png" title="Add">
										</div>
										<div id="addr">
										<?php 
										$countries	 = Locations_Model::fetchLocationsByType( 0 );
										foreach( $user_addresses as $intKey => $objUserAddress ) {
										
												 	$intAddressCnt		= $intKey + 1;
													$states	= array();
										 			$cities	= array();
													$states		 = Locations_Model::fetchLocationsByCountryIdByType( $objUserAddress->country_id, '1' );
													$cities			= Locations_Model::fetchLocationsByCountryIdByStateIdByType( $objUserAddress->country_id, $objUserAddress->state_id, '2' );
											?>
											<div style="border-bottom:1px solid #ccc;float:left;padding:18px 10px 20px;">
																<div class="form-grp-profile-tabg">
																			<label>Address</label>
																			<textarea type="text" style="width:58%;" placeholder="Address" id="address<?php echo $intAddressCnt;?>" name="address[]" maxlength="105"><?php  echo $objUserAddress->address; ?></textarea>
																			<div id="err_address<?php echo $intAddressCnt;?>" class="error alert-error"></div>
																			<div>
																					<label>Zip Code </label>
																					<input type="text" class="form-control-profile" maxlength="6" style="width:20%;margin:6px 0px;" placeholder="Zip code" id="zipcode<?php echo $intAddressCnt;?>" name="zipcode[]" maxlength="50" value="<?php echo $objUserAddress->zipcode; ?>" >
																					<div id="err_zipcode<?php echo $intAddressCnt;?>" class="error alert-error"></div>
																			</div>	
																			
																</div>
																<div class="form-grp-profile-tabg">
																		<label>Country</label>
																		<select  class="form-control-profile-select styled"   id="country_id<?php echo $intAddressCnt;?>" name="country_id[]" onchange="setLocations( this,state_id<?php echo $intAddressCnt; ?>,<?php echo $intAddressCnt;?> );">
																					<option value="">Select Country</option>
																					<?php  foreach( $countries as $country ) { ?>
																					<option value="<?php echo $country->id; ?>" <?php if($country->id == $objUserAddress->country_id) { ?> selected="selected" <?php } ?> ><?php echo $country->name; ?> </option>
																					<?php } ?>
																		</select>
																</div>	
																<div class="form-grp-profile-tabg">
																			<label>State</label>
																			<select class="form-control-profile-select styled" id="state_id<?php echo $intAddressCnt;?>" name="state_id[]" onchange="setLocations( this, city_id<?php echo $intAddressCnt; ?>,<?php echo $intAddressCnt;?>);">
																				<option value="">Select state</option>
																				<?php foreach( $states as $state ) { ?> 
																				<option value="<?php echo $state->id; ?>" <?php if($state->id ==$objUserAddress->state_id) { ?> selected="selected" <?php } ?> ><?php echo $state->name; ?></option>
																				<?php } ?>
																			</select>
																</div>
																<div class="form-grp-profile-tabg">
																		<label>City</label>
																		<select class="form-control-profile-select styled" id="city_id<?php echo $intAddressCnt;?>" name="city_id[]">
																			<option value="" >Select city</option>
																			<?php foreach( $cities as $city ){ ?>
																			<option value="<?php echo $city->id; ?>" <?php if( $city->id == $objUserAddress->city_id ) { ?> selected="selected" <?php } ?> ><?php echo $city->name; ?></option>
																			<?php } ?>
																		</select>
															</div>
														<?php if( 1 != $intAddressCnt ) {?>
														<img style="margin-right:3px;" align="right" class="remove_field" src="<?php echo base_url(); ?>/images/remove-icon.png" title="Remove" >
														<?php } ?>
														<div class="form-grp-profile-tabg">
														<input type="hidden" id="user_addr_id<?php echo $intAddressCnt;?>" name="user_addr_id[]" value='<?php echo $objUserAddress->id; ?>'>
											</div>
											</div>
										<?php } ?>
										</div>
										</div>
										<div class="form-grp-profile-tabg">
												<input type="hidden" id="addr_value" name="addr_value" value='<?php echo count( $user_addresses ); ?>'>
										</div>
										<div class="btn-grp">
												<button class="signup" type="submit"  class="btn btn-primary">Update</button>
												<button class="signup" type="reset" class="btn btn-default">Cancel</button>
										</div>
								</div>
								<?php  echo form_close(); ?>
							</div>
					<div class="simpleTabsContent">
		       <?php 	$attributes = array('class' => 'email', 'id' => 'social');	
		       			echo form_open_multipart( '',$attributes ); ?>
						<div id='so_suc_msg' class ="success alert-success"></div>
						<div id='so_err_msg' class ="error alert-error"></div>
	        			<div class="form-holder">
						<div class="social_fields_wrap">
							<?php foreach( $user_social_websites as $intIndexKey => $objWebsite ) { 
											$intWebsiteCnt		= $intIndexKey + 1;
							?>
							<div class="form-grp-profile">
							<label>Website</label>
							<input type="text" class="form-control-profile" maxlength="40" placeholder="Website" id="website<?php echo $intWebsiteCnt;?>" name="website[]"  value="<?php echo $objWebsite->website; ?>">
							<?php if( 1 != $intWebsiteCnt ) {?>
								<img style="vertical-align:text-bottom;" class="remove_website_field" src="<?php echo base_url(); ?>/images/remove-icon.png" title="Remove Website" >
								<?php } else { ?>
								<img style="vertical-align:text-bottom;" class="remove_website_field" src="<?php echo base_url(); ?>/images/remove-icon.png" title="Remove Website" >
								<img  style="vertical-align:text-bottom;" id="add_website" src="<?php echo base_url(); ?>/images/add-icon.png" title="Add website"><?php } ?>
								<div id="error_website<?php echo $intWebsiteCnt;?>" class="error alert-error"></div>
								<div class="form-grp-profile">
									<input type="hidden" id="user_website_id<?php echo $intWebsiteCnt;?>" name="user_website_id[]" value='<?php echo $objWebsite->id; ?>'>
								</div>
							</div>
						<?php } ?>
						</div>
						<div class="form-grp-profile-tabg">
							<input type="hidden" id="website_value" name="website_value" value='<?php echo count( $user_social_websites ); ?>'>
						</div>		
						<div class="btn-grp">
							<button class="signup" type="submit" class="btn btn-primary">Update</button>
							<button class="signup" type="reset" class="btn btn-default">Cancel</button>
						</div>
						</div>
					<?php  echo form_close(); ?>
		          </div>
					<div class="simpleTabsContent">
					   <?php 	$attributes = array('class' => 'email', 'id' => 'notify');		echo form_open_multipart('',$attributes); ?>
								<div class="form-holder">
								<div id='pref_suc_msg' class = 'success alert-success'></div>
											<div class="form-grp-profile">
														<label class="label-pref">Device Fault Notification</label>
			 											<select  class="form-control-pref styled" id="fault_notify" name="fault_notify">
	                               					<option value="0" <?php if($user_info->fault_notify=='0'){ ?> selected="selected" <?php } ?>>No</option>
                                						<option value="1" <?php if($user_info->fault_notify=='1'){ ?> selected="selected" <?php } ?> >Yes</option>
	                                			</select>
											</div>
											
											<div class="form-grp-profile">
														<label class="label-pref">Newsletters Updates</label>
												<select  class="form-control-pref styled" id="newsletter_notify" name="newsletter_notify">
	                               					<option value="0" <?php if($user_info->newsletter_notify=='0'){ ?> selected="selected" <?php } ?>>No</option>
                                						<option value="1" <?php if($user_info->newsletter_notify=='1'){ ?> selected="selected" <?php } ?> >Yes</option>
	                                			</select>
											</div>
										
											<div class="form-grp-profile">
														<label class="label-pref">Show alert notifications</label>
															<select  class="form-control-pref styled" id="alert_notify" name="alert_notify">
	                               					<option value="0" <?php if($user_info->alert_notify=='0'){ ?> selected="selected" <?php } ?>>No</option>
                                						<option value="1" <?php if($user_info->alert_notify=='1'){ ?> selected="selected" <?php } ?> >Yes</option>
	                                			</select>
			
											</div>
												<div class="btn-grp" style="clear:both";>
													<button class="signup" type="submit" class="btn btn-primary">Update</button>
													<button class="signup" type="reset" class="btn btn-default">Cancel</button>
										</div>
								</div>
								 <?php  echo form_close(); ?>
					</div>
		</div>
</div>

<div id="load-dialog" title="Verify your mobile number" style="display:none;">
			<div class="form-holder">
						
						<?php 	$attributes = array('class' => 'mobile-verify',  'id' => 'mobile_otp');		echo form_open_multipart('',$attributes); ?>
						<div>
						<div  class="form-grp-profile success alert-success">
									<p>Please enter the OTP you received on your mobile number. If not received, then <a href="javascript:void(0)" onclick="verify_number('mobile_no');">resend</a> OTP</p> 
						</div>	
								<div class="form-grp-profile">

								<label>Enter Verification Code</label>
									<input type="text" class="form-control-profile" maxlength="6" placeholder="Enter verification code" id="verify_code" name="verify_code" value="">
									<div id='err_mobile_verify' class="error alert-error"></div>
								</div>
								<div class="btn-grp">
										<input type="hidden" id="mobile_no" name="mobile_no" value="">
										<input type="hidden" id="field" name="field" value="">
										<input type="hidden" id="pid" name="pid" value="">
											<button class="signup" type="submit" class="btn btn-primary">Verify</button>
								</div>
						</div>
						<?php  echo form_close(); ?>
			</div>
</div>
</div>
<?php $this->load->view('common/footer');?>
 <script type="text/javascript">
var strUrl = "<?php echo base_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/locations.js"></script>   
<script type="text/javascript">
function readURL(input) {
 	if (input.files && input.files[0]) {
    	var reader = new FileReader();
		reader.onload = function (e) {
        	$('#thumb').attr('src', e.target.result);
        }
    reader.readAsDataURL(input.files[0]);
    }
}
 </script>   
 <script type="text/javascript">
$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text' ) ||
            nodeName === 'textarea' ||  e.target.type === 'password'
            ||	nodeName === 'select' ||  e.target.type === 'option') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }

    $("input[name='zipcode[]']").keypress(function (e) {
    		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			    	 if( true == $('#'+ $(this).parent("div").siblings('div').attr('id')).hasClass("error")) {
						 // $('#'+ $(this).parents("div").children('.error').attr("id") ).html("Digits Only").show();
			   				$('#'+ $(this).parent("div").siblings('div').attr('id')).hide();
						 		$('#'+ $(this).parent("div").siblings('div').attr('id')).html("Digits Only").show();
						  		 return false;
			        }
    		} else {
    				$('#'+ $(this).parent("div").siblings('div').attr('id')).html("Digits Only").hide();
		   }
	   
  });

    $("input[name='country_code[]']").keypress(function (e) {
    	
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

					 if( true == $('#'+ $(this).siblings('div').attr('id')).hasClass("error")) {
						 // $('#'+ $(this).parents("div").children('.error').attr("id") ).html("Digits Only").show();
			   				$('#'+ $(this).siblings('div').attr('id')).hide();
						 		$('#'+ $(this).siblings('div').attr('id')).html("Digits Only").show();
						  		 return false;
			        }
	 		} else {
	       	 	$('#'+ $(this).siblings('div').attr('id')).html("Digits Only").hide();
		    }
	 });

    $("input[name='mobile_number[]']").keypress(function (e) {
	    
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

			       	  if( true == $('#'+ $(this).siblings('div').attr('id')).hasClass("error")) {
			   		  // $('#'+ $(this).parents("div").children('.error').attr("id") ).html("Digits Only").show();
			   				$('#'+ $(this).siblings('div').attr('id')).hide();
						 		$('#'+ $(this).siblings('div').attr('id')).html("Digits Only").show();
						  		 return false;
			        }
	          
	 		} else {
	       	 	$('#'+ $(this).siblings('div').attr('id')).html("Digits Only").hide();
	      }
	 });
	 
});
</script>
<script type="text/javascript">
//$(document).ready(function() {
    var max_fields      = 4; //maximum input boxes allowed
    var y=3;
    
    var social_wrapper = $(".social_fields_wrap");

    var q = $('#website_value').val();
  $('#add_website').click(function(e){ 
 	 
 	    e.preventDefault();

        if( q <= max_fields){ //max input box allowed
            q++; //text box increment

            $( social_wrapper ).append('<div class="form-grp-profile">'+
               '<label>Website</label>'+
 					'<input style="margin-left:4px;" type="text" class="form-control-profile" maxlength="40" placeholder="Website" id="website'+q+'" name="website[]" value="">'+
 					'<img style="margin-left:5px;vertical-align:text-bottom;" class="remove_website_field" src="<?php echo base_url(); ?>/images/remove-icon.png" title="Remove" >'+
 					'<div id="error_website'+q+'" class="error alert-error"></div></div>');
            y++;
            $("#website_value").val( q );
        } else {
            		alert( 'You cannot add more than 5 websites.' );
        }
  });
  
  $( social_wrapper ).on("click",".remove_website_field", function(e){ //user click on remove text
      q = $('#website_value').val();
     
 		        if( 1 != q ) {
// 	 		        	 var r = confirm("Do you want to delete this?")
// 	 		             if ( r == true ) {
					    e.preventDefault(); $(this).parent('div').remove(); q--;
 					    $("#website_value").val( q );
 		         } else {
// 			             	 	e.preventDefault();
 		        		
 		        		$(this).parent('div').remove();
 		        		$(this).parent('div').empty();
 		        		q--;
				        $("#website_value").val( q );
				        q++;
				        $( social_wrapper ).append('<div class="form-grp-profile">'+
				         '<label>Website</label>'+
				  		 '<input style="margin-left:4px;" type="text" class="form-control-profile" maxlength="40" placeholder="Website" id="website'+q+'" name="website[]" value="">'+
				  		 '<img style="margin-left:5px;vertical-align:text-bottom;" class="remove_website_field" src="<?php echo base_url(); ?>/images/remove-icon.png" title="Remove" >'+
				  		 '<img  style="vertical-align:text-bottom;" id="add_website" src="<?php echo base_url(); ?>/images/add-icon.png" title="Add website">'+
				  		 '<div id="error_website'+q+'" class="error alert-error"></div>'+
				  		'<div class="form-grp-profile">'+
						'<input type="hidden" id="user_website_id'+q+'" name="user_website_id[]" value="">'+
						'</div></div>');

					     $("#website_value").val( q );
// 			             	return false;
// 	 		       	} 
  		       }
  });

  var mobile_wrapper = $(".mobile_fields_wrap");

  var p = $('#mobile_value').val();
  $('#add_mobile').click(function(e){ 
 	 
 	    e.preventDefault();

        if( p <= max_fields){ //max input box allowed
            p++; //text box increment
			
            $(mobile_wrapper).append("<div class='form-grp-profile'>"+
                    "<label>Alternate Mobile</label>"+
                    "<input type='text'  placeholder='Country Code'   id='country_code'+p+'' name='country_code[]' value=''>"+
      					"<input type='text' class='form-control-profile' maxlength='40' placeholder='Mobile Number' id='mobile_number"+p+"' name='mobile_number[]' value=''>"+
      					"<button id='load-dialog-link' type='button' class='verify-btn btn btn-primary' onclick='verify_number( \"mobile_number"+p+"\" );'>Verify</button>"+
      					"<img style='margin-left:8px;vertical-align:text-bottom;' class='remove_mobile_field' src='<?php echo base_url(); ?>/images/remove-icon.png' title='Remove' ><div id='error_mobile'+p+'' class='error alert-error'></div></div>");
         	   y++;
           		$("#mobile_value").val( p );
        } else {
            	alert( 'You cannot add more than 5 Mobile Numbers.' );
        }
  });
  
  $(mobile_wrapper).on("click",".remove_mobile_field", function(e){ //user click on remove text
      p = $('#mobile_value').val();
     
 		      //  if( 1 != p ) {
// 	 		        	 var r = confirm("Do you want to delete this?")
// 	 		             if ( r == true ) {
						        e.preventDefault(); $(this).parent('div').remove(); p--;
	 					        $("#mobile_value").val( p );
// 	 		         } else {
// 			             	 	 return false;
// 	 		       	} 
 		     //  }
  });
 //Fields wrapper
    var email_wrapper = $(".email_fields_wrap");

   var x = $('#email_value').val();
 	$('#add_email').click(function(e){ 
	 
	    e.preventDefault();

       if(x <= max_fields){ //max input box allowed
           x++; //text box increment

           $(email_wrapper).append('<div class="form-grp-profile">'+
                   '<label>Alternate Email</label>'+
					'<input type="text" class="form-control-profile" maxlength="40" placeholder="Email address" id="email_address'+x+'" name="email_address[]" value="">'+
					'<img style="margin-left:4px;vertical-align:middle;margin-bottom:10px;" class="remove_email_field" src="<?php echo base_url(); ?>/images/remove-icon.png" title="Remove" ><div class="error'+x+'"></div></div>');
           y++;
           $("#email_value").val( x );
       } else {
       			alert( 'You cannot add more than 5 Email Addresses.' );
       }
 });
 
 $(email_wrapper).on("click",".remove_email_field", function(e){ //user click on remove text
     x = $('#email_value').val();
    
		       // if( 1 != x ) {
// 			        	 var r = confirm("Do you want to delete this?")
// 	 		             if ( r == true ) {
						        e.preventDefault(); $(this).parent('div').remove(); x--;
						        $("#email_value").val( x );
// 	 		         } else {
// 	 		        			return false;
// 	 		         }
		     // } 
 });
 
 var wrapper      = $(".input_fields_wrap"); 
 var add_button   = $(".add_addr"); //Add button ID
 
 var k = $('#addr_value').val(); //initlal text box count
    $('#add_addr').click(function(e){ //on add input button click
    	e.preventDefault();
    	 
        if( k <= max_fields) { //max input box allowed
        	
            k++;
            $(wrapper).append('<div id="addr'+k+'" style="border-bottom:1px solid #ccc;float:left;padding:18px 10px 20px;">'+
					'<div class="form-grp-profile-tabg">'+
					'<label>Address</label>'+
					'<textarea type="text" style="width:58%" placeholder="Address" id="address'+k+'" name="address[]" maxlength="105">'+
					'</textarea>'+
					'<div id="err_address'+k+'" class="error alert-error"></div>'+
					'<div>'+
							'<label>Zip Code </label>'+
							'<input type="text" class="form-control-profile"  style="width:20%;margin:6px 0px;" placeholder="Zip code" id="zipcode'+k+'" name="zipcode[]" maxlength="50" value="" >'+
							'<div id="err_zipcode'+k+'" class="error alert-error"></div>'+
					'</div>'+	
		'</div>'+
		'<div class="form-grp-profile-tabg">'+
				'<label>country</label>'+
				'<select class="form-control-profile-select styled"  id="country_id'+k+'" name="country_id[]" onchange="setLocations( this, state_id'+k+', '+k+' );">'+
							'<option value="" >Select Country</option>'+
							<?php foreach( $countries as $country ) { ?>
							'<option value="<?= $country->id; ?>"><?= $country->name; ?> </option>'+
							<?php } ?>
				'</select>'+
				
		'</div>'+	
		'<div class="form-grp-profile-tabg">'+
					'<label>State</label>'+
					'<select class="form-control-profile-select styled" id="state_id'+k+'" name="state_id[]" onchange = "setLocations( this, city_id'+k+','+k+' );">'+
						'<option value="" >Select State</option>'+
						
					'</select>'+
					
		'</div>'+
		'<div class="form-grp-profile-tabg">'+
					'<label>City</label>'+
				'<select class="form-control-profile-select styled"  id="city_id'+k+'" name="city_id[]">'+
					'<option value="" >Select City</option>'+
					
				'</select>'+
				
		'</div>'+
		'<img align="right" style="padding-right:3px;" class="remove_field" src="<?php echo base_url(); ?>/images/remove-icon.png" title="Remove" ></div>' ); //add input box
            y++;
            $("#addr_value").val(k);
        } else {
   			alert( 'You cannot add more than 5 Addresses.' );
        }
        document.getElementById( 'addr'+k ).scrollIntoView(); 
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        k = $('#addr_value').val();
       
// 		        if( 1 != k ) {
// 			       		 var r = confirm("Do you want to delete this?")
// 			             if ( r == true ) {
						        e.preventDefault(); $(this).parent('div').remove(); k--;
						        $("#addr_value").val( k );
						        
// 			          } else {
// 			        			 return false;
// 			          }
// 		       } 
    });
      
//});
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css" />
<script src="<?php echo base_url(); ?>js/external/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script type="text/javascript">
function verify_number( mob_number, pid ){

	pid	= pid || null;

	var boolIsNumExist		= false;

	var mobile_no = $( "#"+mob_number ).val();

	if( '' != mobile_no ) {
				if( 'user_mobile_number' ==  mob_number ) {
						$('input[name="mobile_number[]"]').each(function(index) { 
								    // do something here
						  			if( mobile_no == $(this).val() ) {
							  				boolIsNumExist	 =	true;
							  				return false;
						  			}	
						})
			
				}	else {
						if( mobile_no == $("#user_mobile_number").val() ) {
									boolIsNumExist	 =	true;
						}	
					
						if( false == boolIsNumExist ) {
									$('input[name="mobile_number[]"]').each(function(index) { 
									
												if( mob_number != $(this).attr( 'id' ) ) {
														if( mobile_no === $(this).val() ) {
										
																boolIsNumExist	 =	true;
																return false;
														}
												}		
									})
						}	
				}
				
				if( 'user_mobile_number' ==  mob_number  && true == boolIsNumExist ) { 
							$("#err_user_mobile").show();
							$("#err_user_mobile").html( "Mobile number already used." );
							
				} 
				if( 'user_mobile_number' !==  mob_number  && true == boolIsNumExist ) { 
				
							if( true == $('#'+ $("#"+ mob_number).siblings('div').attr('id')).hasClass("error")) {
										//$(this).parent("div").append('<div class="error">Digits Only</div>');
									$('#'+ $("#"+ mob_number).siblings('div').attr('id')).html("");
							 		$('#'+ $("#"+ mob_number).siblings('div').attr('id')).show();
							 		$('#'+ $("#"+ mob_number).siblings('div').attr('id')).html( "Mobile number already used." );
							}
				}
	
	
				if( false == boolIsNumExist ) {
							$("#user_mobile_number").val();
							ajaxindicatorstart('loading data.. please wait..');
						// event.preventDefault();
						 $.ajax({
						     url: base_url+"profile/action/createOtp/",
						     type: "post",
						     data: "&mobile_number="+mobile_no,
						     success: function(d) { 
						    		 ajaxindicatorstop();
						    		 
							             if( d ) {
								             	
								     			//	$( "#dialog" ).dialog( "close" );
								     				$( "#load-dialog" ).dialog( "open" );
								     				$("#verify_code" ).val( '' );
								     				$("#mobile_no").val( mobile_no );
								     				$("#field").val( mob_number );
								     				if( null == pid ) {
								     						$("#pid").val( '' );
								     				} else {
								     					$("#pid").val( pid ); 
								     				}	
								     				//event.preventDefault();
						            	} else {
								            	 $('#err_user_mobile').show();
								            	 $('#err_user_mobile').html('Could not send sms');
						            }
						     }
						         
						 });
				}
		}
}
   $( "#dialog" ).dialog({
				autoOpen: false,
				width: 800,
				modal: true,
				buttons: [
					
// 					{
// 						text: "Cancel",
// 						click: function() {
// 							$( this ).dialog( "close" );
// 						}
// 					}
				],
				beforeClose: function(event, ui) { 
					//alert( $('.success:visible').length );
						if( 0 != $('#dialog').find( '.alert-success' ).text().length ){
							window.parent.location.reload();
						}
		            }
			});

   $( "#load-dialog" ).dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		buttons: [
			
//			{
//				text: "Cancel",
//				click: function() {
//					$( this ).dialog( "close" );
//				}
//			}
		]
	});
		   
	// Link to open the dialog
	$( "#dialog-link" ).click(function( event ) {
		$( "#dialog" ).dialog( "open" );
		event.preventDefault();
	});
			
// 			$( "#load-dialog-link" ).click(function( event ) {
// 				var mobile_no = $("#user_mobile_number").val();
// 					ajaxindicatorstart('loading data.. please wait..');
// 		         event.preventDefault();
// 				 $.ajax({
// 		             url: base_url+"profile/action/createOtp/",
// 		             type: "post",
// 		             data: "&mobile_number="+mobile_no,
// 		             success: function(d) { 
// 		            		 ajaxindicatorstop();
// 		            		 	var d = JSON.parse(d);
		            		
// 					             if(  true == d.sms ) {
						             	
// 						     			//	$( "#dialog" ).dialog( "close" );
// 						     				$( "#load-dialog" ).dialog( "open" );
						     				
// 						     				event.preventDefault();
// 				            	} else {
// 						            	 $('#err_user_mobile').show();
// 						            	 $('#err_user_mobile').html('Could not send sms');
// 				            }
// 		             }
			             
// 		         });
		         

				
// 			});
			
			
			$("button[type='reset']").on("click", function(){
			
				if( 0 != $('#dialog').find( '.alert-success' ).text().length ){
					window.location.reload();
				} else {
					$('#dialog').find( '.success' ).text('');
					$('#dialog').find( '.error' ).text('');
					$( '#dialog' ).dialog( "close" );
				}
			
			});
			// Hover states on the static widgets
			$( "#dialog-link, #icons li" ).hover(
				function() {
					$( this ).addClass( "ui-state-hover" );
				},
				function() {
					$( this ).removeClass( "ui-state-hover" );
				}
			);
		
				  //called when key is pressed in textbox
				
	  $("#details input[type='text']").keypress(function (e) { 
		
			  //if the letter is not digit then display error and don't type anything
	  		   if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					
						if( true == $('#'+ $(this).siblings('div').attr('id')).hasClass("error")) {
									//$(this).parent("div").append('<div class="error">Digits Only</div>');
			   				$('#'+ $(this).siblings('div').attr('id')).hide();
						 		$('#'+ $(this).siblings('div').attr('id')).html("Digits Only").show();
						  		 return false;
			        }
	   		} else {
	   				//$(this).parent("div").remove( '.error' );
	   				$('#'+ $(this).siblings('div').attr('id')).html("Digits Only").hide();
	   		}
	   });
</script>
<script src="<?php echo base_url(); ?>js/profile.js"></script>
