<?php $this->load->view('common/header');?>
<?php $this->load->view('common/template_style');?>
<div id="contact">
	<div class="sub-contact">
		<h2> Disclaimer </h2>
		<p>This disclaimer governs your use of our website. By using our website, you accept this disclaimer in full. If you disagree with any part of this disclaimer, do not use our website.</p>
		<p>Unless otherwise stated, we or our licensors own the intellectual property rights in the website and material on the website. Subject to the licence below, all our intellectual property rights are reserved.You may view, download for caching purposes only, and print pages from the website, provided that:</p>
		<p>(a) You must not republish material from this website (including republication on another website), or reproduce or store material from this website in any public or private electronic retrieval system. (Though we encourage fair use for academic / non-commercial purposes);</p>
		<p>(b) You must not reproduce, duplicate, copy, sell, resell, visit, or otherwise exploit our website or material on our website for a commercial purpose, without our express written consent;</p>
		<p>The information on this website is provided free-of-charge, and you acknowledge that it would be unreasonable to hold us liable in respect of this website and the information on this website. Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy, nor do we commit to ensuring that the website remains available or that the material on this website is kept up-to-date.</p>
		<p>To the maximum extent permitted by applicable law we exclude all representations, warranties and conditions (including, without limitation, the conditions implied by law of satisfactory quality, fitness for purpose and the use of reasonable care and skill).</p>
		<p>Our liability is limited and excluded to the maximum extent permitted under applicable law. We will not be liable for any direct, indirect or consequential loss or damage arising under this disclaimer or in connection with our website, whether arising in tort, contract, or otherwise &ndash; including, without limitation, any loss of profit, contracts, business, goodwill, data, income, revenue or anticipated savings.</p>
		<p>However, nothing in this disclaimer shall exclude or limit our liability for fraud, for death or personal injury caused by our negligence, or for any other liability which cannot be excluded or limited under applicable law.</p>
		<p>The website contains links to other websites. We are not responsible for the content of third party websites.</p>
		<p>We may revise this disclaimer from time-to-time. Please check this page regularly to ensure you are familiar with the current version.</p>
		<p>This disclaimer constitutes the entire agreement between you and us in relation to your use of our website, and supersedes all previous agreements in respect of your use of this website. This notice will be governed by and construed in accordance with Indian law, and any disputes relating to this notice shall be subject to the exclusive jurisdiction of the courts of India.</p>
	</div><!--sub-contact-->
</div><!--contact-->			
<?php $this->load->view('common/footer');?>