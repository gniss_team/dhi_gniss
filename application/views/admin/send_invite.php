<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/admin-sidebar');?>
 <link href="<?php echo base_url(); ?>css/toggles.css" rel="stylesheet">
 <link href="<?php echo base_url(); ?>css/toggles-light.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/toggles.js"></script>
<script type="text/javascript" >
$(document).ready(function() {
	//$('#tog1').toggles({drag:false,text:{on:'Yes',off:'No'}});
	//$('#is_default').val(0);
        
});

function updateStatus(id)
{
		var putbox = "#is_default";
		var newtog = '#tog'+id;
		var curval = $(putbox).val();
		
		if(curval==0)
		{
			$(putbox).val(1);
			myToggle.toggle();
		}else{
			$(putbox).val(0);
			myToggle.toggle();
		}
		var myToggle = $(newtog).data('toggles');
}

function getStatus(id,val)
{
		var opt = '';
	
		if(val==1){
			opt = true;
			$('#tog1').toggles({on:true,drag:false,text:{on:'Yes',off:'No'}});
			$('#is_default').val(val);
		}else
		{
			opt = false;
			$('#tog1').toggles({drag:false,text:{on:'Yes',off:'No'}});
			$('#is_default').val(val);
		}
}
</script>
<style>
.form-control {
		margin:0px 5px;
}
.form-grp li {
    display: block;
    float: left;
    margin-right: 30px;
}
#marker_label {
	float:left;
}
#icon_label {
	clear: both;
	float: left;
	position:relative;
	left:68px;
}

.form-grp img{
vertical-align:middle;
}
#preview {
    float: right;
    margin: 50px 150px;
}

select {
    display:inline;
}

.form-main-grp {
    margin: 0;
    padding: 5px;
    width: 100%;
}
.form-grp {
    float: none;
   
}
.error {
		clear:none;
}
</style>
<div class="content clearfix">
		<div class="page-title"><h4>Send Invitation</h4></div>	<?php if($this->session->userdata('successmsg')) { ?>
							<div class="alert alert-success" >
						 				<?php echo $this->session->userdata('successmsg');
						 				$this->session->unset_userdata('successmsg');
						 				?>
							</div>
				<?php } ?>
		
		<div class="clearfix">
				<span class="asterisk-msg">All fields marked with * are mandatory.</span>
<?php echo  form_open_multipart('admin/ticket/action/send_invite'); ?>
 		
	   <div class="form-grp">
		         <label>Name<span style="color:red;"> *</span></label>
					<input class="form-control" name="name" id="name" placeholder=" Name"  value="<?php echo set_value('name'); ?>"   >
               <span style="color:red;"><?php echo form_error('name'); ?></span>
       </div>
        		
	   <div class="form-grp">
		         <label>Email<span style="color:red;"> *</span></label>
					<input class="form-control" name="email" id="email" placeholder="Email" value="<?php echo set_value('email'); ?>"  >
               <span style="color:red;"><?php echo form_error('email'); ?></span>
       </div>
      
       <div class="form-grp">
	                        <label>Subject<span style="color:red;"> *</span></label>
							<input class="form-control" name="subject" id="subject" placeholder="subject" value="<?php echo set_value('subject'); ?>" >
                            <span style="color:red;"><?php echo form_error('subject'); ?></span>
                        </div>
       <div class="form-grp">
       				<label>Message</label>
						<textarea class="form-control" name="message" placeholder = "subject" id="message">
					<table border="0" cellpadding="0" cellspacing="0" style="background-color:rgb(255, 255, 255); height:225px; width:699px">
	<tbody>
		<tr>
			<td style="background-color:#ffffff; vertical-align:top">
			<h3><span style="font-size:16px">&nbsp;&nbsp;&nbsp; Registration Invitation For Gniss </span></h3>
				Global Network Information Security System, makes communication easier.
			<h3>&nbsp;</h3>

		

			<p style="text-align:left"><span style="font-size:18px"><span style="font-family:arial,helvetica,sans-serif">Click Here To&nbsp; <a href="http://gniss.net/users/action/register">Register</a></span></span></p>
			</td>
		</tr>
	</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="background-color:#4C4D4F; height:108px; margin-bottom:50px; width:600px">
	<tbody>
		<tr>
			<td>
			<table border="0" cellpadding="0" cellspacing="0" style="height:134px; width:695px">
				<tbody>
					<tr>
						<td style="text-align:left">
						<p style="text-align:left"><a href="http://www.utk.edu/" style="color:#fefefe; text-decoration:none;">CONTACT US</a><br />
						<a href="http://chancellor.utk.edu/" style="color:#fefefe; text-decoration:none;">Gniss Technologies</a><br />
						<span style="color:#FFF0F5">India Pune<br />
						E-mail: </span><a href="mailto:xxx@utk.edu" style="color:#ffffff; text-decoration:none;"><span style="color:#FFF0F5">info@gniss.com</span></a><br />
						<span style="color:#FFF0F5">Phone: 865.974.xxxx | Fax: 865.974.xxxx</span></p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
						
						</textarea>
						<span style="color:red;"><?php echo form_error('message'); ?></span>
       </div>
      

        <div class="form-grp">
         <div class="btn-grp">
        			<button type="submit"   class="signup">Send </button>
            	<button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/module/action/view'">Cancel</button>
        </div>
        </div>
		<?php  echo form_close(); ?>	
		</div>
</div>
  <script type="text/javascript">
  $( document ).ready( function() {
		$( 'textarea#message' ).ckeditor();
	} );
</script>
  
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/ckeditor.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.4/adapters/jquery.js"></script>



<?php $this->load->view('common/footer');?>
