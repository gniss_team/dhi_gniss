<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/admin-sidebar');?>
<style>
.signup {
	margin: 0;
}
.error {
	left:160px;
}
.alert-success {
 	padding:5px 0;
 	margin:5px 0;
 }
.breadcrums li a {
    background: none;
}
.form-grp label {
	 width: 40%;
}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}

.dataTables_length label {
	width: auto;
	display: inline-flex;
}
.dataTables_length select {
	margin: -6px 12px;
	padding: 5px 10px 5px 0px;
	/*-webkit-border-radius:4px;*/
   -moz-border-radius:4px;
   /* border-radius:4px;
  -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;*/
   background: url("../../../images/select-arrow.png") no-repeat scroll 98% center #fff;
   color:#555;
   border:1px solid #e3e3e3;
   outline:none;
   display style="margin:0 18px;": inline-block;
   -webkit-appearance:none;
   -moz-appearance:none;
   appearance:none;
   cursor:pointer;
}
.dataTables_paginate{
    float: right;
    font-size: 13px;
    padding-top: 0px;
    padding-right: 12px;
    padding-bottom: 6px;
    padding-left: 12px;
    text-align: right;
}
.dataTables_wrapper .dataTables_filter{
	margin-top: 4px;
}
#myTable_info{
	font-size:13px;
}
table.dataTable {
	width:98% !important;
	margin-left:0px !important;	
}
.img_icon {
	margin-right:10px;
	cursor:pointer;
}	
#img_livetrack {
	cursor:pointer;
}
table.tablesorter thead tr .noheader {
    border-right:1px solid;
}
</style>
<div class="content clearfix">
<div class="page-title"><h4>Manage User Roles</h4></div>
<button class="signup" type="button" onclick="window.location='<?php echo base_url(); ?>admin/roles/action/add'">Add Role</button>
<div class="content-wrap clearfix">
			<?php if(isset($msg) && $msg != '') { ?>
			<div class="alert alert-success" >
						 <?php echo $msg; ?>
			</div>
		<?php } ?>
		<?php if(isset($errmsg) && $errmsg != '') { ?>
		<div class="alert alert-error" >
				 <?php echo $errmsg; ?>
		</div>
		<?php } ?>
					<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	                    <thead>
                              <tr>
                              	  <th style="display:none">Id</th>
                              	  <th>Role</th>
			     				 			 <th class="noheader">Action</th>
                              </tr>
                         </thead>
                         <tbody>
						 <?php foreach ( $roles as $role ){?>
						<tr class="active">
								<td style="display:none"><?php echo $role->id; ?></td>
                        <td><?php echo $role->name; ?></td>
			  					<td>
								 <img class="img_icon" src="<?php echo base_url(); ?>images/edit.png" onclick="window.location='<?php echo base_url(); ?>admin/roles/action/edit/<?php echo $role->id;?>'" height="15px" alt="Edit" title="Edit">
								 <img class="img_icon" onclick="deleteRecord( <?php echo $role->id;?>, 'admin/roles/action/delete/' );"  src="<?php echo base_url(); ?>images/delete.png"  height="15px" alt="Delete" title="Delete">
 								<img class="img_icon" src="<?php echo base_url(); ?>images/key.png" onclick="window.location='<?php echo base_url(); ?>admin/roles/action/assign/<?php echo $role->id;?>'" height="15px" alt="Manage Permission" title="Manage Permission">
 								</td>
 								</tr>
                          <?php } ?>
                          </tbody>
                      </table>
	</div>
	</div>
</div>
<?php $this->load->view('common/footer');?>