<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/admin-sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Manage Asset Type</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>admin/action/viewdashboard">Dashboard</a></li>
			<li>Manage Asset Type</li>
		</ul>
	</div>
	<button class="btn btn-xs btn-success" style="margin:0 10px 20px 25px;" type="button" onclick="window.location='<?php echo base_url(); ?>assettype/action/addassettype'">Add Asset Type</button>
	<div class="content-wrap clearfix">
        <?php if($msg!="") { ?>
			<div class="alert alert-success">
			    <?php echo $msg; ?>
			</div>
		<?php } ?>
		<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	    	<thead>
            	<tr>
                	<th style="display:none">Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Created On</th>
                    <th>Created By</th>
			     	<th class="noheader">Action</th>
                </tr>
            </thead>
            <tbody>
				<?php foreach ( $assettype as $types ){?>
                <tr class="active">
                	<td style="display:none"><?= $types->id; ?></td>
                    <td><?= $types->name; ?></td>
                    <td><?= substr($types->description,0,50).'...'; ?></td>
                    <td><?= date("D, d M Y ",strtotime($types->created_on)); ?></td>
                    <td><?= $types->first_name . ' ' . $types->last_name; ?></td>
					<td><button class="btn btn-xs btn-success" type="button" onclick="window.location='<?php echo base_url(); ?>assettype/action/editassettype/<?= $types->id; ?>'">Edit</button>
 						<button class="btn btn-xs btn-danger" type="button" onclick="deletedata(<?php echo $types->id;?>,'assettype/action/deleteassettype/');">Delete</button>
 					</td>
 				</tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</div>
<?php $this->load->view('common/footer');?>
