<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/admin-sidebar');?>

<style>
.form-control {
		margin:0px 5px;
}
.form-grp li {
    display: block;
    float: left;
    margin-right: 30px;
}
#marker_label {
	float:left;
}
#icon_label {
	clear: both;
	float: left;
	position:relative;
	left:68px;
}

.form-grp img{
vertical-align:middle;
}
</style>
<div class="content clearfi">
		<div class="page-title"><h4>Edit User Role</h4></div>
		<div class=" clearfix">
				<span class="asterisk-msg">All fields marked with * are mandatory.</span>
		<?php echo form_open( 'admin/roles/action/edit/' . $role->id ); ?>
	        <div class="form-grp">
		    	<label>Name <span class="asterisk-msg">*</span></label>
	            	<input class="form-control" placeholder="Name" name="name" id="name" maxlength="21" 
	            	value="<?php  if( false == is_array( $this->input->post( ) ) ) { echo $role->name; } else{ echo set_value('name'); } ?>"  >
	                <?php echo form_error('name'); ?>
	        </div>
	         <div class="form-grp">
		        	<label>Description</label>
					<input class="form-control" name="description" id="description" placeholder="Description" 
					value="<?php if( false == is_array( $this->input->post( ) ) ) { echo $role->description; } else { echo  set_value('description'); } ?>"  >
		          <?php echo form_error('description'); ?>
        	</div>
         <div class="btn-grp">
        			<button type="submit"   class="signup">Save </button>
            	<button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/roles/action/view'">Cancel</button>
        </div>
		<?php  echo form_close(); ?>	
		</div>
</div>
  <script type="text/javascript">
$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>

<?php $this->load->view('common/footer');?>
