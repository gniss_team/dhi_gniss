<?php $this->load->view('common/admin-header');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<?php $this->load->view('common/admin-sidebar');?>
<script type="text/javascript">

  $(document).ready(function(){
		  $('#select_role').change(function(){
		  			$('#form_role').submit();
		  });
  
		  var role = "<?php echo @$this->session->userdata('role_id'); ?>";
		  if( role ) {
		  			$('#select_role').val(role);
		  }  
		  
	});
</script>
<style>
.error {
	left:160px;
}
.alert-success {
 	padding:5px 0;
 	margin:5px 0;
 }
.breadcrums li a {
    background: none;
}
.form-grp label {
	 width: 40%;
}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}

.dataTables_length label {
	width: auto;
	display: inline-flex;
}
.dataTables_length select {
	margin: -6px 12px;
	padding: 5px 10px 5px 0px;
	/*-webkit-border-radius:4px;*/
   -moz-border-radius:4px;
   /* border-radius:4px;
  -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;*/
   background: url("../../../images/select-arrow.png") no-repeat scroll 98% center #fff;
   color:#555;
   border:1px solid #e3e3e3;
   outline:none;
   display style="margin:0 18px;": inline-block;
   -webkit-appearance:none;
   -moz-appearance:none;
   appearance:none;
   cursor:pointer;
}
.dataTables_paginate{
    float: right;
    font-size: 13px;
    padding-top: 0px;
    padding-right: 12px;
    padding-bottom: 6px;
    padding-left: 12px;
    text-align: right;
}
.dataTables_wrapper .dataTables_filter{
	margin-top: 4px;
}
#myTable_info{
	font-size:13px;
}
table.dataTable {
	width:98% !important;
	margin-left:0px !important;	
}
.img_icon {
	margin-right:10px;
	cursor:pointer;
}	
#img_livetrack {
	cursor:pointer;
}
table.tablesorter thead tr .noheader {
    border-right:1px solid;
}
.text_remove {
  text-decoration: none !important;
}
</style>
<div class="content clearfix">
	<div class="page-title"><h4>Manage Users</h4></div>
	<button class="signup"  type="button" onclick="window.location='<?php echo base_url(); ?>admin/user/action/addUser'">Add User</button>
	<div class="content-wrap clearfix">
        <?php if(@$this->session->userdata('msg')!="") { ?>
			<div class="alert alert-success">
			    <?php echo $this->session->userdata('msg'); 
			    $this->session->unset_userdata('msg');?>
			</div>
		<?php } ?>
		<div style="clear:both;float:left;left: 25%;position:absolute;z-index:999;width:20%;">
			<?php echo form_open('admin/user/action/view', array( 'id' => 'form_role')); ?>
	            <select class="styled" name="select_role" id="select_role">
	            	<option value="">Select Role</option>
	            	<?php foreach ($roles as $id => $role) { ?>
	            		<option value="<?php echo $id; ?>"><?php echo $role; ?></option>
	            	<?php } ?>
	            </select>
	        <?php echo form_close();?>
        </div>
		<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	    	<thead>
            	<tr>
                	<th style="display:none">Id</th>
					<th>Name</th>
					<th>Username</th>
					<th class="noheader">Email Address</th>
					<th class="noheader">Role</th>
					<th class="noheader">Gniss ID</th>
					<th class="noheader">Action</th>
                </tr>
            </thead>
            <tbody>
				<?php foreach ($users as $user){?>
             <tr class="active">
		            <td style="display:none"><?php echo $user->id; ?></td>
		            <td><?php echo $user->first_name.' '.$user->last_name; ?></td>
		            <td><?php echo $user->username; ?></td>
		            <td><?php  echo $user->email_address; ?></td>
		            <td><?php echo @$roles[$user->role]; ?></td>
		            <td><?php echo $user->account_number; ?></td>
		            <td><?php $strImageName	= ( '0' == $user->is_active ) ?  'on-icon.png' : 'off-icon.png';
		            						$strLabelName= ( '0' == $user->is_active ) ?  'Deactivate' : 'Activate';  ?>
		        <img class="img_icon" onclick="window.location='<?php echo base_url(); ?>admin/user/action/editUser/<?php echo $user->id;?>'" src="<?php echo base_url(); ?>images/edit.png" height="15px" alt="Edit" title="Edit">
                  <img class="img_icon" onclick="deleteRecord( <?php echo $user->id;?>, 'admin/user/action/deleteUser/' );"  src="<?php echo base_url(); ?>images/delete.png"  height="15px" alt="Delete" title="Delete">
              <img class="img_icon" onclick="window.location='<?php echo base_url(); ?>admin/user/action/activate/<?php echo $user->id;?>/<?php echo $user->is_active;?> '"  src="<?php echo base_url(); ?>images/<?php echo $strImageName; ?>"  height="15px" alt="<?php echo $strLabelName;?>" title="<?php echo $strLabelName;?>">
              <img class="img_icon" src="<?php echo base_url(); ?>images/key.png" onclick="window.location='<?php echo base_url(); ?>admin/user/action/assign/<?php echo $user->id;?>'" height="15px" alt="Manage Permission" title="Manage Permission">
            </td>
          </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->load->view('common/admin-footer');?>
