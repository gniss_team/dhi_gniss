<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/admin-sidebar');?>
 <link href="<?php echo base_url(); ?>css/toggles.css" rel="stylesheet">
 <link href="<?php echo base_url(); ?>css/toggles-light.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/toggles.js"></script>
<script type="text/javascript" >
$(document).ready(function() {
	//$('#tog1').toggles({drag:false,text:{on:'Yes',off:'No'}});
	//$('#is_default').val(0);
        
});

function updateStatus(id)
{
		var putbox = "#is_default";
		var newtog = '#tog'+id;
		var curval = $(putbox).val();
		
		if(curval==0)
		{
			$(putbox).val(1);
			myToggle.toggle();
		}else{
			$(putbox).val(0);
			myToggle.toggle();
		}
		var myToggle = $(newtog).data('toggles');
}

function getStatus(id,val)
{
		var opt = '';
	
		if(val==1){
			opt = true;
			$('#tog1').toggles({on:true,drag:false,text:{on:'Yes',off:'No'}});
			$('#is_default').val(val);
		}else
		{
			opt = false;
			$('#tog1').toggles({drag:false,text:{on:'Yes',off:'No'}});
			$('#is_default').val(val);
		}
}
</script>
<style>
.form-control {
		margin:0px 5px;
}
.form-grp li {
    display: block;
    float: left;
    margin-right: 30px;
}
#marker_label {
	float:left;
}
#icon_label {
	clear: both;
	float: left;
	position:relative;
	left:68px;
}

.form-grp img{
vertical-align:middle;
}
#preview {
    float: right;
    margin: 50px 150px;
}

select {
    display:inline;
}

.form-main-grp {
    margin: 0;
    padding: 5px;
    width: 100%;
}
.form-grp {
    float: none;
   
}
.error {
		clear:none;
}
</style>
<div class="content clearfix">
		<div class="page-title"><h4><?php if( true == isset( $modules ) ) { ?> Edit <?php } else {?>Add <?php } ?> module</h4></div>
		<div class="clearfix">
				<span class="asterisk-msg">All fields marked with * are mandatory.</span>
		<?php if( true == isset( $modules[0]->id ) ) {
								 echo  form_open_multipart( base_url() . 'admin/module/action/edit/'.$modules[0]->id);
						} else { 
								echo  form_open_multipart('admin/module/action/add');
		} ?>
 		<div class="form-grp">
		    		<div id="preview">
            		<h4 align="center">Module Icon</h4>
            		<div style="background:#fff;padding: 3px;">
            					<img width="120px" height="120px" src="<?php if( true == isset( $modules[0]->image ) && false == empty( $modules[0]->image ) ) { echo base_url(); ?>uploads/<?= $modules[0]->image; } else { echo base_url(); ?>images/module-default-ico.png <?php } ?>" id="thumb" />
      				</div>
	   			</div>
	   <div class="form-grp">
		         <label>Name<span style="color:red;"> *</span></label>
					<input class="form-control" name="name" id="name" placeholder="Module name" value="<?php if( true == isset( $modules[0]->name ) ) { echo $modules[0]->name; } else { echo set_value('name'); } ?>"  >
               <span style="color:red;"><?php echo form_error('name'); ?></span>
       </div>
       <div class="form-grp">
		         <label>Parent Module</label>
					<select class="chosen-select" id="parent_module" name="parent_module">
							<option value="">Select</option>
		              	<?php  foreach( $modules_list as $module_list ) { ?>
							<option value="<?php echo $module_list->id; ?>"><?php echo $module_list->name; ?> </option>
							<?php } ?>
               </select>
               <span style="color:red;"><?php echo form_error('parent_module'); ?></span>
       </div>
       <div class="form-grp">
	                        <label>Module Link<span style="color:red;"> *</span></label>
							<input class="form-control" name="internal_url" id="internal_url" placeholder="Module URL" value="<?php if( true == isset( $modules[0]->internal_url ) ) { echo $modules[0]->internal_url; } else { echo set_value('internal_url'); } ?>"  >
                            <span style="color:red;"><?php echo form_error('internal_url'); ?></span>
                        </div>
       <div class="form-grp">
       				<label>Description</label>
						<textarea class="form-control" name="description" placeholder = "Module description" id="description"><?php if( true == isset( $modules[0]->description ) ) { echo $modules[0]->description; } else { echo set_value('description'); } ?></textarea>
						<span style="color:red;"><?php echo form_error('description'); ?></span>
       </div>
       <div class="form-grp">
       			<label>Is published</label>
	            <select class="chosen-select" id="is_published" name="is_published">
	            				<option value="0" <?php  if( true == isset( $modules[0]->is_published ) ) { if( '0' == $modules[0]->is_published ) { ?> selected="selected" <?php } echo set_select('is_published', $modules[0]->is_published ); } ?> >No</option>
									<option value="1" <?php  if( true == isset( $modules[0]->is_published ) ) {  if( '1' == $modules[0]->is_published ) { ?> selected="selected" <?php } echo set_select('is_published', $modules[0]->is_published ); } ?> >Yes</option>
               </select>
       </div>
       <div class="form-grp">
                                <label>Module Type <span style="color:red;">*</span></label>
                                <select class="chosen-select" id="is_admin" name="is_admin">
                                <option value="">Select type</option>
                                <option value="0" <?php  if( true == isset( $modules[0]->is_admin ) ) {  if( '0' == $modules[0]->is_admin ) { ?> selected="selected" <?php }   echo set_select('is_admin', $modules[0]->is_admin ); } ?> >Admin</option>
                                <option value="1" <?php  if( true == isset( $modules[0]->is_admin ) ) { if( '1' == $modules[0]->is_admin ) { ?> selected="selected" <?php }  echo set_select('is_admin', $modules[0]->is_admin ); } ?> >Customer</option>
                                </select>
                                <span style="color:red;"><?php echo form_error('is_admin'); ?></span>
                         </div> 
       <div class="form-grp">
        				<label>Module position <span style="color:red;">*</span></label>
                                <select class="chosen-select" id="position" name="position">
                                <option value="">Select type</option>
                                <option value="1" <?php  if( true == isset( $modules[0]->position ) ) {  if( '1' == $modules[0]->position ) { ?> selected="selected" <?php }   echo set_select('position', $modules[0]->position ); } ?>>Sidebar</option>
                                <option value="2" <?php  if( true == isset( $modules[0]->position ) ) {  if( '2' == $modules[0]->position ) { ?> selected="selected" <?php }   echo set_select('position', $modules[0]->position ); } ?>>Profile</option>
                                <option value="3" <?php  if( true == isset( $modules[0]->position ) ) {  if( '3' == $modules[0]->position ) { ?> selected="selected" <?php }   echo set_select('position', $modules[0]->position ); } ?>>Breadcrum</option>
                                </select>
                                <span style="color:red;"><?php echo form_error('position'); ?></span>
        </div>
<!--        <div class="form-grp"> -->
<!--        			<label>Default Module </label> -->
<!--                <select class="chosen-select" id="is_default" name="is_default"> -->
                           <!-- <option value="0" --><?php  //if( true == isset( $modules[0]->is_default ) ) {  if( '0' == $modules[0]->is_default ) { ?><!-- selected="selected" --> <?php //}   echo set_select('is_default', $modules[0]->is_default ); } ?> <!--  >No</option> -->
                           <!-- <option value="1" --><?php  //if( true == isset( $modules[0]->is_default ) ) { if( '1' == $modules[0]->is_default ) { ?><!-- selected="selected" --> <?php //}  echo set_select('is_default', $modules[0]->is_default ); } ?><!--  >Yes</option> -->
<!--                 </select> -->
<!--        </div> -->
       <?php if( true == isset( $modules[0]->order_num ) ) { ?> 
       <div class="form-grp">
       			<label>Order Number</label>
					<input class="form-control" name="order_num" id="order_num" placeholder="Order number" value="<?php if( true == isset( $modules[0]->order_num ) ) { echo $modules[0]->order_num; } else { echo set_value('order_num'); } ?>"  >
                <span style="color:red;"><?php echo form_error('order_num'); ?></span>
       </div>
       <?php } ?>
        <div class="form-grp">
       			<label>Module Image</label>
               <input maxlength="20" type="file"  id="module_img" name="module_img" onchange="readURL(this);">
               <?php if( true == isset( $modules[0]->image ) ) { ?>
               <input type="hidden" value="<?php echo $modules[0]->image; ?>" name="existing_module_img">
               <?php } ?>
        </div>
        <div class="form-grp">
         <div class="btn-grp">
        			<button type="submit"   class="signup">Save </button>
            	<button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/module/action/view'">Cancel</button>
        </div>
        </div>
		<?php  echo form_close(); ?>	
		</div>
</div>
  <script type="text/javascript">
$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
function readURL(input) {
 	if (input.files && input.files[0]) {
    	var reader = new FileReader();
		reader.onload = function (e) {
        	$('#thumb').attr('src', e.target.result);
        }
    reader.readAsDataURL(input.files[0]);
    }
 }
</script>
<?php $this->load->view('common/footer');?>
