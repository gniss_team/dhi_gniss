<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<div class="content-body-wrapper-dashboard">
<?php $this->load->view('common/admin-sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Edit Asset Type</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>admin/action/viewdashboard">Dashboard</a></li>
			<li><a class="big" href="<?php echo base_url(); ?>assettype/action/manageassettype">Manage Asset Type</a></li>
			<li>Edit Asset Type</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
    	<?php echo form_open('assettype/action/editassettype/'.$assettype_info[0]->id); ?>
        <div class="form-grp">
			<label>Asset type<span style="color:red;"> *</span></label>
            <input class="form-control" placeholder="Asset type" name="typename" id="typename" value="<?= $assettype_info[0]->name; ?>">
            <span style="color:red;"><?php echo form_error('typename'); ?></span>
        </div>
		<div class="form-grp">
        	<label>Description<span style="color:red;"> *</span></label>
            <textarea class="form-control" placeholder="Description" name="typedescription" id="typedescription" style="height: 68px;width: 204px;"><?= $assettype_info[0]->description; ?></textarea>
            <span style="color:red;"><?php echo form_error('typedescription'); ?></span>
        </div>
        <div class="form-grp">
        	<label>Created by</label>
            <input class="form-control-readonly" readonly="readonly" value="<?= $assettype_info[0]->first_name; ?> (<?= $assettype_info[0]->email_address; ?>)">
        </div>
		<div class="form-grp">
        	<label>Date of creation</label>
            <input class="form-control-readonly" readonly="readonly" value="<?= date("D, d M Y ",strtotime($assettype_info[0]->created_on)); ?>">
        </div>
     	<div class="btn-grp">
        	<button type="submit" class="btn btn-primary">Update </button>
            <button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>assettype/action/manageassettype'">Cancel</button>
		</div>
        <?php  echo form_close(); ?>
	</div>
</div>
</div>
<style>
.error {
	left:160px;
}
</style>
<?php $this->load->view('common/footer');?>   
