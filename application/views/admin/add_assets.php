<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/admin-sidebar');?>
<div class="content clearfix">
		<div class="page-title"><h4>Add Asset</h4></div>
		<div class=" clearfix">
				<span class="asterisk-msg red">All fields marked with * are mandatory.</span>
		<?php echo form_open('admin/tracker/action/add'); ?>
	        <div class="form-grp">
		    	<label>Name <span class="red">*</span></label>
	            	<input class="form-control" placeholder="Name" name="name" id="name" maxlength="21" value="<?php echo set_value('name'); ?>"  >
	                <?php echo form_error('name'); ?>
	        </div>
	        <div class="form-grp">
				    	<label>Sim number</label>
						<input class="form-control" name="sim_number" id="sim_number" placeholder="Sim number" value="<?php echo set_value('sim_number'); ?>"  >
			      		<?php echo form_error('sim_number'); ?>
			</div>
			<div class="form-grp">
		        	<label>Asset number<span class="red"> *</span></label>
					<input class="form-control" name="asset_number" id="asset_number" placeholder="Asset number" maxlength="16" value="<?php echo set_value('asset_number'); ?>"  >
					<?php echo form_error('asset_number'); ?>
        </div>
        <div class="form-grp">
		        	<label>IMEI number<span class="red"> *</span></label>
					<input class="form-control" name="imei_number" id="imei_number" placeholder="IMEI number" value="<?php echo set_value('imei_number'); ?>"  >
		            <?php echo form_error('imei_number'); ?>
        </div>
        <div class="form-grp">
		        	<label>Phone number<span class="red"> *</span></label>
					<input class="form-control" name="phone_number" id="phone_number" placeholder="Phone number" value="<?php echo set_value('phone_number'); ?>"  >
		          <?php echo form_error('phone_number'); ?>
        </div>
        <div class="form-grp">
		        	<label>Model number<span class="red"> *</span></label>
					<input class="form-control" name="model_number" id="model_number" placeholder="Model number" value="<?php echo set_value('model_number'); ?>"  >
		            <?php echo form_error('model_number'); ?>
        </div>
        <div class="form-grp">
		        	<label>Asset Mode<span class="red"> *</span></label>
		            <select class="form-control-select styled" name="mode" id="mode">
		          		<option value="">Select</option>
		            	<option value="m" placeholder="Mode" <?php if( true == is_array( $this->input->post() ) ) { if( 'm' == $this->input->post( 'mode' ) ) { ?> selected="selected" <?php } } ?> <?php echo set_select('mode', 'm' ); ?>>Master</option>
		            	<option value="s" placeholder="Mode"  <?php if( true == is_array( $this->input->post() ) ) { if( 's' == $this->input->post( 'mode' ) ) { ?> selected="selected" <?php } } ?> <?php echo set_select('mode', 's' ); ?>>Slave</option>
		            </select>
		 			<?php echo form_error('mode'); ?>
        </div>
        <div class="form-grp">
        			<label>Status </label>
	            <input type="radio" name="status" value="1" <?php  if( true == is_array( $this->input->post() ) ) {  if( '1' == $this->input->post( 'status' ) ) { ?>  checked="checked"  <?php }   echo set_select('status', $this->input->post( 'status' ) ); } ?> ><span >Enabled</span>
	            <input type="radio" name="status" value="0" <?php  if( true == is_array( $this->input->post() ) ) {  if( '0' == $this->input->post( 'status' ) ) { ?>  checked="checked"  <?php }   echo set_select('status', $this->input->post( 'status' ) ); } else { ?>checked="checked" <?php } ?> ><span>Disabled</span>
        </div>
        <div class="btn-grp">
        			<button type="submit"   class="signup">Save </button>
            	<button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/tracker/action/manage'">Cancel</button>
        </div>
		<?php  echo form_close(); ?>	
		</div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });
  </script>
  <script type="text/javascript">

$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>
<style>
.error {
		margin:0px 5px 0 227px;
}
</style>
<?php $this->load->view('common/footer');?>