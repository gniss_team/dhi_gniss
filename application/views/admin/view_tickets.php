<?php $this->load->view('common/admin-header');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<?php $this->load->view('common/admin-sidebar');?>

<style>
.signup {
	margin: 0;
}
.error {
	left:160px;
}
.alert-success {
 	padding:5px 0;
 	margin:5px 0;
 }
.breadcrums li a {
    background: none;
}
.form-grp label {
	 width: 40%;
}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}

.dataTables_length label {
	width: auto;
	display: inline-flex;
}
.dataTables_length select {
	margin: -6px 12px;
	padding: 5px 10px 5px 0px;
	/*-webkit-border-radius:4px;*/
   -moz-border-radius:4px;
   /* border-radius:4px;
  -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;*/
   background: url("../../../images/select-arrow.png") no-repeat scroll 98% center #fff;
   color:#555;
   border:1px solid #e3e3e3;
   outline:none;
   display style="margin:0 18px;": inline-block;
   -webkit-appearance:none;
   -moz-appearance:none;
   appearance:none;
   cursor:pointer;
}
.dataTables_paginate{
    float: right;
    font-size: 13px;
    padding-top: 0px;
    padding-right: 12px;
    padding-bottom: 6px;
    padding-left: 12px;
    text-align: right;
}
.dataTables_wrapper .dataTables_filter{
	margin-top: 4px;
}
#myTable_info{
	font-size:13px;
}
table.dataTable {
	width:98% !important;
	margin-left:0px !important;	
}
.img_icon {
	margin-right:10px;
	cursor:pointer;
}	
#img_livetrack {
	cursor:pointer;
}
table.tablesorter thead tr .noheader {
    border-right:1px solid;
}
</style>
<!-- main body-->  
<div class="content clearfix">
	<div class="page-title"><h4>Manage Supports</h4></div>
	<div class="content-wrap clearfix">
        <?php if(@$this->session->userdata('msg')!="") { ?>
			<div class="alert alert-success">
			    <?php echo $this->session->userdata('msg'); 
			    $this->session->unset_userdata('msg');?>
			</div>
		<?php } ?>
             <table id="myTable" class="table table-bordered table-hover table-striped tablesorter">
			    			<thead>
								<tr>
									<th style="display: none;">Id</th>
									<?php if(1==$this->session->userdata('role')) { ?>
				    				<th>Customer Name</th>
				    				<?php } ?>
				    				<th>Categories</th>
				    				<th>Status </th>
				    				<th>Opening Date </th> 
				    				<th>Unread Messages </th> 
				    				<th class="noheader">Action</th>
								</tr>
			    			</thead>
			    			<tbody>
							<?php foreach ($ticket as $tickets){?>
                            	<tr>
                            		<td style="display: none;"><?php echo $tickets->id; ?></td>
                            		<?php if(1==$this->session->userdata('role')) { ?>
                                   	<td><?php echo $tickets->name; ?></td>
                                   	<?php } ?>
                                   	<td><?php echo $tickets->subject; ?></td>
                                   	<td>
                                   	<?php 
                                   	$statusticket = $tickets->status;
                                   	if($statusticket==1){echo 'Open';}
                                   	else{ echo 'Closed';}
                                   	?></td>
                                    <td><?php echo date("d M Y, H:i:s",strtotime($tickets->created_date));?></td>
                                    <td><?php
                                    if(1==$this->session->userdata('role'))
                                    {
                                    	echo $tickets->unread_admin;
                                    }else
                                    {
                                    	echo $tickets->unread_user;
                                    }
                                     ?></td>
                                   	<td>
										<button class="btn-primary" type="button" onclick="window.location='<?php echo base_url(); ?>admin/ticket/action/viewfull/<?php echo $tickets->id; ?>'" >View</button>                                       
                                   		<?php 	if($statusticket==1){?>
                                   		<button class="btn-danger" type="button"  onclick="closethis(<?php echo $tickets->id;?>,'admin/ticket/action/close/');" >Close</button>
                                   		<?php }else{?>
                                   		<button class="btn-success" type="button" >Closed</button>
                                   		<?php }?>
						    		</td>
                           	   </tr>
                           <?php } ?> 
					 </tbody>
					</table>
    </div>
</div>
<script>
function closethis(id,urln){
    var r=confirm("Do you want to close this?")
     if (r==true)
       window.location = '<?php echo base_url();?>'+urln+id;
     else
       return false;
     } 
</script>
<?php $this->load->view('common/admin-footer');?>
