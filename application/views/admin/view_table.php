<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<div class="content-body-wrapper-dashboard">
<?php $this->load->view('common/admin-sidebar');?>
<div style="min-height:800px;">
     <div class="content clearfix">
	<div class="page-title"><h3>View Table</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>admin/action/viewdashboard">Dashboard</a></li>
			<li><a class="big" href="<?php echo base_url(); ?>admin/energymeter/action/viewTableColumns">Manage Energy Meter</a></li>
			<li>View Table</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
		<?php if( true == isset( $msg ) && false == empty( $msg ) ) { ?>
          			<div class="alert alert-success" >
          				 <?php echo $msg; ?>
          			</div>
          		<?php } ?>
          		<?php if( true == isset($errmsg) && false == empty( $errmsg )  ) { ?>
          			<div class="alert alert-error" style="color:red;" >
          				 <?php echo $errmsg; 	?>
          			</div>
          			<?php } ?>

				<div class="form-grp">
                    		<div class="btn-grp"><label>Table Fields</label>
                    	<button  name="add" onclick="window.location='<?php echo base_url(); ?>admin/energymeter/action/addTableColumn'"  class="btn btn-primary">Add Column </button>
					</div>
                   </div>
                   <div class="form-grp">
					<?php
						//$intIndex	= 1; 
						foreach ( $table_fields as $strTableField ) { ?>
                    		<label><?php echo $strTableField;?></label>
               <?php 
						//$intIndex++;
					 } ?>
                   </div>
                <?php  echo form_close(); ?>
	</div>
</div>
 </div>
</div>
<script type="text/javascript">

$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>
<style>
.error {
	left:160px;
}
.breadcrums li a {
    background: none;
   
}
.form-grp label {
	 width: 40%;

}
</style>
<?php $this->load->view('common/footer');?>
