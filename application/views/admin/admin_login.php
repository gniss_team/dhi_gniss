<?php $this->load->view('common/admin-header');?>
	<div class="comp-heading">welcome to gniss Admin</div>
       <div class="comp-sub-txt">global network information security system</div>		
      
		  <div class="login-txt">Administrator login</div>
		  
		  <?php echo form_open('admin/action/login'); ?>
              <div class="content-login-wrapper">
                  <?php if(isset($msg) && $msg != '') { ?>
          			<div class="alert alert-success" >
          				 <?php echo $msg; ?>
          			</div>
          		<?php } ?>
          		<?php if(isset($errmsg) && $errmsg != '') { ?>
          			<div class="alert alert-error" style="color:red;" >
          				 <?php echo $errmsg; ?>
          			</div>
          		<?php } ?>
            		<div class="content-inner-wrapper clearfix">
       					<div><input class="user-ico-login" name="username" type="text" placeholder="Username" value="<?php echo set_value('username'); ?>" maxlength="50" />
        						<span style="color:red;"><?php echo form_error('username'); ?></span>
        				</div>
         			<div><input class="password-ico-login"  name="password" type="password" placeholder="Password"<?php echo set_value('password'); ?>maxlength="50" />
         				<span style="color:red;"><?php echo form_error('password'); ?></span>
         			</div>
<!--        <div class="error">error</div>
        <div class="error">error password</div>
        	<?php  echo form_close(); ?>
-->   	
    <div class="forgot-txt"><a href="<?php  echo base_url();?>admin/action/forgotPass">Forgot Password?</a></div>
	<div style="float:right;clear:both;"><button type="submit" name="login" value="login"  class="signup">Sign In</button></div>
 </div>
        
</div>
<?php $this->load->view('common/footer');?>
</body>
<style>
.alert-error {
		width:auto;
		margin: 7px;
		text-indent: 0%; 
}	      
.alert-success {
	  margin-left:6px;
}
</style>
</html>
