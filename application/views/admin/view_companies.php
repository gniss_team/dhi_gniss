<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/admin-sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Manage Company</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>admin/action/viewdashboard">Dashboard</a></li>
			<li>Manage Company</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
		<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	                    <thead>
                              <tr>
                              	  <th style="display:none">Id</th>
                                  <th>Name</th>
			     				  <th class="noheader">Action</th>
                              </tr>
                         </thead>
                         <tbody>
						 <?php foreach ($companies as $company){?>
                                <tr class="active">
                                  <td style="display:none"><?= $company->id; ?></td>
                                  <td><?= $company->company_name; ?></td>
                                 
								  <td>
								  <button class="btn btn-xs btn-success" type="button" onclick="window.location='<?php echo base_url(); ?>admin/action/companystatus/<?= $company->id; ?>/<?php echo $company->is_approved;?>'">
								  <?php if($company->is_approved==0){?>Approve <?php }else{?>Declined<?php }?>
								  </button>
 								 		  <button class="btn btn-xs btn-success" type="button" onclick="window.location='<?php echo base_url(); ?>admin/action/viewcompanydetail/<?= $company->id; ?>'">
								  View Company Details
								  </button>
								  <button class="btn btn-xs btn-success" type="button" onclick="window.location='<?php echo base_url(); ?>admin/action/viewcompanyusers/<?= $company->id; ?>'">
								  View Users
								  </button>
 								  </td>
 								</tr>
                          <?php } ?>
                          </tbody>
                      </table>
	</div>
</div>
</div>
<?php $this->load->view('common/footer');?>