<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/admin-sidebar');?>
 <link href="<?php echo base_url(); ?>css/toggles.css" rel="stylesheet">
 <link href="<?php echo base_url(); ?>css/toggles-light.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/toggles.js"></script>
<script type="text/javascript" >
$(document).ready(function() {
	//$('#tog1').toggles({drag:false,text:{on:'Yes',off:'No'}});
	//$('#is_default').val(0);
		
});

function updateStatus(id)
{
		var putbox = "#is_default";
		var newtog = '#tog'+id;
		var curval = $(putbox).val();
		
		if(curval==0)
		{
			$(putbox).val(1);
			myToggle.toggle();
		}else{
			$(putbox).val(0);
			myToggle.toggle();
		}
		var myToggle = $(newtog).data('toggles');
}

function getStatus(id,val)
{
		var opt = '';
	
		if(val==1){
			opt = true;
			$('#tog1').toggles({on:true,drag:false,text:{on:'Yes',off:'No'}});
			$('#is_default').val(val);
		}else
		{
			opt = false;
			$('#tog1').toggles({drag:false,text:{on:'Yes',off:'No'}});
			$('#is_default').val(val);
		}
}
</script>
<style>
.form-control {
		margin:0px 5px;
}

.form-grp li {
		display: block;
		float: left;
		margin-right: 30px;
}

#marker_label {
		float:left;
}

#icon_label {
		clear: both;
		float: left;
		position:relative;
		left:68px;
}

.form-grp img{
		vertical-align:middle;
}

#preview {
		float: right;
		margin: 50px 150px;
}

select {
		display:inline;
}

.form-main-grp {
		margin: 0;
		padding: 5px;
		width: 100%;
}
.form-grp {
		float: none;
}
.error {
		clear:none;
		text-indent: 11%;
}

.form-grp label {
    display: inline-block;
    width: 11%;
}
</style>
<div class="content clearfix">
		<div class="page-title"><h4><?php if( true == isset( $subscription_plan ) && true == is_array( $subscription_plan ) ) { ?> Edit <?php } else {?>Add <?php } ?> Subscription Plan</h4></div>
		<div class="clearfix">
				<span class="asterisk-msg">All fields marked with * are mandatory.</span>
		<?php if( true == isset( $subscription_plan[0]->id ) ) {
								 echo  form_open_multipart( base_url() . 'admin/plan/action/edit/'.$subscription_plan[0]->id);
						} else { 
								echo  form_open_multipart('admin/plan/action/add');
		} ?>
 		<div class="form-grp">
					<div id="preview">
					<h4 align="center">Plan Image</h4>
					<div style="background:#fff;padding: 3px;">
								<img width="120px" height="120px" src="<?php if( true == isset( $subscription_plan[0]->thumbnail_image ) && false == empty( $subscription_plan[0]->thumbnail_image ) ) { echo base_url(); ?>uploads/<?php echo $subscription_plan[0]->thumbnail_image; } else { echo base_url(); ?>images/module-default-ico.png <?php } ?>" id="thumb" />
	  				</div>
	   			</div>
	   <div class="form-grp">
				 <label>Name<span style="color:red;"> *</span></label>
					<input class="form-control" name="name" id="name" placeholder="Module name" value="<?php if( true == isset( $subscription_plan[0]->name ) ) { echo $subscription_plan[0]->name; } else { echo set_value('name'); } ?>"  >
			   <span style="color:red;"><?php echo form_error('name'); ?></span>
	   </div>
	   <div class="form-grp">
	   				<label>Description</label>
						<textarea class="form-control" name="description" placeholder = "Module description" id="description"><?php if( true == isset( $subscription_plan[0]->description ) ) { echo $subscription_plan[0]->description; } else { echo set_value('description'); } ?></textarea>
						<span style="color:red;"><?php echo form_error('description'); ?></span>
	   </div>
	   <div class="form-grp">
					<label>Subscription Period<span style="color:red;"> *</span></label>
					<input style="width:12%;" class="form-control" name="subscription_length" id="subscription_length" placeholder="Subscription period" value="<?php if( true == isset( $subscription_plan[0]->subscription_length ) ) { echo $subscription_plan[0]->subscription_length; } else { echo set_value('subscription_length'); } ?>"  >
					<select class="chosen-select" id="subscription_period" name="subscription_period">
										<option value="days" <?php  if( true == isset( $subscription_plan[0]->subscription_period ) ) {  if( 'days' == $subscription_plan[0]->subscription_period ) { ?> selected="selected" <?php }   echo set_select('subscription_period', $subscription_plan[0]->subscription_period ); } ?>>Days</option>
										<option value="weeks" <?php  if( true == isset( $subscription_plan[0]->subscription_period ) ) {  if( 'weeks' == $subscription_plan[0]->subscription_period ) { ?> selected="selected" <?php }   echo set_select('subscription_period', $subscription_plan[0]->subscription_period ); } ?>>Weeks</option>
										<option value="months" <?php  if( true == isset( $subscription_plan[0]->subscription_period ) ) {  if( 'months' == $subscription_plan[0]->subscription_period ) { ?> selected="selected" <?php }   echo set_select('subscription_period', $subscription_plan[0]->subscription_period ); } ?>>Months</option>
										<option value="years" <?php  if( true == isset( $subscription_plan[0]->subscription_period ) ) {  if( 'years' == $subscription_plan[0]->subscription_period ) { ?> selected="selected" <?php }   echo set_select('subscription_period', $subscription_plan[0]->subscription_period ); } ?>>Years</option>
							</select>
					<span style="color:red;"><?php echo form_error('subscription_length'); ?></span>
		</div>
		<div class="form-grp">
	   				<label>Price</label>
						<input class="form-control" name="price" placeholder = "Price" id="price" value="<?php if( true == isset( $subscription_plan[0]->price ) ) { echo $subscription_plan[0]->price; } else { echo set_value('price'); } ?>" >
						<span style="color:red;"><?php echo form_error('price'); ?></span>
	   </div>
	   <div class="form-grp">
	   			<label>Is published</label>
				<select class="chosen-select" id="is_published" name="is_published">
								<option value="0" <?php  if( true == isset( $subscription_plan[0]->is_published ) ) { if( '0' == $subscription_plan[0]->is_published ) { ?> selected="selected" <?php } echo set_select('is_published', $subscription_plan[0]->is_published ); } ?> >No</option>
								<option value="1" <?php  if( true == isset( $subscription_plan[0]->is_published ) ) {  if( '1' == $subscription_plan[0]->is_published ) { ?> selected="selected" <?php } echo set_select('is_published', $subscription_plan[0]->is_published ); } ?> >Yes</option>
			   </select>
	   </div>
	   <div class="form-grp">
				<label>Is Lifetime Plan</label>
							<select class="chosen-select" id="is_lifetime_plan" name="is_lifetime_plan">
										<option value="0" <?php  if( true == isset( $subscription_plan[0]->is_lifetime_plan ) ) {  if( '0' == $subscription_plan[0]->is_lifetime_plan ) { ?> selected="selected" <?php }  echo set_select('is_lifetime_plan', $subscription_plan[0]->is_lifetime_plan ); } ?> >No</option>
										<option value="1" <?php  if( true == isset( $subscription_plan[0]->is_lifetime_plan ) ) { if( '1' == $subscription_plan[0]->is_lifetime_plan ) { ?> selected="selected" <?php }  echo set_select('is_lifetime_plan', $subscription_plan[0]->is_lifetime_plan ); } ?> >Yes</option>
							</select>
				<span style="color:red;"><?php echo form_error('is_admin'); ?></span>
		 </div> 
		<div class="form-grp">
	   		<label>Plan Image</label>
			   <input maxlength="20" type="file"  id="thumbnail_image" name="thumbnail_image" onchange="readURL(this);">
			   <?php if( true == isset( $subscription_plan[0]->image ) ) { ?>
			   <input type="hidden" value="<?php echo $subscription_plan[0]->image; ?>" name="existing_plan_img">
			   <?php } ?>
		</div>
		<div class="form-grp">
		 <div class="btn-grp">
					<button type="submit"   class="signup">Save </button>
				<button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/plan/action/view'">Cancel</button>
		</div>
		</div>
		<?php  echo form_close(); ?>	
		</div>
</div>
  <script type="text/javascript">
$(document).keydown(function(e) {
	var nodeName = e.target.nodeName.toLowerCase();

	if (e.which === 8) {
		if ((nodeName === 'input' && e.target.type === 'text') ||
			nodeName === 'textarea') {
			// do nothing
		} else {
			e.preventDefault();
		}
	}
});
function readURL(input) {
 	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#thumb').attr('src', e.target.result);
		}
	reader.readAsDataURL(input.files[0]);
	}
 }
</script>
<?php $this->load->view('common/footer');?>
