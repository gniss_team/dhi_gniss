<?php $this->load->view('common/admin-header');?>
<div class="comp-heading"></div>
       <div class="comp-sub-txt"></div>	
		<div class="login-txt">Forgot Password</div>
		<?php $attributes = array('class' => 'form-signin'); echo form_open('admin/action/forgotPass', $attributes); ?>
		 <div class="content-login-wrapper">
	<?php	if( true == isset($msg) && false == empty( $msg) ) { ?>
          	<div class="alert alert-success" ><?php echo $msg; ?></div>
      <?php } ?>
      <?php if( true == isset($errmsg) && false == empty( $errmsg ) ) { ?>
      		<div class="alert alert-error" style="color:red;" ><?php echo $errmsg; ?></div>
 	  <?php } ?>
 	       	<div class="content-inner-wrapper clearfix">
                		<div><label>Username / Email Address</label>
                		<input class="user-ico-login" name="username" type="text" placeholder="" <?php echo set_value('username'); ?>" />
                    <?php echo form_error('username'); ?>
              		  </div>
    	       		 <div style="float:right;clear:both;margin:10px;"><button type="submit" name="add" value="add"  class="signup">Submit</button>
           			<button type="reset" name="reset" value="reset"  class="signup" onclick="window.location='<?php echo base_url(); ?>admin/action/login'">Cancel</button></div>
        		</div>
        		</div>
   </div>
  <?php  echo form_close(); ?>
<style>
.error {
		width:auto;
		margin: 5px 0 5px 4px;
		text-indent: 0%; 
}	      
.alert-error {
		margin-left: 5px;
}
.forgot_div {
		margin-left: 5px;
}

form label{
	margin-left: 5px;
}          	
.content-login-wrapper{
	height:240px;
}       
</style>
<?php $this->load->view('common/admin-footer');?>