<?php $this->load->view('common/admin-header');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<?php $this->load->view('common/admin-sidebar');?>
<style>
.signup {
	margin: 0;
}
.error {
	left:160px;
}
.alert-success {
 	padding:5px 0;
 	margin:5px 0;
 }
.breadcrums li a {
    background: none;
}
.form-grp label {
	 width: 40%;
}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}

.dataTables_length label {
	width: auto;
	display: inline-flex;
}
.dataTables_length select {
	margin: -6px 12px;
	padding: 5px 10px 5px 0px;
	/*-webkit-border-radius:4px;*/
   -moz-border-radius:4px;
   /* border-radius:4px;
  -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;*/
   background: url("../../../images/select-arrow.png") no-repeat scroll 98% center #fff;
   color:#555;
   border:1px solid #e3e3e3;
   outline:none;
   display style="margin:0 18px;": inline-block;
   -webkit-appearance:none;
   -moz-appearance:none;
   appearance:none;
   cursor:pointer;
}
.dataTables_paginate{
    float: right;
    font-size: 13px;
    padding-top: 0px;
    padding-right: 12px;
    padding-bottom: 6px;
    padding-left: 12px;
    text-align: right;
}
.dataTables_wrapper .dataTables_filter{
	margin-top: 4px;
}
#myTable_info{
	font-size:13px;
}
table.dataTable {
	width:98% !important;
	margin-left:0px !important;	
}
.img_icon {
	margin-right:10px;
	cursor:pointer;
}	
#img_livetrack {
	cursor:pointer;
}
table.tablesorter thead tr .noheader {
    border-right:1px solid;
}
</style>
<div class="content clearfix">
	<div class="page-title"><h4>Manage Products</h4></div>
	<button class="signup" style="margin:0 10px 20px 25px;" type="button" onclick="window.location='<?php echo base_url(); ?>admin/devices/action/add'">Add Product</button>
	<div class="content-wrap clearfix">
        <?php if($msg!="") { ?>
			<div class="alert alert-success">
			    <?php echo $msg; ?>
			</div>
		<?php } ?>
		<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	    	<thead>
            	<tr>
                	<th style="display:none">Id</th>
                    <th>Name</th>
                    <th>Created On</th>
                    <th class="noheader">Action</th>
                </tr>
            </thead>
            <tbody>
				<?php foreach ( $device_types as $types ){?>
                <tr class="active">
                	<td style="display:none"><?= $types->id; ?></td>
                    <td><?php echo $types->name; ?></td>
                    <td><?php echo date("D, d M Y ",strtotime($types->created_on)); ?></td>
                    <td>
                    <img class="img_icon" onclick="window.location='<?php echo base_url(); ?>admin/devices/action/edit/<?php echo $types->id; ?>'" src="<?php echo base_url(); ?>images/edit.png"  height="15px" alt="Edit" title="Edit">
                  <img class="img_icon" onclick="deleteRecord( <?php echo $types->id;?>, 'admin/devices/action/delete/' );"  src="<?php echo base_url(); ?>images/delete.png"  height="15px" alt="Delete" title="Delete">
 					</td>
 				</tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->load->view('common/admin-footer');?>
