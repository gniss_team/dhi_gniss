<?php $this->load->view('common/admin-header');?>

<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div class="content-body-wrapper-dashboard">
<?php $this->load->view('common/admin-sidebar');?>
<div style="min-height:800px;">
<div class="content clearfix">
	<div class="page-title"><h3>Manage Mobile Tracker</h3></div>

	<div class="content-wrap clearfix">
	<?php if(isset($msg) && $msg != '') { ?>
			<div class="alert alert-success" >
				 <?php echo $msg; ?>
			</div>
	<?php } ?>
	<?php if(isset($errmsg) && $errmsg != '') { ?>
		<div class="alert alert-error" >
		 <?php echo $errmsg; ?>
	</div>
	<?php } ?>
	<button class="signup" type="button" onclick="window.location='<?php echo base_url(); ?>admin/tracker/action/add'">Add Tracker</button>
	<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	<thead>
    	<tr>
        	<th style="display:none">Id</th>
            <th>Name</th>
            <th>Asset Number</th>
            <th>IMEI Number</th>
            <th>Phone Number</th>
             <th>Tracker Mode</th>
              <th>Status</th>
			<th class="noheader">Action</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach( $assets as $asset ) { ?>
    	<tr class="active">
        	<td style="display:none"><?= $asset->id; ?></td>
            <td><?= $asset->name; ?></td>
            <td><?= $asset->asset_number; ?></td>
             <td><?= $asset->imei_number; ?></td>
             <td><?= $asset->phone_number; ?></td>
  				<td><?= $asset->mode; ?></td>
  				<td><?= $asset->asset_status; ?></td>
			<td><button class="signup" type="button" onclick="window.location='<?php echo base_url(); ?>admin/tracker/action/edit/<?=  $asset->id; ?>'">Edit</button>
 			<button class="signup" type="button" onclick="deletedata(<?php echo $asset->id;?>,'admin/tracker/action/delete/');">Delete</button>
 		</tr>
	<?php } ?>
    </tbody>
    </table>
</div>
</div>
</div>
</div>
<style>
.error {
	left:160px;
}
.breadcrums li a {
    background: none;
}
.form-grp label {
	 width: 40%;
}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}
</style>
<?php $this->load->view('common/footer');?>