<?php $this->load->view('common/header');?>
<?php $this->load->view('common/top-menu');?>
<div class="main-container clearfix">
	<div class="content">
    	<!-- breadcrums-->
        <div class="breadcrums">
        	<ul>
            	<li><a class="big" href="<?php echo base_url(); ?>welcome">Dashboard</a></li>
            	<li><a href="<?php echo base_url(); ?>ticket/action/view">Support</a></li>
                <li>Add Ticket</li>
            </ul>    
        </div>
        <!-- breadcrums end-->
        <div class="pushmenu-push">
            <div class="main-content-body clearfix">
				<?php $this->load->view('common/sidebar');?>
				<div class="main-content">
				<h3 class="page-title">Add Ticket</h3>
              		<div class="user-forms">
            			<div class="forms-bg">
                      	<?php echo form_open(base_url().'ticket/action/new'); ?>
			                <div class="form-grp">
                		        <label>Category  <span style="color:red;">*</span></label>
                                <select class="form-control styled" name="subject" id="subject">
                                <option value="">Select category</option>
									<?php foreach($subjectlist as $type){ ?>
				 					<option value="<?= $type->title; ?>"<?php echo set_select('subject', $type->title ); ?>  ><?= $type->title; ?></option>
									<?php } ?>
                                </select>
                               <span style="color:red;"><?php echo form_error('subject'); ?></span>
                            </div>
                           <div class="form-grp" >
                                <label>Message <span style="color:red;">*</span></label>
                                <textarea id="message"  name="message"><?php echo set_value('message'); ?></textarea>
                            	
                                <span style="color:red;"><?php echo form_error('message'); ?></span>
                            </div>
                            <div class="btn-grp">
                        	    <button type="submit" name="add"  class="btn btn-primary">Send</button>
                            	<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>ticket/action/view'">Cancel</button>
							</div>
                    	   	<?php  echo form_close(); ?>
                   		</div>
	                </div>
	            </div>
			</div>
		</div>
    </div>
</div>
<?php $this->load->view('common/footer');?>