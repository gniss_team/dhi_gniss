<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<?php $this->load->view('common/admin-sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h4>Manage Permissions</h4></div>
			<div class="content-wrap clearfix">
			 <?php if(@$this->session->userdata('msg')!="") { ?>
			<div class="alert alert-success">
			    <?php echo $this->session->userdata('msg'); 
			    $this->session->unset_userdata('msg');?>
			</div>
		<?php } ?>
               <?php echo form_open( base_url() . 'admin/user/action/assign/' . $user_id ); ?>
						<div style="margin-bottom: 15px; width: 60%;"><h2 style="font-size:16px;font-weight:normal;margin:0 0 16px0;pdding:5px 12px;text-transform:capitalize;">Modules</h2>
	                    	<div class="permission-checkbox">    
								<?php 	foreach( $user_modules as $intKey => $strModule ) {?>
								<label>
												<input class="checkbx" type="checkbox"  name="permission[]"  id="module_<?php echo $intKey;?>" value="<?php echo $strModule->id;?>" <?php echo ($strModule->user_id != '' ) ?  "checked='checked'" : '';?> >
												<label class="chkbox" for="module_<?php echo $intKey;?>"><span></span><?php echo $strModule->name; ?></label>
								</label>
   							<?php }  ?>
   							</div>      
	        			</div>
	        			<div class="chkbox-btn-grp">
                    			<button class="signup" type="submit" name="add"  class="btn btn-primary">Save </button>
                    			<button type="reset" class="btn btn-default signup" onclick="window.location='<?php echo base_url(); ?>admin/user/action/view'">Cancel</button>
                  </div>
                <?php  echo form_close(); ?>
			</div>
</div>
<style>
.error {
	left:160px;
}
</style>
<?php $this->load->view('common/footer');?>