<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<div class="content-body-wrapper-dashboard">
<?php $this->load->view('common/admin-sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Add Asset Type</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>admin/action/viewdashboard">Dashboard</a></li>
			<li><a class="big" href="<?php echo base_url(); ?>assettype/action/manageassettype">Manage Asset Type</a></li>
			<li>Add Asset Type</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
                    <?php echo form_open(base_url().'assettype/action/addassettype'); ?>
					<div class="form-grp">
                    	<label>Asset type<span style="color:red;"> *</span></label>
                        <input class="form-control" placeholder="Asset type" name="typename" id="typename" value="<?php echo set_value('typename'); ?>"  >
                        <?php echo form_error('typename'); ?>
                    </div>
					<div class="form-grp">
                    	<label>Description<span style="color:red;"> *</span></label>
                        	<textarea class="form-control" placeholder="Description" id="typedescription" name="typedescription"style="height: 68px;width: 204px;"><?php echo set_value('typedescription'); ?></textarea>
                            <?php echo form_error('typedescription'); ?>
                    </div>
     				<div class="btn-grp">
                    	<button type="submit" name="addassettype"  class="btn btn-primary">Save </button>
                        <button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>assettype/action/manageassettype'">Cancel</button>
					</div>
	                <?php  echo form_close(); ?>
	</div>
</div>
</div>
<script type="text/javascript">

$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>
<style>
.error {
	left:160px;
}
</style>
<?php $this->load->view('common/footer');?>
