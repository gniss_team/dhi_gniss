<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<div class="content-body-wrapper-dashboard">
<?php $this->load->view('common/admin-sidebar');?>

	<div class="page-title"><h4>Edit Product</h4></div>

	<div class="content-wrap clearfix">
                    <?php echo form_open(base_url().'admin/devices/action/edit/'.$deviceid); ?>
					<div class="form-grp">
                    	<label>Device Name</label>
                        <?php echo $devicename;?>
                   
                    </div>
					  <?php $i=1; foreach ( $device_info as $column ) { ?>
                    	<div class="form-grp">
                    	<label>Parameter <?php echo $i;?><span style="color:red;"> *</span></label>
                        	<input class="form-control" type="text"  placeholder="Parameter <?php echo $i;?>" name="<?php echo $column->COLUMN_NAME; ?>" id="para1" value="<?php echo $column->COLUMN_NAME; ?>"  >
                        	 <a class="signup" href="<?php echo base_url()?>admin/devices/action/deletecolumn/<?php echo $column->COLUMN_NAME; ?>/<?php echo $devicename;?>">Remove</a>
                            <?php echo form_error($column->COLUMN_NAME); ?>
                    </div>
                    <?php $i++; } ?>
                    	
                    <div class="input_fields_wrap form-grp">

                    <label><span> </span></label>
  				   <button class="add_field_button signup">Add More Parameters</button> 	
    				
				</div>
				<?php echo form_error('para[]'); ?>
     				<div class="">
                    	
                        <button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/devices/action/viewdevices'">Cancel</button>
					<button type="submit" name="addassettype"  class="signup">Save </button>
					</div>
					<br>
	                <?php  echo form_close(); ?>

</div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   var y=<?php echo count($device_info)+1;?>;
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-grp"><label>Parameter '+y+'</label><input type="text" class="form-control" name="para[]"/><a href="#" class="remove_field signup">Remove</a></div>'); //add input box
            y++;
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
<style>
.error {
	left:160px;
}
</style>
<?php $this->load->view('common/footer');?>
