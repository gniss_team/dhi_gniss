<?php $this->load->view('common/admin-header');?>

<div class="content-body-wrapper-dashboard">
<?php $this->load->view('common/admin-sidebar');?>
<div style="min-height:800px;">
<div class="content clearfix">
	<div class="page-title"><h3>Add Table Column</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>admin/action/viewdashboard">Dashboard</a></li>
			<li><a class="big" href="<?php echo base_url(); ?>admin/energymeter/action/viewTableColumns">Manage Energy Meter</a></li>
			<li>Add Column</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
                    <?php echo form_open( base_url() . 'admin/energymeter/action/addTableColumn' ); ?>

					<div class="form-grp">
                    	<label>Field Name : </label>
                        <input class="form-control" placeholder="Field name" name="field_name" id="field_name" value="<?php echo set_value('field_name'); ?>"  >
                        <?php echo form_error('field_name'); ?>
                    </div>
					
     				<div class="btn-grp">
                    	<button type="submit" name="add"  class="btn btn-primary" >Save </button>
                        <button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>admin/energymeter/action/viewTableColumns'">Cancel</button>
					</div>
	                <?php  echo form_close(); ?>
	</div>
</div>
</div>
</div>
<script type="text/javascript">

$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>
<style>
select {
		margin : 0 5px;
		width : 20%;
}

.form-control {
		margin : 0 0px;
		width : 20%
}
.form-grp {
	padding : 0px opx;
}
.form-grp label {
		 text-align:center;
}

.error {
	margin:2% 20%;
}
.breadcrums li a {
    background: none;
}
.btn-grp {
	margin-left:20%;
}
.content-wrap {
	left:0%;
}
</style>
<?php $this->load->view('common/footer');?>
