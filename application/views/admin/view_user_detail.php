<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/admin-sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Manage Users</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>admin/action/viewdashboard">Dashboard</a></li>
			<li><a class="big" href="<?php echo base_url(); ?>admin/action/viewusers">Manage Users</a></li>
			<li>View User</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
				<div class="form-grp">
					<label>First name <span style="color:red;">*</span></label><span><?= $user_info->first_name; ?></span>
				</div>

				<div class="form-grp"> 
					<label>Last name <span style="color:red;">*</span></label><span><?= $user_info->last_name; ?></span>
					
				</div>

				<div class="form-grp">
					<label>Email address <span style="color:red;">*</span></label><span><?= $user_info->email_address; ?></span>
				</div>

				<div class="form-grp">
					<label>Mobile number <span style="color:red;">*</span></label><span><?= $user_info->mobile_number; ?></span>
					
				</div>
				<div class="form-grp">
					<label>Street address </label><span><?= $user_info->street_address; ?></span>
		            
				</div>
				<div class="form-grp">
					<label>Country <span style="color:red;">*</span></label>
					<span>
						<?php foreach( $countries as $country ) { 
						 if($country->id ==$user_info->country_id) {  echo $country->name; }} ?>
					</span>
					
				</div>

				<div class="form-grp">
					<label>State <span style="color:red;">*</span></label>
					<span>
						<?php foreach( $states as $state ) { 
						 if($state->id ==$user_info->state_id) {  echo $state->name; }} ?>
					</span>
				</div>

				<div class="form-grp">
					<label>City <span style="color:red;">*</span></label>
					<span>
						<?php foreach( $cities as $city ) { 
						 if($city->id ==$user_info->city_id) {  echo $city->name; }} ?>
					</span>
				</div>

				<div class="form-grp">
					<label>Zip Code </label><span><?= $user_info->zip_code; ?></span>
				</div>	
				
				<div class="form-grp">
            		<label>Profile Picture</label>
            		<img width="160px" height="120px" src="<?php echo base_url(); ?>uploads/<?php echo $user_info->profile_picture; ?>" id="thumb" />
       			</div>
       			<div class="btn-grp">
				
				<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>admin/action/viewusers'">Back</button>
			</div>
</div>
</div></div>
<?php $this->load->view('common/footer');?>