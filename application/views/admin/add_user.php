<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<div class="content-body-wrapper-dashboard">
<?php $this->load->view('common/admin-sidebar');?>

	<div class="page-title"><h4>Add User</h4></div>

	<div class="content-wrap clearfix">
                    <?php echo form_open(base_url().'admin/user/action/addUser'); ?>
					<div class="form-grp">
                    	<label>First Name<span style="color:red;"> *</span></label>
                        <input class="form-control" type="text" placeholder="First name" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>">
                        <?php echo form_error('first_name'); ?>
                    </div>
					
                	<div class="form-grp">
                	   <label>Last Name<span style="color:red;"> *</span></label>
                       <input class="form-control" type="text"  placeholder="Last Name" name="last_name" id="last_name" value="<?php echo set_value('last_name'); ?>">
                        <?php echo form_error('last_name'); ?>
                    </div>
                    <div class="form-grp">
                    	<label>Email Id<span style="color:red;"> *</span></label>
                        	<input class="form-control" type="text" placeholder="Email Address" name="email_address" id="email_address"  value="<?php echo set_value('email_address'); ?>">
                            <?php echo form_error('email_address'); ?>
                    </div>
                    <div class="form-grp">
                        <label>Mobile Number<span style="color:red;"> *</span></label>
                            <input class="form-control" type="text" placeholder="Mobile Number" name="mobile_number" id="mobile_number" value="<?php echo set_value('mobile_number'); ?>">
                            <?php echo form_error('mobile_number'); ?>
                    </div>
                    <div class="form-grp">
                        <label>Username<span style="color:red;"> *</span></label>
                            <input class="form-control" type="text" placeholder="Username" name="username" id="username" value="<?php echo set_value('username'); ?>">
                            <?php echo form_error('username'); ?>
                    </div>
                    
                    <div class="form-grp"> 
                        <label>Role<span class="asterisk-msg">*</span></label>
                        <select  class="form-control styled"   id="role_id" name="role" value="<?php echo set_value('role'); ?>">
                            <option value="">Select Role</option>
                            <?php  foreach( $roles as $role ) { ?>
                                <option value="<?php echo $role->id; ?>"><?php echo $role->name; ?> </option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('role'); ?>
                    </div>
                    
     				<div class="btn-grp">
                    	<button type="submit" name="addUserassettype"  class="signup">Save </button>
                        <button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/user/action/view'">Cancel</button>
					</div>
					
	                <?php  echo form_close(); ?>

</div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    
});
</script>

<style>
.error {
	left:160px;
}
</style>
<?php $this->load->view('common/footer');?>
