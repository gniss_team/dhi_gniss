<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<div class="content-body-wrapper-dashboard">
<?php $this->load->view('common/admin-sidebar');?>

	<div class="page-title"><h4>Update Customer</h4></div>

	<div class="content-wrap clearfix">
                    <?php echo form_open(base_url().'admin/user/action/editCustomer/'.$this->uri->segment(5).''); ?>
					<div class="form-grp">
                    	<label>First Name<span style="color:red;"> *</span></label>
                        <input class="form-control" type="text" placeholder="First name" name="first_name" id="first_name" value="<?php echo $user->first_name ?>" >
                        <?php echo form_error('first_name'); ?>
                    </div>
					
                	<div class="form-grp">
                	   <label>Last Name<span style="color:red;"> *</span></label>
                       <input class="form-control" type="text"  placeholder="Last Name" name="last_name" id="last_name" value="<?php echo $user->last_name ?>">
                        <?php echo form_error('last_name'); ?>
                    </div>
                    <div class="form-grp">
                    	<label>Email Id<span style="color:red;"> *</span></label>
                        	<input class="form-control" type="text" placeholder="Email Address" name="email_address" id="email_address"  value="<?php echo $user->email_address ?>">
                            <?php echo form_error('email_address'); ?>
                    </div>
                    <div class="form-grp">
                        <label>Mobile Number<span style="color:red;"> *</span></label>
                            <input class="form-control" type="text" placeholder="Mobile Number" name="mobile_number" id="mobile_number" value="<?php echo $user->mobile_number ?>" >
                            <?php echo form_error('mobile_number'); ?>
                    </div>
                    <div class="form-grp">
                        <label>Username<span style="color:red;"> *</span></label>
                            <input class="form-control" type="text" placeholder="Username" name="username" id="username"  value="<?php echo $user->username ?>">
                            <?php echo form_error('username'); ?>
                    </div>
                    
     				<div class="btn-grp">
                    	<button type="submit" name="addUserassettype"  class="signup">Update </button>
                        <button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/user/action/viewCustomers'">Cancel</button>
					</div>
					
	                <?php  echo form_close(); ?>

</div>
</div>

<style>
.error {
	left:160px;
}
</style>
<?php $this->load->view('common/footer');?>
