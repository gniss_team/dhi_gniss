<?php $this->load->view('common/admin-header');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<?php $this->load->view('common/admin-sidebar');?>
<style>
.signup {
	margin: 0;
}
.error {
	left:160px;
}
.alert-success {
 	padding:5px 0;
 	margin:5px 0;
 }
.breadcrums li a {
    background: none;
}
.form-grp label {
	 width: 40%;
}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}

.dataTables_length label {
	width: auto;
	display: inline-flex;
}
.dataTables_length select {
	margin: -6px 12px;
	padding: 5px 10px 5px 0px;
	/*-webkit-border-radius:4px;*/
   -moz-border-radius:4px;
   /* border-radius:4px;
  -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;*/
   background: url("../../../images/select-arrow.png") no-repeat scroll 98% center #fff;
   color:#555;
   border:1px solid #e3e3e3;
   outline:none;
   display style="margin:0 18px;": inline-block;
   -webkit-appearance:none;
   -moz-appearance:none;
   appearance:none;
   cursor:pointer;
}
.dataTables_paginate{
    float: right;
    font-size: 13px;
    padding-top: 0px;
    padding-right: 12px;
    padding-bottom: 6px;
    padding-left: 12px;
    text-align: right;
}
.dataTables_wrapper .dataTables_filter{
	margin-top: 4px;
}
#myTable_info{
	font-size:13px;
}
table.dataTable {
	width:98% !important;
	margin-left:0px !important;	
}
.img_icon {
	margin-right:10px;
	cursor:pointer;
}	
#img_livetrack {
	cursor:pointer;
}
table.tablesorter thead tr .noheader {
    border-right:1px solid;
}
</style>
<div class="content clearfix">
<div class="page-title"><h4>Manage Modules</h4></div>
<button class="signup" type="button" onclick="window.location='<?php echo base_url(); ?>admin/module/action/add'">Add Module</button>
<input class="form-control" type ="hidden" readonly="readonly"  name="module_val" id="module_val" value="<?php echo count( $modules ); ?>">
	<div class="content-wrap clearfix">
				<?php if(isset($msg) && $msg != '') { ?>
							<div class="alert alert-success" >
						 				<?php echo $msg; ?>
							</div>
				<?php } ?>
				<?php if(isset($errmsg) && $errmsg != '') { ?>
							<div class="alert alert-error" >
									 <?php echo $errmsg; ?>
							</div>
				<?php } ?>
					<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	                    <thead>
                              <tr><th style="display: none;">Id</th>
		                        <th>Module name</th>
		                        <th>Module Position</th>
		                        <th>Module Type</th>
		                          <th>Is published</th>
		                        <th>Order</th>
								<th class="noheader">Action</th>
		                    </tr>
                         </thead>
                         <tbody>
						<?php foreach( $modules as $module ) {?>	
							<tr class="active" id="sortable"> 
								<td style="display: none;"><?php echo $module->id; ?></td>
							    <td><?php echo $module->name; ?></td>
							    <td><?php echo $module->position; ?></td>
							    <td><?php echo $module->is_admin; ?></td>
							      <td><?php echo $module->is_published; ?></td>
							    <td><?php echo $module->order_num; ?></td>
								<td>
								<img class="img_icon" src="<?php echo base_url(); ?>images/edit.png" onclick="window.location='<?php echo base_url(); ?>admin/module/action/edit/<?php echo $module->id;?>'" height="15px" alt="Edit" title="Edit">
								<img class="img_icon" src="<?php echo base_url(); ?>images/delete.png" onclick="deleteRecord( <?php echo $module->id;?>, 'admin/module/action/delete/' );"  height="15px" alt="Delete" title="Delete">
							</tr>
						   <?php } ?>
                          </tbody>
                      </table>
	</div>
	</div>

  <script type="text/javascript">
//  $("#para").on("submit", function(event) {
//         event.preventDefault();

//         $.ajax({
//             url: base_url+"devices/action/edit/",
//             type: "post",
//             data: $(this).serialize(),
//             success: function(d) {
             
//             	var d = JSON.parse(d);
//             }
//         });
//  });

//  $("#myTable input[name='order_num']").blur(function() {
	 var base_url = '<?php echo base_url();?>';
// 	 alert($(this).attr('id'));
// 		alert($(this).val());
// 			 $.get( base_url+"admin/module/action/sort/", function( data ) {
// 					$('.ajax-loader').hide();
// 				 	$("#tempchart").html( data );
// 			});
			
// 	});
 
  </script>
<?php $this->load->view('common/admin-footer');?>