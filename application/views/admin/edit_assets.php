<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/admin-sidebar');?>
<div class="clearfix">
	<div class="page-title"><h4>Edit Mobile tracker</h4></div>
	<div class="content-wrap clearfix">
	<span class="asterisk-msg">All fields marked with * are mandatory.</span>
	<?php echo form_open('admin/tracker/action/edit/'. $assets->id ); ?>

		<div class="form-grp">
        	<label>Name <span class="red">*</span></label>
            <input class="form-control" placeholder="Name" name="name" id="name" maxlength="21" value="<?php echo $assets->name;  ?>" >
            <?php echo form_error('name'); ?>
        </div>
        <div class="form-grp">
	    	<label>Sim number</label>
			<input class="form-control" name="sim_number" id="sim_number" placeholder="Sim number" value="<?php echo $assets->sim_number; ?>"  >
            <?php echo form_error('sim_number'); ?>
        </div>
        <div class="form-grp">
        	<label>Asset number<span class="red"> *</span></label>
			<input class="form-control" name="asset_number" id="asset_number" placeholder="Asset number" maxlength="16" value="<?php echo $assets->asset_number; ?>"  >
            <?php echo form_error('asset_number'); ?>
        </div>
        <div class="form-grp">
        	<label>IMEI number<span class="red"> *</span></label>
			<input class="form-control" name="imei_number" id="imei_number" placeholder="IMEI number" value="<?php echo $assets->imei_number; ?>"  >
            <?php echo form_error('imei_number'); ?>
        </div>
        <div class="form-grp">
        	<label>Phone number</label>
			<input class="form-control" name="phone_number" id="phone_number" placeholder="phone number" value="<?php echo $assets->phone_number;  ?>"  >
            <?php echo form_error('phone_number'); ?>
        </div>
        <div class="form-grp">
        	<label>Model number<span style="color:red;"> *</span></label>
			<input class="form-control" name="model_number" id="model_number" placeholder="Model number" value="<?php echo $assets->model_number; ?>"  >
            <?php echo form_error('model_number'); ?>
        </div>
        <div class="form-grp">
        	<label>Asset Mode<span style="color:red;"> *</span></label>
            <select class="form-control-select styled" name="mode" id="mode">
          		<option value="">Select</option>
            	<option value="m" placeholder="Mode" <?php  if( 'm' == $assets->mode) { ?> selected="selected" <?php  } ?>>Master</option>
            	<option value="s" placeholder="Mode" <?php  if( 's' == $assets->mode) { ?> selected="selected" <?php  } ?>>Slave</option>
            </select>
 			<?php echo form_error('mode'); ?>
        </div>
		<div class="form-grp">
        	<label>Status </label>
            <input type="radio" name="status" value="1"   <?php  if( '1' == $assets->status) { ?> checked="checked" <?php  } ?>><span>Enabled</span>
            <input type="radio" name="status" value="0"  <?php  if( '0' == $assets->status) { ?> checked="checked"  <?php  } ?> ><span>Disabled</span>
		</div>	
      	<div class="btn-grp">
        	<button type="submit"  class="signup">Save </button>
            <button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/tracker/action/manage'">Cancel</button>
        </div>
	    <?php  echo form_close(); ?>
	</div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });
</script>
<script type="text/javascript">

$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>
<style>
.error {
		margin:0px 5px 0 227px;
}
</style>
<?php $this->load->view('common/footer');?>  