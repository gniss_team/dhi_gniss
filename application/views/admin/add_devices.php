<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<div class="content-body-wrapper-dashboard">
<?php $this->load->view('common/admin-sidebar');?>

	<div class="page-title"><h4>Add Product</h4></div>

	<div class="content-wrap clearfix">
	
	<div style="width:100%;">
	<?php echo form_open(base_url().'admin/devices/action/add'); ?>
	<div style="width:50%;float:left;">
                    
					<div class="form-grp-product">
                    	<label>Device Name<span style="color:red;"> *</span></label>
                        <input class="form-control" type="text" placeholder="Device name" name="device_name" id="device_name" value="<?php echo set_value('device_name'); ?>"  >
                        <input type="hidden" name="cnt_start" id="cnt_start" value="4">
                        <?php echo form_error('device_name'); ?>
                    </div>
                         <div class="form-grp-product">
                    	<label style="float:left;">Protocol  </label>
                        	<select name='no_of_fields' style="float:left;" >
                        	<option value="">Select</option>
                        	<option value="modbus">Modbus</option>
                        	<option value="gps">GPS</option>
                        	<option value="gprmc">GPRMC</option>

                        	</select>
                       
                    </div>
                      <div class="form-grp-product">
                    	<label style="float:left;">Communication  </label>
                        	<select name='no_of_fields' style="float:left;" onchange="changeval(this.value);">
                        	<option value="">Select</option>
                        	<option value="wifi">WIFI</option>
                        	<option value="mobile">Mobile(Sim Base)</option>
                        	<option value="both">Both</option>
                        	

                        	</select>
                           
                    </div>
                    </div>
                    <div style="float:left;width:50%;">
                   <div class="form-grp">
                    	<label>Words 1</label>
                        	
                        	<input readonly='readonly' class="form-control"  name="para2[]" value="Gniss pid">
                    </div>
                      <div class="form-grp">
                    	<label>Words 2</label>
                        	
                        <input readonly='readonly' class="form-control"  name="para2[]" value="Serial number">
                    </div>
                     <div class="form-grp">
                    	<label>Words 3</label>
                        	
                        	 <input readonly='readonly' class="form-control"  name="para2[]" value="Ip Address">
                    </div>
                         <div class="form-grp">
                    	<label>Words 4</label>
                    	
                         <input readonly='readonly' class="form-control"  name="para2[]" value="version">	
                    </div>
                       <div class="form-grp wifi_f" style="display:none">
                    	<label>Words 5</label>
                    	
                         <input readonly='readonly' class="form-control"  name="para2[]" value="Mac Address">	
                    </div>
                     <div class="form-grp mobile_f" style="display:none">
                    	<label>Words<span id="imei"> 5<span id="imei"></label></span>
                        	 <input readonly='readonly' class="form-control"  name="para2[]" value="imei number">
                    </div>
                       <div class="form-grp mobile_f" style="display:none">
                    	<label>Words<span id="ph"> 6</span></label>
                        	 <input readonly='readonly' class="form-control"  name="para2[]" value="phone number">
                    </div>
                    
                 
                    </div>
               
                    <div class="form-grp">
                    <div style="width:100%;background:#f4f4f4;padding:5px 8px;width:80%;float:left;">
                    <div style="width:50%;float:left;">
                    	<div style="font-size:16px;">Add More Parameter
                    	<!--   <button class="add_field_button signup">+</button> -->
                    </div></div>
                    <div style="float:right;">
                    <label style="float:left;width:auto;">Number Of Fields </label>
                   
                        	<select class="styled" name='no_of_fields' style="float:left;" onchange="getfields(this.value);">
                        	<option value="">Select</option>
                        	<?php for($i=1;$i<=30;$i++){?>
                        	<option value="<?php echo $i;?>"><?php echo $i;?></option>
                        	<?php } ?>
                        	</select>
                               
                    </div>
                    </div>
                    </div>
                    <div class="input_fields_wrap2"></div>
                   
     				<div class="btn-grp">
                    	<button type="submit" name="addassettype"  class="signup">Save </button>
                        <button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>admin/devices/action/viewdevices'">Cancel</button>
					</div>
					
	                <?php  echo form_close(); ?>
</div>
</div>
</div>
<script type="text/javascript">
function changeval(val)
{
	if(val=='wifi')
	{
      $('.mobile_f').hide();
      $('.wifi_f').show();
      $('#cnt_start').val("5");
	}
	else if(val=='mobile')
	{
		$('.mobile_f').show();
	     $('.wifi_f').hide();
	     $('#cnt_start').val("6");
	 
	}
	else if(val=='both')
	{
		$('.mobile_f').show();
	     $('.wifi_f').show();
	     $('#imei').html(" 6");
	     $('#ph').html(" 7");
	     $('#cnt_start').val("7");
	 
	}
	
	else {
		$('.wifi_f').hide();
		 $('.mobile_f').hide();
		 $('#cnt_start').val("4");
	}
	

}
function getfields(val)
{
	var start = $('#cnt_start').val();
	$('.input_fields_wrap2').html("");
	 var wrapper         = $(".input_fields_wrap2"); //Fields wrapper
     var add_button      = $(".add_field_button"); //Add button ID
     var cnt ;
     for (var i = 1; i <= val; i++) {
    	 cnt = parseInt(i) + parseInt(start);
    	 $(wrapper).append('<div class="form-grp"><input type="text" placeholder="Word '+cnt+' " class="form-control" name="para[]"/><input class="unit" type="text" class="form-control" name="unit_type[]" placeholder="unit"/><input class="range" type="text" class="form-control" name="range[]" placeholder="Range"/><input class="sts" type="text" class="form-control" name="sts[]" placeholder="Status"/><a href="#" class="remove_field signup">Remove</a></div>'); //add input box
    	 cnt++; 
    	}
}
$(document).ready(function() {
    var max_fields      = 30; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap2"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   var y=3;
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-grp"><input type="text" placeholder="word '+y+' " class="form-control" name="para[]"/><input class="unit" type="text" class="form-control" name="unit_type[]" placeholder="unit"/><input class="range" type="text" class="form-control" name="range[]" placeholder="Range"/><input class="sts" type="text" class="form-control" name="sts[]" placeholder="Status"/><a href="#" class="remove_field signup">Remove</a></div>'); //add input box
            y++;
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
<style>
.error {
	left:160px;
}

.form-grp-product{
width: 100%;
float:left;

}

.form-grp-product label{
width: 15%;
display:inline-block;
}

.unit{width:5%;}

.range{width:10%;}

.sts{width:10%;}
</style>
<?php $this->load->view('common/footer');?>
