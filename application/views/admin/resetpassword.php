<?php $this->load->view('common/admin-header');?>
	<div class="comp-heading"></div>
       <div class="comp-sub-txt"></div>		
       <div class="login-txt">Reset Password</div>
	<?php
		$attributes = array('class' => 'form-signin');
		echo form_open('admin/action/resetPass/'.$ver_code, $attributes);?>
		<div class="content-login-wrapper">
		<?php	if( true == isset($msg) && false == empty( $msg) ) { ?>
          	<div class="alert alert-success" ><?php echo $msg; ?></div>
      <?php } ?>
      <?php if( true == isset($errmsg) && false == empty( $errmsg ) ) { ?>
      		<div class="alert alert-error" style="color:red;" ><?php echo $errmsg; ?></div>
 	  <?php } ?>
	<div class="content-inner-wrapper clearfix">
	   <div>
            <label class="label-reset"> New Password <span class="red">*</span></label>
            <input class="user-ico-login" type="password" placeholder="Password" id="password" name="password"  maxlength="25" />
            <?php echo form_error('password'); ?>
       </div>
       <div>
            <label class="label-reset"> Confirm Password <span class="red">*</span></label>
            <input class="user-ico-login" type="password" placeholder="Confirm password" id="passconf" name="passconf"  maxlength="25"  />
            <span class="red"><?php echo form_error('passconf'); ?></span> 
       </div>
       <div style="float:right;clear:both;"><button type="submit" name="add" value="add"  class="signup">Submit</button>
       		<button type="reset" name="reset" value="reset"  class="signup" onclick="window.location='<?php echo base_url(); ?>admin/action/login'">Cancel</button></div>
       </div>
    
</div>
 <?php  echo form_close(); ?>
<style>
.error {
		width:auto;
		margin: 5px 0 5px 4px;
		text-indent: 0%; 
}	
   
.content-login-wrapper{
	height:275px;
	margin-top:20px;
}         
form label {
	margin-left: 5px;     	
</style>
<?php $this->load->view('common/admin-footer');?>