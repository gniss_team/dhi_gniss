<?php $this->load->view('common/header');?>
<div id="container">
	<h1>Profile</h1>

	<div id="body">
		<span><a href="<?php echo base_url(); ?>welcome/logout">Log Out</a> / 
		<a href="<?php echo base_url(); ?>admin/action/viewdashboard">Dashboard</a> / 
		<a href="<?php echo base_url(); ?>admin/action/viewusers">View Users</a></span>
        <!-- main content body-->
        <div class="pushmenu-push">
         	<div class="main-content-body clearfix">
				<div class="main-content">
           		<?php if($error_upload_msg!="") { ?>
					<div class="alert alert-success"> 
						 <?php echo $error_upload_msg; ?>
 					</div>
					<?php } 
					
					?>
				<?php echo form_open_multipart('admin/action/editusers/'.$this->uri->segment(4).''); ?>
				<div class="user-forms">
            	<div class="forms-bg">
            	<div class="form-grp">
					<label>First name <span style="color:red;">*</span></label>
					<input class="form-control" maxlength="20" placeholder="First name" name="first_name" id="first_name" value="<?= $user_info[0]->first_name; ?>">
					   <span style="color:red;"><?php echo form_error('first_name'); ?></span>
				</div>

				<div class="form-grp"> 
					<label>Last name <span style="color:red;">*</span></label>
					<input class="form-control" maxlength="20" placeholder="Last name" id="last_name" value="<?= $user_info[0]->last_name; ?>" name="last_name">
					<span style="color:red;"><?php echo form_error('last_name'); ?></span>
				</div>

				<div class="form-grp">
					<label>Email address <span style="color:red;">*</span></label>
					<input class="form-control-readonly" maxlength="40" placeholder="Email address" id="email_address" name="email_address" readonly="readonly" value="<?= $user_info[0]->email_address; ?>">
					<span style="color:red;"><?php echo form_error('email_address'); ?></span>
				</div>

				<div class="form-grp">
					<label>Mobile number <span style="color:red;">*</span></label>
					<input class="form-control" maxlength="10" placeholder="Mobile number" id="mobile_number" name="mobile_number" value="<?= $user_info[0]->mobile_number; ?>">
					<span style="color:red;"><?php echo form_error('mobile_number'); ?></span>
				</div>
				<div class="form-grp">
					<label>Street address </label>
		            <textarea class="form-control" placeholder="Street address" id="street_address" name="street_address" maxlength="105"><?php  echo $user_info[0]->street_address ?></textarea>
		            <span style="color:red;"><?php echo form_error('street_address'); ?></span>
				</div>
				<div class="form-grp">
					<label>Country <span style="color:red;">*</span></label>
					<select class="form-control styled" id="country_id" name="country_id">
						<option value="" >Select country</option>
						<?php foreach( $countries as $country ) { ?>
						<option value="<?= $country->id; ?>" <?php if($country->id == $user_info[0]->country_id) { ?> selected="selected" <?php } ?> ><?= $country->name; ?> </option>
						<?php } ?>
					</select>
					<span style="color:red;"><?php echo form_error('country_id'); ?></span>
				</div>

				<div class="form-grp">
					<label>State <span style="color:red;">*</span></label>
					<select class="form-control styled" id="state_id" name="state_id" >
						<option value="" >Select state</option>
						<?php foreach( $states as $state ) { ?> 
						<option value="<?= $state->id; ?>" <?php if($state->id ==$user_info[0]->state_id) { ?> selected="selected" <?php } ?> ><?= $state->name; ?></option>
						<?php } ?>
					</select>
					<span style="color:red;"><?php echo form_error('state_id'); ?></span>
				</div>

				<div class="form-grp">
					<label>City <span style="color:red;">*</span></label>
					<select class="form-control styled" id="city_id" name="city_id">
						<option value="" >Select city</option>
						<?php foreach( $cities as $city ){ ?>
						<option value="<?= $city->id; ?>" <?php if( $city->id == $user_info[0]->city_id ) { ?> selected="selected" <?php } ?> ><?= $city->name; ?></option>
						<?php } ?>
					</select>
					<span style="color:red;"><?php echo form_error('city_id'); ?></span>
				</div>

				<div class="form-grp">
					<label>Zip Code </label>
					<input class="form-control" placeholder="Zip code" id="zip_code" name="zip_code" maxlength="50" value="<?= $user_info[0]->zip_code; ?>" >
					<span style="color:red;"><?php echo form_error('zip_code'); ?></span>
				</div>	
				
				<div id="preview" class="form-grp">
            	<label>Profile Picture</label>
            		<img width="160px" height="120px" src="<?php echo base_url(); ?>uploads/<?php echo $user_info[0]->profile_picture; ?>" id="thumb" />
       			</div>
				<div class="form-grp">
    			    <label></label>
                    <input maxlength="20" type="file"  id="profile_img" name="profile_img" onchange="readURL(this);">
                	
       			</div>
       			<div class="btn-grp">
				<button type="submit" class="btn btn-primary">Update</button>
				<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>admin/action/viewusers'">Cancel</button>
				</div>
				<?php  echo form_close(); ?>
			</div>
	            	</div>
	        	</div>
			</div>
		</div>
	</div>
</div>
 <script type="text/javascript">
var strUrl = "<?php echo base_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/locations.js"></script>   
<script type="text/javascript">
 function readURL(input) {
 	if (input.files && input.files[0]) {
    	var reader = new FileReader();
		reader.onload = function (e) {
        	$('#thumb').attr('src', e.target.result);
        }
    reader.readAsDataURL(input.files[0]);
    }
 }
 </script>   
 <script type="text/javascript">
$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>
 <?php $this->load->view('common/footer');?>