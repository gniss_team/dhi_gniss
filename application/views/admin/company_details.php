<?php $this->load->view('common/admin-header');?>
<?php $this->load->view('common/style');?>
<div class="content-body-wrapper-dashboard clearfix">

<?php $this->load->view('common/admin-sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Company Details</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>admin/action/viewdashboard">Dashboard</a></li>
			<li>Manage Company</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
		<?php echo form_open('company/action/add'); ?>
       
			<div class="form-grp">
				<label>Company Name </label>
				<label><?php echo $companies_details->company_name; ?></label>
				
			</div>
				<div class="form-grp">
				<label>Status </label>
				<label><?php  if($companies_details->is_approved==0) { ?> Not Approved <?php }else { ?> Approved <?php }?></label>
				
			</div>
					<div class="form-grp">
				<label>Owner Name </label>
				<label><?php echo $companies_details->first_name." ".$companies_details->last_name; ?></label>
				
			</div>
			<div class="form-grp">
	            <label>Email address</label>
	           <label><?php echo $companies_details->email_address; ?></label>
			</div>
			<div class="form-grp">
	            <label>Mobile number </label>
	            <label><?php echo $companies_details->mobile_number; ?></label>
			</div>
			<div class="form-grp">
				<label>Username </label>
			<label><?php echo $companies_details->username; ?></label>
			</div>
		
			<div class="form-grp">
				<label>Company address </label>
				<label><?php echo $companies_details->company_address; ?></label>
			</div>
				<div class="form-grp">
		            <label>Country </label>
	    	        <label><?php echo $companies_details->country_name; ?></label>
			</div>
			<div class="form-grp">
	            <label>State</label>
	            <label><?php echo $companies_details->state_name; ?></label>
			</div>
			<div class="form-grp">
	            <label>City </label>
	           <label><?php echo $companies_details->city_name; ?></label>
			</div>
			<div class="form-grp">
				<label>Zip code </label>
			       <label><?php echo $companies_details->zip_code; ?></label>
			</div>
		
			<div class="btn-grp">
				
				<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>admin/action/viewcompanies'">Back</button>
			</div>

		<?php  echo form_close(); ?>
	</div>
</div></div>
<script type="text/javascript">
var strUrl = "<?php echo base_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/locations.js"></script>
<?php $this->load->view('common/footer');?>