<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<style>
.signup {
	margin: 0;
}
.error {
	left:160px;
}
.alert-success {
 	padding:5px 0;
 	margin:5px 0;
 }
.breadcrums li a {
    background: none;
}
.form-grp label {
	 width: 40%;
}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}
.dataTables_length label {
	width: auto;
	display: inline-flex;
}
.dataTables_length select {
	margin: -6px 12px;
	padding: 5px 10px 5px 0px;
	/*-webkit-border-radius:4px;*/
   -moz-border-radius:4px;
   /* border-radius:4px;
  -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;*/
   background: url("../../../images/select-arrow.png") no-repeat scroll 98% center #fff;
   color:#555;
   border:1px solid #e3e3e3;
   outline:none;
   display style="margin:0 18px;": inline-block;
   -webkit-appearance:none;
   -moz-appearance:none;
   appearance:none;
   cursor:pointer;
}
.dataTables_paginate{
    float: right;
    font-size: 13px;
    padding-top: 0px;
    padding-right: 12px;
    padding-bottom: 6px;
    padding-left: 12px;
    text-align: right;
}
.dataTables_wrapper .dataTables_filter{
	margin-top: 4px;
}
#myTable_info{
	font-size:13px;
}
table.dataTable {
	width:98% !important;
	margin-left:0px !important;	
}
.img_icon {
	margin-right:10px;
	cursor:pointer;
}	
#img_livetrack {
	cursor:pointer;
}
table.tablesorter thead tr .noheader {
    border-right:1px solid;
}
</style>
<div class="content clearfix">
	<div class="page-title"><h4><?php echo $device_name; ?> Devices</h4></div>
	<button class="signup" style="margin:0 10px 20px 0;" type="button" onclick="window.location='<?php echo base_url(); ?>devices/action/add'">Add Device</button>
	
	<div class="content-wrap clearfix">
        <?php if($msg!="") { ?>
			<div class="alert alert-success">
			    <?php echo $msg; ?>
			</div>
		<?php } ?>
		<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	    	<thead>
            	<tr>
                	<th style="display:none">Id</th>
                    <th>Name</th>
                     <th>Device Type</th>
                    <th>Device Number</th>
                     <th>Sim Number</th>
                    <th>Address</th>
                    <th class="noheader">Action</th>
                </tr>
            </thead>
            <tbody>
				<?php foreach ( $devices->result() as $intIndex => $device ) {?>
                <tr class="active">
                	<td style="display:none"><?php $device->id; ?></td>
                    <td><?php echo $device->name; ?></td>
                    <td><?php echo str_replace( '_', ' ', $device->device_type ); ?></td>
                     <td><?php echo $device->device_number; ?></td>
                     <td><?php echo $device->sim_number; ?></td>
                   <td><?php echo $device->address; ?></td>
                    <td>
							<img class="img_icon" src="<?php echo base_url(); ?>images/delete.png"  onclick="deletedata(<?php echo $device->id;?>,'devices/action/delete/',<?php echo $device->device_type_id;?>);" height="15px" alt="Delete" title="Delete">
							<?php if( 1 == $device->unit_type ) { ?>	<img class="img_icon" src="<?php echo base_url(); ?>images/edit.png"  onclick="window.location='<?php echo base_url(); ?>devices/action/edit/<?php echo  $device->device_type_id; ?>/<?php echo  $device->id; ?>/<?php echo $device->device_number; ?>'" height="15px" alt="Edit" title="Edit"><?php } ?>
                    <button class="signup" type="button" onclick="window.location='<?php echo base_url(); ?>welcome/action/displayGraph/<?php echo $device->device_type_id; ?>/<?php echo $device->device_number; ?>'">view</button>
 							</td>
 				</tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->load->view('common/footer');?>
