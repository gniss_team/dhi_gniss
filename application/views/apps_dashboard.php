<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/css/jquery.gridster.css">

  <!-- Load local lib and tests. -->
  <script src="<?php echo base_url()?>/js/jquery.gridster.js"></script>

  <script type="text/javascript">
  var gridster;
    $(function(){ //DOM Ready

      $(".gridster ul").gridster({
          widget_margins: [10, 10],
          widget_base_dimensions: [140, 140]
      });

     
      $('.js-seralize').on('click', function() {
    	  var gridster = $(".gridster ul").gridster().data('gridster');
          gridData = gridster.serialize();
        //  alert(gridData.toSource());
      
          $('#log').val(JSON.stringify(gridData));
      })

  });

  </script>

		
		<div style="background:#fff; display:block;float:left; margin-top: 14px; padding:10px 20px 0;border-radius:2px;width:86%;">
		<div class="title-box">
	<div class="page-title"><h4 style="text-transform:capitalize"></h4></div>
		

		
		<div class="track-btn-container">
      		<?php if( true == isset( $devices ) ) { ?>
				<select class="form-control-select styled" name="device_id" id="device_id">
						<option value="">Select Device</option>
						<?php foreach( $devices as $device ) { ?>
									<option value="<?php echo $device->device_number; ?>" <?php if( $device->device_number == $device_id ) { ?> selected="selected" <?php } ?>><?php echo $device->name; ?></option>
				      <?php } ?>
       		</select>
        <?php }  ?>
         
        <!-- <a href="#" id="dialog-link" class="ui-state-default ui-corner-all">Set Parameters</a> -->
       <!--    <button class="signup">view historical data</button> --> 
		</div>
		</div>

		<!-- <div class="controls">
        <button class="js-seralize">Serialize</button>
    </div>

      <textarea id="log"></textarea> -->
<section class="container"  ng-app="myApp" ng-controller="myCtrl">
	<h1><?php //echo $devices[0]->name?> Street Light- <?php echo $this->uri->segment(4);?></h1>

	<p></p>
	  	<div class="wrapper gridster">
	    	<ul style="height: 100%; position: relative; width: 100%;">
	        	
	        	
	        	<li data-row="1" data-col="1" data-sizex="2" data-sizey="1" class="bg-blueDark fg-white-link">
	        	  <a href="javascript:void(0);" onclick="getgraph('lux','<?php echo $this->uri->segment(4)?>');">
	        	<span class="facebook" ><p style="font-size: 40px;">{{lux}}</p></span>
	        	 </a>
	        	</li>

						<li data-row="3" data-col="5" data-sizex="1" data-sizey="1" class="bg-orangeDark fg-white-link">
						Switch Mode
					
					
					<input type="button" class="signup" value="On" onclick="switchlight('ON','<?php echo $this->uri->segment(4)?>');"> 
					<input type="button" class="signup" value="Off" onclick="switchlight('OFF','<?php echo $this->uri->segment(4)?>');"> 
					 Settings <br>
					 <input type="button" class="signup" value="Auto" onclick="switchlight('AUTO','<?php echo $this->uri->segment(4)?>');"> 
					 <input type="button" class="signup" value="Manual" onclick="switchlight('MANUAL','<?php echo $this->uri->segment(4)?>');">
					
					
					</span>
					</li>
					
	        	
	        	<li data-row="2" data-col="4" data-sizex="6" data-sizey="3" class="bg-pink fg-white-link">
	        	<div class="ajax-loader1">
	        	<img  src="<?php echo base_url()?>/images/ajax-loader1.gif" >
	        	</div>
	        	<span class="contact">	
	        	
	        	<div id="luxchart" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
	        	</span>
	        	<p style="font-size: 20px;" id="graph_title"></p>
	        	</li>

		        <li data-row="1" data-col="2" data-sizex="1" data-sizey="1" class="bg-greenDark fg-white-link">
		         <a href="javascript:void(0);" onclick="getgraph('humidity','<?php echo  $this->uri->segment(4)?>');">
		        <span class="articles"><p style="font-size: 40px;">{{humitity}}</p></span>
		        </a>
		        </li>
		        <li data-row="1" data-col="3" data-sizex="1" data-sizey="1" class="bg-yellow fg-darken-link">
		        <a href="javascript:void(0);" onclick="getgraph('temprature','<?php echo  $this->uri->segment(4)?>');">
		        <span class="tutorials"><p style="font-size: 40px;">
		        {{set_temperature}}
		        </p></span></a></li>
	    	    
	        
	        	
	      	</ul>
	    </div>



</section>
<script src="http://www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
		<script src="http://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>

		<!-- amCharts javascript code -->


	
<script>
var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http,$interval) {
	 $interval(callAtInterval, 1000);

	 function callAtInterval() {
	 	 
  $http.get("<?php echo base_url();?>welcome/action/ajaxgetEnergyStats/<?php echo $this->uri->segment(4);?>")
  .then(function(response) {
      $scope.myWelcome = response.data;
      $scope.lux = response.data.lux;
      $scope.temperature = response.data.temprature;
      $scope.set_temperature = response.data.temprature;
      $scope.kwh = response.data.kwh;
      $scope.cyclic_period = response.data.cyclic_period; 
      $scope.ac_switch_duration = response.data.ac_switch_duration; 
      $scope.humitity = response.data.humidity;
      
  });
	 }
});
</script>
<script type="text/javascript">
$( document ).ready(function() {
	getgraph('lux','<?php echo $this->uri->segment(4)?>');
});
		function getgraph(type,device_id) {
			var device_type_id= 4;
				
					$('.ajax-loader1').show();
					
					 var graph_type	= 'line';
					if( '' != $.trim( device_id ) ) {
							$.get( "<?php echo base_url();?>welcome/action/viewLiveGraph/"+type+"/"+graph_type+"/"+device_type_id+'/'+device_id, function( data ) {
								$('.ajax-loader1').hide();
								
								 	$("#luxchart").html( data );
								
								 		$('#graph_title').html(type+" Vs Time");
									
							});
					}
					
			
				
		}

	function switchlight(type,id)
	{
		
		$.get( "<?php echo base_url();?>welcome/action/switchlight/"+type+"/"+id, function( data ) {
		  
				
		});
	}
		</script>


	