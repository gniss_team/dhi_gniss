<?php $this->load->view('common/header');?>
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Profile</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>welcome">Dashboard</a></li>
			<?php 
			$intCompanyId	= $this->session->userdata( 'company_id' );
			if( true == empty( $intCompanyId ) ) { ?>
				<li><a class="big" href="<?php echo base_url(); ?>company/action/viewcompanies">Manage Company</a></li>
				<li>Edit Company</li>
	  <?php } else {
			?>
			<li>Profile</li>
		<?php } ?>
		</ul>
	</div>
	<div class="content-wrap clearfix">
		<?php if($msg!="") { ?>
			<div class="alert alert-success">
				<?php   echo $msg; ?>
			</div>
		<?php } ?>
		<div class="form-holder">
		<div class="form-space">
		<span class="asterisk-msg">All fields marked with * are mandatory.</span>
		<?php $uri = $this->uri->segment(4); echo form_open_multipart('profile/action/companyprofile/'.$this->uri->segment(4) ); ?>
		 
		<div class="form-grp-profile">
			<label>Company name <span class="red">*</span></label>
		    <input class="form-control-profile" placeholder="Company name" id="company_name" name="company_name" maxlength="55"  value="<?php echo $company_info->company_name; ?>" >
		    <?php echo form_error('company_name'); ?>
		</div>
		<div class="form-grp-profile">
        	<label>Email <span class="red">*</span></label>
            <input class="form-control-readonly" maxlength="40" placeholder="Email address" id="email_address" name="email_address" readonly="readonly" value="<?= $company_info->email_address; ?>">
            <?php echo form_error('email_address'); ?>
        </div>
		<div class="form-grp-profile">
        	<label>Mobile Number <span class="red">*</span></label>
            <input class="form-control-profile" maxlength="15" placeholder="Mobile number" id="mobile_number" name="mobile_number" value="<?= $company_info->mobile_number; ?>">
            <?php echo form_error('mobile_number'); ?>
        </div>
		<div class="form-grp-profile">
			<label>Company address <span class="red">*</span></label>
		    <textarea class="form-control-profile" placeholder="Company address" id="company_address" name="company_address"  maxlength="105"><?php echo $company_info->company_address; ?></textarea>
		    <?php echo form_error('company_address'); ?>
		</div>
		<div class="form-grp-profile">
			<label>Country <span class="red">*</span></label>
			<select class="form-control-profile-select styled" id="country_id" name="country_id">
				<option value="" >Select country</option>
				<?php foreach( $countries as $country ) { ?>
				<option value="<?= $country->id; ?>" <?php if($country->id == $company_info->country_id) { ?> selected="selected" <?php } ?> ><?= $country->name; ?> </option>
				<?php } ?>
			</select>
			<?php echo form_error('country_id'); ?>
		</div>
		<div class="form-grp-profile">
			<label>State <span class="red">*</span></label>
			<select class="form-control-profile-select styled" id="state_id" name="state_id" >
				<option value="" >Select state</option>
				<?php foreach( $states as $state ) { ?> 
				<option value="<?= $state->id; ?>" <?php if($state->id ==$company_info->state_id) { ?> selected="selected" <?php } ?> ><?= $state->name; ?></option>
				<?php } ?>
			</select>
			<?php echo form_error('state_id'); ?>
		</div>
		<div class="form-grp-profile">
			<label>City <span class="red">*</span></label>
			<select class="form-control-profile-select styled" id="city_id" name="city_id">
				<option value="" >Select city</option>
				<?php foreach( $cities as $city ){ ?>
				<option value="<?= $city->id; ?>" <?php if( $city->id == $company_info->city_id ) { ?> selected="selected" <?php } ?> ><?= $city->name; ?></option>
				<?php } ?>
			</select>
			<?php echo form_error('city_id'); ?>
		</div>
		<div class="form-grp-profile">
			<label>Zip Code </label>
			<input class="form-control-profile" placeholder="Zip code" id="zip_code" name="zip_code" maxlength="50" value="<?= $company_info->zip_code; ?>" >
			<?php echo form_error('zip_code'); ?>
		</div>	
		
        <div class="btn-grp">  
    	    <button type="submit" class="btn btn-primary">Update </button>
            <?php 
            if( true == empty( $intCompanyId ) ) { ?>
            	<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>company/action/viewcompanies'">Cancel</button>
            <?php } else {?>
            	<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>welcome'">Cancel</button>
            <?php } ?>
        </div>
        </div>
        <div class="profile-holder">
			<?php $profile_picture = $company_info->profile_picture;
			if( 'default.png' == $profile_picture ) {
				$profile_picture_path = 'images/'.$profile_picture;	
			} else {
				$profile_picture_path = 'uploads/'.$profile_picture;
			}
			?>
        	<img width="160px" height="120px" src="<?php echo base_url().$profile_picture_path; ?>" id="thumb" />
        	<div class="pro-txt">Change profile image</div>
       	
			<label class="browse-button"><input maxlength="20" type="file"  id="profile_img" name="profile_img" onchange="readURL(this);">Browse</label>
		</div>
		
		</div>
		<?php  echo form_close(); ?>
	</div>
    
</div>
</div>
<script type="text/javascript">
var strUrl = "<?php echo base_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/locations.js"></script> 
 <script type="text/javascript">
 function readURL(input) {
 	if (input.files && input.files[0]) {
    	var reader = new FileReader();
		reader.onload = function (e) {
        	$('#thumb').attr('src', e.target.result);
        }
    reader.readAsDataURL(input.files[0]);
    }
 }
 </script>   
 <script type="text/javascript">

$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>
<?php $this->load->view('common/footer');?>