<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>

<style>
.error {
	text-indent: 6%;
}
#name_spn {
	color: #252525;
    font-size: 12px;
    padding: 10px 0;
    text-transform: capitalize;
	margin: 0 0 0 88px;
}
#number_spn {
	color: #252525;
    font-size: 12px;
    padding: 10px 0;
    text-transform: capitalize;
	margin: 0 0 0 230px;
}
.filter {

	margin: 0 0 0 137px;
}
#no_record {
	font-size: 1.0em;
    margin: 120px;
    text-align: center;
}
label {
	color: #252525;
    font-size: 12px;
    padding: 10px 0;
    text-transform: capitalize;
}
</style>

<div class="main-container clearfix">
	<div class="content">

    	<div class="pushmenu-push">
            <div class="main-content-body clearfix">
				<?php 	
				//print_r($asset_info);
				$attributes = array('class' => 'route',  'id' => 'route-class');		
				echo form_open( base_url() . 'livetracking/routedatewisemap/' . $asset_info->phone_number, $attributes); ?>
				
 				<button class="button-back" type="button" onclick="window.location='<?php echo base_url(); ?>livetracking'">Back</button>
 					 <div class="title-box">
						<div class="page-title"><h4>Route Report</h4></div>
					</div>
            	<div class="filter">
            	<label>Device Name : <?php if( true == is_object( $asset_info )) { echo $asset_info->name; }?></label>
            	<label style="margin-left: 162px;">Phone Number : <?php if( true == is_object( $asset_info )) { echo $asset_info->phone_number; } ?></label>
            	<label style="margin-left: 230px;"> Total Distance : <span id="total"></label>
            	</div>
            	
            	<div class="filter">
					<label>From Date:</label>
				   	<input id="from_date" type="text" name="from_date" class="form-control-cal" value="<?php if( false == empty($from_date) ) { echo $from_date; }?>" readonly="readonly" >
				   	<label style=" margin-left: 25px;">To Date:</label>
					<input id="to_date" type="text" name="to_date" class="form-control-cal" value="<?php if( false == empty($to_date) ) { echo $to_date; } ?>" readonly="readonly" >
			 		<button name="show" class="signup" type="submmit" id="show" onclick="return showroute(<?php echo $asset_info->phone_number;?>)"  class="btn btn-primary">Show</button>
			 		<div id="from_to_error"></div>
			 	</div>
			 		
            	<div class="main-content">
                	<?php if(true == isset( $reports ) && count($reports) > 0){?> 
                	<div class="user-forms">
                	
            		<div id="map" style="width:98%; height:600px;"></div>
            			 </div>
            			<?php } else { ?>
            			<div id="no_record">No route history found. </div>
            			<?php } ?>
                  
                </div>
                <?php  echo form_close(); ?>	
                <!-- /.row -->
                
</div>
		</div>
    </div> 
</div>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script>
    
      jQuery(function() {
    var stops = [
                <?php foreach($reports as $report){?>
			           {"Geometry":{"Latitude":<?php echo $report->latitude;?>,"Longitude":<?php echo $report->longitude;?>}},
			       <?php } ?>   
			                    ] ;
	
    var map = new window.google.maps.Map(document.getElementById("map"));

    // new up complex objects before passing them around
    var directionsDisplay = new window.google.maps.DirectionsRenderer({suppressMarkers: false});
    var directionsService = new window.google.maps.DirectionsService();

    Tour_startUp(stops);

    window.tour.loadMap(map, directionsDisplay);
    window.tour.fitBounds(map);

    if (stops.length > 1)
        window.tour.calcRoute(directionsService, directionsDisplay);

   	 directionsDisplay.addListener('directions_changed', function() {
        computeTotalDistance(directionsDisplay.getDirections());
      });
   	 
});

function computeTotalDistance(result) {
   var total = 0;
   var myroute = result.routes[0];
   for (var i = 0; i < myroute.legs.length; i++) {
   		total += myroute.legs[i].distance.value;
   }
   total = total / 1000;
   document.getElementById('total').innerHTML = total + ' km';
}

function Tour_startUp(stops) {
    if (!window.tour) window.tour = {
        updateStops: function (newStops) {
            stops = newStops;
        },
        // map: google map object
        // directionsDisplay: google directionsDisplay object (comes in empty)
        loadMap: function (map, directionsDisplay) {
            var myOptions = {
                zoom: 13,
                center: new window.google.maps.LatLng(21.1500,79.0900), // default to London
                mapTypeId: window.google.maps.MapTypeId.ROADMAP
            };
            map.setOptions(myOptions);
            directionsDisplay.setMap(map);
        },
        fitBounds: function (map) {
            var bounds = new window.google.maps.LatLngBounds();

            // extend bounds for each record
            jQuery.each(stops, function (key, val) {
                var myLatlng = new window.google.maps.LatLng(val.Geometry.Latitude, val.Geometry.Longitude);
                bounds.extend(myLatlng);
            });
            map.fitBounds(bounds);
        },
        calcRoute: function (directionsService, directionsDisplay) {
            var batches = [];
            var itemsPerBatch = 10; // google API max = 10 - 1 start, 1 stop, and 8 waypoints
            var itemsCounter = 0;
            var wayptsExist = stops.length > 0;

            while (wayptsExist) {
                var subBatch = [];
                var subitemsCounter = 0;

                for (var j = itemsCounter; j < stops.length; j++) {
                    subitemsCounter++;
                    subBatch.push({
                        location: new window.google.maps.LatLng(stops[j].Geometry.Latitude, stops[j].Geometry.Longitude),
                        stopover: true
                    });
                    if (subitemsCounter == itemsPerBatch)
                        break;
                }

                itemsCounter += subitemsCounter;
                batches.push(subBatch);
                wayptsExist = itemsCounter < stops.length;
                // If it runs again there are still points. Minus 1 before continuing to
                // start up with end of previous tour leg
                itemsCounter--;
            }

            // now we should have a 2 dimensional array with a list of a list of waypoints
            var combinedResults;
            var unsortedResults = [{}]; // to hold the counter and the results themselves as they come back, to later sort
            var directionsResultsReturned = 0;

            for (var k = 0; k < batches.length; k++) {
                var lastIndex = batches[k].length - 1;
                var start = batches[k][0].location;
                var end = batches[k][lastIndex].location;

                // trim first and last entry from array
                var waypts = [];
                waypts = batches[k];
                waypts.splice(0, 1);
                waypts.splice(waypts.length - 1, 1);

                var request = {
                    origin: start,
                    destination: end,
                    waypoints: waypts,
                    travelMode: window.google.maps.TravelMode.WALKING
                };
                (function (kk) {
                    directionsService.route(request, function (result, status) {
                        if (status == window.google.maps.DirectionsStatus.OK) {

                            var unsortedResult = { order: kk, result: result };
                            unsortedResults.push(unsortedResult);

                            directionsResultsReturned++;

                            if (directionsResultsReturned == batches.length) // we've received all the results. put to map
                            {
                                // sort the returned values into their correct order
                                unsortedResults.sort(function (a, b) { return parseFloat(a.order) - parseFloat(b.order); });
                                var count = 0;
                                for (var key in unsortedResults) {
                                    if (unsortedResults[key].result != null) {
                                        if (unsortedResults.hasOwnProperty(key)) {
                                            if (count == 0) // first results. new up the combinedResults object
                                                combinedResults = unsortedResults[key].result;
                                            else {
                                                // only building up legs, overview_path, and bounds in my consolidated object. This is not a complete
                                                // directionResults object, but enough to draw a path on the map, which is all I need
                                                combinedResults.routes[0].legs = combinedResults.routes[0].legs.concat(unsortedResults[key].result.routes[0].legs);
                                                combinedResults.routes[0].overview_path = combinedResults.routes[0].overview_path.concat(unsortedResults[key].result.routes[0].overview_path);

                                                combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getNorthEast());
                                                combinedResults.routes[0].bounds = combinedResults.routes[0].bounds.extend(unsortedResults[key].result.routes[0].bounds.getSouthWest());
                                            }
                                            count++;
                                        }
                                    }
                                }
                                directionsDisplay.setDirections(combinedResults);
                                var legs = combinedResults.routes[0].legs;
                                // alert(legs.length);
                                for (var i=0; i < legs.length;i++){
                  var markerletter = "A".charCodeAt(0);
                  markerletter += i;
                                  markerletter = String.fromCharCode(markerletter);
                                  createMarker(directionsDisplay.getMap(),legs[i].start_location,"marker"+i,"<br>"+legs[i].start_address,markerletter);
                                }
                                var i=legs.length;
                                var markerletter = "A".charCodeAt(0);
                    markerletter += i;
                                markerletter = String.fromCharCode(markerletter);
                                createMarker(directionsDisplay.getMap(),legs[legs.length-1].end_location,"marker"+i,"some text for the "+i+"marker<br>"+legs[legs.length-1].end_address,markerletter);
                            }
                        }
                    });
                })(k);
            }
        }
    };
}
var infowindow = new google.maps.InfoWindow(
  { 
    size: new google.maps.Size(150,50)
  });

var icons = new Array();
icons["red"] = new google.maps.MarkerImage("mapIcons/marker_red.png",
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(20, 34),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 9,34.
      new google.maps.Point(9, 34));



function getMarkerImage(iconStr) {
   if ((typeof(iconStr)=="undefined") || (iconStr==null)) { 
      iconStr = "red"; 
   }
   if (!icons[iconStr]) {
      icons[iconStr] = new google.maps.MarkerImage("https://www.google.com/mapfiles/marker"+ iconStr +".png",
      // This marker is 20 pixels wide by 34 pixels tall.
      new google.maps.Size(20, 34),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 6,20.
      new google.maps.Point(9, 34));
   } 
   return icons[iconStr];

}
  // Marker sizes are expressed as a Size of X,Y
  // where the origin of the image (0,0) is located
  // in the top left of the image.

  // Origins, anchor positions and coordinates of the marker
  // increase in the X direction to the right and in
  // the Y direction down.

  var iconImage = new google.maps.MarkerImage('http://www.google.com/mapfiles/shadow50.png',
      // This marker is 20 pixels wide by 34 pixels tall
      new google.maps.Size(20, 34),
      // The origin for this image is 0,0.
      new google.maps.Point(0,0),
      // The anchor for this image is at 9,34.
      new google.maps.Point(9, 34));
  var iconShadow = new google.maps.MarkerImage('http://www.google.com/mapfiles/shadow50.png',
      // The shadow image is larger in the horizontal dimension
      // while the position and offset are the same as for the main image.
      new google.maps.Size(37, 34),
      new google.maps.Point(0,0),
      new google.maps.Point(9, 34));
      // Shapes define the clickable region of the icon.
      // The type defines an HTML &lt;area&gt; element 'poly' which
      // traces out a polygon as a series of X,Y points. The final
      // coordinate closes the poly by connecting to the first
      // coordinate.
  var iconShape = {
      coord: [3,4,9],
      type: 'poly'
  };


function createMarker(map, latlng, label, html, color) {
// alert("createMarker("+latlng+","+label+","+html+","+color+")");
    var contentString = '<b>'+label+'</b><br>'+html;
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        shadow: iconShadow,
        icon: getMarkerImage(color),
        shape: iconShape,
        title: label,
        zIndex: Math.round(latlng.lat()*-100000)<<5
        });
        marker.myname = label;

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString); 
        infowindow.open(map,marker);
        });
    return marker;
}
    </script>
<link type="text/css" href="<?php echo base_url(); ?>js/jquery.datetimepicker.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.datetimepicker.js"></script>
<script type="text/javascript" >

function showroute( phonenumber ) {

	var from_date 	= document.getElementById('from_date').value;
	var to_date 	= document.getElementById('to_date').value; 
	
	if( '' === from_date ) {
		
		$('#from_to_error').html( 'From date is required.' );
		$('#from_to_error').addClass( "error alert-error" );
		return false;
	}	
	if( '' === to_date ) {
		
		$('#from_to_error').html( 'To date is required.' );
		$('#from_to_error').addClass( "error alert-error" );
		return false;
	}
	if( '' === from_date && '' === to_date ) {
		
		$('#from_to_error').html( 'Date is required.' );
		$('#from_to_error').addClass( "error alert-error" );
		return false;
		
	} else {
		var startDate	= createDate( from_date );
		var endDate	= createDate( to_date );
		
		if (startDate < endDate){
			return true;
			
			
		} else {
			
			$('#from_to_error').html( 'From Date should be less than To Date' );
			$('#from_to_error').addClass( "error alert-error" );
			return false;
		}	
	}

}

jQuery(function(){
		Date.prototype.format = function( format ){
		 //you code for convert date object to format string
		 //for example
		 switch( format ) {
		 
			   case 'd': return this.getDate();
			   case 'H:i:s': return this.getHours()+':'+this.getMinutes()+':'+this.getSeconds();
			   case 'h:i a': return  ( (this.getHours() %12) ? this.getHours() % 12 : 12)+':'+this.getMinutes()+(this.getHours() < 12 ? 'am' : 'pm');
			   case 'Y-m-d H:i': return  this.getDate() + ' ' + this.getHours()+':'+this.getMinutes();
			   case 'd-m-Y H:i': return  this.getDate() + ' ' + this.getHours()+':'+this.getMinutes();
			   
		 }
		 	// or default format
			 return this.getDate()+'.'+(this.getMonth()+ 1)+'.'+this.getFullYear();
	};

	jQuery('#from_date').datetimepicker({
			 format:'Y-m-d H:i',
			 onShow:function( ct ){
					 var start_date	= ( false == !jQuery('#to_date').val() ) ? jQuery('#to_date').val().split(' ') : null;
					  this.setOptions({
							maxDate: ( null !== start_date ) ? new Date( start_date[0] ) : false
					  })
			 },
			 timepicker:true
	});
	
	jQuery('#to_date').datetimepicker({
		 	format:'Y-m-d H:i',
			 onShow:function( ct ){
					 var end_date	= ( false == !jQuery('#from_date').val() ) ? jQuery('#from_date').val().split(' ') : null;
					 this.setOptions({
							minDate:( null !== end_date ) ? new Date( end_date[0] ) :false
					})
			 },
			 timepicker:true
	});
});

function createDate( str1 ){
	
	// str1 format should be dd/mm/yyyy. Separator can be anything e.g. / or -. It wont effect
	var yr1  = parseInt(str1.substring(0,4));
	var mon1 = parseInt(str1.substring(5,7));
	var dt1  = parseInt(str1.substring(8,10));

// 	var dt1  = parseInt(str1.substring(0,2));
// 	var mon1 = parseInt(str1.substring(3,5));
// 	var yr1  = parseInt(str1.substring(6,10));
	
	var h1	 = parseInt(str1.substring(11,13));
	var m1	 = parseInt(str1.substring(14,16));
	
	var date1 = new Date(yr1, mon1-1, dt1, h1, m1 );
	
	return date1;
}
</script>
<?php $this->load->view('common/footer');?>