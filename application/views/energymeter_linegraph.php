<?php $this->load->view('common/header');?>
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/sidebar');?>
<script>	 var base_url = "<?php echo base_url(); ?>";</script>

<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasAxisLabelRenderer.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasAxisLabelRenderer.min.js"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>graph_assets/jquery-ui.css">
<script src="<?php echo base_url(); ?>graph_assets/jquery-ui.js"></script> 
<link href="<?php echo base_url(); ?>css/datepicker.css" rel="stylesheet">
<script type="text/javascript">
$(document).ready(function () {
	
		$( '#watts' ).attr( "checked", true );
	    if( 'kwh' == $('input[type="radio"][name="type"]:checked').val() ) {
	    	var unit_type = $('input[type="radio"][name="type"]:checked').val();
			
			var date = $('#date').val();
			var urlData = "/"+unit_type+"/"+date;

			$.ajax({
				  method: "GET",
				  url: base_url + 'energymeter/action/viewLineGraph' + urlData
				
				})
				  .done(function( msg ) {
					  $("#chart1").html(msg);
				  });
	 	 }
	 	 
		$('input[name="type"]').click( function (){
					var unit_type = $(this).val();
					
					var date = $('#date').val();
					var urlData = "/"+unit_type+"/"+date;

					$.ajax({
						  method: "GET",
						  url: base_url + 'energymeter/action/viewLineGraph' + urlData
						
						})
						  .done(function( msg ) {
							  $("#chart1").html(msg);
						  });

		}) ; 
		  
        $("#date").datepicker({
    	  dateFormat: 'yy-mm-dd',
    	    onSelect: function(date, instance) {

    	    	var unit_type = $('input[type="radio"][name="type"]:checked').val();
    	  	 	//	var date = $(this).val();

    	        $.ajax
    	        ({
    	              type: "GET",
    	              url: base_url + 'energymeter/action/viewLineGraph'+ '/'+unit_type+'/'+date,
    	            
    	              success: function(result) {
    	            	 		 $("#chart1").html(result);
    	              }
    	         });  
    	     }
    	});
     
		});

</script>
<style >
        .jqplot-target {
            margin: 20px;
            height: 340px;
            width: 600px;
            color: #dddddd;
        }
#chart1 {
	text-align:center;

}
        table.jqplot-table-legend {
				    border: 0px;
				    background-color: rgba(100,100,100, 0.0);
				}

				.jqplot-highlighter-tooltip {
				    background-color: rgba(57,57,57, 0.9);
				    padding: 7px;
				    color: #dddddd;
				}

}


    </style>
    <pre class="code brush:js"></pre>
<div class="content clearfix">
	<div class="page-title"><h3> Energy Meter Line Graph</h3> </div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>welcome">Dashboard</a></li>
		
			<li>View Energy Meter Graph </li>
		</ul>
	
	</div>
	<form>
	<div>
		<label style=" margin-left: 25px;">Date:</label>
	   <input id="date" type="text" name="date" class="form-control-cal" value="<?php echo date('Y-m-d');?>" readonly="readonly" >
	  <div>
		<label style=" margin-left: 25px;">View Graph By<label>
	   		
	</div>
		<div>
		<label style=" margin-left: 25px;"><label>
	   		<input class="checkbx" type="radio"  name="type"  id="watts" value="kwh" ><label class="chkbox" for="label_watt">Watt<span></span></label>
	</div>
	<div>
		<label style=" margin-left: 25px;"><label>
	   		<input class="checkbx" type="radio"  name="type"  id="current" value="current" ><label class="chkbox" for="label_watt">Current<span></span></label>
		</div>
	<div>
	<label style=" margin-left: 25px;"><label>
			<input class="checkbx" type="radio"  name="type"  id="voltage" value="voltage" ><label class="chkbox" for="label_watt">Voltage<span></span></label>
	</div>
	</div>
	
	
	</form>
	<div class="content-wrap clearfix">
	
	<div class="ui-widget ui-corner-all">
				<div class="ui-widget-header ui-corner-top" style="text-align:center;">Hourly Energy Meter Graph</div>
        <div class="ui-widget-content ui-corner-bottom" >
        
            <div id="chart1"></div>
             </div>
    </div>
	</div>
</div>
	<style>.canvasjs-chart-credit {
		display:none;
	}
	input[type='text']{
		width:9%;
	}
	select{
		width:4%;
	}
</style>
<script>
$(function() {
		$( "#date" ).datepicker({ dateFormat: 'yy-mm-dd' }).datepicker("setDate", new Date());
});
  </script>
<?php $this->load->view('common/footer');?>