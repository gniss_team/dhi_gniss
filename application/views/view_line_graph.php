<script type="text/javascript">

$(document).ready(function(){
	 $.jqplot.config.enablePlugins = true;
	 
	  <?php for( $j=0 ;$j <$cnt_graph_parameter; $j++) {  ?>

	  var line<?php echo $j;?>=[<?php for( $i=0 ;$i<count($energy_meter_data); $i++) {
			if( $i != count($energy_meter_data) -1  ){
			?>
					["<?php echo $energy_meter_data[$i]->created_on;?>", <?php if( 'voltage' == $type[$j] || 'current' == $type[$j] ) { echo $energy_meter_data[$i]->$type[$j]/10; } else { echo $energy_meter_data[$i]->$type[$j];}?>],

			 <?php } else {?>
			 ["<?php echo $energy_meter_data[$i]->created_on;?>", <?php if( 'voltage' == $type[$j] || 'current' == $type[$j] ) { echo $energy_meter_data[$i]->$type[$j]/10; } else { echo $energy_meter_data[$i]->$type[$j];}?>]
			 <?php  } } ?> ] <?php } ?>
		 
     
	//  var line2=['2015-07-03','2015-07-04'];
	  var plot2 = $.jqplot('chart1',[
	                                   <?php for( $k=0 ;$k < $cnt_graph_parameter; $k++) { 
	                                       if( $k != ($cnt_graph_parameter) - 1 ) { ?><?php echo 'line'. $k . ',';  }else { echo 'line'.$k; } } ?>], {
		  seriesColors: ["rgba(0, 100, 255, 1)", "rgb(255, 0, 0)","rgb(76, 153, 0)","rgb(153, 0, 153)","rgb(255, 255, 51)","rgb(0, 255, 255)"],
	      title:'', 
	      highlighter: {
              show: true,
              sizeAdjust: 1,
              tooltipOffset: 9
          },
          grid: {
        		 background: 'rgba(57,57,57,0.0)',
              drawBorder: false,
              shadow: false,
              gridLineColor: '#D6D6C2',
              gridLineWidth: 0.5
          },
          legend: {
              show: true,
              placement: 'outside'
          },
          seriesDefaults: {
              rendererOptions: {
                  smooth: true,
                  animation: {
                      show: true
                  }
              },
             
              showMarker: false,
             
              lineWidth: 0.7
          },
          series: [
			  <?php for($t=0;$t<count($type);$t++) { ?>
              {
                  label: '<?php echo $type[$t]; ?>'
              },
             <?php } ?>
          ],
          axesDefaults: {
              rendererOptions: {
                  baselineWidth: 1,
                  baselineColor: '#0000',
                  drawBaseline: false
                  //lineWidth:0.7
              }
          },
	      axes:{
	        xaxis:{
	          renderer:$.jqplot.DateAxisRenderer, 
	         // ticks:line2,
	          tickOptions:{pad: .2,showGridline: true,formatString:'%b %#d, %R',fontSize:'8pt', fontFamily:'Tahoma', angle:50,textColor:'#666666',mark: 'outside'},
	         min:'<?php echo $this->input->post('from_date');?>', 
	         max:'<?php echo $this->input->post('to_date');?>'
	         // tickInterval:'1 hour'
	        }
	      },
	      
	 //    series:[{lineWidth:0.7}]
	     
	  });
	});

</script>
  
<style>
.blank_div{ 
	color: #757575;
    display: inline;
    font-size: 13px;
}
.jqplot-highlighter-tooltip{background:#000; color:#fff;}
.jqplot-target {
     color:#666666;
}
.ui-widget-content {
    background:white !important;
}
</style>