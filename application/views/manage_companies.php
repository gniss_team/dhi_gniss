<?php $this->load->view('common/header');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Manage Company</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>">Dashboard</a></li>
			<li>Manage Company</li>
		</ul>
	</div>
	<button class="btn btn-xs btn-success add-btn" type="button" onclick="window.location='<?php echo base_url(); ?>company/action/createcompany'">Register Company</button>
	<div class="content-wrap clearfix">
	<?php if(isset($msg) && $msg != '') { ?>
		<div class="alert alert-success">
			<?php echo $msg; ?>
		</div>
	<?php } ?>
	<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
		<thead>
			<tr>
                <th style="display:none">Id</th>
                <th>Company Name</th>
                <th>Role</th>
			    <th class="noheader">Action</th>
            </tr>
        </thead>
        <tbody>
			<?php foreach ( $companies as $company ){?>
        	<tr>
            	<td style="display:none"><?= $company->id; ?></td>
                <td><?= $company->company_name; ?></td>
                <td><?php if( '1' == $company->user_type ) { echo 'Owner'; }else echo 'User'; ?></td>
				<td>
					<div class="tablebutton"><button class="btn btn-xs btn-success" type="button" onclick="window.location='<?php echo base_url(); ?>company/action/viewcompanyAssets/<?= $company->id; ?>'">View Assets</button></div>
					<?php if( '1' == $company->user_type ) {?>
	 					<div class="tablebutton"><button class="btn btn-xs btn-success" type="button" onclick="window.location='<?php echo base_url(); ?>company/action/viewcompanyusers/<?= $company->id; ?>'">View Users</button></div>
						<div class="tablebutton"><button class="btn btn-xs btn-success" type="button" onclick="window.location='<?php echo base_url(); ?>company/action/createcompanyuser/<?= $company->id; ?>'">Add Users</button></div>
						<div class="tablebutton"><button class="btn btn-xs btn-success" type="button" onclick="window.location='<?php echo base_url(); ?>profile/action/companyprofile/<?= $company->id; ?>'">Edit Company</button></div>
					<?php } ?>
 				</td>
 			</tr>
			<?php } ?>
        </tbody>
    </table>
	</div>
</div>
</div>
<?php $this->load->view('common/footer');?>