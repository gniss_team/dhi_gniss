
<table class="table table-bordered table-hover table-striped tablesorter  no-more-tables"  id="myTable">
      			<thead>
      			<tr class="head">
      					<th>Batch Id</th>
      					<th>Asset Id</th>
      					<th>Channel Id</th>
      					<th>Voltage</th>
      					<th>Current</th>
      					<th>Watts</th>
      					<th>W/Sec</th>
      					<th>W/Hour</th>
      					<th>KWH</th>
      					<th>FA Sum</th>
      					<th>Raw String</th>
					</tr>
      		</thead>
      		<tbody>
     <?php  
      	if( 0 < $energy_meter_data->num_rows ){
      				foreach ($energy_meter_data->result() as $objEnergyMeter ) {
      ?>
      <tr>
      												<td data-title="Batch Id" style="text-align:right;"><?php echo $objEnergyMeter->batch_number ;?></td>
      												<td data-title="Asset Id" style="text-align:right;"><?php echo $objEnergyMeter->unit_number; ?></td>
      												<td data-title="Channel Id" style="text-align:right;"><?php echo $objEnergyMeter->channel_number; ?></td>
      												<td data-title="Voltage" style="text-align:right;"><?php echo $objEnergyMeter->voltage; ?></td>
      												<td data-title="Current" style="text-align:right;"><?php echo $objEnergyMeter->current;?></td>
      												<td data-title="Watts" style="text-align:right;"><?php echo $objEnergyMeter->watts; ?></td>
      												<td data-title="Watts Second" style="text-align:right;"><?php echo $objEnergyMeter->watts_second; ?></td>
      												<td data-title="Watts Hour" style="text-align:right;"><?php echo $objEnergyMeter->watts_hour; ?></td>
      												<td data-title="KWH" style="text-align:right;"><?php echo $objEnergyMeter->kwh; ?></td>
      												<td data-title="FA Sum" style="text-align:right;"><?php echo $objEnergyMeter->checksum;?></td>
      												<td data-title="Raw String"><?php echo $objEnergyMeter->raw_string;?></td>
      </tr>
    <?php   			}
      } else { ?>
      <tr>
      			<td></td>
      			<td></td>
      			<td></td>
      			<td></td>
      			<td>No Data Available</td>
      			<td></td>
      			<td></td>
      			<td></td>
      			<td></td>
      </tr>
     <?php  } ?>
      	</tbody></table> 
      	<script>    $("#myTable").dataTable({
      	 "bPaginate":false,
      	"bFilter": false,
     } );</script>
