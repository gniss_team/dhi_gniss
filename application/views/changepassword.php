<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<div class="">
	<div class="page-title"><h4>Change Password</h4></div>
	<div class="content-wrap clearfix">
        		<?php if(isset($msg) && $msg != '') { ?>
			<div class="alert alert-success">
                   <?php echo $msg; ?>
                </div>
		
			<?php } ?>
       
        	<span class="asterisk-msg">All fields marked with * are mandatory.</span>
				<?php echo form_open_multipart('users/action/changepassword/'); ?>
				<div class="form-grp">
                	<label>Old Password <span class="red">*</span></label>
                    <input class="form-control" type="password" placeholder="Old password" maxlength="20" name="old_password" id="old_password"  >
                    <?php echo form_error('old_password'); ?>
                </div>
                <div class="form-grp">
		            <label>New Password<span class="red">*</span></label>
        	            <input class="form-control" type="password" maxlength="50" placeholder="New password" name="new_password" id="new_password"  >
                        <?php echo form_error('new_password'); ?>
                </div>
			    <div class="form-grp">
            	    <label>Confirm Password <span class="red">*</span></label>
                	    <input class="form-control" type="password" maxlength="50" placeholder="Confirm password" id="confirm_password" name="confirm_password" >
                        <?php echo form_error('confirm_password'); ?>
                </div>
			   	<div class="btn-grp">              
                <button type="submit" class="signup">Change Password </button>
                <button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>welcome'">Cancel</button>
                </div>
                <?php  echo form_close(); ?>
            </div>
	    </div>
<script type="text/javascript">
function readURL(input) {
	if (input.files && input.files[0]) {
    	var reader = new FileReader();
		reader.onload = function (e) {
        	$('#thumb').attr('src', e.target.result);
		}
    reader.readAsDataURL(input.files[0]);
    }
}
</script>   
<?php $this->load->view('common/footer');?>