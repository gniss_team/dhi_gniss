<?php $this->load->view('common/header');?>
<?php $this->load->view('common/template_style');?>
<div id="contact">
	<div class="sub-contact">
		<h2> Privacy Policy </h2>
		<p>We are committed to safeguarding the privacy of our website visitors. This policy sets out how we will treat your personal information.</p>
		<p>We may collect, store and use the following kinds of personal data:<br>
(a) information about your visits to and use of this website;<br>
(b) information about any transactions carried out between you and us on or in relation to this<br>
website, including information relating to any purchases you make of our goods or services;<br>
(c) information that you provide to us for the purpose of registering with us and/or subscribing to our website services and/or email notifications.</p>
		<p>We may collect information about your computer and your visits to this website such as your IP<br>
address, geographical location, browser type, referral source, length of visit and number of page views. We may use this information in the administration of this website, to improve the website’s usability, and for marketing purposes.</p>
		<p>We use cookies on this website. A cookie is a text file sent by a web server to a web browser, and stored by the browser. The text file is then sent back to the server each time the browser requests a page from the server. This enables the web server to identify and track the web browser.</p>
		<p>We may send a cookie which may be stored by your browser on your computer’s hard drive. We may<br>
use the information we obtain from the cookie in the administration of this website, to improve the website’s usability and for marketing purposes. We may also use that information to recognise your computer when you visit our website, and to personalise our website for you. Our advertisers may also send you cookies.</p>
		<p>Most browsers allow you to refuse to accept cookies. This will, however, have a negative impact upon the usability of many websites, including this one.</p>
		<p>Personal data submitted on this website will be used for the purposes specified in this privacy policy or in relevant parts of the website.</p>
		<p>In addition to the uses identified elsewhere in this privacy policy, we may use your personal<br>
information to:<br>
(a) improve your browsing experience by personalising the website;<br>
(b) send information (other than marketing communications) to you which we think may be of interest to you by post or by email or similar technology;<br>
(c) send to you marketing communications relating to our business [or the businesses of carefully- selected third parties] which we think may be of interest to you by post or, where you have specifically agreed to this, by email or similar technology (you can inform us at any time if you no longer require marketing communications to be sent by unsubscribing from our mailing lists).<br>
(d) provide other companies with statistical information about our users &ndash; but this information will not be used to identify any individual user. We will not without your express consent provide your personal information to any third parties for the purpose of direct marketing.</p>
		<p>In addition to the disclosures reasonably necessary for the purposes identified elsewhere in this privacy policy, we may disclose information about you:</p>
		<p>(a) to the extent that we are required to do so by law;<br>
(b) in connection with any legal proceedings or prospective legal proceedings;<br>
(c) in order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk); and<br>
(d) to the purchaser or seller (or prospective purchaser or seller) of any business or asset which we are (or are contemplating) selling or purchasing.<br>
Except as provided in this privacy policy, we will not provide your information to third parties.</p>
		<p>Information that we collect may be stored and processed in and transferred between any of the<br>
countries in which we operate in order to enable us to use the information in accordance with this privacy policy.</p>
		<p>We will take reasonable precautions to prevent the loss, misuse or alteration of your personal<br>
information. Of course, data transmission over the internet is inherently insecure, and we cannot guarantee the security of data sent over the internet.</p>
		<p>We may update this privacy policy from time-to-time by posting a new version on our website. You should check this page occasionally to ensure you are happy with any changes.</p>
		<p>The website contains links to other websites. We are not responsible for the privacy policies (or content) of third party websites.</p>
	</div><!--sub-contact-->
</div><!--contact-->
<?php $this->load->view('common/footer');?>