
<script type="text/javascript" class="code">
$(document).ready(function(){ 
	  var cosPoints=[<?php for( $i=0 ;$i<count($vcdata); $i++) {
			if( $i != count($vcdata) -1  ){
			?>
					[<?php echo $vcdata[$i]->voltage/10;?>, <?php echo $vcdata[$i]->current/10;?>],

			 <?php } else {?>
			 [<?php echo $vcdata[$i]->voltage/10;?>, <?php echo $vcdata[$i]->current/10;?>]
			 <?php  } } ?> ];
	  //for (var i=0; i<2*Math.PI; i+=0.1){ 
	    // cosPoints.push([i, Math.cos(i)]); 
	  //} 
	  var plot3 = $.jqplot('volcurrent', [cosPoints], {  
		  highlighter: {
              show: true,
              sizeAdjust: 1,
              tooltipOffset: 9
          },
	      series:[{showMarker:true}],
	      axes:{
	        xaxis:{
	          label:'Voltage',
	          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
	          labelOptions: {
	            fontFamily: 'Georgia, Serif',
	            fontSize: '12pt'
	          }
	        },
	        yaxis:{
	          label:'Current',
	          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
	          labelOptions: {
	            fontFamily: 'Georgia, Serif',
	            fontSize: '12pt'
	          }
	        }
	      }
	  });
	});
</script>

 


  