<h2>Set Parameter</h2>
	    	
	    	<div ng-bind="msg">{{msg}}</div>
	    	<div style="width: 100%">
	    	<div style="float:left;width:30%;padding:5px 0px;">Set Temperature</div>
	    	<span ng-hide="showTemp">
	    	<input type="hidden" name="serrial_no" ng-model="serrial_no" value="<?php echo $this->uri->segment(5);?>">
	    	    	<input type="hidden" name="serrial_no" ng-model="call_type" value="ajax">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;" ng-bind="temp"></div>
	    	    <div style="float:left;padding:5px 0px;"><button class="para-edit" ng-click="showTemp = ! showTemp">edit</button></div>
	        </span>
	    	<span ng-show="showTemp">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='temp' style="margin:0;padding:0;" type="text" value="20" size="1"></div>
	    	 <div style="float:left;padding:5px 0px;"><button ng-click="addPara()" class="para-edit">Save</button></div>
	    	  <div ><img  src="<?php echo base_url(); ?>images/close_icon.png" ng-click="showTemp = ! showTemp"></div>
	        </span>
	    	</div>
	    	
	    		<div style="width: 100%;clear:both;">
	    	<div style="float:left;width:30%;padding:5px 0px;">AC switch duration</div>
	    	<span ng-hide="showswitch">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;" ng-bind="aswitch"></div>
	    	    <div style="float:left;padding:5px 0px;"><button class="para-edit" ng-click="showswitch = ! showswitch">edit</button></div>
	        </span>
	    	<span ng-show="showswitch">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='aswitch' style="margin:0;padding:0;" type="text" value="20" size="1"></div>
	    	 <div style="float:left;padding:5px 0px;"><button ng-click="addPara('showswitch')" class="para-edit">Save</button></div>
	    	 <div ><img  src="<?php echo base_url(); ?>images/close_icon.png" ng-click="showswitch = ! showswitch"></div>
	        </span>
	    	</div>
	    	
	    	  <div style="width: 100%;clear:both;">
	    	<div style="float:left;width:30%;padding:5px 0px;">Low cut</div>
	    	<span ng-hide="showslcut">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;" ng-bind="lcut"></div>
	    	    <div style="float:left;padding:5px 0px;"><button class="para-edit" ng-click="showslcut = ! showslcut">edit</button></div>
	        </span>
	    	<span ng-show="showslcut">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='lcut' style="margin:0;padding:0;" type="text" value="20" size="1"></div>
	    	 <div style="float:left;padding:5px 0px;"><button ng-click="addPara()" class="para-edit">Save</button></div>
	         	 <div ><img  src="<?php echo base_url(); ?>images/close_icon.png" ng-click="showslcut = ! showslcut"></div>
	        </span>
	    	</div>
	    	
	    	<div style="width: 100%;clear:both;">
	    	<div style="float:left;width:30%;padding:5px 0px;">High cut</div>
	    	<span ng-hide="showshcut">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;" ng-bind="hcut"></div>
	    	    <div style="float:left;padding:5px 0px;"><button class="para-edit" ng-click="showshcut = ! showshcut">edit</button></div>
	        </span>
	    	<span ng-show="showshcut">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='hcut' style="margin:0;padding:0;" type="text" value="20" size="1"></div>
	    	 <div style="float:left;padding:5px 0px;"><button ng-click="addPara()" class="para-edit">Save</button></div>
	    	 <div ><img  src="<?php echo base_url(); ?>images/close_icon.png" ng-click="showshcut = ! showshcut"></div>
	        </span>
	    	</div>
	    	
	      <div style="width: 100%;clear:both;">
	    	<div style="float:left;width:30%;padding:5px 0px;">Temperature offset</div>
	    	<span ng-hide="showToffset">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;" ng-bind="toffset"></div>
	    	    <div style="float:left;padding:5px 0px;"><button class="para-edit" ng-click="showToffset = ! showToffset">edit</button></div>
	        </span>
	    	<span ng-show="showToffset">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='toffset' style="margin:0;padding:0;" type="text" value="20" size="1"></div>
	    	 <div style="float:left;padding:5px 0px;"><button ng-click="addPara()" class="para-edit">Save</button></div>
	    	 <div ><img  src="<?php echo base_url(); ?>images/close_icon.png" ng-click="showToffset = ! showToffset"></div>
	        </span>
	    	</div>
	    	

	    	
	     <div style="width: 100%;clear:both;">
	    	<div style="float:left;width:30%;padding:5px 0px;">Main offset</div>
	    	<span ng-hide="showMoffset">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;" ng-bind="moffset"></div>
	    	    <div style="float:left;padding:5px 0px;"><button class="para-edit" ng-click="showMoffset = ! showMoffset">edit</button></div>
	        </span>
	    	<span ng-show="showMoffset">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='moffset' style="margin:0;padding:0;" type="text" value="20" size="1"></div>
	    	 <div style="float:left;padding:5px 0px;"><button ng-click="addPara()" class="para-edit">Save</button></div>
	    	 <div ><img  src="<?php echo base_url(); ?>images/close_icon.png" ng-click="showMoffset = ! showMoffset"></div>
	    
	        </span>
	    	</div>
	    	
	    	   <div style="width: 100%;clear:both;">
	    	<div style="float:left;width:30%;padding:5px 0px;">Load offset</div>
	    	<span ng-hide="showLoffset">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;" ng-bind="foffset"></div>
	    	    <div style="float:left;padding:5px 0px;"><button class="para-edit" ng-click="showLoffset = ! showLoffset">edit</button></div>
	        </span>
	    	<span ng-show="showLoffset">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='foffset' style="margin:0;padding:0;" type="text" value="20" size="1"></div>
	    	 <div style="float:left;padding:5px 0px;"><button ng-click="addPara()" class="para-edit">Save</button></div>
	    	 <div ><img  src="<?php echo base_url(); ?>images/close_icon.png" ng-click="showLoffset = ! showLoffset"></div>
	        </span>
	    	</div>
	    	
			  <div style="width: 100%;clear:both;">
	    	<div style="float:left;width:30%;padding:5px 0px;">Load Gain </div>
	    	<span ng-hide="showGoffset">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;" ng-bind="logain"></div>
	    	    <div style="float:left;padding:5px 0px;"><button class="para-edit" ng-click="showGoffset = ! showGoffset">edit</button></div>
	        </span>
	    	<span ng-show="showGoffset">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='logain' style="margin:0;padding:0;" type="text" value="20" size="1"></div>
	    	 <div style="float:left;padding:5px 0px;"><button ng-click="addPara()" class="para-edit">Save</button></div>
	    	 <div ><img  src="<?php echo base_url(); ?>images/close_icon.png" ng-click="showGoffset = ! showGoffset"></div>
	        </span>
	    	</div>
