<?php $this->load->view('common/header');?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.8/src/infobox.js"></script>
<?php $this->load->view('common/sidebar');?>
<style>
.nav-map{width: 25%;float: left;}
</style>
<script>
$(document).ready(function(){
	$('.geo-space').click(function(){
	$('.geo-form').slideToggle(200);
	//return flase;
		})
	
})
</script>
<style>h2{font-size:2em;font-weight:normal;}
.geo-form{display:none; background: #fff none repeat scroll 0 0;
    border: 1px solid;
    display: none;
    float: right;
    padding: 5px;
    position: relative;
    right: -56px;
    top: 40px;
    width: 30%;
    z-index: 9;}
#myImage {
  background: none repeat scroll 0 0 #fff;
  border-radius: 10px;
  bottom: 3px;
  box-shadow: -5px 23px 17px 0 rgba(125, 116, 116, 0.81);
  color: balck;
  padding: 15px;
  position: relative;
  z-index: -2147483648;
}

#myImage:after {
  border-color: #fff transparent;
  border-style: solid;
  border-width: 15px 15px 0;
  bottom: -15px;
  content: "";
  display: block;
  left: 125px;
  position: absolute;
  width: 0;
  z-index: 1;
}
#profile-pic {
  border-radius: 20px;
  position: absolute;
  right: 35px;
}

#myImageaa{background-color:#3e3d38;
	 background: url("map_icons/depot.png") no-repeat scroll center bottom 0 rgba(0, 0, 0, 0);
		
		border-radius:10px;
		color:white;
		position:relative;
	    height:auto;
        padding:20px;
        z-index: -2147483648;}
		
		#myImageaa ul li a{color:white;
			text-align:left;}
.info-window, .gm-style-iw, .gm-style-iw > div {

background-color:#3e3d38;!important;
}
.info-window  img {
display:none;
}
#loader{
position:absolute;
z-index:8888;
left:42%;
top:50%;
}
</style>

<div class="content clearfix">
	<div class="page-title"><h4>Shared Route Tracking</h4></div>
	
	    <div>
	
    	<button class="button-back" type="button" onclick="window.location='<?php echo base_url(); ?>livetracking/share/<?php echo $phone_number; ?>'">Back</button>
    </div>
   
	<div class="map-demo">
		<div class="pro-arrow-map"></div>
		<ul class="map-nav">
			<div id="side_bar"></div>
        </ul>
	</div>
	

    <!-- main content body-->
    <div class="clearfix">
    
   <span id="loader"> <img src="<?php echo base_url()?>/images/ajax-loader3.gif" ></span>
  
   
		<div id="map" style="width:98%; height:600px;" >
		
		</div>
	
    </div>
  
    	
    
</div>

<script type="text/javascript">


	var map;

	var arrMarkers = [];

	var arrInfoWindows = [];

	

	function mapInit(){
	
		var centerCoord = new google.maps.LatLng(18.5195700 ,73.8553500); 

		var mapOptions = {

			zoom: 10,

			center: centerCoord,

			mapTypeId: google.maps.MapTypeId.ROADMAP

		};

		map = new google.maps.Map(document.getElementById("map"), mapOptions);
		
		

	}



</script> 
    <script type="text/JavaScript">

		
		</script>
 <script type="text/javascript">
    //<![CDATA[
	$(function() {
		mapInit();
		  		$("#markers").on("click","a", function(){
			var i = $(this).attr("rel");
			arrInfoWindows[i].open(map, arrMarkers[i]);
		});

		  		setTimeout(function(){ readMap(); }, 3000);
   		window.setInterval(function(){
   			readMap();
   			
   		}, 200000);
   		
   		
		});

	 var profile="";
      var side_bar_html = "";
		var gmarkers = [];
		var htmls = [];
		var points = [];
		var lats = [];
		var lngs = [];
		var markersArray = new Array();
		
		var i = 0;
		var infowindow;
		var infobox;
		var base_url = '<?php echo base_url(); ?>';

		  
       
      // A function to create the marker and set up the event window
       function createMarker(point,name,html,lat,lng,icontype,vehicle_icon,token,phoneno,marker_icon) {
    	  
	   
	     	var base_url = '<?php echo base_url(); ?>';
		if(vehicle_icon==0) 
		{
		vehicle_icon="marker_red.png"; 

	     	}
		var blueIcon="images/marker_red.png"; 
	   var marker = new google.maps.Marker({
							position: point,
							map: map,
							optimized:false, // <-- required for animated gif
							//animation: google.maps.Animation.DROP,
							icon: base_url+"images/map_icons/marker_red.png",
							//icon: blueIcon,
							title:name
							});
	
							
		markersArray.push(marker);
		var geocoder= new google.maps.Geocoder();
			geocoder.geocode({'latLng': point }, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) {
				if (results[0]) {
					html=html+results[0].formatted_address;
				}
			}
			}
			});
			
      google.maps.event.addListener(marker, "click", function() {
		

	var st = "<div id='myImage'><div class='clearFix'></div><div class='close'></div>"+html+"</div>";
		if (infobox) infobox.close();
		 
		  infobox = new InfoBox({
			    content: st,
			    disableAutoPan: false,
			    maxWidth: 150,
			    pixelOffset: new google.maps.Size(-160,-160),
			    zIndex: null,
			    boxStyle: {
			                background: "url('') no-repeat",
			                opacity: 1,
			                width: "280px" 
			        },
			    closeBoxMargin: "5,20,30,5",
			    closeBoxURL: "/map_icons/close_hover.png",
			    infoBoxClearance: new google.maps.Size(1, 1)
			});	 
		
		  infobox.open(map, marker);
		});
		points[i]=point;
        lats[i]=lat;
        lngs[i]=lng;
        gmarkers[i] = marker;
		htmls[i] = html;
      //  side_bar_html += '<li><div class="nav-map"><a href="javascript:void(0);" >' + name + '<\/a></div><div class="nav-map"><a href="<?php echo base_url()?>livetracking/routewiselocation/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/route-icon.png">Route Map</a></div><div class="nav-map"><a href="livetracking/livelocation/'+phoneno+'" "><img width="22" src="<?php echo base_url()?>images/map_icons/track-icon.png">Live Track</a></div><div class="nav-map"><a href="livetracking/geofench/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/fench-icon.png">Fench</a></div></li>';
        //side_bar_html += '<a href="javascript:myclick(' + (gmarkers.length-1) + ')">' + name + '<\/a><br>';

        i++;
        
        return marker; 
           
		}
	  
		function clearOverlays() {
		  if (markersArray) {
			for (var i = 0; i < markersArray.length; i++ ) {
			  markersArray[i].setMap(null);
			}
		  }
		}
  

      // This function picks up the click and opens the corresponding info window
     function myclick(i) {
		//alert('fgh');
		google.maps.event.trigger(gmarkers[i], "click");
		map.setZoom(14);
		}


      
      
      
      // A function to read the data
    function readMap(url) {
       //alert("refresh");
        var baseurl= '<?php echo base_url();?>';

        var url=baseurl+"CManageLiveTrackingController/getAllLatLangByDeviceId/<?php echo $phone_number; ?>";
      
            $('#loader').hide();
            side_bar_html = "";
			htmls = [];
			i = 0;
            clearOverlays();
            gmarkers = [];
            address1="";
            $.get(url, function( data ) {
       		 if(data.flag==1) {
       					
       		  $.each(data.live_locations, function(key,value) {
       					      var lat = parseFloat(value.latitude);				  
       						  var lng = parseFloat(value.longitude);
       						  var icon_type = "";
       			              var point = new google.maps.LatLng(lat,lng);
       						  var html = "Name :"+value.name +"<br>"+"Speed:"+value.speed+"<br>Date: "+value.dated+"<br>Address:" +address1;
       			              var name =value.name;
       			           var label =value.name;
       						  var category = "";
       					     var vehicle_icon = "";
       					     var marker_icon = value.marker_icon;
       					     var token = "";
       					     var phoneno = value.phoneno;
       					        	
       					         var marker = createMarker(point,label,html,lat,lng,icon_type,vehicle_icon,token,phoneno,marker_icon);
       						  marker.setMap(map);
       						 side_bar_html += '<li><div class="nav-map"><a href="javascript:void(0);" >' + name + '<\/a></div><div class="nav-map"><a href="<?php echo base_url()?>livetracking/routewiselocation/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/route-icon.png">Route Map</a></div><div class="nav-map"><a href="livetracking/livelocation/'+phoneno+'" "><img width="22" src="<?php echo base_url()?>images/map_icons/track-icon.png">Live Track</a></div><div class="nav-map"><a href="livetracking/geofench/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/fench-icon.png">Fench</a></div></li>';
       				
       					        	});
       						 
       						document.getElementById("side_bar").innerHTML = side_bar_html;
          		  }
       		},"json" );
          
 			
          }
        
       
      // When initially loaded, use the data from "map11.php?q=a"
      readMap("a");
      
   


   $(document).ready(function(){
	   $("#live-down").click(function(){
	       $(".map-demo").slideToggle(150);
	       return false;
	   });
	   $(document).click(function(){
	       $(".map-demo").slideUp(150);
	   });

	  
	   });
    </script>

 <?php $this->load->view('common/footer');?>