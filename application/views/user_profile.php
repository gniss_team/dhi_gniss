<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<div class="content-body-wrapper-dashboard clearfix">
<div class="content clearfix">
	<div class="page-title"><h4>Profile</h4></div>
	<div class="content-wrap clearfix">
		<?php if($msg!="") { ?>
		<div class="alert alert-success"> 
			<?php echo $msg; ?>
		</div>
		<?php } ?>
		<div class="form-holder">
			<?php echo form_open_multipart('profile/action/update/'); ?>
			<div class="form-space">
				
				<span class="asterisk-msg">All fields marked with * are mandatory.</span>
				<div class="form-grp-profile">
					<label>Gniss ID <span class="red">*</span></label>
					<input type="text" class="form-control-readonly form-control-profile" maxlength="20" placeholder="GNISS ID" name="account_number" id="account_number" readonly="readonly" value="<?= $user_info->account_number; ?>">
					<?php echo form_error('account_number'); ?>
				</div>
       			<div class="form-grp-profile">
					<label>First name <span class="red">*</span></label>
					<input type="text" class="form-control-profile" maxlength="20" placeholder="First name" name="first_name" id="first_name" value="<?= $user_info->first_name; ?>">
					<?php echo form_error('first_name'); ?>
				</div>
				
				<div class="form-grp-profile"> 
					<label>Last name <span class="red">*</span></label>
					<input type="text" class="form-control-profile" maxlength="20" placeholder="Last name" id="last_name" value="<?= $user_info->last_name; ?>" name="last_name">
					<?php echo form_error('last_name'); ?>
				</div>
				
				<div class="form-grp-profile">
					<label>Email address <span class="red">*</span></label>
					<input type="text" class="form-control-readonly form-control-profile" maxlength="40" placeholder="Email address" id="email_address" name="email_address" readonly="readonly" value="<?= $user_info->email_address; ?>">
					<?php echo form_error('email_address'); ?>
				</div>
				
				<div class="form-grp-profile">
					<label>Username<span class="red">*</span></label>
					<input type="text" class="form-control-readonly form-control-profile" maxlength="40" placeholder="Username" id="username" name="username" readonly="readonly" value="<?= $user_info->username; ?>">
					<?php echo form_error('email_address'); ?>
				</div>
				
				<div class="form-grp-profile">
					<label>Mobile number <span class="red">*</span></label>
					<input type="text" class="form-control-profile" maxlength="10" placeholder="Mobile number" id="mobile_number" name="mobile_number" value="<?= $user_info->mobile_number; ?>">
					<?php echo form_error('mobile_number'); ?>
				</div>
				
				<div class="form-grp-profile">
					<label>Street address </label>
					<textarea type="text" class="" placeholder="Street address" id="street_address" name="street_address" maxlength="105"><?php  echo $user_info->street_address ?></textarea>
		    		<?php echo form_error('street_address'); ?>
				</div>
				
				<div class="form-grp-profile">
					<label>Country <span class="red">*</span></label>
					<select class="form-control-profile-select styled" id="country_id" name="country_id">
						<option value="" >Select country</option>
						<?php foreach( $countries as $country ) { ?>
						<option value="<?= $country->id; ?>" <?php if($country->id == $user_info->country_id) { ?> selected="selected" <?php } ?> ><?= $country->name; ?> </option>
						<?php } ?>
					</select>
					<?php echo form_error('country_id'); ?>
				</div>
				
				<div class="form-grp-profile">
					<label>State <span class="red">*</span></label>
					<select class="form-control-profile-select styled" id="state_id" name="state_id" >
						<option value="" >Select state</option>
						<?php foreach( $states as $state ) { ?> 
						<option value="<?= $state->id; ?>" <?php if($state->id ==$user_info->state_id) { ?> selected="selected" <?php } ?> ><?= $state->name; ?></option>
						<?php } ?>
					</select>
					<?php echo form_error('state_id'); ?>
				</div>
				
				<div class="form-grp-profile">
					<label>City <span class="red">*</span></label>
					<select class="form-control-profile-select styled" id="city_id" name="city_id">
						<option value="" >Select city</option>
						<?php foreach( $cities as $city ){ ?>
						<option value="<?= $city->id; ?>" <?php if( $city->id == $user_info->city_id ) { ?> selected="selected" <?php } ?> ><?= $city->name; ?></option>
						<?php } ?>
					</select>
					<?php echo form_error('city_id'); ?>
				</div>
				
				<div class="form-grp-profile">
					<label>Zip Code </label>
					<input type="text" class="form-control-profile" placeholder="Zip code" id="zipcode" name="zipcode" maxlength="50" value="<?= $user_info->zipcode; ?>" >
					<?php echo form_error('zipcode'); ?>
				</div>	
		
				<div class="btn-grp">
					<button class="signup" type="submit" class="btn btn-primary">Update</button>
					<button class="signup" type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>welcome'">Cancel</button>
				</div>
		
			</div>
		
			<div class="profile-holder">
				<?php $profile_picture = $user_info->profile_picture;
				if( 'default.png' == $profile_picture ) {
					$profile_picture_path = 'images/'.$profile_picture;	
				} else {
					$profile_picture_path = 'uploads/'.$profile_picture;
				}?>
        		<img width="160px" height="120px" src="<?php echo base_url().$profile_picture_path; ?>" id="thumb" />
        		<div class="pro-txt">Change your profile image</div>
       		
				<label class="browse-button"><input maxlength="20" type="file"  id="profile_img" name="profile_img" onchange="readURL(this);">Browse</label>
			</div>
		
		</div>
		<?php  echo form_close(); ?>
	</div>
</div>
</div>
<?php $this->load->view('common/footer');?>
<style>
	.styled{
		width:42.6%;
	}
	textarea {
		width:40%;
	}
	label {
		float:left;
		width:20%; 
	}
	.form-control-profile {
		/*width:auto !important;*/
		width:39%!important;
	}
	.error {
		text-indent:19%;
	}
	</style>
<script type="text/javascript">
var strUrl = "<?php echo base_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/locations.js"></script>   
<script type="text/javascript">
 function readURL(input) {
 	if (input.files && input.files[0]) {
    	var reader = new FileReader();
		reader.onload = function (e) {
        	$('#thumb').attr('src', e.target.result);
        }
    reader.readAsDataURL(input.files[0]);
    }
 }
 </script>   
 <script type="text/javascript">
$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>