<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<script>	 
var base_url = "<?php echo base_url(); ?>";
var device_type_id	= "<?php echo $devices_type_id; ?>";
</script>
<script src="<?php echo base_url(); ?>js/pagination.js"></script>

<div class="content clearfix">
<div class="title-box">
	<div class="page-title"><h4><span style="text-transform:capitalize"><?php echo $device_name;?> </span>Devices</h4></div>
	<div class="track-btn-container"> 
	<button class="track-but" type="button" onclick="window.location='<?php echo base_url(); ?>devices/action/add'">Add Device</button></div>
	</div><br/><div style="display:inline-block;margin-bottom:14px;">
				<div class="fault_div red" >Fault</div>
				<div class="fault_div green" >OK</div></div>
	
	<div class="content-wrap clearfix">
        <?php if($msg!="") { ?>
			<div class="alert alert-success">
			    <?php echo $msg; ?>
			</div>
		<?php } ?>
		<div style="margin:0px 2px 14px; ">
        	<label>Show </label>
            <select class="form-control" name="select_pagination" id="select_pagination">
		                                     <option value="10">10</option>
							<option value="25">25</option>
							<option value="50">50</option>
							<option value="100">100</option>
            </select>
               <label>Entries </label> 

            <select  class="styled-status" name="device_status" id="device_status">
            <option value="">Device Statuse</option>
          	 <option value="0">Fault</option>
				<option value="1">OK</option>
            </select>
      
          <div style="float:right;"><label>Search: <input type="search"   name="search" aria-controls="myTable"></label></div></div>
         <div id="myTable_filter" class="dataTables_filter">
					<div id="mhead">
					
					</div>
					<div id="pagination"></div>
			</div>
    </div>
</div>

<?php $this->load->view('common/footer');?>
