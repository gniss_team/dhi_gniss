<?php $this->load->view('common/header');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Manage Assets</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>welcome">Dashboard</a></li>
			<li><a class="big" href="<?php echo base_url(); ?>company/action/viewcompanies">Manage Company</a></li>
			<li>View Assets</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
		<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	    	<thead>
            	<tr>
                	<th style="display:none">Id</th>
                    <th>Asset Name</th>
                    <th>Sim Number</th>
                    <th>Asset Number</th>
                    <th>Phone Number</th>
                     <th>IMEI Number</th>
                     <th>Model Number</th>
                     <th>Asset Mode</th>
                     <th>Status</th>
			     	
                </tr>
            </thead>
			<tbody>
			<?php foreach ( $company_assets as $objCompanyAsset){?>
            	<tr class="active">
                	<td style="display:none"><?php echo $objCompanyAsset->id; ?></td>
                    <td><?php echo $objCompanyAsset->name; ?></td>
                    <td><?php echo $objCompanyAsset->sim_number; ?></td>
                    <td><?php echo $objCompanyAsset->asset_number; ?></td>
                    <td><?php echo $objCompanyAsset->phone_number; ?></td>
                    <td><?php echo $objCompanyAsset->imei_number; ?></td>
                    <td><?php echo $objCompanyAsset->model_number; ?></td>
                    <td><?php echo $objCompanyAsset->mode; ?></td>
                     <td><?php echo $objCompanyAsset->status; ?></td>
                    
 				</tr>
            <?php } ?>
            </tbody>
        </table>
	</div>
</div>
<?php $this->load->view('common/footer');?>