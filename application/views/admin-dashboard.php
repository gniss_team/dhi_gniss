<?php $this->load->view('common/header');?>

<div id="container">
	<h1>Administrator Dashboard <span style="margin-left:300px; ">Welcome Administrator</span></h1>
	<div id="body">
	<?php if(isset($msg)) { ?>
					<div class="alert alert-success" >
						 <?php echo $msg; ?>
					</div>
					<?php } ?>
			<span><a href="<?php echo base_url(); ?>welcome/logout">Log Out</a> / 
			<button class="btn btn-xs btn-success" type="button" onclick="window.location='<?php echo base_url(); ?>admin/action/viewcompanies'">View Companies</button>
	</div>
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>

<?php $this->load->view('common/footer');?>