<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- responsive -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!-- responsive style-->
<title>GNISS-Global Network Information Security System</title>
<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>css/responsive-style.css" rel="stylesheet" />

<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">	
<script src="<?php echo base_url(); ?>js/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/custom.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>

<script>
$(document).ready(function(){
$(".profile-styled").click(function(){
    $("#login-panel").slideToggle(150);
    return false;
});
$(document).click(function(){
    $("#login-panel").slideUp(150);
});
});

</script>

</head>
<body style="background:#0e4566">
<?php 
	$strMethod	= $this->uri->segment(3);
    $userId = $this->session->userdata('user_id');
    if( empty( $userId ) ) { ?><div class="watermark">
	<div class="main-wrapper" style="background:none">
		<div class="header">
    		<a href="<?php echo base_url(); ?>"><div class="logo-white"></div></a>
    	</div>
    </div>
    	
<?php } else { ?>
		
		<div class="content-body-wrapper-dashboard">
    		
        <?php
            $profile_picture = $this->arrmixInfo['profile_info']->profile_picture;
			if( 'default.png' == $profile_picture ) {
					$profile_picture_path = 'images/'.$profile_picture;	
			} else {
					$profile_picture_path = 'uploads/'.$profile_picture;
			}
		?>
		
            <div class="all-grp">
            <div class="ac-styled">GNISS ID:<?php echo $this->session->userdata('account_number');?></div>
            <div class="profile-styled">
                    	<img src="<?php echo base_url().$profile_picture_path;?>" />
            	<a href="#"><span><?php echo $this->session->userdata( 'name'); ?></span></a>
          	</div>
            
			<div id="login-panel">
			 <div style ="left: 22%; position: absolute;top: -15px;"><img src="<?php echo base_url();?>images/panel-arrow.png" /></div>
            <ul>
           		<li><a href="<?php echo base_url(); ?>profile/action/view">Profile</a></li>

                <li><a href="#">Settings</a></li>
           		<li><a href="<?php echo base_url(); ?>users/action/changepassword">Change Password</a></li>
                <li><a href="<?php echo base_url(); ?>welcome/logout">Logout</a></li>
            </ul>
        	</div>
        </div>
		
     </div>
   
<?php } ?>


<div class="createacnt-container">
<div class="account-txt">Create your <span>GNISS</span> account</div>
<div class="content-login-wrapper">
	<div class="content-inner-wrapper clearfix">
		
   		<?php if( true == isset( $msg )  && false == empty( $msg )) { ?>
			<div class="alert alert-success" style="float:left;" >
				 <?php echo $msg; 
				 ?>
			</div>
	<?php } ?>
	<?php if(isset( $errmsg ) && $errmsg != '' ) { ?>
		<div class="alert alert-error" >
		 <?php echo $errmsg; ?>
	</div>
	<?php } ?>
    	<div style="width:100%; float:left;">                    
			<?php echo form_open('users/action/addUser'); ?> 
			<div class="form-hide">
				
					<input class="signup-input-length" type="text" placeholder="First name" name" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>"/>
					<?php echo form_error('first_name'); ?>
				
                
                	<input class="signup-input-length" type="text" placeholder="Last name" id="last_name" value="<?php echo set_value('last_name'); ?>" name="last_name"/>
                	<?php echo form_error('last_name'); ?>
                
		
			<div class="signup-input-box" style="clear:both;">
				<input class="signup-input-length" type="email" placeholder="Email" id="email_address" name="email_address" maxlength="40" value="<?php echo set_value('email_address'); ?>"/>
				<?php echo form_error('email_address'); ?>
			</div>
			<div class="signup-input-box">
				<input class="signup-input-length" type="text" placeholder="Mobile number" id="mobile_number" maxlength="10" name="mobile_number" value="<?php echo set_value('mobile_number'); ?>"/>
				<?php echo form_error('mobile_number'); ?>
			</div>
			<div class="signup-input-box">
				<input class="signup-input-length" type="text" placeholder="Username" id="username" name="username" maxlength="40" value="<?php echo set_value('username'); ?>"/>
				<?php echo form_error('username'); ?>
			</div>
			<div class="signup-input-box">
				<input class="signup-input-length" type="password" class="password" placeholder="Password" id="password" name="password" maxlength="20" value="<?php echo set_value('password'); ?>"/>
				<?php echo form_error('password'); ?>
			</div>
			<div class="signup-input-box">
				<input class="signup-input-length" type="password" class="password" placeholder="Confirm password" id="conf_password" name="conf_password" maxlength="20" value="<?php echo set_value('conf_password'); ?>"/>
				<?php echo form_error('conf_password'); ?>
			</div>
<!-- 			<div class="captcha-img"> -->
<!-- 				<img src=" --><?php //echo $captcha['image_src'];?><!-- " alt="CAPTCHA security code" /> -->
<!-- 			</div> -->
<!--             <div class="captcha-txt"> -->
<!--                	<input class="signup-input-length" type="text" placeholder="Captcha" id="captcha" name="captcha" maxlength="25" value=" --><?php //echo set_value('captcha'); ?><!-- "/>-->
               	<?php //echo form_error('captcha'); ?>
<!--             </div> -->
    		<div class="reg-but">
    			<button type="submit" name="add" class="signup-new">Create Account</button>
    		</div>
    		<?php  echo form_close(); ?>
		</div>
	</div>
</div>
</div>
</div>
</div>

<style>.error{text-indent:0;}</style>
<script type="text/javascript">
	    
var strUrl = "<?php echo base_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/locations.js"></script>
<script  type="text/javascript">
if($('.alert alert-success').length > 1 ) {
		$('.alert alert-success').fadeOut('5000');
}		
</script>
<div style="background:#fff; margin:0; padding:10px;">
<?php $this->load->view('common/footer');?>
</div>

