<?php $this->load->view('common/header');?>
<div class="content-login-wrapper-forgot">
<?php
	$radioQues = "display:none";
	$radioAns = "display:none";
	$attributes = array('class' => 'form-signin');     	
	echo form_open('users/action/checkSecurityQuestion', $attributes);?>
    <?php 	if( true == isset($msg) && false == empty( $msg) ) { ?>
        <div class="alert alert-success" >
          	<?php echo $msg; ?>
        </div>
    <?php } ?>
    <?php if( true == isset( $errmsg ) && false == empty( $errmsg ) ) { ?>
         <div class="alert alert-error" style="color:red;" >
         <?php echo $errmsg; ?>
         </div>
     <?php } ?>
          		
	<div class="content-inner-wrapper clearfix">
		<div class="typ-for">
			<div class="forgot" ><h2 class="for-pas">Forgot Password</h2></div>
				<div class="indi-for">
					<?php 	if( 0 < count($security_question) ) {  ?>
					<input type="radio" name="radio_toggle" value="1" id="radio_ques"  <?php 
    			if( true == isset($radio_toggle) && '1' == $radio_toggle) {
				$radioQues = "display:block"; ?>
        		checked="checked" 
    		<?php } ?>/>
			Answer a security question
			<?php } ?>
									                     
				<input type="radio" name="radio_toggle" value="2" id="radio_reset"  <?php 
    if( true == isset($radio_toggle) && '2' == $radio_toggle ) {
$radioAns	= "display:block";	
 ?>
        checked="checked" 
    <?php } ?>/>
Receive a reset link via email  
</div> 
	<?php echo form_error('radio_toggle'); ?>                     
	 </div>
						                 	
	<div id='div_toggle1'  style="<?php echo $radioQues; ?>" class="toHide">
    	<?php 	if( 0 < count($security_question) ) { ?>
        <div class="question">
			<p><b>Security Question:</b> <span class="green"><?php echo $security_question->question; ?>?</span></p>
     	</div>
        <div class="answer">
        	<h4 class="sec" ><b>Answer: </b><input style="margin:0px 12px;" class="ans-type" type="text" placeholder="Answer for security question" name="answer" value="<?php echo set_value('answer'); ?>" ><span style="margin: 0 !important;position: relative; left: 45px"><?php echo form_error('answer'); ?></span></h4>
        </div> 
        <?php } ?>
    </div>
    <div id='div_toggle2'  style="<?php echo $radioAns; ?>" class="toHide">
    	<div class="div_reset">An email with a reset link will be sent to your registered email id.</div>
    </div>
    <div class="comp-typ" ><?php 	if( 0 < count($security_question) ) { ?>
		<input class="form-control" type="hidden" id="question_id" name="question_id" value="<?php echo  $security_question->id; ?>"/>
	<?php } ?>
	<input class="form-control" type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>"/>
	<input class="form-control" type="hidden" id="login_account_type" name="login_account_type" value="<?php echo $login_account_type; ?>"/>
	</div>
	<input name="admin" type="submit" value="Submit" />
    <input class="signup" name="submit" type="reset" value="Cancel"  onclick="window.location='<?php echo base_url(); ?>'" />
</div>
			
</div>
     <?php  echo form_close(); ?>

</div>
<script type="text/javascript" >
$(function() {

if( 2 == $( "input:checked" ).val() ){
	if ( $('#div_toggle'+$( "input:checked" ).val()).length > 0 ){
        
        $('.toHide').hide();
        $("#div_toggle"+$( "input:checked" ).val()).show('slow');
       	$('.error').fadeOut('5000');
    }
}

    $("[name=radio_toggle]").click(function(){
    	 // $('.toHide').hide();
    	if ( $('#div_toggle'+$(this).val()).length > 0 ){
           
            $('.toHide').hide();
            $("#div_toggle"+$(this).val()).show('slow');
            $('.error').fadeOut('5000');
        }
    });
            
 });
</script>
<style>
.error {
	width:auto;
   	margin:6px 0px 6px 27px;
}	                	
</style>
<?php $this->load->view('common/footer');?>
