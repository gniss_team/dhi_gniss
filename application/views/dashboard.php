<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<script src="<?php echo base_url(); ?>js/jQuery.circleProgressBar.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>graph_assets/jquery-ui.css" />
<link type="text/css" href="<?php echo base_url(); ?>js/jquery.datetimepicker.css" rel="stylesheet" />
<link type="text/css" href="<?php echo base_url(); ?>graph_assets/jquery.jqplot.min.dashboard.css" rel="stylesheet" />
<link type="text/css" href="<?php echo base_url(); ?>css/simpletabs.css" rel="stylesheet" />

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<style type="text/css">
.icon-holder{
margin: 0px 5%; 
background: #9ab9ed; 
border-radius: 200px; 
padding: 10px 20px 28px 10px;
height: 114px; 
width: 122px;
}

.jqplot-series-shadowCanvas{display:none;}
select{ font-size: 13px;
    margin: 6px 5px;
    padding: 3px 4px;
    width: 12%;}
</style>
<script>
$(function() {
    $( ".parent" ).sortable();
    $( ".parent" ).disableSelection();
  });
</script>
<link type="text/css" href="<?php echo base_url(); ?>graph_assets/jquery.jqplot.min.css" rel="stylesheet" />
<div style="min-height:450px;">
	<h4>Welcome to gniss</h4>
    <form>
	<div style="margin-bottom:20px;">
		<select class="form-control-select styled" name="device_type_id" id="device_type_id">
            <?php foreach( $devices_types as $devices_type ) { ?>
            <option value="<?php echo $devices_type->unit_type; ?>" <?php if( true == isset( $device_type_id ) && $devices_type->unit_type == $device_type_id ) { ?> selected="selected" <?php } ?>><?php echo $devices_type->name; ?></option>
			<?php } ?>
		</select>
        <?php if( true == isset( $devices ) ) { ?>
		<select class="form-control-select styled" name="device_id" id="device_id">
		<option value="">Select Device</option>
		<?php foreach( $devices as $device ) { ?>
			<option value="<?php echo $device->device_number; ?>" <?php if( $device->device_number == $device_id ) { ?> selected="selected" <?php } ?>><?php echo $device->name; ?></option>
        <?php } ?>
       	</select>
        <?php }  ?>
	</div>
	<div class="inside-wrapper"> <!-- parent -->
		<div class="inside-block">
			<div class="icon-holder">
				<img src="<?php  echo base_url() ;?>images/solar-icon.png"/>
			</div>
		</div>
        <div class="inside-block-graph">
      		<div class="simpleTabs">
	 			<ul class="simpleTabsNavigation">
		        	<li><a href="javascript:void(0)" id="voltage_graph" >Real time voltage</a></li>
		           	<li><a href="javascript:void(0)" id="watt_hour" >Todays Watt </a></li> 
			    </ul>
		        <div class="simpleTabsContent">
		        	<div id="livedata"></div>
		        	<div id="live"></div>
		        </div>
		        <div class="simpleTabsContent">
					<div id="watthour"></div>
				</div>
			</div>
		</div>
    	<div class="inside-block-sts">
        	<ul>
        		<li style="text-transform:uppercase; font-size:14px;margin-bottom:10px;">Quick Summary</li>
        		<li style="background:#f8f8f8;padding:5px;font-size:14px;">Energy Today</li>
        		<li style="margin-bottom:10px;font-size:12px;color:#282828;padding:5px;">3.78 Units</li>
        		<li style="background:#f8f8f8;padding:5px;font-size:14px;">Energy this month</li>
        		<li style="margin-bottom:10px;font-size:12px;padding:5px;">74.81 Units</li>
        		<li style="background:#f8f8f8;padding:5px;font-size:14px;">Total Energy</li>
        		<li style="margin-bottom:10px;font-size:12px;padding:5px;">2.5 K Units</li>
        	</ul>
    	</div>

    	<div class="inside-block-piegraph"> 
       		<span style="margin-left:28%;font-size:18px;color:#666;font-family:'Trebuchet MS',Arial,Helvetica,sans-serif;">Watts</span>
			<div style="width:250px;height:250px;margin:18px auto;">
				<div class="percent" style="width:250px;height:250px;"> 
        			<p style="display:none;">40%</p>
    			</div>
			</div>
		</div>
    
    	<div class="inside-block-graph">
    		<div style="margin-left:10%;margin-top:10%;" id="voltage_pl"></div>
    	</div>
    	<div class="inside-block-sts">
			<ul>
        		<li style="text-transform:uppercase; font-size:14px;margin-bottom:10px;">Devices</li>
        		<li style="font-size: 13px; margin-bottom:15px;background:#f8f8f8;padding:5px;">ACES</li>
         		<li style="font-size: 13px;margin-bottom:15px;">Energy Meter</li>
         		<li style="font-size: 13px;margin-bottom:15px;background:#f8f8f8;padding:5px;">Cvein</li>
         		<button style="background:#252525;border:0;color:#fff;padding:2px;">List all Devices</button>
        	</ul>		
        	
		</div>
	</div>
     <!-- dashboard content area end-->
</div>
</form>
<style>
form select {
	min-width:10%;
}
ul.simpleTabsNavigation li a {
    padding: 7px 3px;
}
form ul li {
	list-style-type:none;
}
/* div.simpleTabsContent.currentTab { */
/*     display: block; */
/*     width: 40%; */
/* } */
</style>
<script type="text/javascript" >
$( '#device_id' ).change( function() { 
		var device_type_id = $('select#device_type_id option:selected').val();
		var device_id = $(this).val();
		window.location='<?php echo base_url(); ?>welcome/action/displayGraph/'+device_type_id+'/' +device_id;
		
});

$( '#device_type_id' ).change( function() { 
		var device_type_id = $('select#device_type_id option:selected').val();
		var device_id = $(this).val();
		window.location='<?php echo base_url(); ?>welcome/action/displayGraph/'+device_type_id+'/' +device_id;
		
});

$( "#watt_hour" ).click(function() {
		getHourlyWatt();
});

function getHourlyWatt() {
	var device_id = $('select#device_id option:selected').val();

	$.get( "<?php echo base_url();?>welcome/action/viewWattHourGraph/"+device_id, function( data ) {
		  //alert( "Load was performed." );
		  $("#watthour").html( data );
		});
}

var type;
var graph_type;
$( "voltage_graph" ).click(function() {

	var device_type_id = $('select#device_type_id option:selected').val();
	var device_id = $('select#device_id option:selected').val();
		 var type = 'voltage';
		 var graph_type	= 'line';
			$.get( "<?php echo base_url();?>welcome/action/viewLiveGraph/"+type+"/"+graph_type+"/"+device_type_id+'/'+device_id, function( data ) {
			  $("#livedata").html( data );
			});
	//setTimeout(getdata, 5000);
	});

$( "#current_graph" ).click(function() {
	//$('#line_graph').submit();
	 var type = 'current';
	var  graph_type	= 'line';
	$.get( "<?php echo base_url();?>welcome/action/viewLiveGraph/"+type+"/"+graph_type, function( data ) {
		  $("#livedata").html( data );
		});
	//setTimeout(getdata, 5000);
	});

	function getlivegraph() { 
		
		var device_type_id = $('select#device_type_id option:selected').val();
		var device_id = $('select#device_id option:selected').val();
		
		var type = 'voltage';
		var graph_type = 'line';
		$.get( "<?php echo base_url();?>welcome/action/viewLiveGraph/"+type+"/"+graph_type+"/"+device_type_id+'/'+device_id, function( data ) {
			  //alert( "Load was performed." );
			  $("#livedata").html( data );
			});
	}
	function getpiegraph(){
		var type = 'voltage';
		var graph_type = 'pie';
		$.get( "<?php echo base_url();?>welcome/action/viewLiveGraph/"+type+"/"+graph_type, function( data ) {
			  //alert( "Load was performed." );
			  $("#livedata").html( data );
			});

	}
    // end here 
	// live stat for current and voltage using meter graph
	function getlivestat() {
		var device_id = $('select#device_id option:selected').val();
		$.get( "<?php echo base_url();?>welcome/action/ajaxgetEnergyStats/"+device_id, function( data ) {
			var obj = $.parseJSON(data);
			//  $("#current_pl").html( data );
			var s1 = [obj.voltage];
			if( 0 != s1 ) {
		    var plot3 = $.jqplot('voltage_pl', [s1], {
	    	 title: 'Latest Voltage',
	        seriesDefaults: {
	            renderer: $.jqplot.MeterGaugeRenderer,
	            rendererOptions: {
	           //     min: 0,
	            //    max: 140,
	            // intervals: [40, 80, 120, 140],
	            //    intervalColors: ['#66cc66', '#93b75f', '#E7E658', '#cc6666'],
	                smooth: true,
	                animation: {
	                    show: true
	                }
	            }
	        }
	    });
			} else {
				 $("#voltage_pl").html( 'No record found' );
				 $("#voltage_pl").css("color", "red");
			}
	   
			});
	}

	function getVoltageVsCurrent() {
		var device_id = $('select#device_id option:selected').val();
		$.get( "<?php echo base_url();?>welcome/action/viewVoltageVsCurrent/"+device_id , function( data ) {
			  //alert( "Load was performed." );
			  $("#volcurrent").html( data );
			});
	}
  // end here 
// for date range graph
$('#line_graph').submit( function() {
	$('#loader').show();
	$("#chart1").html("");
	$.post($('#line_graph').attr('action'), $('#line_graph').serialize(), function( data ) {				
	}).done(function( data ) {

			$('#loader').hide();
			$("#error_mg").html(data);
	
	  });
	return false;			
});
  
$(document).ready(function(){
	
		getlivegraph();
		setInterval(getlivegraph, 20000);
	//	getpiegraph();
		getVoltageVsCurrent();
 	 	getlivestat();
// 		setInterval(getlivestat, 20000);

});
</script>
<script>

$(function () {
    $('.percent').percentageLoader({
        valElement: 'p',
        strokeWidth: 15,
        bgColor: '#d9d9d9',
        ringColor: '#fd9807',
        textColor: '#fd9807',
        fontSize: '35px',
        fontWeight: 'normal'
    });

});
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/simpletabs_1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.meterGaugeRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasAxisLabelRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.barRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.donutRenderer.min.js"></script>
<?php $this->load->view('common/footer');?> 