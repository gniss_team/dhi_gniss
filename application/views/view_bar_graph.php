<script type="text/javascript">

$(document).ready(function(){
       $.jqplot.config.enablePlugins = true;
        var ticks=[<?php for( $i=0 ;$i<count($energy_meter_data); $i++) {
                  if( $i != count($energy_meter_data) -1  ){
                  ?>
                    "<?php echo date('F jS Y', strtotime( $energy_meter_data[$i]->created_on ) );?>",

                   <?php } else {?>
                   "<?php echo date('F jS Y', strtotime( $energy_meter_data[$i]->created_on ) );?>"
                   <?php  } } ?>];
             
        <?php for( $j=0 ;$j <$cnt_graph_parameter; $j++) {  ?>

             var line<?php echo $j;?> = [<?php for( $i=0 ;$i<count($energy_meter_data); $i++) {
                        if( $i != count($energy_meter_data) -1  ){      ?>
                               <?php if( 'voltage' == $type[$j] || 'current' == $type[$j] ) { echo $energy_meter_data[$i]->$type[$j]/10; } else { echo $energy_meter_data[$i]->$type[$j];}?>,
                               <?php } else {?>
                              <?php if( 'voltage' == $type[$j] || 'current' == $type[$j] ) { echo $energy_meter_data[$i]->$type[$j]/10; } else { echo $energy_meter_data[$i]->$type[$j];}?>
                               <?php  }
                         } ?>];
             <?php } ?>
      //  var line2=['2015-07-03','2015-07-04']; 
        var plot2 = $.jqplot('chart1', [
                  <?php for( $k=0 ;$k < $cnt_graph_parameter; $k++) { 
                              if( $k != ($cnt_graph_parameter) - 1 ) { ?><?php echo 'line'. $k . ',';  }else { echo 'line'.$k; } } ?>], {
        	 stackSeries: false,
        	 seriesColors: ["rgba(78, 135, 194, 0.7)", "rgb(211, 235, 59)","rgb(102, 253, 4)","rgb(0, 0, 128)","rgb(191, 68, 136)","rgb(173, 136, 95)"], 
        	 title:'', 
   	      highlighter: {
                 show: true,
                // sizeAdjust: 1,
                // tooltipOffset: 9,
                 showTooltip: false
             },
             grid: {
            	 background: 'rgba(57,57,57,0.0)',
            	 drawBorder: false,
                 shadow: false
             },
             legend: {
                 show: true,
                 placement: 'outside'
             },
             seriesDefaults: {
            	  renderer:$.jqplot.BarRenderer,
                 rendererOptions: {
                     smooth: true,
                     animation: {
                         show: true
                     }
                 },
                 showMarker: false,
                 pointLabels: { show: false }
             },
             series: [ 
						<?php for($t=0;$t<count($type);$t++) { ?>
						{
						    label: '<?php echo $type[$t]; ?>'
						},
						<?php } ?>
             ],
              
            axes:{
              xaxis:{
                  renderer: $.jqplot.CategoryAxisRenderer,
                  ticks: ticks,
                  tickRenderer: $.jqplot.CanvasAxisTickRenderer,
               // ticks:line2,
                tickOptions:{pad: .2,showGridline: false,formatString:'%b %#d, %#I %M  %#p',fontSize:'8pt', fontFamily:'Tahoma', angle:50,textColor:'#666666',mark: 'outside'},
                min:'<?php echo $this->input->post('from_date');?>', 
               max:'<?php echo $this->input->post('to_date');?>', 
                tickInterval:'1 day',
                drawMajorGridlines: false
              },
              yaxis: {
            	    tickOptions: {
            	      textColor: '#666666'
            	    }
            	  },
            },
            
          //  series:[{lineWidth:1,size:2.0,shadowAngle:45, markerOptions:{style:'circle'}}]
           
        });
        $('#chart1').bind('jqplotHighlighterHighlight', 
                function (ev, seriesIndex, pointIndex, data, plot) {
                    // create some content for the tooltip.  Here we want the label of the tick,
                    // which is not supplied to the highlighters standard tooltip.
                    var content = plot.series[seriesIndex].label + ', ' + plot.series[seriesIndex]._xaxis.ticks[pointIndex] + ', ' + data[1];
                    // get a handle on our custom tooltip element, which was previously created
                    // and styled.  Be sure it is initiallly hidden!
                    var elem = $('#customTooltipDiv');
                    elem.html(content);
                    // Figure out where to position the tooltip.
                    var h = elem.outerHeight();
                    var w = elem.outerWidth();
                    var left = ev.pageX - w - 10;
                    var top = ev.pageY - h - 10;
                    // now stop any currently running animation, position the tooltip, and fade in.
                    elem.stop(true, true).css({left:left, top:top}).fadeIn(200);
                }
            );
        $('#chart1').bind('jqplotHighlighterUnhighlight', 
                function (ev) {
                    $('#customTooltipDiv').fadeOut(300);
                }
            );
      });

</script>
<style>
.blank_div{
      color: #757575;
    display: inline;
    font-size: 13px;
}
.jqplot-target {
     color:#666666;
}
.jqplot-target {
    margin: 30px;
}

#customTooltipDiv {
    position: absolute; 
    display: none; 
    color: #333333;
    font-size: 0.8em;
    border: 1px solid #666666; 
    background-color: rgba(255,255,255,255);
    padding: 2px;
}
.ui-widget-content {
    background:white !important;
}
</style>