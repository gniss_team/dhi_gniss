<?php $this->load->view('common/header');?>
<?php $this->load->view('common/template_style');?>
<div class="sub-content"> 
	<h2> About Us </h2>
	<div class="content-1">
		<div class="photo">
			<img src="<?php echo base_url(); ?>images/Rahul-Jaipal.png" />
			<h4>Rahul Jaipal</h4>
			<h5>Managing Director & CEO</h5>
		</div><!--photo-->
		<div class="text">
			<p>Rahul Jaipal, the founding director of Dhi Networks  Private Limited, is an entrepreneur with an impeccable track record of over 15 years that includes the setting up of several start-ups and building them into profitable companies. His extensive experience spans different industries including shipping, IT & ITES, recycling, trading, manufacturing and exports. </p>
			<a class="readmore" href="http://www.rectusenergy.com/rahul-jaipal/" target="_blank" title="Read more on Rahul Jaipal" role="link" >Read More…</a>
		</div><!--text-->
	</div><!--content-1-->
	
	<div class="content-2">
		<div class="photo">
			<img src="<?php echo base_url(); ?>images/Venkat-Venkataraman.png" />
			<h4>Venkat Venkataraman</h4>
			<h5>Director</h5>
		</div><!--photo-->
		<div class="text">
			<p>Venkat is a passionate entrepreneur who shares the Dhi Networks  Private Limited vision of creating cutting-edge solutions aimed at delivering significantly higher levels of energy efficiency than any other product currently available. With over 20 years of experience in the IT industry, he has taken on various roles spanning different functions including technology, software development, sales and marketing. </p>
			<a class="readmore" href="http://www.rectusenergy.com/venkat-venkataraman/" target="_blank" title="Read more on Rahul Jaipal" role="link" >Read More…</a>
		</div><!--text-->
	</div><!--content-2-->
            
	<div class="content-3">
		<div class="photo">
			<img src="<?php echo base_url(); ?>images/Suneet-Doshi.png" />
			<h4>Suneet Doshi</h4>
			<h5>Technical Director</h5>
		</div><!--photo-->
		<div class="text">
			<p>Suneet Doshi is the technical director spearheading all research and development initiatives at Dhi Networks  Private Limited Private Limited. With over 22 years of industry experience, Suneet brings a wealth of knowledge and information crucial to the development of cutting-edge technology. After completing his B.E. in Industrial Electronics from University of Pune, India, Suneet ventured into the world of marketing. </p>
			<a class="readmore" href="http://www.rectusenergy.com/suneet-doshi/" target="_blank" title="Read more on Rahul Jaipal" role="link" >Read More…</a>
		</div><!--text-->
	</div><!--content-3-->
</div><!--sub-content-->
<?php $this->load->view('common/footer');?>
