<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>

<style>

.error {
margin: 5px 0 5px 11%;  
}

.mrk-grp{float: left;
display: block;
width: 200px;
height: 65px;
margin: 10px;
background: #eee;
padding: 10px;}
.mrk-grp li{list-style:none; float:left; margin:10px 10px 0 0; display:block;}
#msg,#msg .error{margin-left:0px;}

</style>

<div class="content clearfix">
	<div class="page-title"><h4>Edit Device</h4></div>
	<div class=" clearfix">
		<span class="asterisk-msg">All fields marked with * are mandatory.</span>
		<?php 
		$attributes = array('id' => 'myForm');
		
		echo form_open_multipart('devices/action/update/' . $device->id, $attributes);?>
		<h5>Device details</h5>
        <div class="form-grp">
	    	<label>Device Name <span class="asterisk">*</span></label>
            <input class="form-control" placeholder="Name" name="name" id="name" maxlength="21" 
            	value="<?php echo $device->name =  ( true == is_array(  $this->input->post() ) ) ? $this->input->post( 'name' ) : $device->name;  ?>" >
                <?php echo form_error('name'); ?>
        </div>
	        
	    <div class="form-grp" id="dev_number">
		    <label>Device number/ Unit number<span class="asterisk"> *</span></label>
			<input class="form-control" name="device_number" id="device_number" placeholder="Device number" maxlength="20" 
			value="<?php  echo $device->device_number = ( true == is_array(  $this->input->post() ) ) ? $this->input->post( 'device_number' ) :  $device->device_number; ?>"  >
		    <?php echo form_error('device_number'); ?>
        </div>
        
        <div class="form-grp">
			<label>Sim number</label>
			<input class="form-control" name="sim_number" id="sim_number" placeholder="Sim number"  maxlength="20"
			value="<?php echo $device->sim_number	= ( true == is_array(  $this->input->post() ) )  ? $this->input->post( 'sim_number' ) : $device->sim_number; ?>"  >
			<?php echo form_error('sim_number'); ?>
		</div>
		
		<div class="form-grp">
		   	<label>Phone number<span class="asterisk">*</span></label>
			<input class="form-control" name="phone_number" id="phone_number" placeholder="Phone number"
			 value="<?php echo $device->phone_number	= ( true == is_array(  $this->input->post() ) )  ? $this->input->post( 'phone_number' ) : $device->phone_number; ?>"  >
		   	<?php echo form_error('phone_number'); ?>
		</div>
		
	    <div class="form-grp">
		   	<label>IMEI number<span class="asterisk">*</span></label>
			<input class="form-control" name="imei_number" id="imei_number" placeholder="IMEI number"
			 value="<?php echo $device->imei_number	= ( true == is_array(  $this->input->post() ) )  ? $this->input->post( 'imei_number' ) : $device->imei_number; ?>"  >
		   	<?php echo form_error('imei_number'); ?>
		</div>
		<h5>Contact</h5>
		<div class="form-grp"> 
		   	<label>Country<span class="asterisk">*</span></label>
			<select  class="form-control styled"   id="country_id" name="country_id">
				<option value=""> Select Country</option>
			<?php  foreach( $countries as $country ) { ?>
				<option value="<?php echo $country->id; ?>"
				<?php if( $country->id == $device->country_id = ( true == is_array(  $this->input->post() ) ) ?  $this->input->post('country_id') : $device->country_id) { ?> selected="selected" <?php } ?>><?php echo $country->name; ?> </option>
			<?php } ?>
			</select>
		<?php echo form_error('country_id'); ?>
		</div>
		
		<div class="form-grp">
			<label>State<span class="asterisk">*</span></label>
			<select class="form-control styled" id="state_id" name="state_id" >
				<option value=""> Select State</option>
			<?php  foreach( $states as $state ) { ?>
				<option value="<?php echo $state->id; ?>"
				<?php if( $state->id == $device->state_id = ( true == is_array(  $this->input->post() ) ) ? $this->input->post('state_id') : $device->state_id) { ?> selected="selected" <?php } ?>><?php echo $state->name; ?> </option>
			<?php } ?>
			</select>				    	
		    <?php echo form_error('state_id'); ?> 	
		</div>
		
		<div class="form-grp">
			<label>City<span class="asterisk">*</span></label>
			<select class="form-control styled" id="city_id" name="city_id" >
				<option value=""> Select City</option>
			<?php  foreach( $cities as $city ) { ?>
				<option value="<?php echo $city->id; ?>"
				<?php if( $city->id == $device->city_id = ( true == is_array(  $this->input->post() ) ) ? $this->input->post('city_id') : $device->city_id) { ?> selected="selected" <?php } ?>><?php echo $city->name; ?> </option>
			<?php } ?>
			</select>	
			<?php echo form_error('city_id'); ?>
		</div>
		
		<div class="form-grp">
	    	<label>Address Line 1<span class="asterisk">*</span></label>
			<input class="form-control" name="address_line1" id="address_line_1" placeholder="address_line_1"
			 value="<?php echo $device->address_line1 = ( true == is_array(  $this->input->post() ) ) ? $this->input->post( 'address_line1') : $device->address_line1; ?>"  >
      		<?php echo form_error('address_line1'); ?>
		</div>
		
		<div class="form-grp">
	    	<label>Address Line 2</label>
			<input class="form-control" name="address_line2" id="address_line_2" placeholder="address_line_2" 
			value="<?php echo $device->address_line2	= ( true == is_array(  $this->input->post() ) ) ? $this->input->post( 'address_line2') : $device->address_line2; ?>"  >
      		<?php echo form_error('address_line2'); ?>
		</div>
		
		<div class="form-grp">
	    	<label>Zip Code<span class="asterisk">*</span></label>
			<input class="form-control" name="zip_code" id="post_code" placeholder="zip_code" 
			value="<?php echo $device->zip_code	= ( true == is_array(  $this->input->post() ) ) ? $this->input->post( 'zip_code') : $device->zip_code; ?>"  >
			<?php echo form_error('zip_code'); ?>
		</div>
			<!-- code of upload marker -->
		<div id="vts_markers" style="display:none;">
			<div class="form-grp"><label></label><a id="load-dialog-link" class="form-control" href="#"style="text-decoration: none; color: green; font-weight: bold; border: none; outline: none; margin-left:5px;padding:14px 0;">Customize Marker </a></div>
		
		</div>
		
	    <div class="btn-grp"> 
        	<button type="submit" class="signup">Save</button>
        	<?php 
        		$strUrl = "";
        		if( 3 == $device->device_type_id ) {
        			$strUrl = base_url() .'assets/action/manage';
        		}
        		if( 1 == $device->device_type_id ){
        			$strUrl = base_url() .'devices/action/view/' . $device->device_type_id;
        		}
        	?>
           	<button type="reset" class="signup" onclick="window.location='<?php echo $strUrl; ?>'">Cancel</button>
        </div>
        
		<?php  echo form_close(); ?>	
	</div>
		<div id="dialog" title="Customize Marker" style="display:none;">
			<div class="form-holder">
			<?php 	
			$attributes = array('class' => 'upload_marker',  'id' => 'upload_marker');	
			echo form_open_multipart('',$attributes);	
			$thumbnail_icon	= $device->marker_icon;
			$device->marker_icon	=  ( true == is_array(  $this->input->post() ) ) ? $this->input->post( 'marker_icon') : strtolower( $device->marker_icon );
			?>
				<div>
				 <div id='msg' ></div>
					<div class="mrk-grp">
				    	<label id ="marker_label" style="text-align:left; font-size: 1em;display:block">Select Marker</label>
						<ul style="display: block;float: left;margin: 0;padding: 0;">
						<li><input type="radio"  name="marker_icon" value="map-marker-icon.png" <?php if( 'map-marker-icon.png' == $device->marker_icon ){  ?> checked="checked" <?php } else { if( true == is_array( $this->input->post() ) && 'map-marker-icon.png' == $this->input->post('marker_icon') ) { echo "checked='checked'"; } } ?>><img width="30" src="<?php echo base_url(); ?>images/map_icons/map-marker-icon.png" ></li>
		                <li><input type="radio" name="marker_icon" value="truck-icon.png" <?php if( 'truck-icon.png' == $device->marker_icon ){  ?> checked="checked" <?php } else { if( true == is_array( $this->input->post() ) && 'truck-icon.png' == $this->input->post('marker_icon') ) { echo "checked='checked'"; } } ?>><img width="30" src="<?php echo base_url(); ?>images/map_icons/truck-icon.png" ></li>
		                <li><input type="radio" name="marker_icon" value="car-icon.png" <?php if( 'car-icon.png' == $device->marker_icon ){  ?> checked="checked" <?php } else { if( true == is_array( $this->input->post() ) && 'car-icon.png' == $this->input->post('marker_icon') ) {	echo "checked='checked'"; } } ?> ><img width="30" src="<?php echo base_url(); ?>images/map_icons/car-icon.png"></li>
						</ul>
					</div>	
					<?php 
					$arrstrMarkerIcon	= array('car-icon.png', 'truck-icon.png', 'map-marker-icon.png');
					if( false == empty( $device->marker_icon ) && true == in_array( $device->marker_icon, $arrstrMarkerIcon ) ) { 
						$device->marker_icon = ''; 
						$thumbnail_icon = 'default_map_icon.png';
					}
					if( true == empty( $device->marker_icon ) && false == in_array( $device->marker_icon, $arrstrMarkerIcon ) ) {
						//$device->marker_icon = '';
						$thumbnail_icon = 'default_map_icon.png';
					}
					//echo 'thumbnail_icon : ' . $thumbnail_icon;
					?> 	
			
				<div class="mrk-grp">
			    	<label id ="marker_label" style="text-align:left; font-size: 1em; display:block">Set profile picture as marker</label>
					<ul style="display: inline-block;float: left;margin: 0;padding: 0;">
						<li ><input type="radio" name="marker_icon" value="<?php echo $device->marker_icon;?>"
							<?php 
								//$arrstrMarkerIcon	= array('car-icon.png', 'truck-icon.png', 'map-marker-icon.png');
								if( false == empty( $device->marker_icon ) && false == in_array( $device->marker_icon, $arrstrMarkerIcon ) ) {  ?> checked="checked" <?php } ?> >
								<?php 	
							 if( true == is_array( $this->input->post() ) && false == empty( $_FILES['marker_img']['name'] ) ) { echo 'yes';
								$imageData = file_get_contents($_FILES['marker_img']['tmp_name']); 
								echo sprintf('<img id="thumb" width="30px" height="30px" src="data:marker_img/png;base64,%s" />', base64_encode($imageData));
							 } else {  //echo base_url() . 'images/map_icons/' . $thumbnail_icon;?>
								<img width="30px" height="30px" src="<?php echo base_url() . 'images/map_icons/' . $thumbnail_icon; ?>" id="thumb" />
						
							<?php } ?>	
						</li>
						</ul>
						</div>
						<div class="mrk-grp">
						<label></label>
							<ul class="browse-button" style="display: inline-block;float: left;margin: 0px;padding: 0px 4px 10px 14px;">
						<li><input  maxlength="20" type="file"  id="marker_img" name="marker_img" onchange="readURL(this);">Upload Marker						
						</li>
					</ul>
					
				</div>
			</div>
			
			<div class="btn-grp" style="margin: 0px 10px 10px; float:left">
				<button class="signup" id="btnUpdate" type="button" class="btn btn-primary">Update</button>
				<button class="signup" type="reset" class="btn btn-default">Cancel</button>
			</div>
			<?php  echo form_close(); ?>
		</div>
			
	</div>
</div>
</div>
 <script type="text/javascript">
var strUrl = "<?php echo base_url(); ?>";
var device_id = <?php echo $device->id;?>;
</script>
 <script src="<?php echo base_url(); ?>js/loader_image.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/locations.js"></script>   
<script type="text/javascript">

function readURL(input) {
 	if (input.files && input.files[0]) {
    	var reader = new FileReader();
		reader.onload = function (e) {
        	$('#thumb').attr('src', e.target.result);
        }
    reader.readAsDataURL(input.files[0]);
    }
 }
 
function checktype(val,flag) {
	if(flag=='0') {
		$('#name').val("");
		$('#device_number').val("");
		$('#sim_number').val("");
		$('#phone_number').val("");
		$('#imei_number').val("");
		$('#address').val("");
	}
	if(val==3) {
		$('#vts_markers').show();
		$('#vts_field').show();
		 // $('#dev_number').hide();
	} else {
		$('#vts_markers').hide();
		$('#vts_field').hide();
	 // $('#dev_number').show();
	  
  }
}

$(document).keydown(function(e) {

    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});

$( document ).ready(function() {
	var val = <?php echo $device->device_type_id; ?>

	var flag="1";
	checktype(val,flag);

});
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css" />
<script src="<?php echo base_url(); ?>js/external/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>

<?php $this->load->view('common/footer');?>
<script type="text/javascript">
$( document ).ready(function() {
	$( "#dialog" ).dialog({
		autoOpen: false,
		width: 265,
		modal: true,
		buttons: [	],
		beforeClose: function(event, ui) { 
			ajaxindicatorstart('loading data.. please wait..');
				if( 0 != $('#dialog').find( '.alert-success' ).text().length ){
					window.parent.location.reload();
				}
				ajaxindicatorstop();
            }
	});

	$("button[type='reset']").on("click", function(){
		ajaxindicatorstart('loading data.. please wait..');
		if( 0 != $('#dialog').find( '.alert-success' ).text().length ){
			$( '#dialog' ).dialog( "close" );
			window.location.reload();
			ajaxindicatorstop();
		} else {
			$('#dialog').find( '.success' ).text('');
			$('#dialog').find( '.error' ).text('');
			$( '#dialog' ).dialog( "close" );
		}
	
	});
	
	//Link to open the dialog
	$( "#load-dialog-link" ).click(function( event ) {
		$( "#dialog" ).dialog( "open" );
		event.preventDefault();
	});

	$("#btnUpdate").on("click", function(event) {
		//ajaxindicatorstart('loading data.. please wait..');
		event.preventDefault();

	    $.ajax({
	    	url: strUrl + "devices/action/updateMarker/" + device_id, // Url to which the request is send
	    	type: "POST",             // Type of request to be send, called as method
	    	data: new FormData($('form')[1]), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
	    	contentType: false,       // The content type used when sending data to the server.
	    	cache: false,             // To unable request pages to be cached
	    	processData:false,        // To send DOMDocument or non processed data file it is set to false
	    	success: function(d)   // A function to be called if request succeeds
	    	{ 
	    		
				var d = JSON.parse(d);
		          
            	if( d.error=='true' ) {
            		
              		$('#msg').html( d.msg );
              		$('#msg').addClass( "error alert-error" );
            	} else 	{
            	
            		$('#msg').html( d.msg );
            		$('#msg').addClass( "success alert-success" );   
             	}
	
	    	}
		});

	});
	
});
</script>
