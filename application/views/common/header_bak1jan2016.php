<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- responsive -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!-- responsive style-->
<title>GNISS-Global Network Information Security System</title>
<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>css/responsive-style.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>css/menu-style.css" rel="stylesheet" />

<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">	
<script src="<?php echo base_url(); ?>js/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/custom.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/script.js"></script>

<style>

#navnot{list-style:none;margin: 0px;
padding: 0px;}
#navnot li {
float: left;
margin-right: 20px;
font-size: 14px;
font-weight:bold;
}
#navnot li a{color:#333333;text-decoration:none}
#navnot li a:hover{color:#006699;text-decoration:none}
#notification_li{position:relative}
#notificationContainer {
background-color: #fff;
border: 1px solid rgba(100, 100, 100, .4);
-webkit-box-shadow: 0 3px 8px rgba(0, 0, 0, .25);
overflow: visible;
position: absolute;
top: 30px;
margin-left: -170px;
width: 400px;
z-index: -1;
display: none;
}
#notificationContainer:before {
content: '';
display: block;
position: absolute;
width: 0;
height: 0;
color: transparent;
border: 10px solid black;
border-color: transparent transparent white;
margin-top: -20px;
margin-left: 188px;
}
#notificationTitle {
z-index: 1000;
font-weight: bold;
padding: 8px;
font-size: 13px;
background-color: #ffffff;
width: 384px;
border-bottom: 1px solid #dddddd;
}
#notificationsBody {
padding: 33px 0px 0px 0px !important;
min-height:300px;
}
#notificationFooter {
background-color: #e9eaed;
text-align: center;
font-weight: bold;
padding: 8px;
font-size: 12px;
border-top: 1px solid #dddddd;
}
#notification_count {
padding: 3px 7px 3px 7px;
background: #cc0000;
color: #ffffff;
font-weight: bold;
margin-left: 77px;
border-radius: 9px;
position: absolute;
margin-top: -11px;
font-size: 11px;
}
</style>

<script>
$(document).ready(function(){
$(".profile-styled").click(function(){
    $("#login-panel").slideToggle(150);
    return false;
});
$(document).click(function(){
    $("#login-panel").slideUp(150);
});
});


$(document).ready(function()
{

$("#notificationLink").click(function()
{
	
	
$("#notificationContainer").fadeToggle(300);
$("#notification_count").fadeOut("slow");
return false;
});

//Document Click
$(document).click(function()
{
$("#notificationContainer").hide();
});
//Popup Click
$("#notificationContainer").click(function()
{
return false
});

});

</script>

</head>
<body>
<?php 
	$strMethod	= $this->uri->segment(3);
  $userId = $this->session->userdata('user_id');
    if( empty( $userId ) ) { ?>
	<div class="main-wrapper">
		<div class="header">
    		<a href="<?php echo base_url(); ?>"><div class="logo"></div></a>
    	</div>
    </div>
    	
<?php } else { ?>
		
		<div class="content-body-wrapper-dashboard">
		
    		
        <?php
            $profile_picture = $this->arrmixInfo['profile_info']->profile_picture;
			if( 'default.png' == $profile_picture ) {
					$profile_picture_path = 'images/'.$profile_picture;	
			} else {
					$profile_picture_path = 'uploads/'.$profile_picture;
			}
		?>
            <div class="all-grp" style="width:32%; position: relative;">
			
            <div class="ac-styled" style="margin: 8px 6px 0;">GNISS ID:<?php echo $this->session->userdata('account_number');?></div>
     	 	 <div style="margin: 13px 36px;"> <ul id="navnot">
				<ul>
				<li id="notification_li">
				<span id="notification_count">0</span>
				<a href="javascript:void(0);" id="notificationLink">Notifications</a>
				<div id="notificationContainer">
				<div id="notificationTitle">Notifications</div>
				<div id="notificationsBody" class="notifications">
				</div>
				<div id="notificationFooter"><a href="#">See All</a></div>
				</div>
				
				</li>
				
				</ul>
            </div>
            <div class="profile-styled" style="margin: -27px 34px;">
            
            <img src="<?php echo base_url().$profile_picture_path;?>" />
            	<a href="#"><span><?php echo $this->session->userdata( 'name'); ?></span></a>
          	</div>
          	
			<div id="login-panel">
			 <div style ="left: 28%; position: absolute;top: -13px;"><img src="<?php echo base_url();?>images/panel-arrow.png" /></div>
            <ul>
           		<li><a href="<?php echo base_url(); ?>profile/action/view">Profile</a></li>
               <li><a href="<?php echo base_url(); ?>welcome/logout">Logout</a></li>
            </ul>
        	</div>
        	
        	
        </div>
                     
				
        
     </div>
   
<?php } ?>
