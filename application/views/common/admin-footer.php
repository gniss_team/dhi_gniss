<?php 
     $strMethod	= $this->uri->segment(3);
     $intAdminUserId = $this->session->userdata('admin_user_id');
     if( empty( $intAdminUserId ) ) { ?>
<div class="footer-wrapper">
     <?php } else {?>
	<div class="footer-wrapper-dashboard">
     <?php }?>
        <div class="footer-menu">
			 <ul>
                <li><a href="<?php echo base_url();?>footermenu/action/about_us" target="_blank">About us</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/contact_us" target="_blank">Contact</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/disclaimer" target="_blank">Disclaimer</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/privacy_policy" target="_blank">Privacy Policy</a></li>
             </ul>
		</div>
		<div class="footer">Copyright © 2015 Rectus Energy Private Limited. All rights reserved.</div>
	</div>
	</div>
</body>
</html>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
<script type="text/javascript" >	
<?php  
$action	=	 NULL;
if( 'manageEnegyMeter' == $action ) {?>
$(document).ready(function() {
        $('#myTable').dataTable({
          
        } );
        
     }
);
<?php } else {?>
$(document).ready(function() {
    $('#myTable').dataTable({
       // "order": [[ 0, "desc" ]],
        "sDom": 'lpfrtip'
    } );
    
 }
);
<?php } ?>
</script>
<script type="text/javascript">
    var url="<?php echo base_url();?>";
    function deletedata(id,urln){
       var r=confirm("Do you want to delete this?")
        if (r==true)
          window.location = url+urln+id;
        else
          return false;
        }
</script>
