<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- responsive -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!-- responsive style-->
<title>GNISS-Global Network Information Security System</title>
<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>css/responsive-style.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url(); ?>css/menu-styles.css" rel="stylesheet" />
<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico">	
<script src="<?php echo base_url(); ?>js/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/custom.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/script.js"></script>
<script src="<?php echo base_url(); ?>js/commonfunctions.js"></script>
<script>
$(document).ready(function(){
	$(".profile-styled").click(function(){
	    $("#login-panel").slideToggle(150);
	    return false;
	});
	$(document).click(function(){
	    $("#login-panel").slideUp(150);
	});
});

</script>
</head>
<body>
<?php 
     $strMethod			= $this->uri->segment(3);
     $intAdminUserId	= ( int ) $this->session->userdata('admin_user_id');
     
      if( true == empty( $intAdminUserId ) ) { ?>
	<div class="main-wrapper">
    <div class="header">
        <a href="<?php echo base_url(); ?>"><div class="logo"></div></a>
      </div>
    </div>
<?php } else {?>
		
		<div class="content-body-wrapper-dashboard">
        <?php
        $profile_picture_path	= '';
        if( true == isset( $this->arrmixInfo['profile_info']->profile_picture ) ){
			if( 'default.png' == $this->arrmixInfo['profile_info']->profile_picture ) {
				$profile_picture_path = 'images/'.$this->arrmixInfo['profile_info']->profile_picture;	
			} else {
				$profile_picture_path = 'uploads/'.$this->arrmixInfo['profile_info']->profile_picture;
			}
		}		
		?> 
		<div class="all-grp">
           
           		 <div class="profile-styled">
                   	<img src="<?php echo base_url() . $profile_picture_path;?>" />
            		<a href="#"><span><?php echo $this->session->userdata( 'name'); ?></span></a>
            	</div>
                        
				<div id="login-panel">
            	<ul>
	           <!-- <li><a href="<?php // echo base_url().$strAction; ?>">Profile</a></li>
	                <li><a href="#">Settings</a></li>-->
	                <li><a href="<?php echo base_url(); ?>admin/profile/action/view">Profile</a></li>
	                <li><a href="<?php echo base_url(); ?>admin/action/logout">Logout</a></li>
            	</ul>
            </div>
        </div>
     </div>
  
<?php } ?>
