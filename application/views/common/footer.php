<script src="<?php echo base_url(); ?>js/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/custom.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/script.js"></script>
<?php 
     $strMethod	= $this->uri->segment(3);
     $userId = $this->session->userdata('user_id');
     if( empty( $userId ) ) { ?>
<div class="footer-wrapper">
     <?php } else {?>
	<div class="footer-wrapper-dashboard">
     <?php }?>
        <div class="footer-menu">
			 <ul>
                <li><a href="<?php echo base_url();?>footermenu/action/about_us" target="_blank">About us</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/contact_us" target="_blank">Contact</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/disclaimer" target="_blank">Disclaimer</a></li>
                <li><a href="<?php echo base_url();?>footermenu/action/privacy_policy" target="_blank">Privacy Policy</a></li>
                <li><a href="#" >Terms & Conditions</a></li>
             </ul>
		</div>
		<div class="footer">Copyright © 2016 Dhi Networks Private Limited. All rights reserved.</div>
	</div>
	</div>
</body>
</html>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script>
<script type="text/javascript" >	
<?php  
$action	=	 NULL;
if( 'manageEnegyMeter' == $action ) {?>
$(document).ready(function() {
        $('#myTable').dataTable({
          
        } );
        
     }
);
<?php } else {?>
$(document).ready(function() {
    $('#myTable').dataTable({
       // "order": [[ 0, "desc" ]],
        "sDom": 'lpfrtip'
    } );

    
 }
);
<?php } ?>
$(document).ready(function() {
	$('#notification_count').html('0');
	checkforsharelocation();

	window.setInterval(function(){
		checkforsharelocation();
	}, 10000);

		
} );
 function checkforsharelocation()
 {
	
	 $('#btm_pop').html("");
		$('#notification_count').html('0');
		$('#notificationsBody').html('<div id="noti-msg">No Notification Avilable</div>');
	 $.get(" <?php echo base_url()?>CManageLiveTrackingController/ajaxcheckforsahrelcation", function( data ) {
		 if(data.flag==1) {
				 if(data.sharelinkS.length > 0)
				 {
				 $('#notification_count').html(data.sharelinkS.length);
				 }
				 if(data.sharelinkS.length > 3){
				$('#notificationFooter').show();
				 }
				if(data.sharelinkS!=undefined) {
					 $('#notificationsBody').html("");
					        $.each(data.sharelinkS, function(key,value) {
					        	//  alert(value.id);
					        	  var link = '<?php echo base_url()?>livetracking/share/'+value.shared_device;
					        	$('#btm_pop').append('<button type="button" class="noti-popup" onclick="go(\'' + link + '\');" id="not_button"><span class="noti-pop-icon"><i class="fa fa-bell"></i></span><span>'+value.first_name+' Your  has shared a location of '+value.shared_device+'</span></button>')
						   	     $('#notificationsBody').append('<div id="noti-msg"><a href="javascript:void(0);" onclick="go(\'' + link + '\');">'+value.first_name+' Your  has shared a location of '+value.shared_device+'</a><button type="button" class="button-default button-dismiss js-dismiss" onclick="javascript:$(this).parent().remove();"><span class="icon-stack"><i class="fa fa-times"></i></span></button></div>');
					        	$('#btm_pop').fadeOut(10000);
					        	});
					   
					
				}
		  }
		},"json" );
 }
</script>

<script type="text/javascript">
    var url="<?php echo base_url();?>";
    function deletedata(id,urln,device_type_id ){
       var r=confirm("Do you want to delete this?")
        if (r==true)
          window.location = url+urln+id+'/'+device_type_id;
        else
          return false;
        }
  function go(id)
  {
	  
    window.location=id;
  }
</script>
