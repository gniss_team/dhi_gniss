<style type="text/css">
.sub-content{
	margin:0 auto;
	padding:0;
	width:1200px;	
	}
.sub-contact{
	margin:0 auto 25px;
	padding:0;
	width:1200px;
}
h2 {
 
  font-size: 24px;
  margin-left: 1%;
}
.photo > img {
  border: 5px solid #cbcbcb;
  float:left;
  margin-left: 1%;
}
h4 {
  font-size: 15px;
  font-weight: bold;
  line-height: 0;
  margin: 37px 0 25px 142px;
  padding-top: 14px;
}
h3 {
  color: #333333;
  font-size: 13px;
  font-weight: bold;
  line-height: 0;
  margin: -5px 1px 1px 142px;
}

h5 {
  color: #333333;
  font-size: 13px;
  font-weight: bold;
  line-height: 0;
  margin: -5px 1px 1px 142px;
}
p {
  color: #333333;
  font-size: 13px;
  margin: 7px 1px 1px 142px;
  padding: 10px 0 0;
  text-align: justify;
}
.readmore {
  background-color: #4c8cfa;
  border-radius: 4px;
  color: white;
  float: right;
  font-size: 12px;
  padding: 5px;
  text-decoration: none;
}
.map {
  background-color: #dfdfff;
  margin: 10px 0 0;
  padding: 15px;
}
.co-address1{
	color: #333333;
 	font-size: 13px;
	line-height: 1.5;
    margin: 0;
}
.co-address2{
	color: #333333;
 	font-size: 13px;
	margin: 0;
	line-height: 1.6;
}
.co-address3{
	color: #333333;
 	font-size: 13px;
	margin: 0;
	line-height: 1.6;
}
.co-business {
  font-size: 14px;
  margin: 0;
  padding: 0;
}
.contact-1 {
	background-color: #f1f1f1;
    border-radius: 7px;
    box-shadow: 3px 4px 1px 0 #ccc;
    float: left;
    height: 205px;
    margin: 13px 14px 10px 0;
    padding: 0 0 0 18px;
    width: 30%;
}

.contact-1 > h1 {
  border-bottom: 1px solid #ddd;
  font-size: 15px;
  margin-right: 28px;
  padding: 0 0 11px;
}

.contact-2 {
  background-color: #f1f1f1;
  border-radius: 7px;
  box-shadow: 3px 4px 1px 0 #ccc;
  float: left;
  height: 205px;
  margin: 16px 13px 10px 17px;
  padding: 0 0 0 18px;
  width: 30%;
}

.contact-2 > h1 {
  border-bottom: 1px solid #ddd;
  font-size: 15px;
  margin-right: 28px;
  padding: 0 0 11px;
}
.contact-3 {
  background-color: #f1f1f1;
  border-radius: 7px;
  box-shadow: 3px 4px 1px 0 #ccc;
  display: inline-block;
  height: 205px;
  margin: 13px 0 0 17px;
  padding: 0 0 0 18px;
  width: 30%;
}
.contact-3 > h1 {
  border-bottom: 1px solid #ddd;
  font-size: 15px;
  margin-right: 28px;
  padding: 0 0 11px;
}

.sub-contact > p {
  font-size: 13px;
  margin-left: 11px;
  line-height: 1.4;
}
</style>