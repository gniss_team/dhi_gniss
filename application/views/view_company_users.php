<?php $this->load->view('common/header');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Manage Users</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>welcome">Dashboard</a></li>
			<?php if( user_type_model::c_individual_account == $this->session->userdata('login_type') ) { ?>
			<li><a class="big" href="<?php echo base_url(); ?>company/action/viewcompanies">Manage Company</a></li>
			<?php } ?>
			<li>View Users</li>
		</ul>
	</div>
	<button class="btn btn-xs btn-success" style="margin:0 10px 20px 25px;" type="button" onclick="window.location='<?php echo base_url(); ?>company/action/createcompanyuser/<?php echo $company_id;?>'">Add User</button>
	<div class="content-wrap clearfix">
	<?php if(isset($msg) && $msg != '') { ?>
		<div class="alert alert-success">
			<?php echo $msg; ?>
		</div>
	<?php } ?>
		<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	    	<thead>
            	<tr>
                	<th style="display:none">Id</th>
                    <th>Account Number</th>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th>Mobile Number</th>
			     	
                </tr>
            </thead>
			<tbody>
			<?php foreach ($arrCompanyUsers as $companyuser){?>
            	<tr class="active">
                	<td style="display:none"><?php echo $companyuser->id; ?></td>
                    <td><?php echo $companyuser->account_number; ?></td>
                    <td><?php echo $companyuser->first_name.' '.$companyuser->last_name; ?></td>
                     <td><?php echo $companyuser->email_address; ?></td>
                    <td><?php echo $companyuser->mobile_number; ?></td>
                   
 				</tr>
            <?php } ?>
            </tbody>
        </table>
	</div>
</div>
<?php $this->load->view('common/footer');?>