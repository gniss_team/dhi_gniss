<?php $this->load->view('common/header');?>
<?php $this->load->view('common/style');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<div class="content-body-wrapper-dashboard clearfix">
<?php $graphtype = array('watts','current','voltage');?>
<?php $this->load->view('common/sidebar');?>
<script type="text/javascript" src="<?php echo base_url();?>js/canvasjs.min.js"></script>
    <link class="include" rel="stylesheet" type="text/css" href="<?php echo base_url();?>graph_assets/jquery.jqplot.min.css" />
    <style type="text/css">

.plot {
    margin-bottom: 30px;
    margin-left: auto;
    margin-right: auto;
}


#chart3 .jqplot-meterGauge-tick, #chart0 .jqplot-meterGauge-tic {
    font-size: 10pt;
}


</style>


<div class="content clearfix">
	<div class="page-title"><h3> Energy Meter Graph<span style="margin-left:200px;"></h3> </div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>welcome">Dashboard</a></li>
		
			<li>View Energy Meter Graph </li>
		</ul>
	
	</div>
	<?php echo form_open_multipart('energymeter/action/viewGraphs/'); ?>
	<div>
		<label style=" margin-left: 25px;">Filter : </label>
	   <select class="styled" name="filter" onchange="this.form.submit();">
	   <option value="week" <?php if($this->input->post('filter')=='week'){ ?> selected <?php } ?> >By Week</option>
	   <option value="month"<?php if($this->input->post('filter')=='month'){ ?> selected <?php } ?>>By Month</option>
	   
	  </select>     
	</div>
	</form>
	<div class="content-wrap clearfix">
	<?php if(isset($msg) && $msg != '') { ?>
		<div class="alert alert-success">
			<?php echo $msg; ?>
		</div>
	<?php } ?>

	<div id="chartContainer" style="height: 300px; width: 100%;">
	</div>
	<div id="current" class="plot" ></div>
	<div id="voltage" class="plot" ></div>
</div>
	<style>.canvasjs-chart-credit {
		display:none;
	}
	</style>
	
	
<?php $this->load->view('common/footer');?>

<script type="text/javascript" class="code">


function getstat()
{
	$.get( "<?php echo base_url();?>energymeter/action/ajaxgetEnergyStats", function( data ) {
		  $( ".result" ).html( data );
		  var obj = $.parseJSON(data)
			var voltage = 	obj.currentvoltage;
			 var watts = obj.currentwatts;
			alert()
		     s1 = [watts];

		   plot3 = $.jqplot('chart3',[s1],{
			   title: 'Latest Watt',
		       seriesDefaults: {
		           renderer: $.jqplot.MeterGaugeRenderer,
		           rendererOptions: {
		               min: 0,
		               max: watts*3
		           }
		       }
		   });
		   voltage = [voltage];
		 
		   plot3 = $.jqplot('voltage',[voltage],{
			   title: 'Latest Voltage',
		       seriesDefaults: {
		           renderer: $.jqplot.MeterGaugeRenderer,
		           rendererOptions: {
		               min: 0,
		               max: voltage*3
		           }
		       }
		   });
		});
}
$(document).ready(function(){
	getstat();
	setInterval(getstat, 5000);
});
</script>




    <script class="include" type="text/javascript" src="<?php echo base_url();?>graph_assets/jquery.jqplot.min.js"></script>
  <script class="include" type="text/javascript" src="<?php echo base_url();?>graph_assets/jqplot.meterGaugeRenderer.min.js"></script>
