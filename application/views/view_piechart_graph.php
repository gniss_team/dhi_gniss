<script type="text/javascript">

$(document).ready(function(){
	 $.jqplot.config.enablePlugins = true;
	 
	  <?php 
		for( $j=0 ;$j <$cnt_graph_parameter; $j++) {  ?>

	  	var piechart<?php echo $j;?>=[<?php for( $i=0 ;$i<count($energy_meter_data); $i++) {
			if( $i != count($energy_meter_data) -1  ){
			?>
					["<?php echo date('H:i', strtotime( $energy_meter_data[$i]->created_on) );?>", <?php echo $energy_meter_data[$i]->$type[$j]/10;?>],

			 <?php } else {?>
			 ["<?php echo date('H:i', strtotime( $energy_meter_data[$i]->created_on ) );?>", <?php echo $energy_meter_data[$i]->$type[$j]/10;?>]
			 <?php  } } ?> ] <?php } ?>
		 
     
	//  var line2=['2015-07-03','2015-07-04'];
	  var plot2 = $.jqplot('chart1',[
	      <?php for( $k=0 ;$k < $cnt_graph_parameter; $k++) { 
	      if( $k != ($cnt_graph_parameter) - 1 ) { ?><?php echo 'piechart'. $k . ',';  }else { echo 'piechart'.$k; } } ?>], {
		  seriesDefaults: {
			  shadow: false, 
	    	  renderer: jQuery.jqplot.PieRenderer, 
	          rendererOptions: {
	       // 	  min:'<?php //echo $this->input->post('from_date');?>', 
	        //       max:'<?php //echo $this->input->post('to_date');?>', 
	            // Put data labels on the pie slices.
	            // By default, labels show the percentage of the slice.
	            tickInterval:'1 day',
	            showDataLabels: true
	          }
          },
          highlighter: {
              show: false,
             // sizeAdjust: 1,
             // tooltipOffset: 9,
              showTooltip: false
          },
          legend: { show:true, location: 'e' },
          grid:{borderColor:false,shadow:false,drawBorder:false,shadowColor:'transparent'},
           });
	});
	
</script>

<style>
.blank_div{
	color: #757575;
    display: inline;
    font-size: 13px;
}
.jqplot-target {
     color:#666666;
}
#chart1 .jqplot-target{width:400px !important;
height:200px !important;}
</style>