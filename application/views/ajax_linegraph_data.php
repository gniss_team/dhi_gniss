<script type="text/javascript">
  
  graph_type = "<?php echo $unit_type; ?>";
  yAxisLabel = '';
  dataParameter = '';
  balloonText = "";
  div = '';
  if( graph_type == 'ambient_temperature' ) {
    div = 'tempchart';
    dataParameter = 'temperature';
    yAxisLabel = 'Temperature';
    balloonText = "[[category]]<br/><b><span style='font-size:14px;'>temperature: [[value]]</span></b>";
  } else if( graph_type == 'cum_watt_hour' ) {
    div = 'wattchart';
    dataParameter = 'watt';
    yAxisLabel = 'Watt';
    balloonText = "[[category]]<br/><b><span style='font-size:14px;'>watt: [[value]]</span></b>";
  } else if( graph_type == 'humidity' ) {
    div = 'humiditychart';
    dataParameter = 'humidity';
    yAxisLabel = 'Humidity';
    balloonText = "[[category]]<br/><b><span style='font-size:14px;'>humidity: [[value]]</span></b>";
  }
  var chartData = generateChartData();
  function generateChartData() {
  
    lineChartData = [<?php 
          if( 0 < count($energy_meter_data) ) {
          for( $i=0 ;$i<count($energy_meter_data); $i++) {
            if( $unit_type == 'ambient_temperature'  ){
            ?>
                {"time":"<?php echo date("H:i",strtotime($energy_meter_data[$i]->created_on));?>","temperature": <?php echo $energy_meter_data[$i]->$unit_type/10;?>},
  
             <?php } else if( $unit_type == 'cum_watt_hour' ) {?>
             {"time":"<?php echo date("H:i",strtotime($energy_meter_data[$i]->created_on));?>", "watt":<?php echo $energy_meter_data[$i]->$unit_type/10;?>},
             <?php } else if( $unit_type == 'humidity' ) {?>
             {"time":"<?php echo date("H:i",strtotime($energy_meter_data[$i]->created_on));?>", "humidity":<?php echo $energy_meter_data[$i]->$unit_type/10;?>},
             
             <?php  }  } 
             
      } else { 
        if( $unit_type == 'ambient_temperature'  ){ ?> 
          {"time":0, "temperature":0} <?php } 
          else if( $unit_type == 'humidity' ) { ?>
                      {"time":0, "humidity":0}
                   <?php }
           else if( $unit_type == 'cum_watt_hour' ) { ?>
            {"time":0, "watt":0}
         <?php }else { ?>
            {"time":0, "humidity":0} 
          <?php } } ?> ]
    
    return lineChartData;
    
  }
  var chart = AmCharts.makeChart(div, {
     "type": "serial",
        "theme": "light",
        "marginRight": 50,
        "autoMarginOffset": 20,
        "marginTop": 7,
        "dataProvider": chartData,
        "valueAxes": [{
            "axisAlpha": 0.2,
            "dashLength": 1,
            "position": "left",
            "labelsEnabled": true
        }],
        "mouseWheelZoomEnabled": true,
        "graphs": [{
            "id": "g1",
            "balloonText": balloonText,
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "hideBulletsCount": 50,
            "title": yAxisLabel,
            "valueField": dataParameter,
            "useLineColorForBulletBorder": true
        }],
        "chartCursor": {

        },
        "categoryField": "time",
        "categoryAxis": {
            //"parseDates": true,
            'period': 'NN',
        
         //   "chart.dataDateFormat" : "YYYY-MM-DD",
            "axisColor": "#DADADA",
            "dashLength": 1,
            "minorGridEnabled": true
        },
        "chartScrollbar": {
        "graph": "g1",
        "oppositeAxis":false,
        "offset":30,
        "scrollbarHeight": 40,
        "backgroundAlpha": 0,
        "selectedBackgroundAlpha": 0.1,
        "selectedBackgroundColor": "#888888",
        "graphFillAlpha": 0,
        "graphLineAlpha": 0.5,
        "selectedGraphFillAlpha": 0,
        "selectedGraphLineAlpha": 1,
        "autoGridCount":true,
        "color":"#AAAAAA"
      },
        "export": {
            "enabled": true
        },
        "allLabels": [
                  {
                    "text": yAxisLabel,
                    //"bold": true,
                    "rotation":270,
                    "size":15,
                    "x": 10,
                    "y": 230
                  },
                  {
                    "text": "Time",
                    //"bold": true,
                    "rotation":0,
                    "size":15,
                    "x": 360,
                    "y": 400
                  }
                ],
                 
  });
  chart.dataDateFormat = "JJ:NN:SS";
  // this method is called when chart is first inited as we listen for "rendered" event
  function zoomChart() {
      // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
      chart.zoomToIndexes(chartData.length - 40, chartData.length - 1);
  }
      
</script>
