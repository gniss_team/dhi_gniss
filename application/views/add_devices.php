<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>

<div class="content clearfix">
<div class="title-box">
	<div class="page-title"><h4>Add Device</h4></div></div>
	
		<div class="asterisk-msg">All fields marked with * are mandatory.</div>
		<div><?php $attributes = array('id' => 'myForm');		
			echo form_open_multipart('devices/action/add',$attributes);?>
            <h5>Device details</h5>
			<div class="form-grp">			 
		    	<label>Device Type<span class="asterisk">*</span></label>
	            	<select class="form-control" name="device_type_id" id="device_type_id" Onchange="checktype(this.value,'0');">
					<option value="" >Select</option>
                    <?php foreach( $devices as $device ) { ?>
                    	<option value="<?php echo $device->unit_type; ?>" <?php if( true == is_array( $this->input->post()) && $device->unit_type == $this->input->post('device_type_id') ) {?> selected="selected"<?php } ?>><?php echo $device->name; ?></option>
                   	<?php } ?>
                </select>
                  <?php echo form_error('device_type_id'); ?>
	        </div>
	        <div class="form-grp">
		    	<label>Device Name <span class="asterisk">*</span></label>
	            <input class="form-control" placeholder="Name" name="name" id="name" maxlength="21" value="<?php echo set_value('name'); ?>"  >
	            <?php echo form_error('name'); ?>
	        </div>
	        <div class="form-grp" id="dev_number">
	        	<label>Device number/ Unit number<span class="asterisk"> *</span></label>
				<input class="form-control" name="device_number" id="device_number" placeholder="Device number" maxlength="20" value="<?php echo set_value('device_number'); ?>"  >
	            <?php echo form_error('device_number'); ?>
 	       </div>
  	       <div class="form-grp">
		    	<label>Phone number<span class="asterisk">*</span></label>
				<input class="form-control" name="phone_number" id="phone_number" placeholder="Phone number" value="<?php echo set_value('phone_number'); ?>"  >
			    <?php echo form_error('phone_number'); ?>
			</div>
	     	<div class="form-grp">
		    	<label>IMEI number<span class="asterisk">*</span></label>
				<input class="form-control" name="imei_number" id="imei_number" placeholder="IMEI number" value="<?php echo set_value('imei_number'); ?>"  >
			    <?php echo form_error('imei_number'); ?>
			</div>
       		<div class="form-grp">
		    	<label>Sim number</label>
				<input class="form-control" name="sim_number" id="sim_number" placeholder="Sim number"  maxlength="20" value="<?php echo set_value('sim_number'); ?>"  >
	      		<?php echo form_error('sim_number'); ?>
			</div>
			<h5>Contact</h5>
			<div class="form-grp"> 
		    	<label>Country<span class="asterisk">*</span></label>
				<select  class="form-control" id="country_id" name="country_id" >
					<option value="">Select Country</option>
					<?php  foreach( $countries as $country ) { ?>
					<option value="<?php echo $country->id; ?>" <?php if( true == is_array( $this->input->post() ) && $country->id == $this->input->post('country_id') ) { ?> selected="selected" <?php } ?> ><?php echo $country->name; ?> </option>
					<?php } ?>
				</select>
				<?php echo form_error('country_id'); ?>
			</div>
			<div class="form-grp">
				<label>State<span class="asterisk">*</span></label>
				<select class="form-control" id="state_id" name="state_id" value="<?php echo set_value('state_id'); ?>">
					<option value="">Select State</option>
					<?php  foreach( $states as $state ) { ?>
					<option value="<?php echo $state->id; ?>"
					<?php if( true == is_array( $this->input->post() ) && $state->id == $this->input->post('state_id') ) { ?> selected="selected" <?php } ?>><?php echo $state->name; ?> </option>
					<?php } ?>
				</select>
			    <?php echo form_error('state_id'); ?> 	
			</div>
			<div class="form-grp">
				<label>City<span class="asterisk">*</span></label>
				<select class="form-control " id="city_id" name="city_id" value="<?php echo set_value('city_id'); ?>" >
					<option value="">Select City</option>
					<?php foreach( $cities as $city ) { ?>
					<option value="<?php echo $city->id; ?>"<?php if( true == is_array( $this->input->post() ) && $city->id == $this->input->post('city_id')) { ?> selected="selected" <?php } ?>><?php echo $city->name; ?> </option>
					<?php } ?>
				</select>	
			    <?php echo form_error('city_id'); ?>
			</div>
			<div class="form-grp">
		    	<label>Address Line 1<span class="asterisk">*</span></label>
				<input class="form-control" name="address_line1" id="address_line_1" placeholder="address1" value="<?php echo set_value('address_line1'); ?>" >
	      		<?php echo form_error('address_line1'); ?>
			</div>
			<div class="form-grp">
		    	<label>Address Line 2</label>
				<input class="form-control" name="address_line2" id="address_line_2" placeholder="address2" value="<?php echo set_value('address_line2'); ?>" >
	      		<?php echo form_error('address_line2'); ?>
			</div>
			<div class="form-grp">
		    	<label>Zip Code<span class="asterisk">*</span></label>
				<input class="form-control" name="zip_code" id="post_code" placeholder="zip_code" value="<?php echo set_value('zip_code'); ?>" >
				<?php echo form_error('zip_code'); ?>
			      	
			</div>
			
			<div id="vts_markers" style="display:none;" >
		      <div class="form-grp">
		    	<label id ="marker_label">Customize Markers</label>
					<ul>
						<li><input type="radio"  name="marker_icon" value="map-marker-icon.png" <?php if( true == is_array( $this->input->post() ) && 'map-marker-icon.png' == $this->input->post('marker_icon') ) { echo "checked='checked'"; } else { if( false == is_array( $this->input->post() ) ) { echo "checked='checked'"; } } ?>><img width="30" src="<?php echo base_url(); ?>images/map_icons/map-marker-icon.png" ></li>
                		<li><input type="radio" name="marker_icon" value="truck-icon.png" <?php if( true == is_array( $this->input->post() ) && 'truck-icon.png' == $this->input->post('marker_icon') ) { echo "checked='checked'"; } ?>><img width="30" src="<?php echo base_url(); ?>images/map_icons/truck-icon.png" ></li>
                		<li><input type="radio" name="marker_icon" value="car-icon.png" <?php if( true == is_array( $this->input->post() ) && 'car-icon.png' == $this->input->post('marker_icon') ) {	echo "checked='checked'"; } ?>><img width="30" src="<?php echo base_url(); ?>images/map_icons/car-icon.png"></li>
					</ul>
			 </div>
	
			<div>
				</div>
			</div>
         <div class="btn-grp"> 
        	<button type="submit"   class="signup">Save </button>
            <button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>'">Cancel</button>
        </div>
        
		<?php  
		
		echo form_close(); ?>	
		</div>
		
</div>
<script type="text/javascript">
function ajaxindicatorstart(text) {
	
	$(".loader").fadeOut("slow");
	if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
		jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="../../images/ajax-loader3.gif"><div>'+text+'</div></div><div class="bg"></div></div>');
	}

	jQuery('#resultLoading').css({
		'width':'100%',
		'height':'100%',
		'position':'fixed',
		'z-index':'10000000',
		'top':'0',
		'left':'0',
		'right':'0',
		'bottom':'0',
		'margin':'auto'
	});

	jQuery('#resultLoading .bg').css({
		
		'opacity':'0.7',
		'width':'100%',
		'height':'100%',
		'position':'absolute',
		'top':'0'
	});

	jQuery('#resultLoading>div:first').css({
		'width': '250px',
		'height':'75px',
		'text-align': 'center',
		'position': 'fixed',
		'top':'0',
		'left':'0',
		'right':'0',
		'bottom':'0',
		'margin':'auto',
		'font-size':'16px',
		'z-index':'10',
		'color':'#ffffff'

	});

    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop()
{
    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}
function readURL(input) {
		
 	if (input.files && input.files[0]) {
    	var reader = new FileReader();
		reader.onload = function (e) {
			ajaxindicatorstart('loading data.. please wait..');
        	$('#thumb').attr('src', e.target.result);
        	ajaxindicatorstop();
        }
    reader.readAsDataURL(input.files[0]);
    }
 	
 }
var strUrl = "<?php echo base_url(); ?>";
function checktype(val,flag)
{
if(flag=='0') {
$('#name').val("");
$('#device_number').val("");
$('#sim_number').val("");
$('#phone_number').val("");
$('#imei_number').val("");
$('#address').val("");
}
  if(val==3)
  {
	  $('#vts_markers').show();
// 	  $('#vts_field').show();
	//  $('#dev_number').hide();
	 
	  
  } else {
	  $('#vts_markers').hide();
// 	  $('#vts_field').hide();
	//  $('#dev_number').show();
	  
  }
}
$( document ).ready(function() {
	
	var val = $('#device_type_id').val();
	var flag="1";
	checktype(val,flag);

	country_id = $('#country_id').val();
	
	$('#country_id').change(function(){
		
		ajaxindicatorstart('loading data.. please wait..');
		
		country_id = this.value;
		$.ajax({
		    type: "POST",
		    url:  strUrl + 'profile/action/getLocationData',
		    data: 'id='+ country_id +'&type=state',
		    cache: false,
		    success: function( strResult ) {
			    
		    	ajaxindicatorstop();
		    	
		    	$( '#state_id' ).empty();
		    	$( '#city_id' ).empty();
		    	$( '#city_id').html("<option value=''>Select City</option>");
		    	$( '#state_id').html("<option value=''>Select State</option>");
			    $( '#state_id' ).append( strResult );
		    }
 		});

	});

	$('#state_id').change(function(){

		ajaxindicatorstart('loading data.. please wait..');
		
		state_id = this.value;
		$.ajax({
		    type: "POST",
		    url:  strUrl + 'profile/action/getLocationData',
		    data: 'id='+ state_id +'&type=city',
		    cache: false,
		    success: function( strResult ) {

		    	ajaxindicatorstop();
		    	
		    	$( '#city_id' ).empty();
		    	$( '#city_id').html("<option value=''>Select City</option>");
			    $( '#city_id' ).append( strResult );
		    }
 		});

	});

	$('input[name="marker_icon"]').click(function() {
		if( '' == $(this).val() ) {
			$('#marker_img').trigger('click');
			
		}
	});

});
$(document).keydown(function(e) {

    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});

</script>
<style>

.error{margin-left:11%;}
</style>
<?php $this->load->view('common/footer');?>
