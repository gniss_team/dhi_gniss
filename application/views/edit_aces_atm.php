<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<!--<style>
.form-control {
		margin:0px 5px;
}

.form-grp li {
    display: block;
    float: left;
    margin-right: 30px;
}
#marker_label {
	float:left;
}
#icon_label {
	clear: both;
	float: left;
	position:relative;
	left:90px;
}
.form-grp img{
vertical-align:middle;
}
</style>-->
<div class="content clearfix">
	<div class="title-box"><div class="page-title"><h4>Edit ACES ATM</h4></div></div>
	<div class="content-wrap clearfix">
	<?php echo form_open('devices/action/edit/'. $device_type_id . '/'. $device_id.'/'.$serrial_no ); ?>
<h5>Device information</h5>
		<div class="form-grp">
        	<label>Set temperature </label>
            <input class="form-control" placeholder="Set temperature" name="set_temperature" id="set_temperature" maxlength="21" value="<?php echo set_value('set_temperature'); ?>">
            <?php echo form_error('set_temperature'); ?>
        </div>
        <div class="form-grp">
        	<label>AC switch duration</label>
			<input class="form-control" name="as_switch_dur" id="as_switch_dur" placeholder="AC switch duration" value="<?php echo set_value('as_switch_dur'); ?>">
            <?php echo form_error('as_switch_dur'); ?>
        </div>
          <div class="form-grp">
	    	<label>Low cut</label>
			<input class="form-control" name="low_cut" id="low_cut" placeholder="Low cut" value="<?php echo set_value('low_cut'); ?>">
            <?php echo form_error('low_cut'); ?>
        </div>
        <div class="form-grp">
        	<label>High cut</label>
			<input class="form-control" name="high_cut" id="high_cut" placeholder="High cut" value="<?php echo set_value('high_cut'); ?>">
            <?php echo form_error('high_cut'); ?>
        </div>
      
         <div class="form-grp">
	    	<label>Temperature offset</label>
			<input class="form-control" name="temperature_offset" id="temperature_offset" placeholder="Temperature offset" value="<?php echo set_value('temperature_offset'); ?>">
            <?php echo form_error('temperature_offset'); ?>
        </div>
   <div class="form-grp">
	    	<label>Main offset</label>
			<input class="form-control" name="main_offset" id="main_offset" placeholder="Main offset" value="<?php echo set_value('main_offset'); ?>">
            <?php echo form_error('main_offset'); ?>
        </div>
        <div class="form-grp">
	    	<label>Load offset</label>
			<input class="form-control" name="load_offset" id="load_offset" placeholder="Load offset" value="<?php echo set_value('load_offset'); ?>">
            <?php echo form_error('load_offset'); ?>
        </div>
         <div class="form-grp">
	    	<label>Load gain</label>
			<input class="form-control" name="load_gain" id="load_gain" placeholder="Load gain" value="<?php echo set_value('load_gain'); ?>">
            <?php echo form_error('load_gain'); ?>
            <input type="hidden" name="serrial_no" value="<?php echo $serrial_no;?>">
             <input type="hidden" name="device_type_id" value="<?php echo $device_type_id;?>">
            
        </div>
        <div class="btn-grp">
        	<button type="submit"  class="signup">Save </button>
            <button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>devices/action/view/<?php echo $device_type_id;?>'">Cancel</button>
        </div>
	    <?php  echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">

$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>

<?php $this->load->view('common/footer');?>  
