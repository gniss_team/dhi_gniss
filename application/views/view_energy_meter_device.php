<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<script>	 var base_url = "<?php echo base_url(); ?>";
var device_type_id	= "<?php echo $devices_type_id; ?>";
</script>
<script src="<?php echo base_url(); ?>js/pagination.js"></script>
<style>
.signup {
	margin: 0;
}
.error {
	left:160px;
}
.alert-success {
 	padding:5px 0;
 	margin:5px 0;
 }
.breadcrums li a {
    background: none;
}
.form-grp label {
	 width: 40%;
}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}

.dataTables_length label {
	width: auto;
	display: inline-flex;
}
.dataTables_length select {
	margin: -6px 12px;
	padding: 5px 10px 5px 0px;
	/*-webkit-border-radius:4px;*/
   -moz-border-radius:4px;
   /* border-radius:4px;
  -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;*/
   background: url("../../../images/select-arrow.png") no-repeat scroll 98% center #fff;
   color:#555;
   border:1px solid #e3e3e3;
   outline:none;
   display style="margin:0 18px;": inline-block;
   -webkit-appearance:none;
   -moz-appearance:none;
   appearance:none;
   cursor:pointer;
}
.dataTables_paginate{
    float: right;
    font-size: 13px;
    padding-top: 0px;
    padding-right: 12px;
    padding-bottom: 6px;
    padding-left: 12px;
    text-align: right;
}
.dataTables_wrapper .dataTables_filter{
	margin-top: 4px;
}
#myTable_info{
	font-size:13px;
}
table.dataTable {
	width:98% !important;
	margin-left:0px !important;	
}
.img_icon {
	margin-right:10px;
	cursor:pointer;
}	
#img_livetrack {
	cursor:pointer;
}
table.tablesorter thead tr .noheader {
    border-right:1px solid;
}
.fault_div {
		font-size:13px;
		float:left;
		margin:17px 15px 8px 0;
}

select#device_status  option[value="CF"] {
background:url(../../../images/current-indicator.png) no-repeat;
padding:0px 20px;
background-position:2% 52%;

}

select#device_status  option[value="VF"] {
background:url(../../../images/voltage-indicator.png) no-repeat;
padding:0px 20px;
background-position:2% 52%;

}

select#device_status  option[value="NC"] {
background:url(../../../images/communication-indicator.png) no-repeat;
padding:0px 20px;
background-position:2% 52%;

}

select#device_status  option[value="OK"] {
background:url(../../../images/ok-indicator.png) no-repeat;
padding:0px 20px;
background-position:2% 52%;
}




</style>
<div class="content clearfix">
	<div class="page-title"><h4>Energy Meter Devices</h4></div>
	<button class="signup" style="margin:0 10px 20px 0;" type="button" onclick="window.location='<?php echo base_url(); ?>devices/action/add'">Add Device</button>
<div>
				<div class='fault_div'><div style="background:red;width:10px;height:10px;border-radius:100px;float:left;margin-right:10px;margin-top:3px;"></div>Current Fault</div>
				<div  class='fault_div'><div style="background:blue;width:10px;height:10px;border-radius:100px;float:left;margin-right:10px;margin-top:3px;"></div>Voltage Fault</div>
				<div  class='fault_div'><div style="background:grey;width:10px;height:10px;border-radius:100px;float:left;margin-right:10px;margin-top:3px;"></div>No Communication</div>
				<div  class='fault_div'><div style="background:yellow;width:10px;height:10px;border-radius:100px;float:left;margin-right:10px;margin-top:3px;"></div>All OK</div>
	</div>
	<div class="content-wrap clearfix">
        <?php if($msg!="") { ?>
			<div class="alert alert-success">
			    <?php echo $msg; ?>
			</div>
		<?php } ?>
		<div style="clear:both;float:left;margin-bottom:10px;width:20%;">
        	<label >Show </label>
            <select class="form-control styled" name="select_pagination" id="select_pagination">
               <option value="10">10</option>
					<option value="25">25</option>
					<option value="50">50</option>
					<option value="100">100</option>
            </select>
               <label>Entries </label>
         </div>
         	<div style="float:left;margin-bottom:10px;width:20%;margin-left:32%;">
            <select  class="styled-status" name="device_status" id="device_status">
            <option value="">Status</option>
          	 <option value="CF">Current Fault</option>
					<option value="VF">Voltage Fault</option>
					<option value="NC">No Communication</option>
					<option value="OK">All OK</option>
            </select>
         </div>
          <div style="float:right;margin-right:2%;"><label>Search:<input type="search"   name="search" class="" placeholder="" aria-controls="myTable"></label></div>
         <div id="myTable_filter" class="dataTables_filter">
					<div id="mhead"></div>
					<div id="pagination" cellspacing="0"></div>
			</div>
    </div>
</div>
<style>

.pagination {
    float: right;
    margin: 0 auto;
    width: auto;
	font-size:13px;
}

.pagination .current{
	padding: 4px 10px;
	color: black;
	margin: 1px 0px 9px 6px;
	display: block;
	text-decoration:none;
	float: left;
	text-transform: capitalize;
	/*background: whitesmoke;*/
	background:rgba(0, 0, 0, 0) linear-gradient(to bottom, #fff 0%, #dcdcdc 100%) repeat scroll 0 0;
	border:1px solid #cacaca;
}

.pagination .page-numbers{
	padding: 4px 10px;
	/* color: white !important; */
	margin: 2px;
	display: block;
	text-decoration:none;
	float: left;
	text-transform: capitalize;
	color:#757575;
	/* background: #00b4cc; */
}

 .page-numbers:hover{
	color:#000;
	background:rgba(0, 0, 0, 0) linear-gradient(to bottom, #fff 0%, #dcdcdc 100%) repeat scroll 0 0;
	border:1px solid #cacaca;
	padding: 4px 10px;
	/* color: white !important; */
	margin: 1px;

 
	 }

table.tablesorter {
/*     background-color: #4c8cfa; */
    clear: both;
    font-family: arial;
    font-size: 14px;
    margin: 18px 5px 9px 0;
    text-align: left;
    width: 100%;
	border-bottom:1px solid #757575 !important;	
}

.form-grp {
    clear: both;
    float: left;
    margin: 0 0 20px;
    padding: 2px 5px;
    width: 16%;
}

#myTable_paginate, #myTable_length{
	display:block;
}

.dataTables_length {
	width:14%;
	display: inline-flex;
	
}

</style>
<?php $this->load->view('common/footer');?>
