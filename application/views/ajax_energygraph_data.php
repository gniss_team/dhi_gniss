<?php if(0 < count($energy_meter_data)) {?>
<script type="text/javascript">
$(document).ready(function () {

    $.jqplot._noToImageButton = true;

  var currentYear = [ <?php for( $i=0 ;$i<count($energy_meter_data); $i++) {
    											if( $i != count($energy_meter_data) -1  ){
												?>
    												["<?php echo $energy_meter_data[$i]->time_added;?>", <?php echo $energy_meter_data[$i]->energy_unit;?>],

    										 <?php } else {?>
    										 ["<?php echo $energy_meter_data[$i]->time_added;?>", <?php echo $energy_meter_data[$i]->energy_unit;?>]
    										 <?php  } } ?> 
    								];

    var plot1 = $.jqplot("chart1", [ currentYear], {
    	title: '<?php echo $graph_type;?> vs. Time',
    	 highlighter: {
             show: true,
             sizeAdjust: 1,
             tooltipOffset: 9
         },
         legend: {
             show: true,
             placement: 'outside'
         },
        
        axesDefaults: {
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer
        },
   
        seriesDefaults: {
            rendererOptions: {
                smooth: true
            },
           
            showMarker: true
        },
       
        axes: {
       
          xaxis: {
            label: "Time",
            labelOptions: {
                fontFamily: 'Georgia, Serif',
                fontSize: '12pt'
              },
            renderer: $.jqplot.DateAxisRenderer,
            tickOptions: {
                
              formatString:  "%R",
//              //  angle: -30,
              textColor: '#ff4466'
           },
         
            pad: 0
          },
          yaxis: {
            label: '<?php echo $graph_type;?>',
            labelOptions: {
                fontFamily: 'Georgia, Serif',
                fontSize: '12pt'
              },
            
            tickOptions: {
            	textColor: '#ff4466',
            	  labelPosition: 'middle', 
                  angle:270
            },
          }
        }
    });

      $('.jqplot-highlighter-tooltip').addClass('ui-corner-all');
     
});

</script>
<?php } else {?>
<div class="blank_div"><?php echo 'No data Found.'; ?></div>	
<?php  } ?>
<style>
.blank_div{
	color: #757575;
    display: inline;
    font-size: 13px;
}
</style>