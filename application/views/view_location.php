<?php $this->load->view('common/header');?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/infobox.js"></script>
 <link rel="stylesheet" href="<?php echo base_url(); ?>css/livetrack.css" />
<?php $this->load->view('common/sidebar');?>
<style>
.nav-map{width: 25%;float: left;}

</style>
<script>

var phone_num = '<?php echo $this->uri->segment(3);?>';
</script>


<div class="content clearfix">
<div class="septr">
<button  class="button-back" onclick="window.location='<?php echo base_url(); ?>livetracking'">Back To Livetracking </button></div>
<div class="title-box">
	<h4 class="page-title">View Location for Device ID: <?php echo $this->uri->segment(3);?></h4>
<div class="track-btn-container">    	
    	<button class="track-but" type="button" onclick="window.location='<?php echo base_url(); ?>assets/action/manage'"><i class="fa fa-cog"></i> &nbsp; Manage VTS (Trackers)</button>
    	<button id="dialog-link" class="track-but"><i class="fa fa-share-square-o"></i>&nbsp; Share</button>
    	<button id="dialog-link2" class="track-but"><i class="fa fa-thumbs-o-up"></i>&nbsp; Shared</button>    
    </div>
</div>    

    <!-- main content body-->
    <div class="clearfix">
    
   <span id="loader"> <img src="<?php echo base_url()?>/images/ajax-loader3.gif" ></span>
   <?php if($tot_tracer == 0) {?> <div style="background:#252525;position:absolute;z-index:3;width:67.5%;height:600px;opacity:0.8;"><div style="width:50%;margin-top:20%;margin-left:auto;margin-right:auto;padding:10px;background:#fff;text-align:center;border-radius:3px;"><h2>Your VTS is not activated. </h2></div></div> <?php }?>
   
		<div id="map" >
		
		</div>
	
    </div>
  
    	
    
</div>

<div id="load-dialog" title="Share Location" style="display:none; overflow:visible">
<form name='share' id="share" method='POST'>
			<div class="form-holder" style="min-height=80%;overflow:visible" >
						
						<div id="result"></div>
						
						<div  class="form-grp-profile success alert-success">
									
						</div>	
								<div class="form-grp-profile">
								  
								<p>Share location with anyone having a GNISS ID</p>
								<div class="serch-wrap">
								<div class="form-wrapper">
								
									<input type="text" class="form-control-profile abhijitscript"  placeholder="Enter Gniss User Name" autocomplete="off"  id="gniss_id" name="gniss_id" value="">
									<input type="hidden" name="sharemobile" value="<?php echo $this->uri->segment(3);?>">
							    </div>								
							<span id="loader_share" style="display:none;"> <img src="<?php echo base_url()?>/images/ajax-loader3.gif" ></span>
										<div class="autodropdown"> 
											<ul class="suggestresult"></ul>
										</div>
										<div class="container_tag" id="ap_tags">
											
											
										</div>
										
								</div>
								
									<div id='err_mobile_verify' class="error alert-error"></div>
									
											
							
								<div class="btn-grp" style= "margin-left:0;">

											<button class="signup"  type="submit" class="btn btn-primary"  >Share</button>
								</div>
						</div>
						
			</div>
			</form>
</div>

<div id="load-dialog2" title="Shared Location" style="display:none;">
			<table class="zebra"> 
<thead> 
<tr> 
    <th>Name</th> 
    <th>Device</th> 
    <th>Shared Link</th> 
    <th>Action</th> 
  
</tr> 
</thead> 
<tbody> 
<?php foreach($getsharedloc as $result ) {?>
<tr> 
    <td id="imgcng_<?php echo $result->id;?>"><?php if($result->is_active=='1') {?> <img src="<?php echo base_url()?>/images/green.png">  <?php }else {?>
    <img src="<?php echo base_url()?>/images/grey.png"> <?php }?>
    
    <?php echo $result->first_name." ".$result->last_name;?></td> 
    <td><?php echo $result->shared_device; ?></td> 
    <td><?php echo base_url();?>livetracking/share/<?php echo $result->shared_device; ?>
     <input type='hidden' id="status_<?php echo $result->id;?>" value="<?php echo $result->is_active;?>">  
    </td> 
   <td>
     <a href="#" onclick="changests('<?php echo $result->id;?>','<?php echo $result->shared_device;?>','<?php echo $result->shared_gnissid;?>');"> 
   <span id="csts_<?php echo $result->id;?>"><?php if($result->is_active=='1') {?> <button class="deact-btn">Deactivate </button><?php }else {?> <button class="act-btn">Activate</button> <?php } ?>
    </span>
    </a>
   
    </td> 
  
</tr> 
<?php } ?>

 
</tbody> 
</table> 
</div>

	
<script type="text/javascript">



var map;

var arrMarkers = [];

var arrInfoWindows = [];



function mapInit(){

	var centerCoord = new google.maps.LatLng(<?php echo $lastlatlong->latitude; ?> ,<?php echo $lastlatlong->longitude; ?>); 

	var mapOptions = {

		zoom: 16,

		center: centerCoord,

		mapTypeId: google.maps.MapTypeId.ROADMAP

	};

	map = new google.maps.Map(document.getElementById("map"), mapOptions);
	
	

}

$(function(){
	mapInit();
	
	$("#markers").on("click","a", function(){
		var i = $(this).attr("rel");
		arrInfoWindows[i].open(map, arrMarkers[i]);
	});

		mapInit();
		  		$("#markers").on("click","a", function(){
			var i = $(this).attr("rel");
			arrInfoWindows[i].open(map, arrMarkers[i]);
		});

		  		setTimeout(function(){ readMap(); }, 3000);
   		window.setInterval(function(){
   			readMap();
   			
   		}, 10000);
   		
   		
		});

	 var profile="";
      var side_bar_html = "";
		var gmarkers = [];
		var htmls = [];
		var points = [];
		var lats = [];
		var lngs = [];
		var markersArray = new Array();
		
		var i = 0;
		var infowindow;
		var infobox;
		var base_url = '<?php echo base_url(); ?>';

		  
       
      // A function to create the marker and set up the event window
       function createMarker(point,name,html,lat,lng,icontype,vehicle_icon,token,phoneno,marker_icon) {
    	  
	   
	     	var base_url = '<?php echo base_url(); ?>';
		if(vehicle_icon==0) 
		{
		vehicle_icon="marker_red.png"; 

	     	}
		var blueIcon="images/marker_red.png"; 
   var marker = new google.maps.Marker({
							position: point,
							map: map,
							  animation: google.maps.Animation.DROP,
								
							optimized:false, // <-- required for animated gif
							//animation: google.maps.Animation.DROP,
							icon: base_url+"images/map_icons/"+marker_icon,
						
								
							//icon: blueIcon,
							title:name
							});
	   marker.setAnimation(google.maps.Animation.BOUNCE);
							
		markersArray.push(marker);
		var geocoder= new google.maps.Geocoder();
			geocoder.geocode({'latLng': point }, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) {
				if (results[0]) {
					html=html+results[0].formatted_address;
				}
			}
			}
			});
			
      google.maps.event.addListener(marker, "click", function() {
		

	var st = "<div id='myImage'><div class='clearFix'></div><div class='close'></div>"+html+"</div>";
		if (infobox) infobox.close();
		 
		  infobox = new InfoBox({
			    content: st,
			    disableAutoPan: false,
			    maxWidth: 150,
			    pixelOffset: new google.maps.Size(-160,-160),
			    zIndex: null,
			    boxStyle: {
			                background: "url('') no-repeat",
			                opacity: 1,
			                width: "280px" 
			        },
			    closeBoxMargin: "5,20,30,5",
			    closeBoxURL: "/map_icons/close_hover.png",
			    infoBoxClearance: new google.maps.Size(1, 1)
			});	 
		
		  infobox.open(map, marker);
		});
		points[i]=point;
        lats[i]=lat;
        lngs[i]=lng;
        gmarkers[i] = marker;
		htmls[i] = html;
    //   side_bar_html += '<li><a href="javascript:void(0);" >' + name + '</a><ul><li><a href="<?php echo base_url()?>livetracking/routedatewisemap/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/route-icon.png">Route Map</a></li><li><a href="livetracking/livelocation/'+phoneno+'" "><img width="22" src="<?php echo base_url()?>images/map_icons/track-icon.png">Live Track</a></li><li><a href="livetracking/geofench/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/fench-icon.png">Fence</a></li></ul></li>';
        //side_bar_html += '<a href="javascript:myclick(' + (gmarkers.length-1) + ')">' + name + '<\/a><br>';

        i++;
        
        return marker;    
		}
    	  
		function clearOverlays() {
		  if (markersArray) {
			for (var i = 0; i < markersArray.length; i++ ) {
			  markersArray[i].setMap(null);
			}
		  }
		}
  

      // This function picks up the click and opens the corresponding info window
     function myclick(i) {
		//alert('fgh');
		google.maps.event.trigger(gmarkers[i], "click");
		map.setZoom(14);
		}


      
      
      
      // A function to read the data
 // A function to read the data
    function readMap(url) {
     
       //alert("refresh");
        var baseurl= '<?php echo base_url();?>';
      <?php if($phone_number!="") { ?>  
        var url=baseurl+"livetracking/demogetLatlong/<?php echo $phone_number; ?>";
        <?php }else{?>
        var url=baseurl+"livetracking/demogetLatlong";
        <?php } ?>
            $('#loader').hide();
          //  side_bar_html = "";
			htmls = [];
			i = 0;
            clearOverlays();
            gmarkers = [];
            address1="";
            $.get(url, function( data ) {
       		 if(data.flag==1) {
       					
       		  $.each(data.live_locations, function(key,value) {
       					      var lat = parseFloat(value.latitude);				  
       						  var lng = parseFloat(value.longitude);
       						  var icon_type = "";
       			              var point = new google.maps.LatLng(lat,lng);
       						  var html = "Name :"+value.name +"<br>"+"Speed:"+value.speed+"<br>Date: "+value.dated+"<br>Address:" +address1;
       			              var name =value.name;
       			           var label =value.name;
       						  var category = "";
       					     var vehicle_icon = "";
       					     var marker_icon = value.marker_icon;
       					     var token = "";
       					     var phoneno = value.phoneno;
       					        	
       					         var marker = createMarker(point,label,html,lat,lng,icon_type,vehicle_icon,token,phoneno,marker_icon);
       						  marker.setMap(map);
       						/* side_bar_html += '<li><div class="nav-map"><a href="javascript:void(0);" >' + name + '<\/a></div><div class="nav-map"><a href="<?php echo base_url()?>livetracking/routewiselocation/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/route-icon.png">Route Map</a></div><div class="nav-map"><a href="livetracking/livelocation/'+phoneno+'" "><img width="22" src="<?php echo base_url()?>images/map_icons/track-icon.png">Live Track</a></div><div class="nav-map"><a href="livetracking/geofench/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/fench-icon.png">Fence</a></div></li>';*/
       				
       					        	});
       						 
       					//	document.getElementById("side_bar").innerHTML = side_bar_html;
          		  }
       		},"json" );
          
 			
          }
        
      
      // When initially loaded, use the data from "map11.php?q=a"
      readMap("a");
      
    
   $(document).ready(function(){
		   
   $( "#load-dialog" ).dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		buttons: [
			
//			{
//				text: "Cancel",
//				click: function() {
//					$( this ).dialog( "close" );
//				}
//			}
		],
		beforeClose: function(event, ui) {
     	$('#gniss_id').val("");
     	$('#ap_tags').html("");
     	$('.suggestresult').html('');
     	
            }
	});

   $( "#load-dialog2" ).dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		buttons: [
			
//			{
//				text: "Cancel",
//				click: function() {
//					$( this ).dialog( "close" );
//				}
//			}
		]
	});

	$( "#dialog-link" ).click(function( event ) {
		$( "#load-dialog" ).dialog( "open" );
		event.preventDefault();
	});

	$( "#dialog-link2" ).click(function( event ) {
		$( "#load-dialog2" ).dialog( "open" );
		event.preventDefault();
	});
	
   });
	

   

   

   function share_location()
   {
	  var tognissId = $('#gniss_id').val();
	  var Sharemobile = "<?php echo $this->uri->segment(3)?>";
	   $.get("<?php echo base_url(); ?>CManageLiveTrackingController/sharelocation/"+Sharemobile+"/"+tognissId, function( data ) {
		   $( "#result" ).html( data );
		  
		 });
   }

   function changests(id,Sharemobile,tognissId)
   {
	 
	  var status = $('#status_'+id).val();
		   $.get("<?php echo base_url(); ?>CManageLiveTrackingController/ShareActiveDeactive/"+status+"/"+Sharemobile+"/"+tognissId, function( data ) {
			
			 if(data.flag=='1')
			 {
				 if(status=='1')
				 {
				   $('#csts_'+id).html('Active');
				   $('#status_'+id).val('0');
				   $('#imgcng_'+id).html("<img src='<?php echo base_url()?>/images/grey.png'>");
				   
				  
				 }else {
				   $('#csts_'+id).html('DeActive');
				   $('#status_'+id).val('1');
				   $('#imgcng_'+id).html("<img src='<?php echo base_url()?>/images/green.png'>");
				 }

		     }
			  
		   },"json" );
   }

   $(document).ready(function(){
		$('.abhijitscript').keyup(function(){
			var query_string = $.trim($(this).val());
			
			if(query_string != ''){
				$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>CManageLiveTrackingController/autosuggetionUserForLocation/",
				data: { name:query_string },
				success: function(data)
				{
					$('.suggestresult').html(data);
					$('.suggestresult li').click(function(){
						
						var return_html = $(this).html();
						var return_value = $(this).text();
						$('#ap_tags').append("<div class='dialog_tag' id="+return_value+">"+return_html+" <a href='javascript:void(0);' onclick='javascript:$(this).parent().remove();' class='close-thik'><i class='fa fa-times'></i></a></div>");
						
						$('.abhijitscript').attr('value', return_value); 
						$('.abhijitscript').val(return_value);
						$('.suggestresult').html('');
						$('#gniss_id').val("");
					});
					
				}
			});
			}else {
				$('.suggestresult').html('');
				}
		});
		   $('.close-thik').click(function(){
			   alert('ff');
			   	 $(this).parrent().remove();

			}); 
			
	});


   $("#share").on("submit", function(event) {
	 	
	 	event.preventDefault();
			$('#loader_share').show();
	        $.ajax({
	            url: "<?php echo base_url(); ?>CManageLiveTrackingController/sharelocation/",
	            type: "post",
	            async: false,
	            data: $(this).serialize(),
	            success: function(d) {
	            	$('.dialog_tag').remove();
	            	$('#gniss_id').val("");
	            	$('#loader_share').hide();
	            				var d = JSON.parse(d);
				          
				            	if(d.error=='true') {
				            	
				              		 $( "#result" ).html( d.msg );
				            } else 	{
				            	 $( "#result" ).html( d.msg );
				            		  
				              		
				             }
	            	}
	        });
});
   
	
		
    </script>
 <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css" />
<script src="<?php echo base_url(); ?>js/external/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
 <?php $this->load->view('common/footer');?>
