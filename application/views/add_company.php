<?php $this->load->view('common/header');?>
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Register Company</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>welcome">Dashboard</a></li>
			<li><a class="big" href="<?php echo base_url(); ?>company/action/viewcompanies">Manage Company</a></li>
			<li>Register Company</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
	<?php echo form_open('company/action/insertcompany'); ?>
       <span class="asterisk-msg">All fields marked with * are mandatory.</span>
			<div class="form-grp">
				<label>Company Name <span class="red">*</span> </label>
				<input class="form-control" placeholder="Company name" id="company_name" name="company_name" maxlength="50" value="<?php echo set_value('company_name'); ?>" >
				<?php echo form_error('company_name'); ?>
			</div>
			<div class="form-grp">
	            <label>Email address <span class="red">*</span> </label>
	            <input class="form-control" placeholder="Email address" id="email_address" name="email_address" maxlength="50" value="<?php if( true == isset( $customers_info ) ) { echo $customers_info[0]->email_address; } else { echo set_value('email_address'); } ?>" >
    	        <?php echo form_error('email_address'); ?>
			</div>
			<div class="form-grp">
	            <label>Mobile number <span class="red">*</span> </label>
	            <input class="form-control" placeholder="Mobile number" id="mobile_number" name="mobile_number" maxlength="12" value="<?php if( true == isset( $customers_info ) ) { echo $customers_info[0]->mobile_number; } else { echo set_value('mobile_number'); } ?>" >
    	        <?php echo form_error('mobile_number'); ?>
			</div>
			<div class="form-grp">
				<label>Username <span class="red">*</span> </label>
				<input class="form-control" placeholder="Username" id="username" name="username" maxlength="40" value="<?php echo set_value('username'); ?>" >
				<?php echo form_error('username'); ?>
			</div>
			<div class="form-grp">
				<label>Password <span class="red">*</span> </label>
				<input class="form-control" type="password" placeholder="Password" id="password" name="password" maxlength="40" value="<?php echo set_value('password'); ?>" >
				<?php echo form_error('password'); ?>
			</div>
			<div class="form-grp">
				<label>Confirm Password <span class="red">*</span> </label>
				<input class="form-control" type="password" placeholder="Confirm password" id="conf_password" name="conf_password" maxlength="40" value="<?php echo set_value('conf_password'); ?>" >
				<?php echo form_error('conf_password'); ?>
			</div>
			<div class="form-grp">
				<label>Company address <span class="red">*</span></label>
				<textarea class="form-control" placeholder="Company address" id="company_address" name="company_address" maxlength="105"><?php if( true == isset( $customers_info ) ) { echo $customers_info[0]->company_address; } else { echo set_value('company_address'); } ?></textarea>
				<?php echo form_error('company_address'); ?>
			</div>
				<div class="form-grp">
		            <label>Country <span class="red">*</span> </label>
	    	        <select class="form-control styled-company" id="country_id" name="country_id" >
		    	        <option value="" >Select country</option>
                	    <?php foreach( $countries as $country ) { ?>
 					<option value="<?= $country->id; ?>" <?php if( true == isset( $customers_info[0]->country_id ) ) { if($country->id ==$customers_info[0]->country_id) { ?> selected="selected" <?php } } ?> <?php echo set_select('country_id', $country->id ); ?>><?= $country->name; ?> </option>
					<?php } ?>
				</select>
				<?php echo form_error('country_id'); ?>
			</div>
			<div class="form-grp">
	            <label>State <span class="red">*</span> </label>
	            <select class="form-control styled-company" id="state_id" name="state_id" >
		            <option value="" >Select state</option>
	    	        <?php foreach( $states as $state ){ ?>
 					<option value="<?= $state->id; ?>" <?php if( true == isset( $customers_info[0]->state_id ) ) { if( $state->id == $customers_info[0]->state_id) { ?> selected="selected" <?php } } ?> <?php echo set_select('state_id', $state->id ); ?> ><?= $state->name; ?></option>
					<?php } ?>
				</select>
				<span id="state_id_loader"></span>
            	<?php echo form_error('state_id'); ?>
			</div>
			<div class="form-grp">
	            <label>City <span class="red">*</span> </label>
	            <select class="form-control styled-company" id="city_id" name="city_id">
		             <option value="" >Select city</option>
	    	        <?php foreach( $cities as $city ){ ?>
 					<option value="<?= $city->id; ?>" <?php if( true == isset( $customers_info[0]->city_id ) ) { if( $city->id == $customers_info[0]->city_id) { ?> selected="selected" <?php } } ?> <?php echo set_select('city_id', $city->id ); ?> ><?= $city->name; ?></option>
					<?php } ?>
        	    </select>
            	<span id="city_id_loader"></span>
            	<?php echo form_error('city_id'); ?>
			</div>
			<div class="form-grp">
				<label>Zip code </label>
			    <input class="form-control" placeholder="Zip code" id="zip_code" name="zip_code" maxlength="50" value="<?php if( true == isset( $customers_info ) ) { echo $customers_info[0]->zip_code; } else { echo set_value('zip_code'); } ?>" >
			    <?php echo form_error('zip_code'); ?>
			</div>
			<div class="form-grp">
				<label>Security Question <span class="red">*</span> </label>
				<select class="form-control styled-company" id="question_id" name="question_id">
			    	<option value = "">Select question</option>
	                <?php  foreach( $security_question as $strQuestion ) { ?>
		 			<option value="<?= $strQuestion->id; ?>" <?php echo set_select('question_id', $strQuestion->id ); ?>><?= $strQuestion->question; ?> </option>
					<?php  } ?>
				</select>
				<?php echo form_error('question_id'); ?>
      		</div>
      		<div class="form-grp">
      		<label>Answer <span class="red">*</span> </label>
				<input class="form-control" placeholder="Answer for security question" id="answer" name="answer" value="<?php echo set_value('answer'); ?>"/>
				<?php echo form_error('answer'); ?>
			</div>
			<div class="btn-grp">
				<button type="submit" name="add"  class="btn btn-primary">Submit</button>
				<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>company/action/viewcompanies'">Cancel</button>
			</div>

		<?php  echo form_close(); ?>
	</div>

</div>
</div>
<script type="text/javascript">
var strUrl = "<?php echo base_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/locations.js"></script>
<?php $this->load->view('common/footer');?>
