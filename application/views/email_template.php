<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>


<style type='text/css'>


.clearfix:before,
.clearfix:after {
  content: "";
  display: table;
}
 
.clearfix:after {
  clear: both;
}
 
.clearfix {
  zoom: 1; /* ie 6/7 */
}	

</style>
</head>

<body >
	
<div style="width:600px;">
	<div class="globe">
	<div class="logo">
    	<img src="<?php echo base_url();?>images/logo-gniss.png" />
    </div>
    <div style="padding:5px 30px;">
    <div class="email-content">
    <p style="font-size:14px;">{header}</p>
<span>{content}</span>

</div>
	
    </div>
    
    <div class="email-footer clearfix" style="padding:5px 20px 3px;
	background:#2d92ca;
	position:relative;
	top:52px;">
    
        <div class="so-icons" style="float:right;">
        	<ul style="margin:0;padding:0;">
            <li style="float:left;list-style:none;"><a href="https://www.facebook.com/rectusenergy" target="_blank"><img src="<?php echo base_url();?>images/facebook-icon.png" /></a></li>
            <li style="float:left;list-style:none;"><a href="#" target="_blank"><img src="<?php echo base_url();?>images/google-icon.png" /></a></li>
            <li style="float:left;list-style:none;"><a href="https://in.linkedin.com/company/rectus-energy-private-limited" target="_blank"><img src="<?php echo base_url();?>images/linkedin-icon.png" /></a></li>
            <li style="float:left;list-style:none;"><a href="https://twitter.com/rectusenergy" target="_blank"><img src="<?php echo base_url();?>images/twitter-icon.png" /></a></li>
            </ul>
        </div>
    </div>
    </div> 
</div>
</body>
</html>
