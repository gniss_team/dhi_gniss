<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<style>
.signup {
	margin: 0;
}
.error {
	left:160px;
}
.alert-success {
 	padding:5px 0;
 	margin:5px 0;
 }
.breadcrums li a {
    background: none;
}
.form-grp label {
	 width: 40%;
}
.content-wrap {
	left:0%;
	margin:0 0px;
}
table.tablesorter {
		background-color:none !mportant;
		margin: 0 0 0;
}
.dataTables_length label {
	width: auto;
	display: inline-flex;
}
.dataTables_length select {
	margin: -6px 12px;
	padding: 5px 10px 5px 0px;
	/*-webkit-border-radius:4px;*/
   -moz-border-radius:4px;
   /* border-radius:4px;
  -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
   box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;*/
   background: url("../../../images/select-arrow.png") no-repeat scroll 98% center #fff;
   color:#555;
   border:1px solid #e3e3e3;
   outline:none;
   display style="margin:0 18px;": inline-block;
   -webkit-appearance:none;
   -moz-appearance:none;
   appearance:none;
   cursor:pointer;
}
.dataTables_paginate{
    float: right;
    font-size: 13px;
    padding-top: 0px;
    padding-right: 12px;
    padding-bottom: 6px;
    padding-left: 12px;
    text-align: right;
}
.dataTables_wrapper .dataTables_filter{
	margin-top: 4px;
}
#myTable_info{
	font-size:13px;
}
table.dataTable {
	width:98% !important;
	margin-left:0px !important;	
}
.img_icon {
	margin-right:10px;
	cursor:pointer;
}	
#img_livetrack {
	cursor:pointer;
}
table.tablesorter thead tr .noheader {
    border-right:1px solid;
}
</style>
<div class="content clearfix">
	<div class="page-title"><h4>Shared Devices</h4></div>
	<div class="content-wrap clearfix">
       
		<table class="table table-bordered table-hover table-striped tablesorter" id="myTable">
	    	<thead>
            	<tr>
                	<th style="display:none">Id</th>
                    <th>Shared By</th>
                    <th>Device Number</th>
                    <th>Name</th>
                     <th>Shared link</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
				<?php foreach ( $users->result() as $intIndex => $objSharedBy ) {?>
                <tr class="active">
                	<td style="display:none"><?php $objSharedBy->id; ?></td>
                    <td><?php echo $objSharedBy->first_name . ' ' . $objSharedBy->last_name; ?></td>
                    <td><?php echo $objSharedBy->phone_number; ?></td>
                     <td><?php echo $objSharedBy->name; ?></td>
                     <td><a href="<?php echo base_url() . 'livetracking/share/' . $objSharedBy->phone_number; ?>" target="_blank"><?php echo base_url() . 'livetracking/share/' . $objSharedBy->phone_number; ?></a></td>
                   <td><?php echo $objSharedBy->address; ?></td>
                    
 				</tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->load->view('common/footer');?>
