<?php $this->load->view('common/header');?>

<div id="container">
	<h1>Login </h1>

	<div id="body">
	<?php if($msg!="") { ?>
					<div class="alert alert-success" >
						 <?php echo $msg; ?>
					</div>
					<?php } ?>
		<?php echo form_open('admin/action/login'); ?>
		<div class="form-grp">
		
			<div class="form-grp">
				<label>Username<span style="color:red;">*</span> </label>
				<input class="form-control" placeholder="Username" id="username" name="username" maxlength="40" value="<?php echo set_value('username'); ?>" >
				<?php echo form_error('username'); ?>
			</div>
			
			<div class="form-grp">
				<label>Password<span style="color:red;">*</span> </label>
				<input type="password" class="form-control" placeholder="Password" id="password" name="password" maxlength="40" value="<?php echo set_value('password'); ?>" >
				<?php echo form_error('password'); ?>
			</div>
			
			<div class="form-grp">
				<input type="checkbox" class="form-control" id="remember_me" name="remember_me" value="yes" >
				<span>Remember Me </span>
				
			</div>
		
			<div class="btn-grp">
				<button type="submit" name="add"  class="btn btn-primary">Submit</button>
				<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>'">Cancel</button>
			</div>

		<?php  echo form_close(); ?>
	
		<a href="<?php echo base_url(); ?>users/action/register">Register</a>
		<a href="<?php echo base_url(); ?>users/action/forgotpass">Forgot Password</a>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>

<?php $this->load->view('common/footer');?>