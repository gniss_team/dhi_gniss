<script type="text/javascript">

$(document).ready(function(){
	 $.jqplot.config.enablePlugins = true;
	  var line=[<?php for( $i=0 ;$i<count($energy_meter_data); $i++) {
			if( $i != count($energy_meter_data) -1  ){
			?>
					["<?php echo $energy_meter_data[$i]->created_on;?>", <?php echo $energy_meter_data[$i]->watts;?>],

			 <?php } else {?>
			 ["<?php echo $energy_meter_data[$i]->created_on;?>", <?php echo $energy_meter_data[$i]->watts;?>]
			 <?php  } } ?> ]
		 
     
	//  var line2=['2015-07-03','2015-07-04'];
	  var plot2 = $.jqplot ('watthour', [line], {
		  seriesColors: ["rgba(0, 100, 255, 1)", "rgb(255, 0, 0)","rgb(76, 153, 0)","rgb(153, 0, 153)","rgb(255, 255, 51)","rgb(0, 255, 255)"],
	      title:'', 
	      highlighter: {
              show: true,
              sizeAdjust: 1,
              tooltipOffset: 9
          },
          grid: {
        		 background: 'rgba(57,57,57,0.0)',
              drawBorder: false,
              shadow: false,
              gridLineColor: '#D6D6C2',
              gridLineWidth: 0.5
          },
          legend: {
              show: true,
              placement: 'outside'
          },
          seriesDefaults: {
              rendererOptions: {
                  smooth: true,
                  animation: {
                      show: false
                  }
              },
             
              showMarker: true,
         
              lineWidth: 0.7
          },
          series: [
              {
                  label: 'Watt'
              },
          ],
          axesDefaults: {
              rendererOptions: {
                  baselineWidth: 1,
                  baselineColor: '#0000',
                  drawBaseline: false
                  //lineWidth:0.7
              }
          },
	      axes:{
	        xaxis:{
	        	label:'Hours',
	          renderer:$.jqplot.DateAxisRenderer, 
	         // ticks:line2,
	          tickOptions:{pad: .2,showGridline: true,formatString:' %R',fontSize:'8pt', fontFamily:'Tahoma', angle:50,textColor:'#666666',mark: 'outside'}
	        //  min:'<?php  echo date('Y-m-d');?>', 
	      //   max:'<?php echo date('Y-m-d');?>'
	        //  tickInterval:'1 day'
	        },
	        yaxis:{
	        	  label:'watt',
		          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
		          labelOptions: {
		            fontFamily: 'Georgia, Serif',
		            fontSize: '12pt'
		          }
		          }
	      }
	      
	 //    series:[{lineWidth:0.7}]
	     
	  });
	});

</script>

<style>
.blank_div{ 
	color: #757575;
    display: inline;
    font-size: 13px;
}
.jqplot-highlighter-tooltip{background:#282828; color:#fff;}
.jqplot-target {
     color:#666666;
}
.ui-widget-content {
    background:white !important;
}
</style>