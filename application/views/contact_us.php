<?php $this->load->view('common/header');?>
<?php $this->load->view('common/template_style');?>
<div class="sub-contact">
	<h2> Contact Us </h2>
	<div class="map">
	<div style="text-decoration:none; overflow:hidden; height:500px; width:1000px; max-width:100%;"><div id="gmap_display" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=GT+Arcade,+BT+Kawade+Road,+Uday+Baug,+Ghorpadi,+Pune+411001+&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="embedded-map-code" href="https://www.hostingreviews.website" id="auth-maps-data">hostingreviews.website</a><style>#gmap_display .text-marker{max-width:none!important;background:none!important;}img{max-width:none}</style></div><script src="https://www.hostingreviews.website/google-maps-authorization.js?id=2c7b9bd6-cbb1-2b36-615d-f3db33b83e5a&c=embedded-map-code&u=1461237719" defer="defer" async="async"></script>
</div><!--map-->
	<div class="contact-1">
		<h1>Headquarters</h1>
		<p class="co-address1">Dhi Networks Private Limited<br>
		Office Nos. 401 & 402,<br>
		4th Floor, GT Arcade,<br>
		BT Kawade Road, Uday Baug,<br>
		Ghorpadi,<br>
		Pune 411001<br>
		Maharashtra, India </p>
	</div><!--contact-1-->
	
	<div class="contact-2">
		<h1>R&D Center</h1>
		<p class="co-address2">Sr. No. 35/1, Yepare Patil Warehouse,<br>
		Dagade Wasti,<br>
		Pisoli, Taluka Haveli,<br>
		Pune 411060<br>
		Maharashtra, India </p>
	</div><!--contact-2-->
			
	<div class="contact-3">
		<h1>Contact Us</h1>
		<p class="co-business">For Business enquiry</p>
		<p class="co-address3"><b>Telephone:</b><br>
		+91-20-60607001<br>
		+91-20-60607002<br>
		+91-20-26718099<br>
		<b>Email:</b> info@dhinet.net </p>
	</div><!--contact-3-->
</div><!--sub-contact-->
<?php $this->load->view('common/footer');?>
