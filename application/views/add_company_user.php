<?php $this->load->view('common/header');?>
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/sidebar');?>
<div class="content clearfix">
	<div class="page-title"><h3>Add User</h3></div>
	<div class="breadcrums">
		<ul>
			<li><a class="big" href="<?php echo base_url(); ?>welcome">Dashboard</a></li>
			<?php if( 'Individual' == $this->session->userdata('login_type') ) {?>
			<li><a class="big" href="<?php echo base_url(); ?>company/action/viewcompanies">Manage Company</a></li>
			<?php } else { ?>
			<li><a class="big" href="<?php echo base_url(); ?>company/action/viewcompanyusers">View Users</a></li>
			<?php }?>
			<li>Add User</li>
		</ul>
	</div>
	<div class="content-wrap clearfix">
	<span class="asterisk-msg">All fields marked with * are mandatory.</span>
	<?php echo form_open('company/action/insertcompanyuser/'.$this->uri->segment(4)); ?>
		<div class="form-grp">
			<label>Account Number<span class="red">*</span> </label>
			<input class="form-control" placeholder="User's Account number" id="account_number" name="account_number" maxlength="40" value="<?php echo set_value('account_number'); ?>" >
		
			<span class="error" style="color:red;left:160px;top:15px;float:left;"><?php echo form_error('account_number'); ?>
			<?php if(isset($msg) && $msg != '') { echo $msg; }?>
			</span>
		</div>
		
		<?php if( 'Individual' == $this->session->userdata('login_type') ) {  ?>
		 <div class="form-grp">
        	<label> Company <span class="red">*</span> </label>
            	<select class="form-control-select styled" name="company_id" id="company_id">
					<option value="" >Select</option>
                    <?php foreach( $companies as $company  ) { 
                    		if( $company->id == $this->uri->segment(4) ) {
                    ?>
                    	<option value="<?php echo $company->id; ?>" selected="selected" ><?php echo $company->company_name; ?></option>
                   		<?php } else {?>
 						<option value="<?php echo $company->id; ?>" placeholder="Select Company" <?php if( true == isset( $assets->owner_id ) ) { if($company->id == $assets->owner_id ) { ?> selected="selected" <?php } } ?> <?php echo set_select('company_id', $company->id ); ?>><?php echo $company->company_name; ?></option>
					<?php } } ?>
                </select>
				<span class="red"><?php echo form_error('company_id'); ?></span>
        </div>
		 <?php } ?>
		<div class="btn-grp">
			<button type="submit" name="add"  class="btn btn-primary">Submit</button>
		<?php if( 'Individual' == $this->session->userdata('login_type') ) { ?>
			<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>company/action/viewcompanies'">Cancel</button>
		<?php } else {?>
			<button type="reset" class="btn btn-default" onclick="window.location='<?php echo base_url(); ?>'">Cancel</button>
		<?php } ?>		
		</div>

		<?php  echo form_close(); ?>
	</div>
</div>
</div>
<?php $this->load->view('common/footer');?>