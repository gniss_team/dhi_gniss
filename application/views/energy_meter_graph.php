<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<script>var base_url = "<?php echo base_url(); ?>";</script>
<link rel="stylesheet" type="text/css" hrf="<?php echo base_url(); ?>graph_assets/jquery-ui.css" />
<link type="text/css" href="<?php echo base_url(); ?>js/jquery.datetimepicker.css" rel="stylesheet" />
<link type="text/css" href="<?php echo base_url(); ?>graph_assets/jquery.jqplot.min.css" rel="stylesheet" />
<link type="text/css" href="<?php echo base_url(); ?>css/simpletabs.css" rel="stylesheet" />
<style type="text/css">
       
</style>

<link type="text/css" href="<?php echo base_url(); ?>graph_assets/jquery.jqplot.min.css" rel="stylesheet" />
    <pre class="code brush:js"></pre>
	<div class="content clearfix">
		<div class="page-title"><h4> Energy Meter Graphs</h4></div>
		
	 <div class="simpleTabs">
	 
		        <ul class="simpleTabsNavigation">
		            <li><a href="javascript:void(0)" id="live_graph" >Real time</a></li>
		              <li><a href="javascript:void(0)" >Range</a></li>
		            <li><a href="javascript:void(0)" id="meter_graph">Realtime Meter Graphs</a></li>
		             <li><a href="javascript:void(0)" id="voltcurr">Voltage v/s Current</a></li>
		             <li><a href="javascript:void(0)" id="watt_hour">Todays Watt Hour</a></li>
		        </ul>
		        <div class="simpleTabsContent">
	
		<div class="para-block" style="">
	   		<input class="radiobx" type="radio" onclick="getlivegraph('voltage');"  name="p_type"  id="t_voltage" value="voltage" checked="checked" ><label class="chkbox" for="label_linegraph"><span>Voltage</span></label>
		</div>
		<div class="para-block">
	   		<input class="radiobx" type="radio" onclick="getlivegraph('current');"  name="p_type"  id="t_current" value="current" ><label class="chkbox" for="label_bargraph"><span>Current</span></label>
		</div>
		<br><br><br>
		        <div id="livedata"></div></div>
		        <div class="simpleTabsContent">
		        	<div id="error_mg" style="margin:0px;"></div>
						<?php echo form_open('energymeter/action/viewGraph', array('id'=>'line_graph')); ?>
						<div id="validation-error" style="color:red"></div>
						<div>
	<div style="clear:both;">
		<label style="">From Date:</label>
	   	<input id="from_date" type="text" name="from_date" class="form-control-cal" value="<?php //echo date('d/m/Y');?>" readonly="readonly" >
	   	<label style=" margin-left: 25px;">To Date:</label>
	   	<?php echo form_error('from_date'); ?>
		<input id="to_date" type="text" name="to_date" class="form-control-cal" value="<?php //echo date('d/m/Y');?>" readonly="readonly" >
	 </div>
	 <div>
	 	<div style="margin:10px 10px 10px 0px;float:left;">
			<label>Type of graph :</label>	   		
		</div>
		<div class="para-block" style="">
	   		<input class="radiobx" type="radio"  name="graphtype"  id="line" value="line" checked="checked" ><label class="chkbox" for="label_linegraph"><span><?php echo CManageEnergyMeterController::C_LINE; ?></span></label>
		</div>
		<div class="para-block">
	   		<input class="radiobx" type="radio"  name="graphtype"  id="bar" value="bar" ><label class="chkbox" for="label_bargraph"><span><?php echo CManageEnergyMeterController::C_BAR; ?></span></label>
		</div>
		<div class="para-block">
	   		<input class="radiobx" type="radio"  name="graphtype"  id="piechart" value="piechart" ><label class="chkbox" for="label_piechart"><?php echo CManageEnergyMeterController::C_PIE; ?><span></span></label>
		</div>
	</div> 
	<div>
		<div style="margin:10px 0px 10px 0px;clear:both;float:left;">
			<label>Graph parameter :</label>	   		
		</div>
		<div>
			<div class="para-block show_chkbox">
				<input class="checkbx" type="checkbox"  name="unit_type[]"  id="voltage" value="voltage" ><label class="chkbox" for="label_watt">Voltage<span></span></label>
			</div>
			<div class="para-block show_chkbox">
				<input class="checkbx" type="checkbox"  name="unit_type[]"   id="current" value="current" ><label class="chkbox" for="label_watt">Current<span></span></label>
			</div>
			<div class="para-block show_chkbox">
				<input class="checkbx" type="checkbox"  name="unit_type[]"   id="watt" value="watts" ><label class="chkbox" for="label_watt">Watt<span></span></label>
			</div>
		</div>
		<div style="margin-left:107px;">
			<div class="para-block show_chkbox" style="clear:both;">
				<input class="checkbx" type="checkbox"  name="unit_type[]"   id="watt-s" value="watts_second" ><label class="chkbox" for="label_watt">Watt-s<span></span></label>
			</div>
			<div class="para-block show_chkbox">
				<input class="checkbx" type="checkbox"  name="unit_type[]"   id="watt-h" value="watts_hour" ><label class="chkbox" for="label_watt">Watt-h<span></span></label>
			</div>
			<div class="para-block">
				<input class="checkbx" type="checkbox"  name="unit_type[]"  id="watts" value="kwh" ><label class="chkbox" for="label_watt">KWH<span></span></label>
			</div>
		</div>
		<div style="margin-left:107px;">
			<div class="para-block show_chkbox" style="clear:both;">
			<label class="chkbox" for="label_watt">Min Value<span></span></label>
				<input class="checkbx" type="text"  name="min"   id="min"  >
							<label class="chkbox" for="label_watt">Max Value<span></span></label>
				<input class="checkbx" type="text"  name="max"   id="max"  >
			</div>
	</div>
		
	</div>
	<div style="clear:both;float:left;margin:10px 0px"><button name="show" class="signup" type="submit" id="show"  class="btn btn-primary">Show Graph </button></div>
	</div>	
	</form>
		<div class="content-wrap clearfix">
		<div class="ui-widget ui-corner-all">
			<div class="ui-widget-content ui-corner-bottom" >
		<img style="margin-left:170px; display:none;" src="<?php echo base_url()?>images/ajax-loader1.gif" id="loader">
        		<div id="info2">	</div>
        		<div id="chart1"></div>
        		
        			<div id="customTooltipDiv"></div>
        	</div>
    	</div>
	</div>
	
	</div>
	
	 
	<div class="simpleTabsContent">
	<div id="current_pl" class="plot" ></div>
		<div id="chart3" class="plot" ></div>
	<div id="voltage_ch" class="plot" ></div>
	<div id="voltage_pl" class="plot" ></div>
	
	</div>
	<div class="simpleTabsContent">
	<div id="volcurrent"></div>
	</div>
	<div class="simpleTabsContent">
	<div id="watthour"></div>
	</div>
	</div>

		        </div>
		        
	

	<style>
	.content-wrap {
   /* margin: 0 auto;*/
    position: static !important;
    width: 61%;
}
		
.para-block{
	float: left;
	margin:10px;
}	
.error{background: #f1f1f1;
    color: red;
    font-size: 13px;
    margin: 0 0 1px;
    padding: 9px;
    width: 80%;
    text-indent:0;
}
.ui-widget-content {
    background:white !important;
}
	 .jqplot-target {
            margin: 20px;
            height: 340px;
            width: 600px;
            color: #dddddd;
            clear:both;
        }
        .ui-widget-content {
            background: rgb(57,57,57);
        }

        table.jqplot-table-legend {
            border: 0px;
            background-color: rgba(100,100,100, 0.0);
        }
        .jqplot-highlighter-tooltip {
            background-color: /*rgba(1,57,57, 0.1)*/ #000 !important;
            padding: 7px;
            color: #ccc;
           
        }
        
	.canvasjs-chart-credit {
		display:none;
	}
	input[type='text']{
		width:16%;
		padding:3px;
	}
	select{
		width:4%;
	}
	label {
    text-transform: capitalize;

}

.chkbox{
    vertical-align:top;
    line-height:20px;
}
</style>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.datetimepicker.js"></script>

<script type="text/javascript" >
jQuery(function(){
		Date.prototype.format = function( format ){
		 //you code for convert date object to format string
		 //for example
		 switch( format ) {
		 
			   case 'd': return this.getDate();
			   case 'H:i:s': return this.getHours()+':'+this.getMinutes()+':'+this.getSeconds();
			   case 'h:i a': return  ( (this.getHours() %12) ? this.getHours() % 12 : 12)+':'+this.getMinutes()+(this.getHours() < 12 ? 'am' : 'pm');
			   case 'Y-m-d H:i': return  this.getDate() + ' ' + this.getHours()+':'+this.getMinutes();
			   
		 }
		 	// or default format
			 return this.getDate()+'.'+(this.getMonth()+ 1)+'.'+this.getFullYear();
	};

	jQuery('#from_date').datetimepicker({
			 format:'Y-m-d H:i',
			 onShow:function( ct ){
					 var start_date	= ( false == !jQuery('#to_date').val() ) ? jQuery('#to_date').val().split(' ') : null;
					  this.setOptions({
							maxDate: ( null !== start_date ) ? new Date( start_date[0] ) : false
					  })
			 },
			 timepicker:true
	});
	
	jQuery('#to_date').datetimepicker({
			 format:'Y-m-d H:i',
			 onShow:function( ct ){
					 var end_date	= ( false == !jQuery('#from_date').val() ) ? jQuery('#from_date').val().split(' ') : null;
					 this.setOptions({
							minDate:( null !== end_date ) ? new Date( end_date[0] ) :false
					})
			 },
			 timepicker:true
	});
});
$(document).ready(function(){
    $('#piechart').click(function(){
        if($('#piechart').is(":checked")){
        	$('.checkbx').attr("checked", false);
            $('.show_chkbox').hide();
        }     
    });
    $('#line').click(function(){
           $('.checkbx').attr("checked", false);
           $('.show_chkbox').show();    
    });
    $('#bar').click(function(){
    	$('.checkbx').attr("checked", false);
        $('.show_chkbox').show();    
 });    
    
});


$( "#voltcurr" ).click(function() {
	getVoltageVsCurrent();
});

$( "#live_graph" ).click(function() {
	//$('#line_graph').submit();
	var type = $('input[name=p_type]:checked').val();
	$.get( "<?php echo base_url();?>energymeter/action/viewLiveGraph/"+type, function( data ) {
		  $("#livedata").html( data );
		});
	//setTimeout(getdata, 5000);
	});

	// get live line graph 
$( "#meter_graph" ).click(function() {
	getlivestat();
	});
$( "#watt_hour" ).click(function() {
	getHourlyWatt();
	});

function getlivegraph()
	{
		var type = $('input[name=p_type]:checked').val();
		$.get( "<?php echo base_url();?>energymeter/action/viewLiveGraph/"+type, function( data ) {
			  //alert( "Load was performed." );
			  $("#livedata").html( data );
			});
	}
    // end here 
	// live stat for current and voltage using meter graph
	function getlivestat()
	{
		$.get( "<?php echo base_url();?>energymeter/action/ajaxgetEnergyStats", function( data ) {
			var obj = $.parseJSON(data)
			//  $("#current_pl").html( data );
			
		
	    var s1 = [obj.voltage];
	    var s2 = [obj.current];

	    var plot3 = $.jqplot('voltage_pl', [s1], {
	    	 title: 'Latest Voltage',
	        seriesDefaults: {
	            renderer: $.jqplot.MeterGaugeRenderer,
	            rendererOptions: {
	           //     min: 0,
	            //    max: 140,
	            // intervals: [40, 80, 120, 140],
	            //    intervalColors: ['#66cc66', '#93b75f', '#E7E658', '#cc6666'],
	                smooth: true,
	                animation: {
	                    show: true
	                }
	            }
	        }
	    });

	    var plot4 = $.jqplot('current_pl', [s2], {
	   	 title: 'Latest Current',
	       seriesDefaults: {
	           renderer: $.jqplot.MeterGaugeRenderer,
	           rendererOptions: {
	             ///  min: 0,
	             //  max: 140,
	          //  intervals: [40, 80, 120, 140],
	           //    intervalColors: ['#66cc66', '#93b75f', '#E7E658', '#cc6666'],
	               smooth: true,
	               animation: {
	                   show: true
	               }
	           }
	       }
	   });
	
			});
	}

	function getVoltageVsCurrent()
	{
		$.get( "<?php echo base_url();?>energymeter/action/viewVoltageVsCurrent/", function( data ) {
			  //alert( "Load was performed." );
			  $("#volcurrent").html( data );
			});
	}
	function getHourlyWatt()
	{
		$.get( "<?php echo base_url();?>energymeter/action/viewWattHourGraph/", function( data ) {
			  //alert( "Load was performed." );
			  $("#watthour").html( data );
			});
	}


  // end here 
// for date range graph
$('#line_graph').submit( function() {
	$('#loader').show();
	$("#chart1").html("");
	$.post($('#line_graph').attr('action'), $('#line_graph').serialize(), function( data ) {				
	}).done(function( data ) {

			$('#loader').hide();
			$("#error_mg").html(data);
	
	  });
	return false;			
});

// end here 
// for run function after specific timestap. 
$(document).ready(function(){
	
	getlivegraph();
	setInterval(getlivegraph, 20000);
	getlivestat()
	setInterval(getlivestat, 20000);
	getVoltageVsCurrent();
	getHourlyWatt(); 
  
});
// end here 

</script>
<link type="text/css" href="<?php echo base_url(); ?>graph_assets/jquery.jqplot.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/simpletabs_1.3.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>graph_assets/jqplot.meterGaugeRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.highlighter.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.canvasAxisLabelRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.logAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.barRenderer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>graph_assets/jqplot.donutRenderer.min.js"></script>
       <?php $this->load->view('common/footer');?>