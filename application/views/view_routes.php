<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.css" rel="stylesheet">
<!-- main body-->  
    <div class="main-container clearfix">
    	<div class="content">
        	
            <!-- main content body-->
            <div class="pushmenu-push">

            <div class="main-content-body clearfix">
			          
         
            <!-- main content-->
            <div class="main-content">
			<div class="septr">
			<button class="button-back" type="button" onclick="window.location='<?php echo base_url(); ?>livetracking'">Back to Livetracking</button></div>
			<div class="title-box">
            	<h4>Manage Reports</h4>
            	 </div>
				 
    	
    	
    </div>
            <div class="user-tables">
             <table id="myTable" class="table table-bordered table-hover table-striped tablesorter">
			    			<thead>
								<tr>
									<th style="display: none;">Id</th>
				    				<th>Device Id</th>
				    				<th>Phone Number</th>
				    				<th>Date From </th>
				    				<th>Date To </th> 
				    			
				    				<th>Distance Traveled in KM </th> 
				    				<th>Trip </th> 
								</tr>
			    			</thead>
			    			<tbody>
							<?php foreach ($reports as $report){
								?><tr>
								
                            	
                            		<td style="display: none;"><?php echo $report['token']; ?></td>
                                   	<td><?php echo $report['deviceid']; ?></td>
                                   	<td><?php echo $report['phoneno']; ?></td>
                                   	<td><?php echo $report['datefrom']; ?></td>
                                    <td><?php echo $report['dateto']; ?></td>
                                   
                                   	<td><?php echo $report['exptdist']; ?></td>
                                   	 <td><?php echo $report['trip_from']."to".$report['trip_end']; ?>
                                   	 <br><a href="<?php echo base_url();?>livetracking/routewisemap/<?php echo $report['phoneno']; ?>/<?php echo $report['token']; ?>" class="signup" >View Map</a>
                                   	 </td>
                           	    
                           	   </tr>
                           <?php } ?> 
					 </tbody>
					</table>
				</div>
            </div>
            
            <!-- main content end-->
            
            </div>
            <!-- main content body end-->
        </div>
     </div></div>
  <!-- body end-->

<?php $this->load->view('common/footer');?>
