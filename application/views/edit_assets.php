<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<style>
.form-control {
		margin:0px 5px;
}

.form-grp li {
    display: block;
    float: left;
    margin-right: 30px;
}
#marker_label {
	float:left;
}
#icon_label {
	clear: both;
	float: left;
	position:relative;
	left:90px;
}
.form-grp img{
vertical-align:middle;
}
</style>
<div class="clearfix">
	<div class="page-title"><h4>Edit tracker</h4></div>
	<div class="content-wrap clearfix">
	<span class="asterisk-msg">All fields marked with * are mandatory.</span>
	<?php echo form_open('assets/action/edit/'. $assets->id ); ?>

		<div class="form-grp">
        	<label>Name <span class="asterisk">*</span></label>
            <input class="form-control" placeholder="Name" name="name" id="name" maxlength="21" value="<?php echo $assets->name;  ?>" >
            <?php echo form_error('name'); ?>
        </div>
        <div class="form-grp">
        	<label>Phone number<span class="asterisk"> *</span></label>
			<input class="form-control" name="phone_number" id="phone_number" placeholder="phone number" value="<?php echo $assets->phone_number;  ?>"  >
            <?php echo form_error('phone_number'); ?>
        </div>
        <div class="form-grp">
        	<label>IMEI number<span class="asterisk"> *</span></label>
			<input class="form-control" name="imei_number" id="imei_number" placeholder="IMEI number" value="<?php echo $assets->imei_number; ?>"  >
            <?php echo form_error('imei_number'); ?>
        </div>
        <div class="form-grp">
	    	<label>Sim number</label>
			<input class="form-control" name="sim_number" id="sim_number" placeholder="Sim number" value="<?php echo $assets->sim_number; ?>"  >
            <?php echo form_error('sim_number'); ?>
        </div>
        <div class="form-grp">
				    	<label id ="marker_label">Markers</label>
						<ul>
								<li><input type="radio" name="marker_icon" value="map-marker-icon.png" <?php if( 'map-marker-icon.png' == $assets->marker_icon ){  ?> checked="checked" <?php } ?>><img width="39" src="<?php echo base_url(); ?>images/map_icons/map-marker-icon.png" ></li>
		                	<li><input type="radio" name="marker_icon" value="truck-icon.png" <?php if( 'truck-icon.png' == $assets->marker_icon ){  ?> checked="checked" <?php } ?>><img width="39" src="<?php echo base_url(); ?>images/map_icons/truck-icon.png"></li>
		                	<li><input type="radio" name="marker_icon" value="car-icon.png" <?php if( 'car-icon.png' == $assets->marker_icon ){  ?> checked="checked" <?php } ?>><img width="39" src="<?php echo base_url(); ?>images/map_icons/car-icon.png" ></li>
		                
                	</ul>
                	</div>

       
        <div class="btn-grp">
        	<button type="submit"  class="signup">Save </button>
            <button type="reset" class="signup" onclick="window.location='<?php echo base_url(); ?>assets/action/manage'">Cancel</button>
        </div>
	    <?php  echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">

$(document).keydown(function(e) {
    var nodeName = e.target.nodeName.toLowerCase();

    if (e.which === 8) {
        if ((nodeName === 'input' && e.target.type === 'text') ||
            nodeName === 'textarea') {
            // do nothing
        } else {
            e.preventDefault();
        }
    }
});
</script>

<?php $this->load->view('common/footer');?>  
