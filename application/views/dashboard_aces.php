<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/aces-dashboard.css" />
<script>
var base_url = '<?php echo base_url();?>';
var device_type_id = <?php echo $device_type_id;?>;
var device_id = <?php echo $this->uri->segment(5);?>;
</script>

<script>
$(document).ready(function(){
    $(".parameter-icon").click(function(){
        $(".form-hide").toggleClass('form-open');
        //$(".signup-but").toggleClass('button-slide');
         
                  })
     });
 
</script>

<!-- <link type="text/css" href="<?php echo base_url(); ?>graph_assets/jquery.jqplot.min.css" rel="stylesheet" /> -->

<div class="loader"></div>
<div class="ajax-loader">  
<img src="<?php echo base_url(); ?>images/ajax-loader3.gif" style="vertical-align: middle; margin-top:5px;" alt="Processing" /></div>
		<div>
					<button class="button-back" onclick="loadaction('<?php echo base_url(); ?>','<?php echo $device_type_id;?>');">Back</button>			
					
		</div>
		<div style="background:#fff; display:block;float:left; margin-top: 14px; padding:10px 20px 0;border-radius:2px;width:86%;">
		<div class="title-box">
	<div class="page-title"><h4 style="text-transform:capitalize"><?php echo $devices[0]->name?> - <?php echo $this->uri->segment(5);?></h4></div>
		

		
		<div class="track-btn-container">
      		<?php if( true == isset( $devices ) ) { ?>
				<select class="form-control-select styled" name="device_id" id="device_id">
						<option value="">Select Device</option>
						<?php foreach( $devices as $device ) { ?>
									<option value="<?php echo $device->device_number; ?>" <?php if( $device->device_number == $device_id ) { ?> selected="selected" <?php } ?>><?php echo $device->name; ?></option>
				      <?php } ?>
       		</select>
        <?php }  ?>
         
        <!-- <a href="#" id="dialog-link" class="ui-state-default ui-corner-all">Set Parameters</a> -->
       <!--    <button class="signup">view historical data</button> --> 
		</div>
		</div>

						<!-- open parameter box -->
		<div class="parameter-icon"  ></div>
		<!-- close parameter box -->
	
		<div class="svg-container">
				<div id="div1">
						
				</div>
				<div id="div2">
					
				</div>
				<div id="div3">
				</div>
				
				<div id="div4">
				</div>
				
		</div>
		
		</div>

			
		<div class="inside-wrapper"> <!-- parent -->
        <div class="inside-block-graph-aces">
		      		<div class="simpleTabs">
						 			<ul class="simpleTabsNavigation">
							        	<li><a href="javascript:void(0)" id="temperature_graph"> Temperature </a></li>
							        	<li><a href="javascript:void(0)" id="watt_hour" > Watt-Hour </a></li> 
							        		<li><a href="javascript:void(0)" id="humidity" > Humidity </a></li> 
								   </ul>
				      <div class="simpleTabsContent">
				      		 	<div id="tempchart"></div>
			        </div>
			        <div class="simpleTabsContent">
								<div id="wattchart"></div>
						</div>
						
						 <div class="simpleTabsContent">
								<div id="chart_div"></div>
						</div>
		</div>
	</div>
	<div class="inside-block-sts-aces">
	<div style="margin-bottom:10%;display:inline-block;">
	<div style="background:#fafafa;border:1px solid #ddd;">
	<div style="font-size:1em;margin-left:0px;padding:10px;">Device Status
	
	<span id="div_sts"></span></div>
	</div>
			<div style="border-left:1px solid #ddd;border-right:1px solid #ddd;border-bottom:1px solid #ddd; display:inline-block;padding:10px;"><div style="font-size:0.8em;">
	  				<div id="fault_device" ></div>
	    	</div>
	    	<div class="sts-aces">
	 					<div id="ac1_on_off"  >AC (1)<br/><span id="ac1_stats_txt"></span></div>
	    	</div>
	    	 <div class="sts-aces">
	      			<div id="ac2_on_off" >AC (2)<br/><span id="ac2_stats_txt"></span></div>
	    	</div>
	    	<div class="sts-aces">
					  <div id="ac1_comp_stats"  >Compressor<br/><span id="ac1_comp_txt"></span></div>
	    	</div>
	    	<div class="sts-aces">
	  				<div id="ac2_comp_stats">Compressor<br/><span id="ac2_comp_txt"></span></div>
	    	</div>
	    	<div class="sts-aces">
					  <div id="ac1_fault_stats" >AC (1)<br/><span id="ac1_fault_txt"></span></div>
	    	</div>
	    	<div class="sts-aces">
	  				<div id="ac2_fault_stats">AC (2)<br/><span id="ac2_fault_txt"></span></div>
	    	</div>
	    </div>
	    
	    
	    <!--  
	         <div  ng-app='setpara'>
	    	
	    	<div class="form-hide" ng-controller='para' >
	    	<div class="block-title">Set Parameter</div>
	    	<div style="display:inline-block; border-left:1px solid #ddd;border-right:1px solid #ddd; border-bottom:1px solid #ddd;font-size:0.8em; padding:10px;width:94%;">
	    	<div style="color:green;" ng-bind="msg">{{msg}}</div>
	    	<div style="width: 100%">
	    	<div class="set-para">Set Temperature</div>
	    	<span ng-hide="showTemp">
	    	<input type="hidden" name="serrial_no" ng-model="serrial_no" value="<?php echo $this->uri->segment(5);?>">
	    	    	<input type="hidden" name="serrial_no" ng-model="call_type" value="ajax">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;width:75px;text-align:right;" ng-bind="temp"></div>
	    	    <div class="edit-para"><img src='<?php echo base_url();?>/images/edit.png' ng-click="showTemp = ! showTemp"></div>
	        </span>
	    	<span ng-show="showTemp">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='temp' style="margin:0;padding:5px;width:150px;" type="text" value="20" size="1"></div>
	    	 
	    	  <div class="edit-para"><img  src="<?php echo base_url(); ?>images/cancel-icon.png" ng-click="showTemp = ! showTemp"></div>
	        <div class="edit-para">
	    	
	    	 <img  src="<?php echo base_url(); ?>images/save-icon.png" ng-click="addPara();showTemp = ! showTemp">
	    	 
	    	 </div></span>
	    	</div>
	    	
	    		<div style="width: 100%;clear:both;">
	    	<div class="set-para">AC switch duration</div>
	    	<span ng-hide="showswitch">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;width:75px;text-align:right;" ng-bind="aswitch"></div>
	    	    <div class="edit-para""><img src='<?php echo base_url();?>/images/edit.png' ng-click="showswitch = ! showswitch"></div>
	        </span>
	    	<span ng-show="showswitch">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='aswitch' style="margin:0;padding:5px;width:150px;" type="text" value="20" size="1"></div>
	    	 
	    	 <div class="edit-para"><img  src="<?php echo base_url(); ?>images/cancel-icon.png" ng-click="showswitch = ! showswitch"></div>
	        <div class="edit-para">
	    	 
	    	 <img  src="<?php echo base_url(); ?>images/save-icon.png" ng-click="addPara();showswitch = ! showswitch">
	    	 </div></span>
	    	</div>
	    	
	    	  <div style="width: 100%;clear:both;">
	    	<div class="set-para">Low cut</div>
	    	<span ng-hide="showslcut">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;width:75px;text-align:right;" ng-bind="lcut"></div>
	    	    <div class="edit-para">
	    	   <img src='<?php echo base_url();?>/images/edit.png' ng-click="showslcut = ! showslcut">
	    	    </div>
	        </span>
	    	<span ng-show="showslcut">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='lcut' style="margin:0;padding:5px;width:150px;" type="text" value="20" size="1"></div>
	    	 
	         	 <div class="edit-para"><img  src="<?php echo base_url(); ?>images/cancel-icon.png" ng-click="showslcut = ! showslcut"></div>
	        <div class="edit-para">
	    	 <img  src="<?php echo base_url(); ?>images/save-icon.png" ng-click="addPara();showslcut = ! showslcut">
	    	 </div></span>
	    	</div>
	    	
	    	<div style="width: 100%;clear:both;">
	    	<div class="set-para">High cut</div>
	    	<span ng-hide="showshcut">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;width:75px;text-align:right;" ng-bind="hcut"></div>
	    	    <div class="edit-para"><img src='<?php echo base_url();?>/images/edit.png' ng-click="showshcut = ! showshcut"></div>
	        </span>
	    	<span ng-show="showshcut">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='hcut' style="margin:0;padding:5px;width:150px;" type="text" value="20" size="1"></div>
	    	 
	    	 <div class="edit-para"><img  src="<?php echo base_url(); ?>images/cancel-icon.png" ng-click="showshcut = ! showshcut"></div>
	        <div class="edit-para"> <img  src="<?php echo base_url(); ?>images/save-icon.png" ng-click="addPara();showshcut = ! showshcut"></div></span>
	    	</div>
	    	
	      <div style="width: 100%;clear:both;">
	    	<div class="set-para">Temperature offset</div>
	    	<span ng-hide="showToffset">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;width:75px;text-align:right;" ng-bind="toffset"></div>
	    	    <div class="edit-para"><img src='<?php echo base_url();?>/images/edit.png' ng-click="showToffset = ! showToffset"></div>
	        </span>
	    	<span ng-show="showToffset">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='toffset' style="margin:0;padding:5px;width:150px;" type="text" value="20" size="1"></div>
	    	 
	    	 <div class="edit-para"><img  src="<?php echo base_url(); ?>images/cancel-icon.png" ng-click="showToffset = ! showToffset"></div>
	        <div class="edit-para"> <img  src="<?php echo base_url(); ?>images/save-icon.png" ng-click="addPara();showToffset = ! showToffset"></div></span>
	    	</div>
	    	

	    	
	     <div style="width: 100%;clear:both;">
	    	<div class="set-para">Main offset</div>
	    	<span ng-hide="showMoffset">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;width:75px;text-align:right;" ng-bind="moffset"></div>
	    	    <div class="edit-para"><img src='<?php echo base_url();?>/images/edit.png' ng-click="showMoffset = ! showMoffset"></div>
	        </span>
	    	<span ng-show="showMoffset">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='moffset' style="margin:0;padding:5px;width:150px;" type="text" value="20" size="1"></div>
	    	
	    	 <div class="edit-para"><img src="<?php echo base_url(); ?>images/cancel-icon.png" ng-click="showMoffset = ! showMoffset"></div>
	     <div class="edit-para"> <img  src="<?php echo base_url(); ?>images/save-icon.png" ng-click="addPara();showMoffset = ! showMoffset"></div>
	        </span>
	    	</div>
	    	
	    	   <div style="width: 100%;clear:both;">
	    	<div class="set-para">Load offset</div>
	    	<span ng-hide="showLoffset">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;width:75px;text-align:right;" ng-bind="foffset"></div>
	    	    <div class="edit-para"><img src='<?php echo base_url();?>/images/edit.png' ng-click="showLoffset = ! showLoffset"></div>
	        </span>
	    	<span ng-show="showLoffset">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='foffset' style="margin:0;padding:5px;width:150px;" type="text" value="20" size="1"></div>
	    	 
	    	 <div class="edit-para" ><img  src="<?php echo base_url(); ?>images/cancel-icon.png" ng-click="showLoffset = ! showLoffset"></div>
	        <div class="edit-para"> <img  src="<?php echo base_url(); ?>images/save-icon.png" ng-click="addPara();showLoffset = ! showLoffset"></div></span>
	    	</div>
	    	
			  <div style="width: 100%;clear:both;">
	    	<div class="set-para">Load Gain </div>
	    	<span ng-hide="showGoffset">
	    		<div style="float:left;margin-right:12px;padding:5px 0px;width:75px;text-align:right;" ng-bind="logain"></div>
	    	    <div class="edit-para"><img src='<?php echo base_url();?>/images/edit.png' ng-click="showGoffset = ! showGoffset"></div>
	        </span>
	    	<span ng-show="showGoffset">
	    	<div style="float:left;margin-right:12px;padding:5px 0px;"><input ng-model='logain' style="margin:0;padding:5px;width:150px;" type="text" value="20" size="1"></div>
	    	 
	    	 <div class="edit-para" ><img  src="<?php echo base_url(); ?>images/cancel-icon.png" ng-click="showGoffset = ! showGoffset"></div>
	        <div class="edit-para"> <img  src="<?php echo base_url(); ?>images/save-icon.png" ng-click="addPara();showGoffset = ! showGoffset"></div></span>
	    	</div>
	    	</div>
	    		
	    	
	    	</div>  
	  
	   		 
	    	</div>
	    	-->
	    	
	    	
	    	
	    	
	    	
	    	
	  
	    	</div>
	    	
	</div>
		</div>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css" />
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/aces-dashboard.js"></script>

		<script src="<?php echo base_url(); ?>js/jQuery.circleProgressBar.js"></script>
<link type="text/css" href="<?php echo base_url(); ?>css/simpletabs.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>js/amcharts.js"></script>
<script src="<?php echo base_url(); ?>js/serial.js"></script>
<script src="http://www.amcharts.com/lib/3/themes/light.js"></script>
	    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!--  
	    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js"></script>
-->



<script type="text/javascript" src="<?php echo base_url(); ?>js/simpletabs_1.3.js"></script>

  <link type="text/css" href="<?php echo base_url(); ?>css/d3.css" rel="stylesheet" />
        <script type="text/javascript" src="<?php echo base_url();?>js/d3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/radialProgress.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>js/d3-loader.js"></script>
   
<?php $this->load->view('common/footer');?> 
<script>
google.charts.load('current', {packages: ['corechart', 'line','annotatedtimeline']});

google.charts.setOnLoadCallback(drawBackgroundColor);
</script>

