
<table class="table table-bordered table-hover table-striped tablesorter  no-more-tables"  id="myTable">
      			<thead>
      			<tr class="head">
      					<th style="display:none">Id</th>
                    <th>Name</th>
                     <th>Ambient Temp</th>
                    <th>Set Temp</th>
                     <th>Grid Voltage</th>
                    <th>kWh</th>
                    <th>AC on Switch duration</th>
                    <th>AC Status</th>
                     <th class="noheader">Action</th>
					</tr>
      		</thead>
      		<tbody>
     <?php  

      	if( 0 < $devices->num_rows ){
      				foreach ( $devices->result() as $device ) {
      				 	$fieldsarr 			= array( 'ambient_temperature', 'set_temperature','grid_voltage', 'cyclic_period', 'ac_switch_duration', 'cum_kwh' , 'cum_watt_hour', 'ac1_stat', 'ac2_stat', 'compressor_status' );
      				 	$getStatData = energy_meter_model::getAcesAtmStatsByDeviceId( $fieldsarr, $device->device_number );
      				 	$strAcStat = 'Off';
      				 	 $strFault = 'red';
      				 		if( true == is_object( $getStatData ) ) {
      				 			
      				 		$strAcStat = (1 == $getStatData->ac1_stat && 0 == $getStatData->ac2_stat )  ? 'AC 1' : 'AC 2';
      				 		
      				 		$intAc1Fault	= ( (1 == $getStatData->ac1_stat) && ( 0 == $getStatData->compressor_status ||  $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10 )) ? 0 : 1;
      				 		$intAc2Fault	= ( ( 1 == $getStatData->ac2_stat )  && ( 0 == $getStatData->compressor_status ||  $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10) ) ? 0 : 1;
      				 		$intAcFault		= ( 0 == $getStatData->ac1_stat && 0 == $getStatData->ac1_stat &&  0 == $getStatData->compressor_status ) ? 0 : 1;
      				 		$intDeviceFault	= ( 0 == $getStatData->ac1_stat && 0 == $getStatData->ac2_stat && $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10 ) ? 0 : 1;
      				 		
	      				 		if($intAc1Fault == 0 || $intAc2Fault == 0 || $intDeviceFault == 0|| $intAcFault == 0 ) {
	      				 			$strFault = 'red';
	      				 		} else {
	      				 			$strFault	= 'green';
	      				 		}
      				 	} 
      				 	$intAmbientTemp = ( true == is_object( $getStatData ) ) ? round( $getStatData->ambient_temperature/10,0 ) : 0;
      				 	$intSetTemp = ( true == is_object( $getStatData ) ) ? round( $getStatData->set_temperature/10, 2 ) : 0;
      				 	if( $intAmbientTemp >  $intSetTemp ) {
      				 		$intAmbientTemp = "<font color='red'>$intAmbientTemp</font>";
      				 	}
      ?>
     <tr class="active">
                	<td style="display:none"><?php echo $device->id; ?></td>
                   <td><div style="background:<?php echo $strFault;?>;width:10px;height:10px;border-radius:100px;float:left;margin-right:10px;margin-top:3px;"></div>
                    <?php echo $device->name; ?>
                   </td>
                   <td ><?php  echo $intAmbientTemp;?> <sup>o</sup>C</td>
                   <td><?php echo $intSetTemp;?> <sup>o</sup>C</td>
                   <td><?php echo $intGridVoltage =  ( true == is_object( $getStatData ) ) ? ( $getStatData->grid_voltage / 2 ): 0; ?> V</td>
                   <td><?php echo $intKwh = ( true == is_object( $getStatData ) ) ? round( $getStatData->cum_kwh . '.'. $getStatData->cum_watt_hour, 2): 0; ?> KWh</td>
                   <td><?php echo $intKwh = ( true == is_object( $getStatData ) ) ? round( $getStatData->ac_switch_duration / 60 ): 0; ?> hrs</td>
                   <td><?php echo $strAcStat; ?></td>
                    <td>
                    <img class="img_icon" src="<?php echo base_url(); ?>images/view.png"  onclick="window.location='<?php echo base_url(); ?>welcome/action/displayGraph/<?php echo $device->device_type_id; ?>/<?php echo $device->device_number; ?>'" height="15px" alt="View" title="View">
                 <?php if( 1 == $device->unit_type ) { ?> <img class="img_icon" src="<?php echo base_url(); ?>images/settings.png"  onclick="window.location='<?php echo base_url(); ?>devices/action/edit/<?php echo  $device->device_type_id; ?>/<?php echo  $device->id; ?>/<?php echo $device->device_number; ?>'" height="15px" alt="Edit Parameter" title="Edit Parameter"><?php } ?>
                    <img class="img_icon" src="<?php echo base_url(); ?>images/edit.png"  onclick="window.location='<?php echo base_url(); ?>devices/action/update/<?php echo  $device->id; ?>'" height="15px" alt="View" title="Edit Device">
                    <img class="img_icon" src="<?php echo base_url(); ?>images/delete.png"  onclick="deletedata(<?php echo $device->id;?>,'devices/action/delete/',<?php echo  $device->device_type_id; ?>);" height="15px" alt="Delete" title="Delete">
 							</td>
 				</tr>
    <?php   			}
      } else { ?>
      <tr>
      			<td></td>
      			<td></td>
      			<td></td>
      			<td></td>
      			<td>No Data Available</td>
      			<td></td>
      			<td></td>
      			<td></td>
      			<td></td>
      		
      </tr>
     <?php  } ?>
      	</tbody></table> 
      	<script>
     $("#myTable").dataTable({
      	 "bPaginate":false,
      	"bFilter": false,
     } );</script>
