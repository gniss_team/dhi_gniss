<?php $this->load->view('common/header');?>
<div class="gniss-bg-image">
	<!--<div class="comp-heading">welcome to gniss </div>
       <div class="comp-sub-txt">global network information security system</div>-->
	   <div class="signin-container">
		  <div class="login-txt">Sign in to your GNISS account</div>
		  <?php echo form_open('users/action/login'); ?>
		  
                 <div class="content-login-wrapper">
                      <?php if(isset( $msg ) && $msg != '' ) { ?>
          			<div class="alert alert-success" style="margin: 3px auto; text-align: center;">
          				 <?php echo $msg; ?>  
          			</div>
          		<?php } ?>
          		<?php if(isset($errmsg) && $errmsg != '') { ?>
          			<div class="alert alert-error" style="color: #D0D5F6; margin: 3px auto; text-align: center;">
          				 <?php echo $errmsg;?>
          			</div>
          		<?php } ?>
          		<?php 
				$setIndividualCheck = 'checked="checked"';
				$setCompanyCheck = '';
				$cookieUserType = 'Individual';
				if(!empty($_COOKIE['gnisscookie'])){
	    			$cookieData = explode(',',$_COOKIE['gnisscookie']); 
	    			$cookieUserName = $cookieData[0]; 
	    			//$cookieUserType = $cookieData[1];
	    		}

				if(empty($cookieUserName)){
					$cookieUserName = set_value('username');
				}
			
	    		?>
            		<div class="content-inner-wrapper clearfix">
        <div><input class="user-ico-login" name="username" type="text" placeholder="Gniss ID" value="<?php echo $cookieUserName; ?>" maxlength="50" /><?php echo form_error('username'); ?> </div>
         <div><input class="password-ico-login"  name="password" type="password" placeholder="Password"<?php echo set_value('password'); ?>maxlength="50" /><?php echo form_error('password'); ?></div>
<!--        <div class="error">error</div>
        <div class="error">error password</div>
        	<?php  echo form_close(); ?>
			
-->   	<div><div class="remember-txt">
        	<input id="remember_me" name="remember_me" value="yes" type="checkbox" value="remember me" /><span>Remember me</span>
        </div> 
        
   <div class="forgot-txt"><a href="<?php  echo base_url();?>users/action/forgotPass">Forgot Password?</a></div>
    
                <div><button type="submit" name="login" value="login" class="signup-new">Sign In</button></div>
            </div>    
     	</div>
        
</div></div>
<div class="new-user-txt">New user? <a href="<?php echo base_url();?>users/action/register">Create an account </a></div>

</div>
<?php $this->load->view('common/footer');?>
</body>
<style>
.error{
text-indent:0;
 	}

</style>
</html>
