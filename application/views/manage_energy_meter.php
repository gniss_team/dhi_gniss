<?php $this->load->view('common/header');?>
<?php $this->load->view('common/style');?>
<link href="<?php echo base_url(); ?>css/tablesort.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/datatables/jquery.dataTables.min.css" rel="stylesheet">
<script>	 var base_url = "<?php echo base_url(); ?>";</script>
<script src="<?php echo base_url(); ?>js/pagination.js"></script>
<div class="content-body-wrapper-dashboard clearfix">
<?php $this->load->view('common/sidebar');?>
<div class="content clearfix">
		<div class="page-title"><h3>Manage Energy Meter Data</h3></div>
		<div class="breadcrums">
				<ul>
						<li><a class="big" href="<?php echo base_url(); ?>welcome">Dashboard</a></li>
						<li>View Energy Meter Data</li>
				</ul>
		</div>
	<a class="graph-but" href="<?php echo base_url();?>energymeter/action/viewGraphs">View Graph</a>
		<div class="content-wrap clearfix">
					<?php if(isset($msg) && $msg != '') { ?>
								<div class="alert alert-success">
											<?php echo $msg; ?>
								</div>
					<?php } ?>
	<div id="" class="dataTables_length">
        	<label >Show </label>
            <select class="form-control styled" name="select_pagination" id="select_pagination">
                <option value="10">10</option>
					<option value="25">25</option>
					<option value="50">50</option>
					<option value="100">100</option>
            </select>
               <label>Entries </label>
         </div>
          <label style="float:right;" >Search:<input type="search"   name="search" class="" placeholder="" aria-controls="myTable"></label>
         <div id="myTable_filter" class="dataTables_filter">
					<div id="mhead"></div>
					<div id="pagination" cellspacing="0"></div>
					</div>
		</div>
</div>
<style>

.pagination {
    float: right;
    margin: 0 auto;
    width: auto;
	font-size:13px;
}

.pagination .current{
	padding: 4px 10px;
	color: black;
	margin: 1px 0px 9px 6px;
	display: block;
	text-decoration:none;
	float: left;
	text-transform: capitalize;
	/*background: whitesmoke;*/
	background:rgba(0, 0, 0, 0) linear-gradient(to bottom, #fff 0%, #dcdcdc 100%) repeat scroll 0 0;
	border:1px solid #cacaca;
}

.pagination .page-numbers{
	padding: 4px 10px;
	/* color: white !important; */
	margin: 2px;
	display: block;
	text-decoration:none;
	float: left;
	text-transform: capitalize;
	color:#757575;
	/* background: #00b4cc; */
}

 .page-numbers:hover{
	color:#000;
	background:rgba(0, 0, 0, 0) linear-gradient(to bottom, #fff 0%, #dcdcdc 100%) repeat scroll 0 0;
	border:1px solid #cacaca;
	padding: 4px 10px;
	/* color: white !important; */
	margin: 1px;

 
	 }

table.tablesorter {
/*     background-color: #4c8cfa; */
    clear: both;
    font-family: arial;
    font-size: 14px;
    margin: 18px 5px 9px 0;
    text-align: left;
    width: 100%;
	border-bottom:1px solid #757575 !important;	
}

.form-grp {
    clear: both;
    float: left;
    margin: 0 0 20px;
    padding: 2px 5px;
    width: 16%;
}

#myTable_paginate, #myTable_length{
	display:block;
}

.dataTables_length {
	width:14%;
	display: inline-flex;
}

</style>
<script>

</script>
<?php $this->load->view('common/footer');?>