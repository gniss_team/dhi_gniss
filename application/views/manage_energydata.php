
<table class="table table-bordered table-hover table-striped tablesorter  no-more-tables"  id="myTable">
      			<thead>
      			<tr class="head">
      					<th style="display:none">Id</th>
                    <th>Batch Id </th>
                    <th>Asset Id</th>
                    <th>Voltage</th>
                    <th>Current</th>
                    <th>Watts</th>	
                    <th>Watts Second	</th>	
                    <th>Watts Hour	</th>	
                    <th>KWH 	</th>
           			<th>FA Sum	</th>
					</tr>
      		</thead>
      		<tbody>
     <?php  

      	if( 0 < $devices->num_rows ){
      				foreach ( $devices->result() as $device ) {
      				 	
      ?>
     <tr class="active">
                	<td style="display:none"><?php echo $device->id; ?></td>
                    <td><div style="background:<?php echo $strFault;?>;width:10px;height:10px;border-radius:100px;float:left;margin-right:10px;margin-top:3px;"></div>
                    <?php echo $device->name; ?>
                    </td>
                    <td><?php echo $device->name;?></td>
                     <td><?php echo $device->batch_number;?></td>
                     <td><?php echo $device->unit_number; ?></td>
                 	 <td><?php echo $device->voltage/10; ?></td> 
                    <td><?php echo $device->current/10; ?></td>
                    <td><?php echo $device->watts; ?></td>
                    <td><?php echo $device->watts_second; ?></td>
                    <td><?php echo $device->watts_hour; ?></td>
                    <td><?php echo $device->kwh; ?></td>
                    <td><?php echo $device->checksum; ?></td>
                    <td>
                    <img class="img_icon" src="<?php echo base_url(); ?>images/view.png"  onclick="window.location='<?php echo base_url(); ?>welcome/action/displayGraph/<?php echo $device->device_type_id; ?>/<?php echo $device->device_number; ?>'" height="15px" alt="View" title="View">
                 <?php if( 1 == $device->unit_type ) { ?> <img class="img_icon" src="<?php echo base_url(); ?>images/settings.png"  onclick="window.location='<?php echo base_url(); ?>devices/action/edit/<?php echo  $device->device_type_id; ?>/<?php echo  $device->id; ?>/<?php echo $device->device_number; ?>'" height="15px" alt="Edit Parameter" title="Edit Parameter"><?php } ?>
                    <img class="img_icon" src="<?php echo base_url(); ?>images/edit.png"  onclick="window.location='<?php echo base_url(); ?>devices/action/update/<?php echo  $device->id; ?>'" height="15px" alt="View" title="Edit Device">
                    <img class="img_icon" src="<?php echo base_url(); ?>images/delete.png"  onclick="deletedata(<?php echo $device->id;?>,'devices/action/delete/',<?php echo  $device->device_type_id; ?>);" height="15px" alt="Delete" title="Delete">
 							</td>
 				</tr>
    <?php   			}
      } else { ?>
      <tr>
      			<td></td>
      			<td></td>
      			<td></td>
      			<td></td>
      			<td>No Data Available</td>
      			<td></td>
      			<td></td>
      			<td></td>
      			<td></td>
      		
      </tr>
     <?php  } ?>
      	</tbody></table> 
      	<script>
     $("#myTable").dataTable({
      	 "bPaginate":false,
      	"bFilter": false,
     } );</script>
