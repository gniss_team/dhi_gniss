<?php $this->load->view('common/header');?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry"></script>
<script type="text/javascript" src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.8/src/infobox.js"></script>
<?php $this->load->view('common/sidebar');?>
<style>
.nav-map{width: 25%;float: left;}
</style>
<script>

var phone_num = '<?php echo $this->uri->segment(3);?>';
</script>

<style>h2{font-size:2em;font-weight:normal;}
.geo-form{float:right;margin-right:20px;}
#myImage {
  background: none repeat scroll 0 0 #fff;
  border-radius: 10px;
  bottom: 3px;
  box-shadow: -5px 23px 17px 0 rgba(125, 116, 116, 0.81);
  color: balck;
  padding: 15px;
  position: relative;
  z-index: -2147483648;
}

#myImage:after {
  border-color: #fff transparent;
  border-style: solid;
  border-width: 15px 15px 0;
  bottom: -15px;
  content: "";
  display: block;
  left: 125px;
  position: absolute;
  width: 0;
  z-index: 1;
}
#profile-pic {
  border-radius: 20px;
  position: absolute;
  right: 35px;
}

#myImageaa{background-color:#3e3d38;
	 background: url("map_icons/depot.png") no-repeat scroll center bottom 0 rgba(0, 0, 0, 0);
		
		border-radius:10px;
		color:white;
		position:relative;
	    height:auto;
        padding:20px;
        z-index: -2147483648;}
		
		#myImageaa ul li a{color:white;
			text-align:left;}
.info-window, .gm-style-iw, .gm-style-iw > div {

background-color:#3e3d38;!important;
}
.info-window  img {
display:none;
}
#loader{
position:absolute;
z-index:8888;
left:42%;
top:50%;
}
</style>

<div class="content clearfix">
	<div class="page-title"><h4>View Location</h4></div>
	<p style="font-size:1.2em;">Location for Device: <?php echo $this->uri->segment(3);?></p>
	    <div style="float:left;">
	 
    	<button  class="track-but" onclick="window.location='<?php echo base_url(); ?>livetracking'">Back To Livetracking </button>
    	<!--   <button id="dialog-link" class="track-but">View Route  </button>-->
    		<button class="button-back" type="button" onclick="window.location='<?php echo base_url(); ?>livetracking/shareroute/<?php echo $phone_number; ?>'">View Route</button>
    
    </div>
    

    <!-- main content body-->
    <div class="clearfix">
    
   <span id="loader"> <img src="<?php echo base_url()?>/images/ajax-loader3.gif" ></span>
   
   
		<div id="map" style="width:98%; height:600px;" >
		
		</div>
	
    </div>
  
    	
    
</div>

<div id="load-dialog"></div>
<script type="text/javascript">



	var map;

	var arrMarkers = [];

	var arrInfoWindows = [];

	

	function mapInit(){
	
		var centerCoord = new google.maps.LatLng(<?php echo $lastlatlong->latitude;?> ,<?php echo $lastlatlong->longitude;?> ); 

		var mapOptions = {

			zoom: 16,

			center: centerCoord,

			mapTypeId: google.maps.MapTypeId.ROADMAP

		};

		map = new google.maps.Map(document.getElementById("map"), mapOptions);
		

		

	
		  
	}

	$(function(){
		mapInit();
		
		$("#markers").on("click","a", function(){
			var i = $(this).attr("rel");
			arrInfoWindows[i].open(map, arrMarkers[i]);
		});

	});

</script> 
    <script type="text/JavaScript">
		var timerID = null;
		var timerRunning = false;
		var flag = 0;
		var delay = 10000;
		
		function InitializeTimer()
		{
		    //alert('InitializeTimer');
		    StopTheClock()
		    StartTheTimer()
		    
		}
		
		function StopTheClock()
		{
		    //alert('InitializeTimer1');
		    if(timerRunning)
		        clearTimeout(timerID)
		    timerRunning = false
		}
		
		function StartTracking()
		{
			//alert('InitializeTimer2');
			
			readMap();
			StartTheTimer();
		}
		
		
		
		function StartTheTimer()
		{
			
			timerRunning = true
			timerID = self.setTimeout("StartTracking()", delay)
		}
		
		</script>
 <script type="text/javascript">
    //<![CDATA[
	$(function() {
   		InitializeTimer()
   		
   	
		});
   if (window.XMLHttpRequest) {
	 var profile="";
      var side_bar_html = "";
		var gmarkers = [];
		var htmls = [];
		var points = [];
		var lats = [];
		var lngs = [];
		var markersArray = new Array();
		
		var i = 0;
		var infowindow;
		var infobox;
		var base_url = '<?php echo base_url(); ?>';

		  
       
      // A function to create the marker and set up the event window
       function createMarker(point,name,html,lat,lng,icontype,vehicle_icon,token,phoneno,marker_icon) {
    	  
	   
	     	var base_url = '<?php echo base_url(); ?>';
		if(vehicle_icon==0) 
		{
		vehicle_icon="marker_red.png"; 

	     	}
		var blueIcon="images/marker_red.png"; 
		 var marker = new google.maps.Marker({
				position: point,
				map: map,
				  animation: google.maps.Animation.DROP,
					
				optimized:false, // <-- required for animated gif
				//animation: google.maps.Animation.DROP,
				icon: base_url+"images/map_icons/"+marker_icon,
			
					
				//icon: blueIcon,
				title:name
				});
marker.setAnimation(google.maps.Animation.BOUNCE);
	
							
		markersArray.push(marker);
					
      google.maps.event.addListener(marker, "click", function() {


	var st = "<div id='myImage'><div class='clearFix'></div><div class='close'></div>"+html+"</div>";
		if (infobox) infobox.close();
		 
		  infobox = new InfoBox({
			    content: st,
			    disableAutoPan: false,
			    maxWidth: 150,
			    pixelOffset: new google.maps.Size(-160,-160),
			    zIndex: null,
			    boxStyle: {
			                background: "url('') no-repeat",
			                opacity: 1,
			                width: "280px" 
			        },
			    closeBoxMargin: "5,20,30,5",
			    closeBoxURL: "/map_icons/close_hover.png",
			    infoBoxClearance: new google.maps.Size(1, 1)
			});	 
		
		  infobox.open(map, marker);
		});
		points[i]=point;
        lats[i]=lat;
        lngs[i]=lng;
        gmarkers[i] = marker;
		htmls[i] = html;
        side_bar_html += '<li><div class="nav-map"><a href="livetracking/" >' + name + '<\/a></div><div class="nav-map"><a href="<?php echo base_url()?>livetracking/routewiselocation/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/route-icon.png">Route Map</a></div><div class="nav-map"><a href="javascript:myclick(' + (gmarkers.length-1) + ')" "><img width="22" src="<?php echo base_url()?>images/map_icons/track-icon.png">Live Track</a></div><div class="nav-map"><a href="livetracking/'+phoneno+'"><img width="22" src="<?php echo base_url()?>images/map_icons/track-icon.png">Fench</a></div></li>';
        //side_bar_html += '<a href="javascript:myclick(' + (gmarkers.length-1) + ')">' + name + '<\/a><br>';

        i++;
        
        return marker;    
		}
	  
		function clearOverlays() {
		  if (markersArray) {
			for (var i = 0; i < markersArray.length; i++ ) {
			  markersArray[i].setMap(null);
			}
		  }
		}
  

      // This function picks up the click and opens the corresponding info window
     function myclick(i) {
		//alert('fgh');
		google.maps.event.trigger(gmarkers[i], "click");
		map.setZoom(14);
		}


      
      
      
      // A function to read the data
    function readMap(url) {
     
       //alert("refresh");
        var baseurl= '<?php echo base_url();?>';
      <?php if($phone_number!="") { ?>  
        var url=baseurl+"livetracking/getshareLatlong/<?php echo $phone_number; ?>";
        <?php }else{?>
        var url=baseurl+"livetracking/getLatlong";
        <?php } ?>
        if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{
				// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{	
			if (xmlhttp.readyState==4)
				{
				$('#loader').hide();
					markers=xmlhttp.responseXML.documentElement.getElementsByTagName("marker");		
           
            // hide the info window, otherwise it still stays open where the removed marker used to be
            //map.clearOverlays();
            side_bar_html = "";
			htmls = [];
			i = 0;
            clearOverlays();
            gmarkers = [];

            for (var i = 0; i < markers.length; i++) {
				
              // obtain the attribues of each marker
              var lat = parseFloat(markers[i].getAttribute("lat"));
			  var lng = parseFloat(markers[i].getAttribute("lng"));
			  var icon_type = parseFloat(markers[i].getAttribute("vehicle_status"));
              var point = new google.maps.LatLng(lat,lng);
			  var html = markers[i].getAttribute("html");
              var label = markers[i].getAttribute("label");
			  var category = markers[i].getAttribute("category");
		     var vehicle_icon = markers[i].getAttribute("vehicle_icon");
		     var marker_icon = markers[i].getAttribute("marker_icon");
		     var token = markers[i].getAttribute("token");
		     var phoneno = markers[i].getAttribute("phoneno");
              var marker = createMarker(point,label,html,lat,lng,icon_type,vehicle_icon,token,phoneno,marker_icon);
			  marker.setMap(map);
			 
			  }
           
			
          }
        }
        xmlhttp.open("GET",url,true);
			xmlhttp.send();
      }
      
      // When initially loaded, use the data from "map11.php?q=a"
      readMap("a");
      
    }

    else {
      alert("Sorry, the Google Maps API is not compatible with this browser");
    }
   $(document).ready(function(){
   $( "#load-dialog" ).dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		open: function(event, ui) {
    
             $(this).load("<?php echo base_url();?>CManageLiveTrackingController/viewmarkwers/"+phone_num, function() {
                 
             });
         },
		
		buttons: [
			
//			{
//				text: "Cancel",
//				click: function() {
//					$( this ).dialog( "close" );
//				}
//			}
		]
	});

	$( "#dialog-link" ).click(function( event ) {
		$( "#load-dialog" ).dialog( "open" );
		event.preventDefault();
	});
   });

   function share_location()
   {
	  var tognissId = $('#gniss_id').val();
	  var Sharemobile = "<?php echo $this->uri->segment(3)?>";
	   $.get("<?php echo base_url(); ?>CManageLiveTrackingController/sharelocation/"+Sharemobile+"/"+tognissId, function( data ) {
		   $( "#result" ).html( data );
		  
		 });
   }
    </script>
 <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css" />
<script src="<?php echo base_url(); ?>js/external/jquery/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
 <?php $this->load->view('common/footer');?>
