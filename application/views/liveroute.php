<?php $this->load->view('common/header');?>
<?php $this->load->view('common/sidebar');?>
<div class="main-container clearfix">
	<div class="content">
    	<div class="pushmenu-push">
            <div class="main-content-body clearfix">
				
 				<button class="button-back" type="button" onclick="window.location='<?php echo base_url(); ?>livetracking/routewiselocation/<?php echo $this->uri->segment(3)?>'">Back</button>
            	<div class="main-content">
                	
                	<?php if(count($reports) > 0){?> 
                	<div class="user-forms">
            			<div id="map_canvas" style="width:98%; height:600px;"></div>
            			 </div>
            			<?php }else { ?>
            			<h4 align="center">No records found.</h4>
            			<?php } ?>
                  
                </div>
                <!-- /.row -->
</div>
		</div>
    </div>
</div>
   <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/polyline.js"></script>
<script type="text/javascript">

var geocoder;
var directionsService = new google.maps.DirectionsService();
var map;
var trafficLayer;
var bikeLayer;
var poly;
var LocationsMarkers = []; 
var Employees =[];
var latlngCenter;
var bounds = new google.maps.LatLngBounds();

function GetMarkers(){
  for (var i = 0; i < Employees.length; i++) {
    var Employee = Employees[i];
    var myLatLng = new google.maps.LatLng(Employee[1], Employee[2]);

    // Add the location to vector for routing
    LocationsMarkers.push(new google.maps.LatLng(Employee[1], Employee[2]));

    // Add a marker
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: Employee[0],
        zIndex: Employee[3]
    });
  }

  // If the length of LocationMarkers > 1, i'm get the routes and i create the polyline.
  if (LocationsMarkers.length > 1){
      var polyOptions = {
        strokeColor: '#000000',
        strokeOpacity: 1.0,
        strokeWeight: 3
      }
      poly = new google.maps.Polyline(polyOptions);
      poly.setMap(map);

      for (var a = 0; a < LocationsMarkers.length-1; a++) {
          calcRoute(LocationsMarkers[a], LocationsMarkers[a+1]);
	  bounds.extend(LocationsMarkers[a]);
      }
  }
}

// calculate the route on the markers.
function calcRoute(start, end) {

    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.DirectionsTravelMode.WALKING
    };
    var path = poly.getPath();        
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
            for (var i = 0; i < response.routes.length; i++) {
               for (var i2 = 0; i2 < response.routes[i].legs.length; i2++) { 
                 for (var i3 = 0; i3 < response.routes[i].legs[i2].steps.length; i3++) {              
                   // add the start and end location by the legs of routes on the path to the polyline
                   path.push(response.routes[i].legs[i2].steps[i3].start_location);
                   path.push(response.routes[i].legs[i2].steps[i3].end_location);
                 }
               }
            }
    } else { 
        // alert("Directions service returned error:"+status+" on ("+start.toUrlValue(6)+") to ("+end.toUrlValue(6)+")");
        if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT)
        {
          var functionStr = "calcRoute(new google.maps.LatLng"+start+",new google.maps.LatLng"+end+")";
	  // alert(functionStr);
          setTimeout(functionStr, 5000);
        }
    }
     map.fitBounds(bounds);
    });
}

function TrafficOn()     { trafficLayer.setMap(map); }
function TrafficOff()    { trafficLayer.setMap(null); }
function BicyclingOn()   { bikeLayer.setMap(map); }
function BicyclingOff()  { bikeLayer.setMap(null);}
function StreetViewOn()  { map.set("streetViewControl", true); }
function StreetViewOff() { map.set("streetViewControl", false); }

function initialize() {
    geocoder = new google.maps.Geocoder();

    CollectData();

    var myOptions = {
       zoom: 18,
       center: latlngCenter,
       mapTypeId: google.maps.MapTypeId.ROADMAP
    }; 

    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    trafficLayer = new google.maps.TrafficLayer();
    bikeLayer = new google.maps.BicyclingLayer();
    GetMarkers();
}

function CollectData(){
 // In this example I used only 20 markers, but there will be situations where I have  to show more than 50.   
     var Employees = [
                      
                <?php $i=1; foreach($reports as $report){?>
			         
			           ["<?php echo  $i;?>",'<?php echo $report->latitude;?>','<?php echo $report->longitude;?>' , <?php echo  $i;?>],
			       <?php $i++;} ?>   
			      
			                    ] ;       

  latlngCenter = new google.maps.LatLng(Employees[0][1],Employees[0][2]);
}
jQuery(function() {
	initialize();
    
});
</script>    
<!-- footer-->   
   
    <div class="footer"><img src="http://localhost/gniss_ci/images/footer-logo.png" />
    <div class="footer-txt">Copyright Rectus Energy Pvt. Ltd.</div>
    </div>
    <!-- footer end-->
</div>








</body>
</html>
    
