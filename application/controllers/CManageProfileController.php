<?php 
ini_set("SMTP","www.gniss.net");

// Please specify an SMTP Number 25 and 8889 are valid SMTP Ports.
ini_set("smtp_port","25");

// Please specify the return address to use
ini_set('sendmail_from', 'http://www.gniss.net');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// This controller is for handling profile module.
class CManageProfileController extends CI_Controller { 

	public $m_intProfileCont =0; 			//  This variable is used for calaulating percentage for profile status. 
	
	public function __construct() {
    	parent::__construct(); 
    }
    
    public function action( $strAction = '' ) {
    	
    	switch( $strAction ) {
    		
    		case 'load':																	// This action is used for populating country, state, city dropdown.
    			$this->loadLocations();				
    			break;
    			
    		case 'loadData':
    				$this->loadLocationsData();					// This action is used for populating country, state, city dropdown if we add country, state and city for multiple address.
    				break;
    				
    		case 'getLocationData':
    				$this->getLocationData();
    				break;
    				
    		case 'view':																// This action is for showing Profile page. 
    				$this->view();
    				break;
    				
    		case 'details':															// This action is used for updaing contact details. 
    				$this->details();
    				break;
    	
    	 	case 'address':														// This action is used for updaing Address details. 
    				$this->address();
    				break;
    			
    		case 'social':															// This action is used for updaing social details.
    				$this->social();
    				break;
    				
    		case 'notify':															// This action is used for updaing notification details.
    				$this->notify();
    				break;
    				
	   		case 'update':														// This action is used for updaing general details.
    				$this->update();
    				break;
   			
    		case 'createOtp' :
    				$this->createOtp();
    				break;
    				
    		case 'verifyOtp' :
    				$this->verifyOtp();
    				break;
    					
    		default:
    			show_404();
    			
	    }
    }
    
    public function details() {
    	
	    	$intUserId = $this->session->userdata( 'user_id');

	    	// To validate the form inputs for contact details. 
	    	$this->form_validation->set_rules( 'mobile_number[]', 'Mobile number', 'trim|required|is_digit|min_length[10]|max_length[10]|xss_clean');
	    	$this->form_validation->set_rules( 'user_mobile_number', 'Mobile number', 'trim|required||is_digit|min_length[10]|max_length[10]|xss_clean');
	    	$this->form_validation->set_rules( 'country_code[]', 'Country code', 'trim|is_digit|max_length[4]|xss_clean');
	    	$this->form_validation->set_rules( 'user_country_code', 'Country code', 'trim|is_digit|max_length[4]|xss_clean');
	    	$this->form_validation->set_rules( 'office_country_code', 'Country code', 'trim|is_digit|max_length[4]|xss_clean');
	    	$this->form_validation->set_rules( 'home_country_code', 'Country code', 'trim|is_digit|max_length[4]|xss_clean');
	    	$this->form_validation->set_rules( 'fax_country_code', 'Country code', 'trim|is_digit|max_length[4]|xss_clean');
	    	$this->form_validation->set_rules( 'office_ext', 'Office extension', 'trim|is_digit|xss_clean');
	    	$this->form_validation->set_rules( 'home_ext', 'Home extension', 'trim|is_digit|xss_clean');
	    	$this->form_validation->set_rules( 'fax_ext', 'Fax extension', 'trim|is_digit|xss_clean');
	    	$this->form_validation->set_rules( 'office_contact', 'Office contact', 'trim|is_digit|xss_clean');
	    	$this->form_validation->set_rules( 'home_contact', 'Home contact', 'trim|is_digit|xss_clean');
	    	$this->form_validation->set_rules( 'fax_country', 'Fax country', 'trim|is_digit|xss_clean');
	    	$this->form_validation->set_rules( 'fax_ext', 'Fax extension', 'trim|is_digit|xss_clean');
	    	$this->form_validation->set_rules( 'fax', 'Fax', 'trim|is_digit|xss_clean');
	    	
	    	$arrmixUserInfo['user_info']	= Users_model::fetchUserById( $intUserId );
	    	
	    	// Fetch ids of  alternate mobile numbers for user.  
	    	$arrintUserMobileNumerIds	= Users_model::fetchUserMobileNumberIdsByUserId( $intUserId );
    	
    		if( true == $this->form_validation->run() ) {
    			
	    		if( true == Users_model::update( $this->input->post() ) ) {

	    			$intInputFieldCount	= ( int ) $this->input->post( 'mobile_value' );
	    			$arrmixFormInput		= array_keys( $this->input->post() );
	    			
	    			if( false == empty( $this->input->post('user_email_id') ) ) {
						// Get ids of deleted mobile numbers.
    					$arrintDeletedIds = array_diff( $arrintUserMobileNumerIds, $this->input->post('user_mobile_id') );
	    			} else {
    					$arrintDeletedIds = $arrintUserMobileNumerIds;
	    			}
	    			
	    			$arrmixFlipFormInput = array_flip( $arrmixFormInput );
	    			unset( $arrmixFlipFormInput['mobile_value'] );
	    			 
	    			$arrmixFormInput =	array_flip( $arrmixFlipFormInput );
	    			
	    			$boolIsUpdate	= false;
    			   // Remove deleted ids.
	    			if( false == empty( $arrintDeletedIds ) ) {
	    				if( true == Users_model::deleteMobile( $arrintDeletedIds ) ) {
	    					$boolIsUpdate	= true;
	    				}
	    			}
		    			
    				for($i = 0;$i< $intInputFieldCount ;$i++ ) {
    				 
	    				$k = 0;
	    				$arrmixInsertData = array();
	    			
	    				foreach( $arrmixFormInput as $strFields => $strFieldValue ) {
	    					foreach(  $this->input->post() as $strFieldName =>$arrmixInputValues ){
	    							
	    						if( $strFieldValue == $strFieldName ) {
	    							$arrmixInsertData[$strFieldValue]		= ( true == isset( $arrmixInputValues[$i] ) )  ? $arrmixInputValues[$i] : '';
	    						} 
	    					}
	    					 
	    					if( 3 == $k ) {
	    						// Remove unnecessary fields from post array.
	    							
	    						unset( $arrmixInsertData['office_ext'] );
	    						unset( $arrmixInsertData['office_contact'] );
	    						unset( $arrmixInsertData['office_country_code'] );
	    						unset( $arrmixInsertData['home_ext'] );
	    						unset( $arrmixInsertData['home_contact'] );
	    						unset( $arrmixInsertData['user_country_code'] );
	    						unset( $arrmixInsertData['user_mobile_number'] );
	    						unset( $arrmixInsertData['home_country_code'] );
	    						unset( $arrmixInsertData['fax_country_code'] );
	    						unset( $arrmixInsertData['fax_ext'] );
	    						unset( $arrmixInsertData['fax'] );
	    						
	    						if( true == array_key_exists('user_mobile_id', $arrmixInsertData ) && false == empty( $arrmixInsertData['user_mobile_id'] ) ) {
	    							
	    							if( true == Users_model::updateMobile($arrmixInsertData ) ) {
	    								$boolIsUpdate	= true;
	    							}
	    			
	    						} else{
	    							if( true == array_key_exists( 'user_mobile_id', $arrmixInsertData ) ) {
	    									unset( $arrmixInsertData['user_mobile_id'] );
	    							}
	    							$arrmixInsertData['status']		= 1;
	    							if( true == Users_model::insertMobile( $arrmixInsertData, $intUserId ) ) {
	    								$boolIsUpdate	= true;
	    							}
	    						}
	    					}
	    					
    					$k++;
	    				}
    				}
    				
    				if( true == $boolIsUpdate ) {
	    					$data = array(
	    									'error' => 'suc',
	    									'msg' => 'Contact details updated successfully.'
	    					);
	    					echo json_encode($data);
    				}
	    		} else {
			    			$data = array(
			    							'error' => 'suc',
			    							'msg' => 'Contact details are not updated.'
			    			);
			    			echo json_encode( $data );
	    		}
    	} else{
    	
	    		$data = array(
    				'error' => 'true',
    				'mob' => form_error( 'mobile_number[]' ),
    				'user_mobile' => form_error( 'user_mobile_number' ),
    				'country_code' => form_error( 'country_code[]' ),
    				'off_country' => form_error( 'office_country_code' ),
    				'hm_country' => form_error( 'home_country_code' ),
    				'off_ext' => form_error( 'office_ext' ),
    				'hm_ext' => form_error( 'home_ext' ),
    				'off_contact' => form_error( 'office_contact' ),
    				'hm_contact' => form_error( 'home_contact' ),
    				'fx_country' => form_error( 'fax_country' ),
    				'fx_ext' => form_error( 'fax_ext' ),
    				'fx' => form_error( 'fax' )
	    		);
	    		echo json_encode($data);
    	}
      
    }
    
   public function address() {
    	
	    	$intUserId = $this->session->userdata( 'user_id');
	    	// To validate form inputs  of address tab. 
	    	$this->form_validation->set_rules( 'address[]', 'Address', 'trim|required|min_length[2]|max_length[255]|validate_address|xss_clean');
	    	$this->form_validation->set_rules( 'zipcode[]', 'Zip code', 'trim|required|is_digit|exact_length[6]|xss_clean' );
	    
	     	$arrmixUserInfo['user_info']	= Users_model::fetchUserById( $intUserId );
	     	// Fetch ids from user addresses.  
	    	$arrintUserAddressIds	= Users_model::fetchUserAddressIdsByUserId( $intUserId );
	    	
    		if( true == $this->form_validation->run() ) {
					// Get total count of added user addresses.
    				$intInputFieldCount	= ( int ) $this->input->post('addr_value');
    				$arrmixFormInput		= array_keys( $this->input->post() ); 
    			
    				if( false == empty( $this->input->post('user_addr_id') ) ) {
    						// Get deleted ids of user addresses.
    						$arrintDeletedIds = array_diff( $arrintUserAddressIds, $this->input->post('user_addr_id') );
    				} else {
    						$arrintDeletedIds		= $arrintUserAddressIds;
    				}
    				
    				$arrmixFlipFormInput = array_flip( $arrmixFormInput );
	   				unset( $arrmixFlipFormInput['addr_value'] );
    		
    				$arrmixFormInput =	array_flip( $arrmixFlipFormInput );
    			
    				$boolIsUpdate	= false;

	    				if( false == empty( $arrintDeletedIds )) { 
		    					if( true == Users_model::deleteAddress( $arrintDeletedIds ) ) {
		    							$boolIsUpdate	= true;
		    					}
	    				}
	    			
	    				for($i = 0;$i< $intInputFieldCount ;$i++ ) {
				    	 
		    					$k = 0;
					    		$arrmixInsertData = array();
		    					
		    					foreach( $arrmixFormInput as $strFields => $strFieldValue ) {
			    						foreach(  $this->input->post() as $strFieldName =>$arrmixInputValues ){
	
							    		 		if( $strFieldValue == $strFieldName ) {
									 		 			$arrmixInsertData[$strFieldValue]		= ( true == isset( $arrmixInputValues[$i] ) )  ? $arrmixInputValues[$i] : '';
							   				} 
						  			  }
						  			
									if( 5 == $k ) {
										// code for updaing existing address.
										// Check 'user_addr_id' key exist in array or not. if found the update the address.
										if( true == array_key_exists('user_addr_id', $arrmixInsertData ) && false == empty( $arrmixInsertData['user_addr_id'] ) ) {
										
												if( true == Users_model::updateAddress($arrmixInsertData ) ) { 
														$boolIsUpdate	= true;
												} 
											
										} else{
											// code for adding new address.
												if( true == array_key_exists('user_addr_id', $arrmixInsertData ) ) {
													unset( $arrmixInsertData['user_addr_id'] );
												}
												
										    	if( true == Users_model::insertAddress( $arrmixInsertData, $intUserId ) ) {
										    			$boolIsUpdate	= true;
										    	}
										}
							    }
				    			$k++;
		    		
		    				}
	    			}
    				
    			if( true == $boolIsUpdate ) {
	    				$data = array(
	    								'error' => 'suc',
	    								'msg' => 'Address Details updated successfully.'
	    				);
	    				echo json_encode($data);
    			}
    			
    	}else{
		    		$data = array(
    				'error' => 'true',
    				'address' => form_error('address[]'),
    				'zipcode' => form_error('zipcode[]')
    
    		);
    		echo json_encode($data);
    	}
    	 
  }
  
    public function social() {
    	
     	$intUserId = $this->session->userdata( 'user_id'); 
    	// To validate form inputs of social tab.
    	$this->form_validation->set_rules( 'website[]', 'Website', 'trim|required|max_length[200]|website_url|valid_url_format|xss_clean');
    	$arrmixUserInfo['user_info']	= Users_model::fetchUserById( $intUserId );
    	
		// Fech ids of  social webstes By user id.. 
    	$arrmixUserSocialWebsites 	= Users_model::fetchUserSocialWebsitesByUserId( $intUserId );
    	
    	$arrintUserWebsiteIds 	= array();
    	$arrstrUserWebsites		= array();
    	
    	// Get website ids, name.
    	foreach(  $arrmixUserSocialWebsites as $intIndex => $objUser ) {
    		$arrintUserWebsiteIds[]		= $objUser->id;
    		$arrstrUserWebsites[]		= $objUser->website;
    	}
    	
    	if( true == $this->form_validation->run() ) {

    			 		// Get total count of websites added by user. 
		    			$intInputFieldCount	= ( int ) $this->input->post('website_value');
		    			$arrmixFormInput		= array_keys( $this->input->post() );
		    			 
		    			// Get deleted id of website.
		    			$arrintDeletedIds		= ( false == empty( $this->input->post('user_website_id') ) ) ? array_diff( $arrintUserWebsiteIds, $this->input->post('user_website_id') ) : $arrintUserWebsiteIds;
		    			
		    			$arrmixFlipFormInput = array_flip( $arrmixFormInput );
		    			unset( $arrmixFlipFormInput['website_value'] );
		    			 
		    			$arrmixFormInput =	array_flip( $arrmixFlipFormInput );
		    			
		    			$boolIsUpdate	= false;
		    			 
		    			if( false == empty( $arrintDeletedIds ) ) {
		    					// Remove deleted websites.
			    				if( true == Users_model::deleteWebsite( $arrintDeletedIds ) ) {
			    						$boolIsUpdate	= true;
			    				}
		    			}
		    			 
		    			for($i = 0;$i< $intInputFieldCount ;$i++ ) {
		    				 
			    				$k = 0;
			    				$arrmixInsertData = array();
		    				 
		    				foreach( $arrmixFormInput as $strFields => $strFieldValue ) {
		    					
			    					foreach(  $this->input->post() as $strFieldName =>$arrmixInputValues ) {
			    			
				    						if( $strFieldValue == $strFieldName ) {
				    								$arrmixInsertData[$strFieldValue]		= ( true == isset( $arrmixInputValues[$i] ) )  ? $arrmixInputValues[$i] : '';
				    						}
			    					}
			    				
		    					if( 1 == $k ) {
		    							// Code for update websites. 
			    						if( true == array_key_exists('user_website_id', $arrmixInsertData ) && false == empty( $arrmixInsertData['user_website_id'] ) ) {
			    			
			    							if( true == Users_model::updateSocial( $arrmixInsertData ) ) {
			    									$boolIsUpdate	= true;
			    							} else {
			    									$boolIsUpdate	= false;
			    									break;
			    							}
			    			
			    						} else{
			    									// Code for inserting websites.
					    							if( true == array_key_exists( 'user_website_id', $arrmixInsertData ) ) {
					    								unset( $arrmixInsertData['user_website_id'] );
					    							}
					    							
					    							if(  true == Users_model::insertSocial( $arrmixInsertData, $intUserId ) ) {
					    									$boolIsUpdate	= true;
					    							} else {
					    									$boolIsUpdate	= false;
					    							}
			    						}
		    					}
		    					$k++;
		    				}
		    			}
		    			
		    			if( true == $boolIsUpdate ) {
				    			$data = array(
				    					'error' => 'suc',
				    					'msg' => 'Social links updated successfully.'
				    			);
				    			echo json_encode($data);
		    			}		
		    			
		    			if( false == $boolIsUpdate ) {
			    				$data = array(
			    								'error' => 'suc',
			    								'errmsg' => 'Social links  are not updated.'
			    				);
			    				echo json_encode($data);
		    			}
    		//	}
   	} else{
	    		$data = array(
	    				'error' => 'true',
	    				'web' =>  form_error('website[]')
	    		);
	    		echo json_encode($data);
    	}
    	
    }
   
    public function notify() {
   
	    	$intUserId = $this->session->userdata( 'user_id');
	 		// Update notifications.
	    	if( true == Users_model::update( $this->input->post(), NULL, $intUserId ) ) {
	    
	    			$data = array(
	    					'error' => 'suc',
	    					'msg' => 'Preferences updated successfully.'
	    			);
	    			echo json_encode($data);
	    	}
   }
    
    public function loadLocations() {
    
    	$strLoadType	= $this->input->post('loadType');
    	$intLoadId		= $this->input->post('loadId');
    
    	// Fetch data for populating country, state  and city.
    	$objDbResult	= Locations_Model::fetchLocationsData( $strLoadType, $intLoadId );
    	$HTML="";
    
    	if( 0 < $objDbResult->num_rows() ) {
    		foreach( $objDbResult->result() as $strList ) {
    			$HTML.="<option value='" . $strList->id . "'>" . $strList->name . 	"</option>";
    		}
    	}
    	echo $HTML;
    }
    
    public function loadLocationsData() {
    
    	$strLoadType	= $this->input->post('loadType');
    	$intLoadId		= $this->input->post('loadId');
    	$intRow			=$this->input->post('row');
    	// Fetch data for populating country, state  and city for adding multiple addresses.
    	$objDbResult	= Locations_Model::fetchLoadLocationsData( $strLoadType, $intLoadId, $intRow );
    	$HTML="";
    	
    	if( 0 < $objDbResult->num_rows() ) {
    		foreach( $objDbResult->result() as $strList ) {
    			$HTML.="<option value='" . $strList->id . "' >" . $strList->name . 	"</option>";
    		}
    	}
    	
    	echo $HTML;
    }

    public function getLocationData() {
    
    	$country_id	= $this->input->post('id');
    	$type	= $this->input->post('type');
    	
    	// Fetch data for state  and city for adding multiple addresses.
    	$objDbResult	= Locations_Model::getLocationsData( $country_id, $type );
    	$HTML="";
    	//$selected = "selected=\"selected\"";
    	$id = '';
    	if( $type == 'state' ) {
    		$id = $this->session->userdata( 'state_id' );
    		$this->session->unset_userdata( 'state_id' );
    	} elseif ( $type == 'city' ) {
    		$id = $this->session->userdata( 'city_id' );
    		$this->session->unset_userdata( 'city_id' );
    	}
    	
    	
    	if( 0 < $objDbResult->num_rows() ) {
    		foreach( $objDbResult->result() as $strList ) {
    			if( $id == $strList->id ) {
    				$HTML.="<option value='" . $strList->id . "' selected>" . $strList->name . 	"</option>";
    			} else {
    				$HTML.="<option value='" . $strList->id . "' >" . $strList->name . 	"</option>";			
    			}
    			
    		}
    	}
		
    	echo $HTML;
    }
    
    public function validate_zipcode( $strFieldValue ) {
	
		if( false == empty( $strFieldValue ) ) {
			if( '100' == $this->input->post('country_id') ) {
				if( '6' == strlen( $strFieldValue ) ) {
					if( true == preg_match( '/^[1-9]{1}[0-9]{5}+$/', $strFieldValue ) ) {
						return true;
					} else {
						$this->form_validation->set_message( 'validate_zipcode', '%s should not start with zero' );
						return false;
					}
				} else {
					$this->form_validation->set_message( 'validate_zipcode', '%s accepts exactly 6 digits' );
					return false;
				}
			} else {
				if( true == preg_match( '/^[\-+]?[0-9]*\.?[0-9]+$/', $strFieldValue ) ) {
					return true;
				}
			}
		}
	}
	
	public function view() {
		
			$intUserId 					= $this->session->userdata( 'user_id');
			$intAccountId 				= $this->session->userdata( 'account_number');
			//	$arrmixUserInfo['user_info'] 	= Users_model::fetchUserById( $intUserId );
			$arrmixUserInfo['user_info'] 	= $this->arrmixInfo['profile_info'];
			//$arrmixUserInfo['prefernce'] 	= Users_model::prefernces(  );
			$strProfileImg					= $arrmixUserInfo['user_info']->profile_picture;
			$arrmixUserInfo['msg']			= $this->session->userdata('msg');
			
			// Fetch ids of user addresses by user. 
			$arrmixUserInfo['user_addresses'] 	= Users_model::fetchUserAddressesById( $intUserId );
			
			// Fetch ids of user Emails by user.
			$arrmixUserInfo['user_emails'] 		= Users_model::fetchEmailAddressesById( $intUserId );
			
			// Fetch ids of user mobile numbers by user.
			$arrmixUserInfo['user_mobile_numbers'] 		= Users_model::fetchMobileNumbersById( $intUserId );
			
			// Fetch ids of user social websited by user.
			$arrmixUserInfo['user_social_websites'] 		= Users_model::fetchSocialWebsitesById( $intUserId );
		
			if( 0 == count( $arrmixUserInfo['user_addresses'] ) ) {
					$arrmixUserInfo['user_addresses'][0] = new stdClass();
					$arrmixUserInfo['user_addresses'][0]->id = '';
					$arrmixUserInfo['user_addresses'][0]->user_id = '';
					$arrmixUserInfo['user_addresses'][0]->address = '';
					$arrmixUserInfo['user_addresses'][0]->zipcode = '';
					$arrmixUserInfo['user_addresses'][0]->country_id = '';
					$arrmixUserInfo['user_addresses'][0]->state_id = '';
					$arrmixUserInfo['user_addresses'][0]->city_id = '';
					$arrmixUserInfo['user_addresses'][0]->country = '';
					$arrmixUserInfo['user_addresses'][0]->state = '';
					$arrmixUserInfo['user_addresses'][0]->city = '';
			}	
			
// 			if( 0 == count( $arrmixUserInfo['user_emails'] ) ) {
// 					$arrmixUserInfo['user_emails'][0] = new stdClass();
// 					$arrmixUserInfo['user_emails'][0]->id = '';
// 					$arrmixUserInfo['user_emails'][0]->user_id = '';
// 					$arrmixUserInfo['user_emails'][0]->email_address = '';
// 			}
			
			if( 0 == count( $arrmixUserInfo['user_mobile_numbers'] ) ) {
					$arrmixUserInfo['user_mobile_numbers'][0] = new stdClass();
					$arrmixUserInfo['user_mobile_numbers'][0]->id = '';
					$arrmixUserInfo['user_mobile_numbers'][0]->user_id = '';
					$arrmixUserInfo['user_mobile_numbers'][0]->country_code = '';
					$arrmixUserInfo['user_mobile_numbers'][0]->mobile_number = '';
			}
			
			if( 0 == count( $arrmixUserInfo['user_social_websites'] ) ) {
					$arrmixUserInfo['user_social_websites'][0] = new stdClass();
					$arrmixUserInfo['user_social_websites'][0]->id = '';
					$arrmixUserInfo['user_social_websites'][0]->user_id = '';
					$arrmixUserInfo['user_social_websites'][0]->website = '';
					$arrmixUserInfo['user_social_websites'][0]->logo = '';
			}
			// Fetch locations of user by country id. 
			$arrmixLocationInfo['countries'] = Locations_Model::fetchLocationsById( $arrmixUserInfo['user_info']->country_id );
			
			//$this->load->view('user_profile', $arrmixUserInfo );
			
			$this->load->view( 'customer_profile', $arrmixUserInfo );
			$this->session->unset_userdata('msg');
		
	}
		
	public function update() {
		
		$intUserId 			= $this->session->userdata( 'user_id');
		$intAccountId		= $this->session->userdata( 'account_number');
		
		$arrmixUserEmailIs	= Users_model::fetchUserEmailIdsByUserId( $intUserId );
		
		$arrintUserEmailIds 	= array();
		$arrstrUserEmails		= array();
		foreach( $arrmixUserEmailIs as $intIndex => $objUser ) {
			$arrintUserEmailIds[]		= $objUser->id;
			$arrstrUserEmails[]			= $objUser->email_address;
		}
		
		if( false == isset( $intAccountId ) || empty( $intAccountId ) ) {
			show_404();
		}
		
		$this->form_validation->set_rules( 'first_name', 'First name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean');
		$this->form_validation->set_rules( 'last_name', 'Last name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean');
		$this->form_validation->set_rules( 'user_email', 'Email', 'trim|required|valid_email|max_length[50]|callback_edit_unique[users.email_address.' . $intUserId .']|xss_clean');
	
		if( true == is_array( $this->input->post() ) && false == empty( $this->input->post( 'email_address' )) ) {
			$this->form_validation->set_rules( 'email_address[]', 'Email', 'trim|required|valid_email|max_length[50]|callback_edit_unique_email[user_emails.email_address.' . $intUserId .']|xss_clean');
		}
		
		$arrmixUserInfo['user_info']	= $this->arrmixInfo['profile_info'];
		$strProfileImg					= $arrmixUserInfo['user_info']->profile_picture;
		$msg = '';
	
		if( true == $this->form_validation->run() ) {
	
			//set the path where the files uploaded will be copied.
		 	$config['upload_path'] = 'uploads/';
	
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->set_allowed_types('gif|jpg|png|jpeg');
		
			$boolImageUploadFlag = true;
			// uplod profile image.
			if( false == empty( $_FILES['profile_img']['name'] )  ) {
				
				if( true == $this->upload->do_upload('profile_img') ) {
					
					if( true == file_exists('uploads/'.$strProfileImg)) {
							$msg	.= "Profile image uploaded successfully.";
							if( false == unlink( 'uploads/'.$arrmixUserInfo['user_info']->profile_picture )) {
		          				$msg	.= "Error deleting old file, ";
		          		}
					}
	          		
	          		$data 					= array( 'msg' => $msg );
	          		$data['upload_data'] 	= $this->upload->data();
	          		$strProfileImg			= $this->upload->file_name;
	          		$boolImageUploadFlag	= true;
				} else {
						
					$strProfileImg	= NULL;
					$msg .= $this->upload->display_errors();
					$boolImageUploadFlag	= false;
				}
	          		
			} else {
         
          		if( true == empty( $_FILES['profile_img']['name']) ) {
	          		$strProfileImg	= $arrmixUserInfo['user_info']->profile_picture;
	          		$boolImageUploadFlag	= true;
          		} 
         	}	
      
          	if( true == $boolImageUploadFlag ) {
          		// Fetch countries by Id.
	          	$arrmixLocationInfo['countries'] 	= Locations_Model::fetchLocationsById( $arrmixUserInfo['user_info']->country_id );
	          	// Fetch countries by Type.
	          	$arrmixUserInfo['countries']		= Locations_Model::fetchLocationsByType( 0 );
	          	// Fetch states by countries by Type.
	          	$arrmixUserInfo['states']			= Locations_Model::fetchLocationsByCountryIdByType( $arrmixUserInfo['user_info']->country_id, '1' );
	          	// Fetch cities by countries by state by Type.
	          	$arrmixUserInfo['cities']			= Locations_Model::fetchLocationsByCountryIdByStateIdByType( $arrmixUserInfo['user_info']->country_id, $arrmixUserInfo['user_info']->state_id, '2' );
	          		
	          	$boolIsUpdate	= false;
	          		
	          	if( true == Users_model::update( $this->input->post(), $strProfileImg, $intUserId ) ) {
	          		$strMessage = 'Your Primary Email address has been updated successfully with Gniss';
	          		$arrstrEmailFormat = array(
          				'header' 	=> 'Dear Recipient,',
          				'content'	=> $strMessage
	          		);
	          				
	          		if( 0 != strcasecmp( $this->input->post('user_email'), $arrmixUserInfo['user_info']->email_address ) ) {
	          	
		        		if( true == $this->verifyemail->check($arrmixInsertData['email_address'] ) && true == sendMail( 'no_reply@rectusenergy.com',  $arrmixInsertData['user_email'],  'Email Verification', $arrstrEmailFormat ) &&  true == Users_model::updatePrimaryEmail( $this->input->post( 'user_email' ), $intUserId ) ) {
		        			$boolIsUpdate = true;
		        		}
	          		} else { 
	          			$boolIsUpdate = true;
	          		}
					// Get total count of emails added by user. 	          				
	          		$intInputFieldCount	= ( int ) $this->input->post('email_value');
	          		$arrmixFormInput		= array_keys( $this->input->post() );
	          				
	          		// Get deleted ids of Alternate emails. 
	          		$arrintDeletedIds	= ( false == empty( $this->input->post('user_email_id') ) ) ? array_diff( $arrintUserEmailIds, $this->input->post('user_email_id') ) : $arrintUserEmailIds;
	          				
	          		$arrmixFlipFormInput = array_flip( $arrmixFormInput );
	          		unset( $arrmixFlipFormInput['email_value'] );
          			
          			$arrmixFormInput =	array_flip( $arrmixFlipFormInput );
          				          			
          			if( false == empty( $arrintDeletedIds ) ) {
		          		if( true == Users_model::deleteEmail( $arrintDeletedIds ) ) {
		          			$boolIsUpdate	= true;
		          		}
          			}
	          			
          			for($i = 0;$i< $intInputFieldCount ;$i++ ) {
          			
	          			$k = 0;
	          			$arrmixInsertData = array();
				          				 
	          			foreach( $arrmixFormInput as $strFields => $strFieldValue ) {
		          			foreach(  $this->input->post() as $strFieldName =>$arrmixInputValues ){
		          								          							
			          			if( $strFieldValue == $strFieldName && true == isset(  $arrmixInputValues[$i] ) ) {
			          				$arrmixInsertData[$strFieldValue]		= $arrmixInputValues[$i];
			          			}
		          			}
				          				
	          				if( 3 == $k ) {
	          					// Remove unnecessary variables from post array.
	          					unset( $arrmixInsertData['first_name'] );
	          					unset( $arrmixInsertData['last_name'] );
	          					unset( $arrmixInsertData['user_email'] );
		          				
	          					if( true == array_key_exists('user_email_id', $arrmixInsertData ) && false == empty( $arrmixInsertData['user_email_id'] ) ) { 
	          								
	          						$strMessage = 'Your Alternate Email address has been updated successfully with Gniss';	
	          						$arrstrEmailFormat = array(
	          							'header' 	=> 'Dear Recipient,',
	          							'content'	=> $strMessage
	          						);
	          							
		          					if( false == in_array( $arrmixInsertData['email_address'], $arrstrUserEmails ) && true == $this->verifyemail->check($arrmixInsertData['email_address'] ) && true == sendMail( 'no_reply@rectusenergy.com',  $arrmixInsertData['email_address'],  'Email Verification', $arrstrEmailFormat )  && true == Users_model::updateEmail( $arrmixInsertData ) ) {
		          						$boolIsUpdate	= true;
		          					} else {
		          						$boolIsUpdate	= false;
		          					 	 break;
		          					}
					          								
	          					} else {
	          						if( true == array_key_exists( 'user_email_id', $arrmixInsertData ) ) {
	          							unset( $arrmixInsertData['user_email_id'] );
	          						}
		          			
			          				$strMessage = 'Your Alternate Email address has been added successfully with Gniss';
			          				$arrstrEmailFormat = array(
	          							'header' 	=> 'Dear Recipient,',
	          							'content'	=> $strMessage
			          				);
			          				
									if(  false == in_array( $arrmixInsertData['email_address'], $arrstrUserEmails ) && true == $this->verifyemail->check($arrmixInsertData['email_address'] ) && true == sendMail( 'no_reply@rectusenergy.com', $this->input->post('user_email'),  'Email Verification', $arrstrEmailFormat ) ) {
		          						
			          					if( true == Users_model::insertEmail( $arrmixInsertData, $intUserId ) ) {
			          						$boolIsUpdate	= true;
				          				} else {
				          					$boolIsUpdate	= false;
				          				}
		          					}
	          					}
	          				}
	          				$k++;
          				}
		        	}
          			$this->session->unset_userdata('name');
          			$this->session->set_userdata( array( 'name' => $this->input->post('first_name') . ' ' . $this->input->post('last_name') ) );
          		}
	                 
				if( true == $boolIsUpdate ) {
			       	$data = array(
			    		'error' => 'suc',
	                 	'msg' => 'General details updated successfully.'
	                 );
	                 echo json_encode($data); 
	            }
	                 
	            if( false == $boolIsUpdate ) {
	                	
					$data = array(
				    	'error' => 'true',
						'msg' => $msg
			        );
			        if( true == is_array( $this->input->post() ) && false == empty( $this->input->post( 'email_address' )) ) {
			        	$data['email_address']		= form_error('email_address[]');
			        }
				   	echo json_encode($data);
		       	}
                 
	        } else {
	          	
	          	$data = array(
	          		'error' => 'true',
	          		'msg' => $msg
	          	);
	          	if( true == is_array( $this->input->post() ) && false == empty( $this->input->post( 'email_address' )) ) {
	          		$data['email_address']		= form_error('email_address[]');
	          	}
		      	echo json_encode($data);
			}

		} else {
				
			$data = array(
				'error' => 'true',
	            'firstn' => form_error('first_name'), 
	            'lastn' => form_error('last_name'),
			    'err_email' => form_error('user_email'),
				'msg' =>$msg
	        );
			      	
			if( true == is_array( $this->input->post() ) && false == empty( $this->input->post( 'email_address' )) ) {
			 	$data['email_address']		= form_error('email_address[]');
			}
			echo json_encode($data);
		}
	}
	// Need to move in common functions.
// 	public function sendMail( $strEmailFrom, $strEmailTo, $strSubject, $arrstrEmailFormat ) {
	
// 		$this->load->library( 'email' );
// 		$this->load->library( 'parser' );
// 		$this->email->mailtype	= 'html';
			
// 		$this->email->from( $strEmailFrom, 'Rectus Energy pvt ltd' );
// 		$this->email->to( $strEmailTo );
// 		$this->email->subject( $strSubject );
			
// 		$strEmailContent = $this->parser->parse( 'email_template',$arrstrEmailFormat, TRUE );
// 		$this->email->message( $strEmailContent );
			
// 		return $this->email->send();
// 	}
	
	public function edit_unique ( $strFieldValue, $strFieldParameter ) {
	
		list( $strTableName, $strFieldName, $intId ) = explode( ".", $strFieldParameter, 3 );
		
			$objDbResult = $this->db->select( $strFieldName )->from( $strTableName )->where( $strFieldName, $strFieldValue )->where( 'id !=', $intId )->where( 'deleted_by IS NULL' )->limit(1)->get();
		
			if ( true == ( boolean ) $objDbResult->row() ) {
					$this->form_validation->set_message( 'edit_unique', 'The %s already exists.' );
					return false;
			} else {
					return true;
			}
	}
	
	public function edit_unique_email( $strFieldValue, $strFieldParameter ) {
	
		//	$this->form_validation->set_message( 'edit_unique', 'The %s already exists.' );
		
			list( $strTableName, $strFieldName, $intId ) = explode( ".", $strFieldParameter, 3 );
		
			$objDbResult = $this->db->select( $strFieldName )->from( $strTableName )->where( $strFieldName, $strFieldValue )->where( 'user_id !=', $intId )->limit(1)->get();
		
			if ( true == ( boolean ) $objDbResult->row() ) {
				$this->form_validation->set_message('edit_unique_email', 'The %s already exists.');
				return false;
			} else {
				return true;
			}
	}
	
	public function createOtp() {
	
			$intMobileNumber	= $this->input->post( 'mobile_number' );
		
			$strToken = $this->generateOtp( 6, 'abcdefghijklmnopqrstuvwxyz1234567890' );
		
			$this->input->set_cookie( 'jsession', md5( $strToken ), 120 );
		
			$this->sendSms( $intMobileNumber, $strToken );
			
	}
	
	public function verifyOtp() {
	
		$this->form_validation->set_rules( 'verify_code', 'Verification Code', 'trim|required|exact_length[6]|xss_clean');
			
		if( true == $this->form_validation->run() ) {
					$strVerifyCode	= md5( $this->input->post( 'verify_code' ) );
					
					$strMobileNumber = $this->input->post( 'mobile_no' );
					$strField		= $this->input->post( 'field' );
					$intPid			= $this->input->post( 'pid' );
					
					$strOtpCode			= $this->input->cookie('jsession');
			
					if( $strVerifyCode == $strOtpCode ) {
			
						if( false == empty( $intPid ) && true == Users_model::updateMobileVerificationStatus( $strField, $intPid ) ) {
								echo json_encode(  array( 'verify' => 'success', 'number' =>$strMobileNumber ) );
						} else {
								echo json_encode(  array( 'verify' => 'success', 'number' => $strMobileNumber ) );
						}
					} else {
							echo json_encode(  array( 'verify' => 'wrong' ) );
					}
			} else{
				echo json_encode(  array( 'verify' => 'fail', 'error' => form_error('verify_code') ) );
			}
	}
		
	public function generateOtp($length , $chars ) {
	
			$chars_length = (strlen($chars) - 1);
			$string = $chars{rand(0, $chars_length)};
			for ($i = 1; $i < $length; $i = strlen($string))
			{
				$r = $chars{rand(0, $chars_length)};
				if ($r != $string{$i - 1}) $string .=  $r;
			}
			return $string;
	}
	
	function valid_url_format($str){
        
		//$pattern = "|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
		//$pattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
		$pattern =	"@^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$@i";
		if (!preg_match( $pattern, $str)){
		     $this->form_validation->set_message('valid_url_format', 'The URL you entered is not correctly formatted.');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// Need to move in common functions.
	public function sendSms( $intMobileNumber, $strToken ) {
	
				// Replace with your username
				$user = "rectusenergy";
					
				// Replace with your API KEY (We have sent API KEY on activation email, also available on panel)
				$apikey = "icv59VrXI2CWEDCHRfOw";
					
				// Replace if you have your own Sender ID, else donot change
				$senderid  =  "MYTEXT";
					
				// Replace with the destination mobile Number to which you want to send sms
				$mobile  =  $intMobileNumber;
					
				// Replace with your Message content
				$message   =  'Dear '. $this->session->userdata( 'name').'! Your One-Time password is: '. $strToken;
				$message = urlencode($message);
					
				// For Plain Text, use "txt" ; for Unicode symbols or regional Languages like hindi/tamil/kannada use "uni"
				$type   =  "txt";
					
		 		$ch = curl_init();
					
// 				// set URL and other appropriate options
 		  		curl_setopt($ch, CURLOPT_URL, "http://smshorizon.co.in/api/sendsms.php?user=".$user."&apikey=".$apikey."&mobile=".$intMobileNumber."&senderid=".$senderid."&message=".$message."&type=".$type."");
 		  		curl_setopt($ch, CURLOPT_HEADER, 0);
					
// 				// grab URL and pass it to the browser
 		 		 $output = curl_exec($ch);
		
// 				// close cURL resource, and free up system resources
				 curl_close($ch);
				
				// Display MSGID of the successful sms push
				echo $output;
		}
}