<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageLiveTrackingController extends CI_Controller { 
	 
	public function __construct() {
        parent::__construct();       
    }
    
    public function index() {
    	
		$arrmixLiveTrackingInfo['deviceid']	= NULL;
		$this->load->view( 'livetracking', $arrmixLiveTrackingInfo );
			 
	}

	public function viewmap( $intDeviceId ) {
		
		$arrmixLiveTrackingInfo['deviceid']	= $intDeviceId;
		$this->load->view( 'livetracking', $arrmixLiveTrackingInfo );
		
	}
	
	public function getLatlong( $intPhoneNumber = NULL ) {
		header("Content-type: text/xml");
	 	echo '<markers>';
	 	
	   if( false == is_null( $intPhoneNumber ) ) {
	   		$arrmixLiveLocationData = Tracker_model::getTrackerByPhoneNumber( $intPhoneNumber );
	   } else {
		     $arrmixLiveLocationData = Tracker_model::getAllTrackers();
	   }
	   
		if( 0 < count($arrmixLiveLocationData) ) {
			
			$strTodayDate		=  date("Y-m-d H:i:s");
			$strTodayDate 	= strtotime($strTodayDate);
			
			foreach ( $arrmixLiveLocationData as $objLiveLocationData ) {
				
					$intLastDate 				= strtotime( $objLiveLocationData->rawdate );
					$fltLatestLatitude 		= round($objLiveLocationData->latitude,5);
					$fltLatestLongitude 	= round($objLiveLocationData->longitude,5);
					
					$strLiveAddress		= $this->getAddress( $fltLatestLatitude, $fltLatestLongitude );
						
					if ( true == empty( $strLiveAddress )) {
							$strLiveAddress	= "Address not available " . $fltLatestLatitude . "," . $fltLatestLongitude;
					}
					$strVehBearing	=	 "";
					$strIgnition			= "";
					$strVhGps				= "";
					$strAc						= "";
					$strVhStIm			= "";
					
					$intSpeedVal	= $objLiveLocationData->speed; 
					
					echo '<marker lat="' . $objLiveLocationData->latitude . '" lng="' . $objLiveLocationData->longitude. '" ';
					echo ' vehicle_dir="' . $strVehBearing . '"';
					echo ' titleb="Speed : ' . $intSpeedVal . ' Km/hr | Ignition : ' . ucfirst( $strIgnition ).' | AC : ' . ucfirst( $strAc ) . ' | GPS : ' . strtoupper( $strVhGps ) . ' | Date-Time : ' . $objLiveLocationData->rawdate . ' | Address : ' . $strLiveAddress .'"';
						// 					echo ' html="&lt;b&gt;Vehicle&lt;/b&gt; : '.ucfirst($result_usr['name']).' &lt;br&gt;&lt;b&gt;Latitude&lt;/b&gt; : '.$res_latest['latitude'].' &lt;br&gt;&lt;b&gt;Longitude&lt;/b&gt; : '.$res_latest['longitude'].' &lt;br&gt;&lt;b&gt;Speed&lt;/b&gt; : '.$intSpeedVal.' Km/hr &lt;br&gt;&lt;b&gt;Ignition&lt;/b&gt; : '.$strIgnition.'&lt;br&gt;&lt;b&gt;AC&lt;/b&gt; : '.$strAc.'&lt;br&gt;&lt;b&gt;GPS&lt;/b&gt; : '.strtoupper($strVhGps).' &lt;br&gt;&lt;b&gt;Date-Time&lt;/b&gt; : '.$res_latest['rawdate'].' &lt;br&gt;&lt;b&gt;Address&lt;/b&gt; : '.$vh_adr.'"';
							
						echo ' html="&lt;b&gt;Name&lt;/b&gt; : '.$objLiveLocationData->phoneno.
									' &lt;br&gt;&lt;b&gt;Speed&lt;/b&gt; : '.$intSpeedVal.' Km/hr &lt;br
									&gt;&lt;b&gt;Date-Time&lt;/b&gt; : '.$objLiveLocationData->dated.' &lt;br
									&gt;&lt;b&gt;Address&lt;/b&gt; : '.$strLiveAddress.'"';
					
					echo ' token="' . $objLiveLocationData->token . '"';
					echo ' phoneno="' . $objLiveLocationData->phoneno . '"';
					echo ' vehicle_icon=""';
					echo ' label="'.$strVhStIm.'  '.$objLiveLocationData->phoneno.'" />';
					 
				}
		}
	  
	 echo '</markers>'; 
	}
	
	public function checkImeiNumber( $intImeiNumber , $intPhoneNumber ) {
		
			$intCntImeiNumber = Tracker_model::getImeiNumberCountByImeiNumberByPhoneNumber( $intImeiNumber , $intPhoneNumber );
			
			if( 0 < $intCntImeiNumber ) {
				
					$strApiKey = md5( time() );
					
					if( true == Tracker_model::updateApiKey( $strApiKey, $intPhoneNumber ) ) {
							echo json_encode( array('imminumber' => $intImeiNumber , 'flag'=>'1','msg'=>'imei number is avilable' ,'api_key'=>$strApiKey) );
					} else {
							echo json_encode( array('imminumber' => $intImeiNumber , 'flag'=>'0','msg'=>'imei number is not avilable' ) );
					}
					
			} else {
					echo json_encode( array('imminumber' => $intImeiNumber , 'flag'=>'0','msg'=>'imei number is not avilable' ) );
			}
	}
	 

	public function routeWiseLocations( $intPhoneNumber ) {
		
		    $arrMixlocationData['asset_info']		= 		asset_model::getAassetInfo( $intPhoneNumber );
			$arrMixlocationData['locations'] 		= 		Tracker_model::fetchDistanceReports( $intPhoneNumber );
			$this->load->view( 'view_routes.php' , $arrMixlocationData );
	}
	

	 public function viewLocationsmap( $intPhoneNumber , $intTokenNumber ) {

	 		$arrMixlocationData['asset_info']= asset_model::getAassetInfo( $intPhoneNumber );
			$arrMixlocationData['locations'] = Tracker_model::getroute( $intPhoneNumber , $intTokenNumber );
		
			$this->load->view('routereport',$arrMixlocationData);
		}
	
		// code review changes are remaining.
		public function insertdata( $arrmixTrackerData ) {
			$arrmixTrackerData = file_get_contents('php://input');
			$arrmixTrackerData = json_decode($arrmixTrackerData, true); 
			//echo "hi".print_r($array); exit;
			try
			{  
				if( Tracker_model::checkforapikey($arrmixTrackerData['api_key']) > 0 ) {
						$result= Tracker_model::insert( $arrmixTrackerData );
						if( $result ) { 
							echo json_encode( array( 'flag'=>'1','msg'=>'Insert data sucesfully' ) );
						} 
				}else {
					echo json_encode( array( 'flag'=>'0','msg'=>'Invalid Api Key. Please login again' ) );
				}
				
			}
			catch( Exception $e ) { //later
				echo json_encode( array( 'flag'=>'0','msg'=>'no data inserted' ) );
			}
		}
		
		// code review changes are remaining.
		public  function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$unit = strtoupper($unit);
		
			if ($unit == "K") {
				$retval = ($miles * 1.609344);
				return number_format($retval, 2, '.', '');
			} else if ($unit == "N") {
				return ($miles * 0.8684);
			} else {
				return $miles;
			}
		}
		
		public function getAddress( $dblLat, $dblLong ) {
		
			$strUrl		= "https://maps.googleapis.com/maps/api/geocode/json?latlng=". $dblLat.",". $dblLong ."&sensor=false";
		
			$strJsonData		= @file_get_contents( $strUrl );
			$objJson 				= json_decode( $strJsonData );
			$strStatus 			= $objJson->status;
		
			$strFormatedAddress = '';
		
			if( 'OK' == $strStatus ) {
				$strFormatedAddress = $objJson->results[0]->formatted_address;
			}
		
			return $strFormatedAddress;
		}
		
		
	
}
