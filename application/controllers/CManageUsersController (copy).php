<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CManageUsersController extends CI_Controller {
	
	public function action( $strAction = '' ) {
		
		switch( $strAction ) {
   	 		case 'register': 
    			$this->createUser();
    			break;
    			
    		case 'insertuser':
    			$this->insertUser();
    			break;
    
    		case 'login':
    			$this->loginUser();
    			break;

			case 'signin':
		          $this->signin();
		          break;
		
	    	case 'securityQuestion':
		          $this->showSecurityQuestion();
		          break;
		          
          	case 'checkSecurityQuestion': 
                    $this->checkSecurityQuestion();
                    break;
		          
          	case 'forgotPass':
                    $this->forgotPassword();
                    break;
    			
    		case 'resetPass':
    			$this->resetPassword();
    			break;
    			
    		default:
    			show_404();
    				
    	}
	}
	public function signin() {
	        $arrmixUserInfo    =  array();

	          if( true == $this->session->userdata( 'successmsg' ) ) {
	                    $arrmixUserInfo['msg'] = $this->session->userdata( 'successmsg' );
	                    $this->session->unset_userdata( 'successmsg' );
	        }
	          
	          if( true == $this->session->userdata( 'errmsg' ) ) {
	                    $arrmixUserInfo['errmsg'] = $this->session->userdata( 'errmsg' );
	                    $this->session->unset_userdata( 'errmsg' );
	        }
	     
	        $this->load->view( 'login', $arrmixUserInfo );
	}
	
	public function createUser() {

			$arrmixUserInfo	=	$this->registrationInfo();
			$this->load->view( 'register', $arrmixUserInfo );
			
	}
	
	public function insertUser() {
		
		if( false == is_array($this->input->post() ) ) {
			$this->session->set_userdata( array( 'successmsg' =>"Invalid Path" ) );
			redirect( base_url().'users/action/register' );
		}
		
			$this->form_validation->set_rules( 'username', 'Username', 'trim|required|min_length[2]|max_length[15]|alpha_dot_underscore|non_numeric_name|is_unique[users.username]|is_unique[companies.username]|xss_clean' );
			$this->form_validation->set_rules( 'email_address', 'Email address', 'trim|required|valid_email|is_unique[users.email_address]|max_length[50]|xss_clean' );
			$this->form_validation->set_rules( 'password', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
			$this->form_validation->set_rules( 'conf_password', 'Confirm password', 'trim|required|min_length[6]|max_length[20]|matches[password]|xss_clean' );
			$this->form_validation->set_rules( 'first_name', 'First name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
			$this->form_validation->set_rules( 'last_name', 'Last name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
			$this->form_validation->set_rules( 'mobile_number', 'Mobile number', 'trim|required|numeric|exact_length[10]|mobile_zero|xss_clean' );
			$this->form_validation->set_rules( 'country_id', 'Country', 'required|xss_clean' );
			$this->form_validation->set_rules( 'state_id', 'State', 'required|xss_clean' );
			$this->form_validation->set_rules( 'city_id', 'City', 'required|xss_clean' );
			$this->form_validation->set_rules( 'zip_code', 'Zip code', 'trim|is_digit|callback_validate_zipcode|exact_length_digit[6]|xss_clean' );
			$this->form_validation->set_rules( 'street_address', 'Street address', 'trim|min_length[2]|max_length[100]|validate_company_address|xss_clean' );
		  	$this->form_validation->set_rules( 'captcha', 'Captcha', 'required|captcha|xss_clean' );
		  	$this->form_validation->set_rules( 'question_id', 'Security question', 'required|xss_clean' );
		  	$this->form_validation->set_rules( 'answer', 'Answer', 'trim|required|min_length[2]|max_length[200]|xss_clean' );
		
			if( true == $this->form_validation->run() ) {
					
				$intCountryId 	= $this->input->post( 'country_id' );
				$arrmixLocationInfo['countries'] = Locations_Model::fetchLocationsById( $intCountryId );
				
				$intSecurityQuestionId 	= $this->input->post( 'question_id' );
				$strAnswer 	= $this->input->post( 'answer' );
				$arrmixSecurityQuestionInfo['security_question'] = Security_questions_model::fetchSecurityQuestions();
				
				$intMobileNumber = $this->input->post('mobile_number');
				$intPhoneCode	 = $arrmixLocationInfo['countries'][0]->code;
				$intMobileNumber = ( false == empty( $intPhoneCode ) ) ? $intPhoneCode . '-' . $intMobileNumber : $intMobileNumber;
				
				$login_url=base_url().'users/action/login';
				$arrmixUserInfo['adduser'] = Users_model::insertUser( $intMobileNumber );
				$intUserId = $arrmixUserInfo['adduser'];
				$arrmixUserInfo['addanswer'] = Security_questions_model::insert( $intSecurityQuestionId, $intUserId, user_type_model::c_individual, $strAnswer );
				$this->session->set_userdata( array( 'successmsg' =>"User added successfully" ) );
				
				$this->load->library('email');
				$this->load->library('parser');
			   $this->email->mailtype='html';
				
				
				$strEmailMsg='Welcome and thank you for registering at Gniss!<br>'.'Following is account details<br><br>'.
				'username :'.$this->input->post('username').'<br><br>'.
				'Password :'.$this->input->post('password').'<br><br>'.
				'email :'.$this->input->post('email_address').'<br><br>'.
				
				'<br>'.'<a href='.$login_url.'>Click he4re to login ';
			
	         $this->email->from('no_reply@rectusenergy.com', 'Rectus Energy pvt ltd');
				$this->email->to($this->input->post('email_address'));
				$this->email->subject('Thank you for registering');
				
				$arrstrEmailData = array(
							'header' 	=> 'Dear Recipient,',
							'content'	=> $strEmailMsg
				);
				
				$strEmailContent = $this->parser->parse('email_template',$arrstrEmailData,TRUE);
				$this->email->subject('Thank you for registering');
				$this->email->message( $strEmailContent );
			
          	   if( true == $this->email->send() ) {
                        redirect('users/action/login');
          	     } else {
          	          $arrmixUserInfo['errmsg'] = ' Could not send mail to user.';
          	      }
				
				//echo $this->email->print_debugger();
				//redirect( base_url().'users/action/login' );
			}
			else{
				$arrmixUserInfo	=	$this->registrationInfo();
				$this->load->view( 'register', $arrmixUserInfo );
			}
	}
		
	public function registrationInfo() {
		
			$arrmixUserInfo['states']	= array();
			$arrmixUserInfo['cities']	= array();
			//$arrmixUserInfo['captcha']	=	array();
			$arrmixUserInfo['security_question']	=	array();
				
			/*$this->load->library('Captcha');
			$arrmixUserInfo['captcha'] = $this->captcha->main();
				
			$this->session->set_userdata('captcha_info', $arrmixUserInfo['captcha']);*/
			
			$intCountryId	= $this->input->post('country_id');
			$intStateId		= $this->input->post('state_id');
				
			$arrmixUserInfo['countries']	= Locations_Model::fetchLocationsByType( 0 );
			
			$arrmixUserInfo['security_question'] = Security_questions_model::fetchSecurityQuestions();
			
			if( false == empty( $intCountryId ) ) {
				$arrmixUserInfo['states']	= Locations_Model::fetchLocationsByCountryIdByType( $intCountryId, '1' );
			}
			
			if( false == empty( $intCountryId ) &&  false == empty( $intStateId ) ) {
				$arrmixUserInfo['cities']		= Locations_Model::fetchLocationsByCountryIdByStateIdByType( $intCountryId, $intStateId, '2' );
			}
	
		return $arrmixUserInfo;
	}
	
/*	public function loginUser( ) {
	
		$cookieData = $this->input->cookie('gnisscookie', TRUE);
		if ($cookieData != "" && 'cookiein' == $this->input->post('cookie')) {
			$this->cookieLogin( $cookieData );
		} else {
	
			$this->sessionLogin();
		}
	}
	
	public function sessionLogin() {
		
		$strMessage['msg'] = $this->session->userdata('successmsg' );
		$this->session->unset_userdata( 'successmsg' );
		if( $this->input->post('login')=='login' ) {
		
			$this->form_validation->set_rules( 'lusername', 'Username', 'trim|required|max_length[50]|xss_clean' );
			$this->form_validation->set_rules( 'lpassword', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
			if( true == $this->form_validation->run() ) {
		
				$strUsername = $this->input->post('lusername');
				$strPassword = $this->input->post('lpassword');
				$strLoginType = $this->input->post('login_type');
				$is_valid = Users_model::fetchAdminUser( $strUsername, $strPassword, $strLoginType );
					
				if($is_valid) {
					$arrSessionData = array(
							'user_name'			=> $is_valid->username,
							'email_address'		=> $is_valid->email_address,
							'is_logged_in'		=> true
					);
					if( 'Individual' == $strLoginType ) {
						$arrSessionData['account_number']	= $is_valid->account_number;
						$arrSessionData['user_id']			= $is_valid->id;
						$arrSessionData['name']				= $is_valid->first_name;
					} else {
						$arrSessionData['company_id']		= $is_valid->id;
						$arrSessionData['user_id']			= $is_valid->created_by;
						$arrSessionData['name']				= $is_valid->company_name;
					}
					$arrSessionData['login_type']			= $strLoginType;
					$this->session->set_userdata($arrSessionData);
						
					if(	'yes' == $this->input->post('remember_me'))	{
						
						$cookieData = $is_valid->username.','.$strPassword.','.$strLoginType;
						$this->input->set_cookie('gnisscookie', $cookieData, 1296000);
					} else {
						
						//$this->input->set_cookie('gnisscookie', '');
					}
		
					redirect( base_url() );
				} else {
					 $this->session->set_userdata( array( 'errmsg' => "Invalid user details." ) );
                              			       redirect('users/action/signin');
				}
			}
		}
		$strMessage	=	$this->registrationInfo();
		$this->load->view( 'register',$strMessage );
	}
	
	public function  cookieLogin( $cookieData ) {
		
		$arrCookieData		=	explode(',',$cookieData);
		$strUserName		=	$arrCookieData[0];
		$strUserType		=	$arrCookieData[1];
		
		$arrUserSessionData		= Users_model::fetchUserByUsername( $strUserName, $strUserType );

		if( isset($arrUserSessionData) && $arrUserSessionData != '' ) {
			$arrSessionData = array(
					'user_name'			=> $arrUserSessionData->username,
					'email_address'		=> $arrUserSessionData->email_address,
					'is_logged_in'		=> true
			);
			if( 'Individual' == $strUserType ) {
				$arrSessionData['account_number']	= $arrUserSessionData->account_number;
				$arrSessionData['user_id']			= $arrUserSessionData->id;
				$arrSessionData['name']				= $arrUserSessionData->first_name;
			} else {
				$arrSessionData['company_id']		= $arrUserSessionData->id;
				$arrSessionData['user_id']			= $arrUserSessionData->created_by;
				$arrSessionData['name']				= $arrUserSessionData->company_name;
			}
			$arrSessionData['login_type']			= $strUserType;
			$this->session->set_userdata($arrSessionData);
			$this->load->view( 'dashboard' );
		} else {
			$this->session->sess_destroy();
			$this->input->set_cookie('gnisscookie', '');
			redirect(base_url());
		}
	}
*/
	public function loginUser() {
	
		if($this->session->userdata('is_logged_in')) {
			redirect(base_url());
		}	else{
			$strMessage['msg'] = $this->session->userdata('successmsg' );
			$this->session->unset_userdata( 'successmsg' );
	
			if( $this->input->post('login')=='login' ) {
				$this->form_validation->set_rules( 'lusername', 'Username', 'trim|required|max_length[50]|xss_clean' );
				$this->form_validation->set_rules( 'lpassword', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
	
				if( true == $this->form_validation->run() ) {
					 
					$strUsername = $this->input->post('lusername');
					$strPassword = $this->input->post('lpassword');
					$strLoginType = $this->input->post('login_type');
					$is_valid = Users_model::fetchAdminUser( $strUsername, $strPassword, $strLoginType );
	
					 
					if($is_valid) {
						$arrSessionData = array(
								'user_name'			=> $is_valid->username,
								'email_address'		=> $is_valid->email_address,
								'is_logged_in'		=> true
						);
						if( 'Individual' == $strLoginType ) {
							$arrSessionData['account_number']	= $is_valid->account_number;
							$arrSessionData['user_id']			= $is_valid->id;
							$arrSessionData['name']				= $is_valid->first_name;
						} else {
							$arrSessionData['company_id']		= $is_valid->id;
							$arrSessionData['user_id']			= $is_valid->created_by;
							$arrSessionData['name']				= $is_valid->company_name;
						}
						$arrSessionData['login_type']			= $strLoginType;
						$this->session->set_userdata($arrSessionData);
						 
						/*if(	'yes' == $this->input->post('remember_me'))
						 {
						$cookieData = $is_valid->username.','.$is_valid->password.','.$strLoginType;
						$this->input->set_cookie('gnisscookie', $cookieData, 1296000);
						}	else {
						$this->input->set_cookie('gnisscookie', '');
						}*/
	
						redirect( base_url() );
					}	else {
						$this->session->set_userdata( array( 'errmsg' => "Invalid user details." ) );
						redirect('users/action/signin');
					}
				}
			}
			$arrmixUserInfo	=	$this->registrationInfo();
			$this->load->view( 'register',$arrmixUserInfo );
		}
	}
	public function  forgotPassword() {
	          $arrmixUserInfo    = array();
	           
	          $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]|xss_clean');
	          
	          if( true == is_array( $this->input->post())  && true == $this->form_validation->run()) {
	          
	                    $strLoginAccountType    	= $this->input->post('login_account_type');
	                    $strUsername                 = $this->input->post('username');
	          
	                    $arrobjUsers                 = Users_model::fetchEmailAddressByUsername( $strUsername, $strLoginAccountType );
	                
	                    $intUserRecordCount      = count( $arrobjUsers );
	                          
	                          if( 0 < $intUserRecordCount ) {
	                             $this->showSecurityQuestion( $arrobjUsers->id,  $strLoginAccountType );
      
	                    }
	          } else { 
	                   $this->load->view( 'forgotpassword', $arrmixUserInfo );
	          }     
	}
	
	public function showSecurityQuestion( $intUserId, $strLoginAccountType ) {
                 
          $arrmixUserInfo    = array();
          $arrmixUserInfo['user_id'] = ( true == isset( $intUserId ) && false == empty( $intUserId ) ) ? $intUserId : NULL;   
          $arrmixUserInfo['login_account_type'] = ( true == isset( $strLoginAccountType ) && false == empty( $strLoginAccountType ) ) ? $strLoginAccountType : NULL;
          $arrmixUserInfo['security_question'] = Security_questions_model::fetchSecurityQuestionsByUserId($intUserId);
          $this->load->view( 'security_question', $arrmixUserInfo );
	}
	
	public function checkSecurityQuestion() {
		
				$arrmixUserInfo      = array();
				$this->form_validation->set_rules('radio_toggle', 'Verification method', 'trim|required|xss_clean');
				
				if( true == is_array( $this->input->post()) && 	true == $this->form_validation->run() ) {
							$strLoginAccountType		= $this->input->post('login_account_type');
							$intUserId								= $this->input->post('user_id');
							
							if( false == empty( $this->input->post('answer') )  ) {
										$this->form_validation->set_rules('question_id', 'Security Question', 'trim|required|xss_clean');
										$this->form_validation->set_rules('answer', 'Security Answer', 'trim|required|max_length[200]|xss_clean');
										 
										if( true == $this->form_validation->run()) { 
													$intQuestionId			= $this->input->post('question_id');
													$strAnswer				= strtolower( $this->input->post('answer') );
										
													 
													$intUserTypeId      = NULL;
													if('Individual' == $strLoginAccountType ) {
														$intUserTypeId      = 1;
													}
										
													if('Company' == $strLoginAccountType ) {
														$intUserTypeId      = 2;
													}
								
													$objSecurityAnswer               = Security_questions_model::fetchSecurityQuestionByUserIdByUserTypeIdByQuestionIdByAnswer( $intUserId, $intUserTypeId, $intQuestionId, $strAnswer );
										
												$intRecordCount      = count( $objSecurityAnswer );
											
										
													if( 0 < $intRecordCount ) { 
																$objUser                 = Users_model::fetchUserById( $intUserId );
										
																$StrRandomTempPass = md5(time());
																$boolIsUpdate = false;
																 
																if('Individual' == $strLoginAccountType ) {
																			if( true == Users_model::updateResetPassword(  $objUser->email_address,  $StrRandomTempPass ) ) {
																						$boolIsUpdate = true;
																			}
																	 
																}
																if('Company' == $strLoginAccountType ) {
																			if( true ==Company_model::updateResetPassword(  $objUser->email_address,  $StrRandomTempPass ) ) {
																						$boolIsUpdate = true;
																			}
																}
																
																redirect("users/action/resetPass/" . $StrRandomTempPass . "/". $strLoginAccountType );
																
											 
													} else {
																$arrmixUserInfo['user_id'] = ( true == isset( $intUserId ) && false == empty( $intUserId ) ) ? $intUserId : NULL;
																$arrmixUserInfo['login_account_type'] = ( true == isset( $strLoginAccountType ) && false == empty( $strLoginAccountType ) ) ? $strLoginAccountType : NULL;
																$arrmixUserInfo['security_question'] = Security_questions_model::fetchSecurityQuestionsByUserId($intUserId);
																$arrmixUserInfo['errmsg']       = 'You have entered incorrect answer.';
													}
									} else {
												$arrmixUserInfo['user_id'] = ( true == isset( $intUserId ) && false == empty( $intUserId ) ) ? $intUserId : NULL;
												$arrmixUserInfo['login_account_type'] = ( true == isset( $strLoginAccountType ) && false == empty( $strLoginAccountType ) ) ? $strLoginAccountType : NULL;
												$arrmixUserInfo['security_question'] = Security_questions_model::fetchSecurityQuestionsByUserId($intUserId);
											
									}
							} else {
										$objUser				= Users_model::fetchUserById( $intUserId );
										
										$StrRandomTempPass = md5(time());
										$boolIsUpdate = false;
										 
										if('Individual' == $strLoginAccountType ) {
													if( true == Users_model::updateResetPassword(  $objUser->email_address,  $StrRandomTempPass ) ) {
																$boolIsUpdate = true;
													}
											 
										}
										if('Company' == $strLoginAccountType ) {
													if( true ==Company_model::updateResetPassword(  $objUser->email_address,  $StrRandomTempPass ) ) {
																$boolIsUpdate = true;
													}
										}
								 
								if( true == $boolIsUpdate ) {
									
											$this->load->library('email');
											$this->load->library('parser');
											$this->email->from('no_reply@rectusenergy.com', 'Rectus Energy pvt ltd');
											$this->email->to( $objUser->email_address );
											$this->email->mailtype='html';
											$strUrl	= base_url()."users/action/resetPass/" . $StrRandomTempPass . "/". $strLoginAccountType;
										
											$strEmailMsg= 'Please click on the following link for reset your password.</br></br>'.
													'<br><br>'.'<a href='.$strUrl.'>Reset Password ';
										
											$arrstrEmailData = array(
																		'header' 	=> 'Dear Recipient,',
																		'content'	=> $strEmailMsg
											);
									 
											$strEmailContent = $this->parser->parse('email_template',$arrstrEmailData,TRUE);
											 
											$this->email->subject('Reset Password');
										
											$this->email->message( $strEmailContent );
											 
											if( true == $this->email->send() ) {
														$this->session->set_userdata( array( 'successmsg' => "Reset password link sent to  your email address. Please check the email to reset password." ) );
														redirect('users/action/signin');
											} else {
														$this->session->set_userdata( array( 'errmsg' => "Could not send mail to user." ) );
														redirect('users/action/signin');
											}
								} else {
											$arrmixUserInfo['user_id'] = ( true == isset( $intUserId ) && false == empty( $intUserId ) ) ? $intUserId : NULL;
											$arrmixUserInfo['login_account_type'] = ( true == isset( $strLoginAccountType ) && false == empty( $strLoginAccountType ) ) ? $strLoginAccountType : NULL;
											$arrmixUserInfo['security_question'] = Security_questions_model::fetchSecurityQuestions();
											$arrmixUserInfo['errmsg']       = 'Database error:- Unable to update record';
								}
								
							}
							if( true == $this->session->userdata( 'successmsg' ) ) {
										$arrmixUserInfo['msg'] = $this->session->userdata( 'successmsg' );
										$this->session->unset_userdata( 'successmsg' );
							}
							
							if( true == $this->session->userdata( 'errmsg' ) ) {
										$arrmixUserInfo['errmsg'] = $this->session->userdata( 'errmsg' );
										$this->session->unset_userdata( 'errmsg' );
							}
							
						
				}
				$arrmixUserInfo['user_id'] = ( true == isset( $intUserId ) && false == empty( $intUserId ) ) ? $intUserId : NULL;
				$arrmixUserInfo['login_account_type'] = ( true == isset( $strLoginAccountType ) && false == empty( $strLoginAccountType ) ) ? $strLoginAccountType : NULL;
				
				$this->load->view( 'security_question', $arrmixUserInfo );
	}
	
	public function resetPassword() {
	
	          $strLoginAccountType 		= $this->uri->segment(5);
	          $strVerfictionString				= $this->uri->segment(4);
	        
	          $this->form_validation->set_rules( 'password', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
	          $this->form_validation->set_rules( 'passconf', 'Confirm password', 'required|matches[password]|min_length[6]|max_length[20]|xss_clean' );
	
	          if( true == is_array( $this->input->post()) ){
	
	               $strResult = Users_model::checkForVerifiationLink( $strVerfictionString, $strLoginAccountType );
	
	                    if( 0  < $strResult ) 	{
	                              if(true == $this->form_validation->run()) { 
	                              			if( 'Individual' == $strLoginAccountType ) {
	                              				if( Users_model::updateResetPasswordToken( $strVerfictionString, $strLoginAccountType ) ){
	                              					$this->session->set_userdata( array( 'successmsg' => "password reset successfully." ) );
	                              					redirect('users/action/signin');
	                              				}
	                              }
		                              		if( 'Company' == $strLoginAccountType ) {
		                           				   	if( Company_model::updateResetPasswordToken( $strVerfictionString, $strLoginAccountType ) ){
		                              		$this->session->set_userdata( array( 'successmsg' => "password reset successfully." ) );
		                              		redirect('users/action/signin');
		                              	}
		                              }
	                              }	
	                    } else {
	                              $this->session->set_userdata( array( 'errmsg' => "Your reset password link is expired" ) );
	                    }
	          }
	          if( true == $this->session->userdata( 'successmsg' ) ) {
	                    $arrmixUserInfo['msg'] = $this->session->userdata( 'successmsg' );
	                    $this->session->unset_userdata( 'successmsg' );
	          }
	           
	          if( true == $this->session->userdata( 'errmsg' ) ) {
	                    $arrmixUserInfo['msg'] = $this->session->userdata( 'errmsg' );
	                    $this->session->unset_userdata( 'errmsg' );
	          }
	          $arrmixUserInfo['ver_code']=$strVerfictionString . '/' .$strLoginAccountType ;
	          $this->load->view('resetpassword',$arrmixUserInfo);
	}
	
	public function validate_zipcode ( $strFieldValue ) {
	
		if( false == empty( $strFieldValue ) ) {
			if( '100' == $this->input->post('country_id') ) {
				if( '6' == strlen( $strFieldValue ) ) {
					if( true == preg_match( '/^[1-9]{1}[0-9]{5}+$/', $strFieldValue ) ) {
						return true;
					} else {
						$this->form_validation->set_message( 'validate_zipcode', '%s should not start with zero' );
						return false;
					}
				} else {
					$this->form_validation->set_message( 'validate_zipcode', '%s should contain 6 digits' );
					return false;
				}
			} else {
				if( true == preg_match( '/^[\-+]?[0-9]*\.?[0-9]+$/', $strFieldValue ) ) {
					return true;
				}
			}
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
