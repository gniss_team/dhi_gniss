<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 This controller is developed for  Display all APPS of logged user
for the user location.Here we used google map API
for showing location.
Last Updated : 23 Oct 2015
Updated By : Bhatu Jadhav .
*/

class CManageAppsController extends CI_Controller {
	echo "ddddd";
	public function __construct() {
			parent::__construct();
			$this->load->model('devices_model');
	}
	
	public function action( $strAction = '' ) { 
          	$tet=1;       
		switch( $strAction ) {
		    
                  
				case 'view':
						$this->viewApps( $strAction );
						break;
                              						
				default:
						show_404();
						break;
                    			
		}
                  
	}
         
	public function viewApps() {
		    // This function written for , display all functions apps on dashboard page .
			$intUserId					= $this->session->userdata('user_id');
			$arrmixAppDetails['devices']	= Devices_model::fetchDeviceTypeByUserId( $intUserId );
			$isVtsRegister =  Tracker_model::getVtsTotalRow( $intUserId ); 
			$arrmixAppDetails['isVtsRegister'] = $isVtsRegister;
			$this->load->view( 'apps', $arrmixAppDetails );
	}       
}
