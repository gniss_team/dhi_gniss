<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author rectus
 *
 */

class CManageUsersController extends CI_Controller {

	public function action( $strAction = '' ) { 

			switch( $strAction ) {
			
					case	'register': 
								$this->createUser();
					    		break;
					
					case	'addUser':
								$this->addUser();
								break;
		
					case	'login': 
								$this->loginUser();
								break;
					
					case 'changepassword':
								$this->changePassword();
								break;
								
					case 'changepass':
								$this->changePasswordprofile();
								break;
					
					case	'forgotPass':
					        	$this->forgotPassword();
								break;
					    			
					case	'resetPass':
								$this->resetPassword();
								break;
						
					case	'verify':
								$this->verifyEmail();
								break;
								
					case	'welcome':
								$this->welcome();
								break;
								
					default:
								show_404();
	    				
	    	}
	}

	public function createUser() {
		
				$arrmixUserInfo		= array();
				// commented code for captcha.
				
// 			$this->load->library('Captcha');
			
// 			$arrmixUserInfo['captcha'] = array();
// 			$arrmixUserInfo['captcha'] = $this->captcha->main();
		
// 			$this->session->set_userdata( 'captcha_info', $arrmixUserInfo['captcha'] );
			$this->load->view( 'register', $arrmixUserInfo );
			
	}
	
	public function addUser() {
			if( false == is_array( $this->input->post() ) ) {
					redirect( base_url() . 'users/action/register' );
			}
			
			$this->form_validation->set_rules( 'first_name', 'First name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
			$this->form_validation->set_rules( 'last_name', 'Last name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
			$this->form_validation->set_rules( 'email_address', 'Email address', 'trim|required|valid_email|is_unique[users.email_address]|max_length[50]|xss_clean' );
			$this->form_validation->set_rules( 'username', 'Username', 'trim|required|min_length[3]|max_length[15]|validate_username|non_numeric_name|non_character_name|consicutive_periods_underscore|is_unique[users.username]|xss_clean' );
			$this->form_validation->set_rules( 'password', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
			$this->form_validation->set_rules( 'conf_password', 'Confirm password', 'trim|required|min_length[6]|max_length[20]|matches[password]|xss_clean' );
			$this->form_validation->set_rules( 'mobile_number', 'Mobile number', 'trim|required|numeric|exact_length[10]|mobile_zero|xss_clean' );
			
			// code is commented for captcha. 
// 			$this->form_validation->set_rules( 'captcha', 'Captcha', 'required|captcha|xss_clean' );
	
			if( true == $this->form_validation->run() ) {
					
					$strToken = md5( time() );
					$strAccountNumber = Users_model::checkforaccountNumber();
					
					if( true == Users_model::insert( $strToken , $strAccountNumber) ) {
							
							$arrmixUserInfo['msg'] = "User added successfully.";
							
						 	$strMessage		= 'You have chosen:<br><br>'.
																'Gniss Id :' . $strAccountNumber. '<br><br>'.
																'Password :' . $this->input->post( 'password' ). '<br><br>'.
																'Email Id :' . $this->input->post( 'email_address' ). '<br><br>'.
																'A few more steps will connect you to the world of GNISS. Click <a href=' . base_url().'users/action/verify/' . $strToken . '>here </a>to complete your registration and get started.'. '<br><br>'.
																'Should you experience any trouble setting up, please contact us at info@rectusenergy.com' .'<br><br>'.
																'If you did not create this ID on GNISS and think that someone else has used your email ID, please click <a href=' . base_url().'>here </a>to remove your email ID from this account' ;
							
						 	$strHeaderMsg	= '<div style="font-size:1.5em;">Welcome to GNISS!</div><br><br/>'.
								 										'Dear Recipient,<br><br>';
						 	$arrstrEmailFormat = array(
				 					'header' 	=> $strHeaderMsg,
				 					'content'	=> $strMessage
						 	);
						 	
							if( true == $this->sendMail( 'no_reply@rectusenergy.com', $this->input->post('email_address'),  'Thank you for Your Registration', $arrstrEmailFormat ) ) {
									$this->session->set_userdata( array( 'successmsg' => "You have registered successfully, Please check your email." ) );
							} else {
									$this->session->set_userdata( array( 'errmsg' => "Could not send mail to user." ) );
							}
							redirect('users/action/login');
					} else{
							$arrmixUserInfo['errmsg']		= "User cannot be added.";
					}
					
// 					$this->load->library('Captcha');
			
// 					$arrmixUserInfo['captcha'] = array();
// 					$arrmixUserInfo['captcha'] = $this->captcha->main();
				
// 					$this->session->set_userdata( 'captcha_info', $arrmixUserInfo['captcha'] );
					$this->load->view( 'register', $arrmixUserInfo );
			} else {
						$this->createUser();
			}
	}
	
	// function to login.
	public function loginUser() {
			
			$strCookieData 		= $this->input->cookie('gnisscookie', TRUE );
			
			$arrmixUserInfo 			= array();
			$arrmixUserInfo['errmsg']	=	 '';
			
			$objUser = NULL;
			
			if( false == empty( $strCookieData ) && 'cookiein' == $this->input->post('cookie') ) {
				
					$arrCookieData		= explode( ',',$strCookieData );
					$strUserName			= $arrCookieData[0];
					$objUser						= Users_model::fetchUserByUsername( $strUserName );
					
					if( false == is_object( $objUser ) ) {
							$this->session->sess_destroy();
							$this->input->set_cookie('gnisscookie', '');
							redirect( base_url() );
					}
					
			} 
			
			if( 'login' == $this->input->post('login') ) {
				
					$this->form_validation->set_rules( 'username', 'Username', 'trim|required|min_length[3]|max_length[50]|xss_clean' );
					$this->form_validation->set_rules( 'password', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
					
					if( true == $this->form_validation->run() ) {
						
							$strUserName 	= $this->input->post('username');
							$strPassword 	= $this->input->post('password');
							
							$objUser = Users_model::fetchUserByUsernameByPasswordByUserType( $strUserName, $strPassword, user_type_model::USER );
						
							if( true == is_object( $objUser )  && '0' == $objUser->status ) {
									$arrmixUserInfo['errmsg']	=	 "Your account is not verified."; 
									goto gotoLoginView;
							}
							if( false == is_object( $objUser ) ) {
									$arrmixUserInfo['errmsg']	=	 "Invalid login credentials.";
									goto gotoLoginView;
							} 
						
					}
					
			}
				
			if( true == is_object( $objUser ) ) {
				
					$arrmixUserSessionData = array(
							'user_name'			=> $objUser->username,
							'email_address'		=> $objUser->email_address,
							'is_logged_in'		=> true
					);
				
					$arrmixUserSessionData['account_number']	= $objUser->account_number;
					$arrmixUserSessionData['user_id']							= $objUser->id;
					$arrmixUserSessionData['name']							= $objUser->first_name .' '. $objUser->last_name;
					$arrmixUserSessionData['status']							= $objUser->status;
									
					$this->session->set_userdata( $arrmixUserSessionData );
						
					if(	'yes' == $this->input->post( 'remember_me' ) )	{
							$cookieData = $objUser->username;
							$this->input->set_cookie( 'gnisscookie', $cookieData, 1296000 );
					}
					
					if( 2 == $arrmixUserSessionData['status'] ) {
							$arrPost	= array();
							$arrPost['status']	= 2;
							if( true == Users_model::update( $arrPost ) ) {
									redirect(  base_url() . 'users/action/welcome' );
							} else {
								redirect( base_url() );
							}
					} else {
							redirect( base_url() );
					}
					///$this->load->view( 'welcome_gniss', $arrmixUserInfo );
					//redirect( base_url() );
			} 
			
			gotoLoginView:
					if( true == $this->session->userdata( 'successmsg' ) ){
						$arrmixUserInfo['msg'] = $this->session->userdata( 'successmsg' );
						$this->session->unset_userdata( 'successmsg' );
					}
					
					if( true == $this->session->userdata( 'errmsg' ) ){
						$arrmixUserInfo['errmsg'] = $this->session->userdata( 'errmsg' );
						$this->session->unset_userdata( 'errmsg' );
					}
					$this->load->view( 'login', $arrmixUserInfo );
	}

	public function verifyEmail() {
		
		 	$strToken 	= $this->uri->segment(4);
			$objUser		= Users_model::fetchUserStatusByResetPasswordCode( $strToken );
	
			if( true == is_object( $objUser )  && '0' == $objUser->status ) {
					if( true  == Users_model::updateStatus( $strToken ) ) 	{
						
							$arrmixUserInfo['msg']	= "Your account is verified.";
							$this->load->view( 'login', $arrmixUserInfo );
					} 
			} else {
	       	 		$arrmixUserInfo['msg']	= "Your account is already active.";
	           		$this->load->view( 'login', $arrmixUserInfo );
			}
		
	}
	
	public function welcome() {
		
			$this->load->view( 'welcome_gniss' );
			
	}
	
	public function checkOldPassword( $strPassword ) {
		$objPassword	= Users_model::checkPasswordAlreadyExists( $strPassword, 0 );
		 
		if ( false == is_object( $objPassword ) ) {
	
			$this->form_validation->set_message( 'checkOldPassword', ' %s  not found in system' );
			return FALSE;
			 
		} else {
			return TRUE;
		}
	}
	
	public function changePassword() {
		 
		$arrmixUserInfo = array();
		$arrmixUserInfo['msg']	= $this->session->userdata( 'msg' );
	
		$this->session->unset_userdata( 'msg' );
		$this->form_validation->set_rules( 'old_password', 'Old password', 'trim|required|callback_checkOldPassword|xss_clean' );
		$this->form_validation->set_rules( 'new_password', 'New password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
		$this->form_validation->set_rules( 'confirm_password', 'Confirm password', 'trim|required|xss_clean|matches[new_password]' );
		$arrmixFormInputData = $this->input->post();
		if( true == is_array( $arrmixFormInputData ) && true == $this->form_validation->run() ) {
	
				if( true == Users_model::changePassword( user_type_model::USER ) ) {
					 
						$this->session->set_userdata( array( 'msg' =>"Password changed sucessfully" ) );
						redirect(  base_url().'users/action/changepassword' );
				}
		}
		$this->load->view( 'changepassword' , $arrmixUserInfo);
	}
	
	public function changePasswordprofile() {
			
		$arrmixUserInfo = array();
		$arrmixUserInfo['msg']	= $this->session->userdata( 'msg' );
	
		$this->session->unset_userdata( 'msg' );
		$this->form_validation->set_rules( 'old_password', 'Old password', 'trim|required|callback_checkOldPassword|xss_clean' );
		$this->form_validation->set_rules( 'new_password', 'New password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
		$this->form_validation->set_rules( 'confirm_password', 'Confirm password', 'trim|required|xss_clean|matches[new_password]' );
		$arrmixFormInputData = $this->input->post();
	
		if( true == is_array( $arrmixFormInputData ) && true == $this->form_validation->run() ) {
	
			if( true == Users_model::changePassword( user_type_model::USER ) ) {
					
				//$this->session->set_userdata( array( 'msg' =>"Password changed sucessfully" ) );
				$data = array(
						'error' => 'suc',
						'msg' => form_error('Password changed sucessfully.')
				);
				echo json_encode($data);
			}
		}else {
	
			$data = array(
					'error' => 'true',
					'old_pass' => form_error('old_password'),
					'new_pass' => form_error('new_password'),
					'confirm_pass' => form_error('confirm_password')
			);
			echo json_encode($data);

		}
		//	$this->load->view( 'changepassword' , $arrmixUserInfo);
	}
	
	public function forgotPassword() {
		
			$arrmixUserInfo    = array();
			$arrmixUserInfo['msg']			= $this->session->userdata('successmsg');
			$this->session->unset_userdata('successmsg');
				
			$arrmixUserInfo['errmsg']			= $this->session->userdata('errmsg');
			$this->session->unset_userdata('errmsg');
			
			if( true == is_array( $this->input->post() ) ) {
	   			 $this->form_validation->set_rules('username', 'Username / Email Address / GNISS ID', 'trim|required|max_length[50]|xss_clean');
					 
				    if(  true == $this->form_validation->run()) {
				    
				    	$objUser					= Users_model::fetchEmailAddressByForgotPasswordFieldByUserType( $this->input->post('username'), user_type_model::USER );
				          
					        if( true  == is_object( $objUser ) ) {
					        			
									$strRandomTempPass = md5( time() );
														
									if( true == Users_model::updateResetPasswordCode( $objUser->email_address,  $strRandomTempPass ) ) {
										
											$strUrl			= base_url()."users/action/resetPass/" . $strRandomTempPass;
										 	$strMessage		= 'Please click on the following link for reset your password.'.
																							'<br><br>Username : '.  $objUser->username .
																							'<br><br>Account Number : '.  $objUser->account_number .
																							'<br><br>'.'<a href='.$strUrl.'>Reset Password ';
											
											$arrstrEmailFormat = array(
									 				'header' 	=> 'Dear Recipient,',
									 				'content'	=> $strMessage
											);
											
											if( true == $this->sendMail( 'no_reply@rectusenergy.com',  $objUser->email_address,  'Reset Password', $arrstrEmailFormat ) ) {
													$this->session->set_userdata( array( 'successmsg' => "Reset password link sent to  your email address. <br/> Please check the email to reset password." ) );
													redirect('users/action/login');
											} else {
													$this->session->set_userdata( array( 'errmsg' => "Could not send mail to user." ) );
													redirect('users/action/login');
											}
											
									} else {
											$arrmixUserInfo['errmsg']	= 'Unable to update reset password token.';
											$this->load->view( 'forgotpassword', $arrmixUserInfo );
									}
				
			      		} else {
				        			$arrmixUserInfo['errmsg']	= '<span class="error">Usename / Email Address / GNISS ID not found.</span>';
		        			}
						} 
			} 
					
			$this->load->view( 'forgotpassword', $arrmixUserInfo );

	}

	public function resetPassword() {

			$strToken	= $this->uri->segment(4);
			$objUser 	= Users_model::fetchTokenExpirationTimeByResetPasswordCode( $strToken );

	    	if( true == is_array( $this->input->post() ) ) {
	    		
		    		$this->form_validation->set_rules( 'password', 'New Password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
		    		$this->form_validation->set_rules( 'passconf', 'Confirm password', 'trim|required|matches[password]|min_length[6]|max_length[20]|xss_clean' );
	    		
		    		if( true == $this->form_validation->run() ) {
		   		 		$objUser = Users_model::fetchTokenExpirationTimeByResetPasswordCode( $strToken );
		
		   					 if( true == is_object( $objUser ) ) {
	
					                if( true == Users_model::updatePassword( $strToken  ) ) {
					                   	$this->session->set_userdata( array( 'successmsg' => "Password has been reset successfully." ) );
					                    redirect('users/action/login');
					             } else {
							             	$this->session->set_userdata( array( 'successmsg' => "Unable to update password" ) );
							             	redirect('users/action/login');
					             }
					       } else {
					       		$this->session->set_userdata( array( 'errmsg' => "Token not found." ) );
					       }	
	       	} 
		} else {
				if( true == is_object( $objUser ) ) {
						if( time() > $objUser->token_expiration_time ) {
							$this->session->set_userdata( array( 'errmsg' => "Your reset password link is expired" ) );
							redirect('users/action/forgotPass');
						}
				} else {
						$this->session->set_userdata( array( 'errmsg' => "Your reset password link is expired" ) );
						redirect('users/action/forgotPass');
				}
		}
			     
		$arrmixUserInfo['ver_code']	=$strToken;
		$this->load->view( 'resetpassword', $arrmixUserInfo );
	}
	
	// Need to move in common functions.
	public function sendMail( $strEmailFrom, $strEmailTo, $strSubject, $arrstrEmailFormat ) {

			$this->load->library( 'email' );
			$this->load->library( 'parser' );
			$this->email->mailtype	= 'html';
			
			$this->email->from( $strEmailFrom, 'Rectus Energy pvt ltd' );
			$this->email->to( $strEmailTo );
			$this->email->subject( $strSubject );
			
			$strEmailContent = $this->parser->parse( 'email_template',$arrstrEmailFormat, TRUE );
			$this->email->message( $strEmailContent );
			
			return $this->email->send();
	}

}