<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageEnergyMeterController extends CI_Controller {
	 
		const C_LINE	= 'Line';
		const C_BAR		= 'Bar';
		const C_PIE		= 'Pie';
		
      public function __construct() {
			parent::__construct();

     }
	
      public function action( $strAction = '' ) { 
          	       
                    	switch( $strAction ) {
                    			case 'showgraph':
                               	case 'displayGraphFilters':
                           			$this->displayGraphFilters( $strAction );
                              					break;
                              						
                              	case 'viewGraph':
                        		      $this->viewGraph( $strAction );
                              		  break;
                              	case 'ajaxgetEnergyStats':
                              		  $this->ajaxgetEnergyStats(  );
                              		  break;
                              	case 'viewVoltageVsCurrent':
                              		  $this->viewVoltageVsCurrent();
                              		  break;
                              		 case 'viewLiveGraph':
                              		 $this->viewLiveGraph( $strAction );
                              		break;
                              		
                               case 'viewWattHourGraph':
                              		 $this->viewWattHourGraph( $strAction );
                              		break;
                              					
                              					
                              	default:
                              	show_404();
                    			
                  }
                  
         }
     
     	public function displayGraphFilters() {      	
      		$this->load->view( 'energy_meter_graph' );
    	}
      
         public function viewGraph() {
      	
	      	if( false == is_array( $this->input->post() ) ) return;
	     
	      $this->form_validation->set_rules( 'from_date', 'From Date', 'trim|required|xss_clean' );
      	  $this->form_validation->set_rules( 'to_date', 'To Date', 'trim|required|xss_clean|callback_checkValidDate' );
      	  $this->form_validation->set_rules( 'unit_type', 'Parameter Type', 'required|xss_clean'  );
      	  $this->form_validation->set_rules( 'max', 'Min Value', 'numeric|callback_checkmin'  );
      	  $this->form_validation->set_rules( 'min', 'max Value', 'numeric'  );
	      	
      		if( false == $this->form_validation->run() ) {
      				echo validation_errors(); exit;
      	}
      	
     		$arrstrUnitType 			= $this->input->post( 'unit_type' );
      	$intCntUnitType			= count( $arrstrUnitType );
      	$strFromDateTime 	= $this->input->post( 'from_date' );
      	$strToDateTime 			= $this->input->post( 'to_date' );
      	   
      	$arrmixEnergymeterData		= array();
      	$strField	= NULL;
      	$arrmixEnergymeterData['type']											=	$arrstrUnitType;
      	$arrmixEnergymeterData['cnt_graph_parameter']		=	$intCntUnitType;

      	$strUnitType		= ( 1 < $intCntUnitType ) ? implode( ', ',  $arrstrUnitType ) :  $arrstrUnitType[0];
		
      		if( self::C_LINE != ucfirst( $this->input->post( 'graphtype' ) ) ) {
      		
      			$strFromDate	= date( 'Y-m-d', strtotime( $strFromDateTime) );
      			$strToDate			= date( 'Y-m-d', strtotime( $strToDateTime) );
      	
      			$strUnitType				= 'round( AVG(' . implode( '), 2)  # round( AVG(',  $arrstrUnitType ) . '), 2)';
      			$arrstrUnitTypes		= explode( '#', $strUnitType );
      			$strUnitTypes			=	'';
      				foreach( $arrstrUnitTypes as $intKey => $strUnit ) {
      					$strUnitTypes .= $strUnit . ' as ' . $arrstrUnitType[$intKey] . ',';
      			}
      			$strUnitType	= rtrim( $strUnitTypes, ',' );
      			
      				if( date( 'm', strtotime( $strFromDateTime ) ) == date( 'm', strtotime( $strToDateTime ) ) ) {
      							$strField	=	'date';
      						
	      					if( strtotime( date( 'Y-m-d', strtotime( $strFromDateTime ) ) ) == strtotime(  date( 'Y-m-d', strtotime( $strToDateTime ) ) ) ) {
	      						$strField	=	'hour';
	      				}	
      						
      			} else {
      					$strField	=	'month';
      			}
      	
      	}
      	
      	$arrmixEnergymeterData['energy_meter_data']		= energy_meter_model::fetchEnergyMeterDataByUnitTypeByFromDateByToDateByField( $strUnitType, $strFromDateTime , $strToDateTime,  $strField );
      
     	 	if( false == empty( $arrmixEnergymeterData['energy_meter_data'] ) ) { 
	     	 		if( false == empty( $this->input->post( 'graphtype' ) ) ) {
     	 					$this->load->view( 'view_' . $this->input->post( 'graphtype' ) . '_graph', $arrmixEnergymeterData );
	     	 		}	
	     	 		
      	} else { 
      				echo "<span style='color:red';>No records Found </span>";exit;
      	}	

	}

      function checkValidDate( $curdate ) {
 
      	$todaydate	= date('Y-m-d H:i');
      	$from_date = $this->input->post('from_date');
      	$todate = $this->input->post('to_date');
      	
      	 
      	if ( !date('Y-m-d H:i', strtotime( $todate ) ) == $todate ) {
      		$this->form_validation->set_message( 'checkValidDate', 'The %s To date must be in format "yyyy-mm-dd hh:mm"');
      		return FALSE;
      	}
      	
      	if ( date('Y-m-d H:i', strtotime( $from_date ) ) > $todaydate ) {
      		$this->form_validation->set_message( 'checkValidDate', 'From date should not be greater than todays date.');
      		return FALSE;
      	}
      
      	if ( date('Y-m-d H:i', strtotime( $todate ) ) > $todaydate ) {
      		$this->form_validation->set_message( 'checkValidDate', 'To date should not be greater than todays date.');
      		return FALSE;
      	}      	
      	
      	if ( strtotime( $todate ) < strtotime( $from_date ) ) {
      			$this->form_validation->set_message( 'checkValidDate', 'To date time should be greater than equal to From date time');
      		return FALSE;
      	
      	}
      
      }
      function checkmin()
      {
      
      	$min = $this->input->post('min');
      	$max = $this->input->post('max');
      	if ($min > $max ) {
      		$this->form_validation->set_message( 'checkmin', 'Min value should not be greater than Max value');
      		return FALSE;
      	}
      }
      
      public function ajaxgetEnergyStats() {
      	$fieldsarr = array('current','voltage');
      	$getStatData = energy_meter_model::getlatestStats( $fieldsarr );
      	//$currentvoltage  = energy_meter_model::getlatestStats('voltage');
      	$stats = array("current"=>$getStatData->current/10,"voltage"=>$getStatData->voltage/10);
       		 echo json_encode($stats);
      	 
      }
      public function viewLiveGraph() {
      //	$strUnitType='voltage';
      	if($this->uri->segment(4)!="")
      	{
      		$strUnitType=$this->uri->segment(4);
      	}else {
      		$strUnitType='voltage';
      	}
      	$arrmixEnergymeterData['energy_meter_data']		= energy_meter_model::getLastHourEnergyMeterData( $strUnitType );
      	$arrmixEnergymeterData['unit_type'] 			= $strUnitType;
      
      	if( false == empty( $arrmixEnergymeterData['energy_meter_data'] ) ) {
      		$arrmixEnergymeterData['cnt_graph_parameter']		=	1;
      		$arrmixEnergymeterData['type']											=	'line';
      		//print_r($arrmixEnergymeterData);
      		$this->load->view( 'ajax_linegraph_data', $arrmixEnergymeterData );
      	}else {
      		echo "<span style='color:red;font-size:13px;text-align:center;';>No records Found </span>";exit;
      	}
      }
      
      public function viewVoltageVsCurrent()
      {
      	$arrmixEnergymeterData['vcdata']		= energy_meter_model::getVoltageVesesCurrentData();
     // 	print_r($arrmixEnergymeterData);
     	if(count($arrmixEnergymeterData['vcdata']) > 0) {
      	$this->load->view( 'lateststats' , $arrmixEnergymeterData );
     	}else {
     		echo "<span style='color:red;font-size:13px;text-align:center;';>No records Found </span>";exit;
     	}
      	
      }
      public function viewWattHourGraph()
      {
      	$arrmixEnergymeterData['energy_meter_data']		= energy_meter_model::getHourlyWatt( );
      	// 	print_r($arrmixEnergymeterData);
      	if(count($arrmixEnergymeterData['energy_meter_data']) > 0) {
      		$this->load->view( 'watt_hour_graph' , $arrmixEnergymeterData );
      	}else {
      		echo "<span style='color:red;font-size:13px;text-align:center;';>No records Found </span>";exit;
      	}
      	 
      }
      
     
}