<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageFooterMenuController extends CI_Controller {
	 
          	public function __construct() {
          		parent::__construct();
         }
	
          	public function action( $strAction = '' ) { 
          	          
                    	switch( $strAction ) {
                               
                    	         case 'about_us':
                              	 case 'contact_us':
                                 case 'disclaimer':
          	                   	 case 'privacy_policy':     
          	                   	 	case 'terms-conditions':
                              	$this->viewPage( $strAction );
                              	         	break;
                              
                              	default:
                              	show_404();
                    			
                  }
                  
          	}
          	
	    	public function viewPage( $strAction ) { 
         	    $this->load->view( $strAction );
          } 
}