<?php 
date_default_timezone_set('Asia/Kolkata');
ini_set('memory_limit', '256M');
/*
  This controller is developed for  Live Tracking 
  for the user location.Here we used google map API
  for showing location. 
  Last Updated : 23 Oct 2015
  Updated By : Bhatu Jadhav .
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Starting Live Controller Class
class CManageLiveTrackingController extends CI_Controller { 
	 
	public function __construct() {
        parent::__construct();       
    }
    
    // Index function , 
    public function index() {
    	 
      //Its show all live location on google map Api 
    	$gettotTracker =  Tracker_model::getTrackercount();
   	$arrmixLiveTrackingInfo['vtsdevices'] = Tracker_model::getAllVTSDevices();
		$arrmixLiveTrackingInfo['phone_number']	= NULL;
		$arrmixLiveTrackingInfo['tot_tracer']	= $gettotTracker;
		$this->load->view( 'livetracking', $arrmixLiveTrackingInfo );
    	//$gettotTracker =  Tracker_model::getTrackercount();
    	//$data['tot_tracer']	= $gettotTracker;
    	//$this->load->library('googlemaps');
    	//$config['center'] = '18.49908, 73.934412';
    //	$config['zoom'] = 'auto';
    //	$this->googlemaps->initialize($config);
     //   $arrmixLiveLocationData = Tracker_model::getAllTrackers();
 
			 
	}
	
	public function demo2()
	{
		$html_side="";
		//Its show all live location on google map Api
		//	$gettotTracker =  Tracker_model::getTrackercount();
		//	$arrmixLiveTrackingInfo['vtsdevices'] = Tracker_model::getAllVTSDevices();
		//	$arrmixLiveTrackingInfo['phone_number']	= NULL;
		//	$arrmixLiveTrackingInfo['tot_tracer']	= $gettotTracker;
		//	$this->load->view( 'livetracking', $arrmixLiveTrackingInfo );
		$gettotTracker =  Tracker_model::getTrackercount();
		$data['tot_tracer']	= $gettotTracker;
		$this->load->library('googlemaps');
		$config['center'] = '18.49908, 73.934412';
		$config['zoom'] = 'auto';
		$this->googlemaps->initialize($config);
		$arrmixLiveLocationData = Tracker_model::getAllTrackers();
		
		//$this->session->set_userdata('liveloc', count($arrmixLiveLocationData	));
		if( 0 < count($arrmixLiveLocationData) ) {
			//print_r($arrmixLiveLocationData);
			foreach($arrmixLiveLocationData as $loc)
			{
				$marker = array();
				$marker['position'] = $loc->latitude.",".$loc->longitude;
				$marker['infowindow_content'] = '1 - Hello World!';
				$marker['icon'] = base_url()."images/map_icons/".$loc->marker_icon;
				$this->googlemaps->add_marker($marker);
				$html_side .='<li>
				 
				<div class="nav-map"><a href="javascript:void(0);" >'.$loc->name.'
				</a></div><div class="nav-map"><a href="'.base_url().'livetracking/routewiselocation/'.$loc->phoneno.'">
				<img width="22" src="'.base_url().'images/map_icons/route-icon.png">Route Map</a></div>
				 
				 
				 
				<div class="nav-map"><a href="'.base_url().'livetracking/livelocation/'.$loc->phoneno.'">
				<img width="22" src="'.base_url().'.images/map_icons/track-icon.png">Live Track</a></div>
				 
				<div class="nav-map"><a href="'.base_url().'livetracking/geofench/'.$loc->phoneno.'">
				<img width="22" src="'.base_url().'.images/map_icons/fench-icon.png">Fence</a></div>
				 
				</li>';
			}
			 
		}
		$data['side_html']=$html_side;
		$data['map'] = $this->googlemaps->create_map();
		 
		$this->load->view('demo2', $data);
		
	}
	// end here 
	public function geofench( $intPhoneNumber ) {
		//Its show all live location on google map Api
		$gettotTracker =  Tracker_model::getTrackercount();
		$arrmixLiveTrackingInfo['geofench'] = Tracker_model::geofench( $intPhoneNumber );
		$arrmixLiveTrackingInfo['phone_number']	= $intPhoneNumber;
		$arrmixLiveTrackingInfo['tot_tracer']	= $gettotTracker;
		$this->load->view( 'geofench',$arrmixLiveTrackingInfo  );
	
	}
	public function livelocation( $intPhoneNumber ) {
		//Its show all live location on google map Api
		$gettotTracker =  Tracker_model::getTrackercount();
		$arrmixLiveTrackingInfo['getsharedloc']= Tracker_model::SharedLocationByuser();
		$arrmixLiveTrackingInfo['lastlatlong'] = Tracker_model::getlastlatlong( $intPhoneNumber );
		$arrmixLiveTrackingInfo['phone_number']	= $intPhoneNumber;
		$arrmixLiveTrackingInfo['tot_tracer']	= $gettotTracker;
		$this->load->view( 'view_location',$arrmixLiveTrackingInfo  );
	
	}
	
	public function getlatlangBYid( $intPhoneNumber ) { 
		//Its show all live location on google map Api
	
		echo json_encode(Tracker_model::getlastlatlong( $intPhoneNumber ));
	
	}
	public function getSavedFench($intPhoneNumber)
	{ 
		echo json_encode(Tracker_model::geofench( $intPhoneNumber ));
	}
	
	public function savefench( $intPhoneNumber,$lat ,$lang , $radious ) {
		//Its show all live location on google map Api
		echo json_encode(Tracker_model::insertFench( $intPhoneNumber,floatval($lat) ,floatval($lang) , $radious ));
	
	
	}
	public function viewmap( $intPhoneNumber ) {
		// This function used for show map for one live location 
		// Phone number should be pass to see live location
		$arrmixLiveTrackingInfo['vtsdevices'] = Tracker_model::getAllVTSDevices();
		$arrmixLiveTrackingInfo['phone_number']	= $intPhoneNumber;
		$this->load->view( 'livetracking', $arrmixLiveTrackingInfo );
		
	}
	
	public function viewmarkwers( $intPhoneNumber ) {
		$arrmixLiveTrackingInfo['phone_number']	= $intPhoneNumber;
		$this->load->view( 'allmarkers' , $arrmixLiveTrackingInfo);
	
	}
	
	public function getLatlong( $intPhoneNumber = NULL ) {
		// This ia an Ajax function used for fetching latest lat and long for users all registerd phone numbers(VTS)
		header("Content-type: text/xml");
	 	echo '<markers>';
	 	
	   if( false == is_null( $intPhoneNumber ) ) {
	   		$arrmixLiveLocationData = Tracker_model::getTrackerByPhoneNumber( $intPhoneNumber );
	   } else {
		     $arrmixLiveLocationData = Tracker_model::getAllTrackers();
	   }
	  
	//$this->session->set_userdata('liveloc', count($arrmixLiveLocationData	));
		if( 0 < count($arrmixLiveLocationData) ) {
			
			$strTodayDate		=  date("Y-m-d H:i:s");
			$strTodayDate 	= strtotime($strTodayDate);
		
			
			
			foreach ( $arrmixLiveLocationData as $objLiveLocationData ) {
				
					$intLastDate 				= strtotime( $objLiveLocationData->rawdate );
					$fltLatestLatitude 		= round($objLiveLocationData->latitude,5);
					$fltLatestLongitude 	= round($objLiveLocationData->longitude,5);
					
					$strLiveAddress		= $this->getAddress( $fltLatestLatitude, $fltLatestLongitude );
						
					if ( true == empty( $strLiveAddress )) {
							$strLiveAddress	= "Address not available " . $fltLatestLatitude . "," . $fltLatestLongitude;
					}
					$strVehBearing	=	 "";
					$strIgnition			= "";
					$strVhGps				= "";
					$strAc						= "";
					$strVhStIm			= "";
					
					$intSpeedVal	= $objLiveLocationData->speed; 
					
					echo '<marker lat="' . $objLiveLocationData->latitude . '" lng="' . $objLiveLocationData->longitude. '" ';
					echo ' vehicle_dir="' . $strVehBearing . '"';
					echo ' titleb="Speed : ' . $intSpeedVal . ' Km/hr | Ignition : ' . ucfirst( $strIgnition ).' | AC : ' . ucfirst( $strAc ) . ' | GPS : ' . strtoupper( $strVhGps ) . ' | Date-Time : ' . $objLiveLocationData->rawdate . ' | Address : ' . $strLiveAddress .'"';
						// 					echo ' html="&lt;b&gt;Vehicle&lt;/b&gt; : '.ucfirst($result_usr['name']).' &lt;br&gt;&lt;b&gt;Latitude&lt;/b&gt; : '.$res_latest['latitude'].' &lt;br&gt;&lt;b&gt;Longitude&lt;/b&gt; : '.$res_latest['longitude'].' &lt;br&gt;&lt;b&gt;Speed&lt;/b&gt; : '.$intSpeedVal.' Km/hr &lt;br&gt;&lt;b&gt;Ignition&lt;/b&gt; : '.$strIgnition.'&lt;br&gt;&lt;b&gt;AC&lt;/b&gt; : '.$strAc.'&lt;br&gt;&lt;b&gt;GPS&lt;/b&gt; : '.strtoupper($strVhGps).' &lt;br&gt;&lt;b&gt;Date-Time&lt;/b&gt; : '.$res_latest['rawdate'].' &lt;br&gt;&lt;b&gt;Address&lt;/b&gt; : '.$vh_adr.'"';
							
						echo ' html="&lt;b&gt;Name&lt;/b&gt; : '.$objLiveLocationData->name.
									' &lt;br&gt;&lt;b&gt;Speed&lt;/b&gt; : '.$intSpeedVal.' Km/hr &lt;br
									&gt;&lt;b&gt;Date-Time&lt;/b&gt; : '.$objLiveLocationData->dated.' &lt;br
									&gt;&lt;b&gt;Address&lt;/b&gt; : '.$strLiveAddress.'"';
					
					//echo ' token="' . $objLiveLocationData->token . '"';
					echo ' marker_icon="' . $objLiveLocationData->marker_icon . '"';
					echo ' phoneno="' . $objLiveLocationData->phoneno . '"';
					echo ' vehicle_icon=""';
					echo ' label="'.$strVhStIm.'  '.$objLiveLocationData->name.'" />';
					 
				}
		}
	  
	 echo '</markers>'; 
	}
	
	
	public function demogetLatlong( $intPhoneNumber = NULL ) {
			 
		if( false == is_null( $intPhoneNumber ) ) {
			$arrmixLiveLocationData = Tracker_model::getTrackerByPhoneNumber( $intPhoneNumber );
		} else {
			$arrmixLiveLocationData = Tracker_model::getAllTrackers();
		}
		 
		//$this->session->set_userdata('liveloc', count($arrmixLiveLocationData	));
		if( 0 < count($arrmixLiveLocationData) ) {
				
			$strTodayDate		=  date("Y-m-d H:i:s");
			$strTodayDate 	= strtotime($strTodayDate);
			echo json_encode( array('flag'=>'1' ,'live_locations'=>$arrmixLiveLocationData) );
	
		}
		 
	
	}
	public function getAllLatLangByDeviceId( $intPhoneNumber )
	{
		 

			$arrmixLiveLocationData = Tracker_model::getAllLatLangByDeviceID( $intPhoneNumber );
		 
		//$this->session->set_userdata('liveloc', count($arrmixLiveLocationData	));
		if( 0 < count($arrmixLiveLocationData) ) {
				
			$strTodayDate		=  date("Y-m-d H:i:s");
			$strTodayDate 	= strtotime($strTodayDate);
			echo json_encode( array('flag'=>'1' ,'live_locations'=>$arrmixLiveLocationData) );
	
		}
		
		
	}
	public function checkImeiNumber( $intImeiNumber , $intPhoneNumber ) {
		// This function is the web servive for android app to check provided phone number and imei number is avilable or not .  
		 	 $intCntImeiNumber = Tracker_model::getImeiNumberCountByImeiNumberByPhoneNumber( $intImeiNumber , $intPhoneNumber );
			
			if( 0 < $intCntImeiNumber ) { 
				
					$strApiKey = md5( time() );
					
					if( true == Tracker_model::updateApiKey( $strApiKey, $intPhoneNumber ) ) {
							echo json_encode( array('imminumber' => $intImeiNumber , 'flag'=>'1','msg'=>'imei number is avilable' ,'api_key'=>$strApiKey) );
					} else {
							echo json_encode( array('imminumber' => $intImeiNumber , 'flag'=>'0','msg'=>'imei number is not avilable' ) );
					}
					
			} else {
					echo json_encode( array('imminumber' => $intImeiNumber , 'flag'=>'0','msg'=>'imei number is not avilable' ) );
			}
	}
	 
	public function routeWiseLocations( $intPhoneNumber ) {
		  // This function is used for , to display users route history by phone number . 
		    $arrMixlocationData['asset_info']		= 		asset_model::fetchAssetByPhoneNumber( $intPhoneNumber );
			$arrMixlocationData['reports'] 		= 		Tracker_model::fetchDistanceReports( $intPhoneNumber );
			$this->load->view( 'view_routes.php' , $arrMixlocationData );
	}
	
	 public function viewLocationsmap( $intPhoneNumber , $intTokenNumber="" ) {
	 		$arrMixlocationData['asset_info']= Tracker_model::fetchAssetByPhoneNumber( $intPhoneNumber );

	 		if( true == isset($intTokenNumber)) {
				$arrMixlocationData['reports'] = Tracker_model::getroute( $intPhoneNumber , $intTokenNumber );
	 		}
	 		
			$arrMixlocationData['from_date'] 	= '';
			$arrMixlocationData['to_date'] 		= '';
			
			$arrMixlocationData['token_number']	= $intTokenNumber;
			$this->load->view('routereport',$arrMixlocationData);
			//$this->load->view('routemap',$arrMixlocationData);
			//$this->load->view('liveroute',$arrMixlocationData);
			//$this->load->view('allmarkers',$arrMixlocationData);
		}
		
		public function viewDateWiseLocationsmap() {
			
			$intPhoneNumber			= ( int ) $this->uri->segment( 3 );
			$arrmixlocationData		= array();
			$arrmixlocationData['asset_info']= Tracker_model::fetchAssetByPhoneNumber( $intPhoneNumber );
			
			$arrmixFormInputData = $this->input->post();
			
			if( true == is_array( $arrmixFormInputData )) {
				
				$arrmixlocationData['from_date']	= $this->input->post( 'from_date' );
				$arrmixlocationData['to_date']		= $this->input->post( 'to_date' );
				$arrmixlocationData['reports']	 	= Tracker_model::getrouteByFromDateByToDate( $intPhoneNumber , $arrmixlocationData['from_date'], $arrmixlocationData['to_date'] );
				
			} else {
				
				$arrmixlocationData['reports'] 		= Tracker_model::getLatestRouteByPhoneNumer( $intPhoneNumber );
				$arrmixlocationData['from_date']	= ( false == empty( $arrmixlocationData['reports'] ) ) ? $arrmixlocationData['reports'][0]->dated : '';
				$arrmixlocationData['to_date']		= ( false == empty( $arrmixlocationData['reports'] ) ) ? $arrmixlocationData['reports'][0]->dated : '';
				
			}	
			
			$this->load->view('routereport',$arrmixlocationData);
			
		}
			// code review changes are remaining.
	public function insertdata() {
		 // This function use as Aandroid Web Service to registerd new VTS Device
			$arrmixTrackerData = file_get_contents('php://input');
			$arrmixTrackerData = json_decode($arrmixTrackerData, true); 
			//echo "hi".print_r($array); exit;
			
			try
			{  
				$objDevice	= Tracker_model::checkforapikey($arrmixTrackerData['api_key']);
				if( true == is_object( $objDevice ) ) {
					$result= Tracker_model::insert( $arrmixTrackerData );
					if( $result ) { 
						echo json_encode( array( 'flag'=>'1','msg'=>'Insert data sucesfully', 'frequency_rate'=> $objDevice->frequency_rate ) );
					} 
				} else {
					echo json_encode( array( 'flag'=>'0','msg'=>'Invalid Api Key. Please login again' ) );
				}
				
			}
			catch( Exception $e ) { //later
				echo json_encode( array( 'flag'=>'0','msg'=>'no data inserted' ) );
			}
		}

	// code review changes are remaining.
	public function getAddress( $dblLat, $dblLong ) {
    // This function written for , fetch detail address from google API , by passing lat and long .
        $strUrl   = "https://maps.googleapis.com/maps/api/geocode/json?latlng=". $dblLat.",". $dblLong ."&sensor=false";

        $strJsonData    = @file_get_contents( $strUrl );
        $objJson        = json_decode( $strJsonData );
        $strStatus      = $objJson->status;

        $strFormatedAddress = '';

        if( 'OK' == $strStatus ) {
        $strFormatedAddress = $objJson->results[0]->formatted_address;
        }

        return $strFormatedAddress;
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
      // This function written for , calculate total travelled distance of user  .
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
    
        if ($unit == "K") {
            $retval = ($miles * 1.609344);
            return number_format($retval, 2, '.', '');
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
    
    public function sendsms( $intMobileNumber ) {
    
    	// Replace with your username
    	$user = "rectusenergy";
    		
    	// Replace with your API KEY (We have sent API KEY on activation email, also available on panel)
    	$apikey = "icv59VrXI2CWEDCHRfOw";
    		
    	// Replace if you have your own Sender ID, else donot change
    	$senderid  =  "MYTEXT";
    		
    	// Replace with the destination mobile Number to which you want to send sms
    	$mobile  =  $intMobileNumber;
    		
    	// Replace with your Message content
    	$message="Your Device ".$intMobileNumber." is went out of fench";
    	$message = urlencode($message);
    		
    	// For Plain Text, use "txt" ; for Unicode symbols or regional Languages like hindi/tamil/kannada use "uni"
    	$type   =  "txt";
    		
    	$ch = curl_init();
    	// set URL and other appropriate options
    	curl_setopt($ch, CURLOPT_URL, "http://smshorizon.co.in/api/sendsms.php?user=".$user."&apikey=".$apikey."&mobile=".$intMobileNumber."&senderid=".$senderid."&message=".$message."&type=".$type."");
    	curl_setopt($ch, CURLOPT_HEADER, 0);
    		
    	// grab URL and pass it to the browser
    	$output = curl_exec($ch);
    
    	// close cURL resource, and free up system resources
    	curl_close($ch);
    
    	// Display MSGID of the successful sms push
    	if( $output ) {
    		
    		echo json_encode( array( 'flag' => '1', 'msg' => 'Message sent successfully!') );
    	} else {
    		echo json_encode( array( 'flag' => '0', 'msg' => 'Message not sent successfully!') );
    	}
    
    		
    }
    public function sharelocation( )
    {
    	 $reponse = Tracker_model::saveshareloc();
    	// print_r($reponse);exit;
    	 echo json_encode($reponse);
    }
    public function ajaxcheckforsahrelcation()
    {
    $share_dtl =  Tracker_model::countShareByGnissId();
    	if( count($share_dtl) > 0 ) {
    	
    	$link=base_url().'livetracking/share/'.$share_dtl[0]->shared_device;
    		echo json_encode( array( 'flag' => '1', 'sharelinkS' => $share_dtl) );
    	} else {
    		echo json_encode( array( 'flag' => '0', 'msg' => 'No  notification') );
    	}
    	
    }
    
    public function getSharedByUsers() {
    	
    	$arrmixSharedLocations	= array();
    	
    	$intGnissId	= $this->session->userdata('account_number');
   		$arrmixSharedLocations['users']	= Tracker_model::fetchUsersSharedByGnissId( $intGnissId );
   		
   		
		$this->load->view('view_shared_locations', $arrmixSharedLocations );
    }
    
    public function share( $intPhoneNumber ) {
    	
    	//Its show all live location on google map Api
    	 // check for share
    	$checkshare =  Tracker_model::checkforshare( $intPhoneNumber );
    	if(count($checkshare) > 0 ) {
    	 // end here 

		    	if($checkshare->is_active==1)
		    	{
		    		$gettotTracker =  Tracker_model::getTrackercount();
		    		$arrmixLiveTrackingInfo['lastlatlong'] = Tracker_model::getlastlatlong( $intPhoneNumber );
		    		$arrmixLiveTrackingInfo['phone_number']	= $intPhoneNumber;
		    		$arrmixLiveTrackingInfo['tot_tracer']	= $gettotTracker;
		    		$mark_read = Tracker_model::setMarkRead( $intPhoneNumber );
		    	    $this->load->view( 'share_location',$arrmixLiveTrackingInfo  );
		    	    
		    	}else 
		    	{
			 		$arrmixLiveTrackingInfo['msg']	= "Your friend revoke your access to access this location";
		    		$this->load->view( 'access-denied.php',$arrmixLiveTrackingInfo );
		    		
		    	}
    	}else
    	 {
    	 	$arrmixLiveTrackingInfo['msg']	= "You dont have permission to access this location";
    		$this->load->view( 'access-denied.php',$arrmixLiveTrackingInfo  );
    	}
    	
   
    }
    
    public function ShareActiveDeactive($status , $DeviceID , $SharedGnissId){
    	
    	$ShareStatus =  Tracker_model::UpdateShareStatus($status , $DeviceID , $SharedGnissId);
    	if($ShareStatus==1) {
    		echo json_encode( array( 'flag' => '1','msg' => ' Location  updated!') );
    	} else {
    		echo json_encode( array( 'flag' => '0', 'msg' => 'Sorry Location not update!') );
    	}
    	
    }
   
    public function getshareLatlong( $intPhoneNumber ) {
    	// This ia an Ajax function used for fetching latest lat and long for users all registerd phone numbers(VTS)
    	header("Content-type: text/xml");
    	echo '<markers>';
    	 
    
    	$arrmixLiveLocationData = Tracker_model::getShareTrackerByPhoneNumber( $intPhoneNumber );
    
    	 
    	//$this->session->set_userdata('liveloc', count($arrmixLiveLocationData	));
    	if( 0 < count($arrmixLiveLocationData) ) {
    			
    		$strTodayDate		=  date("Y-m-d H:i:s");
    		$strTodayDate 	= strtotime($strTodayDate);
    
    			
    			
    		foreach ( $arrmixLiveLocationData as $objLiveLocationData ) {
    
    			$intLastDate 				= strtotime( $objLiveLocationData->rawdate );
    			$fltLatestLatitude 		= round($objLiveLocationData->latitude,5);
    			$fltLatestLongitude 	= round($objLiveLocationData->longitude,5);
    				
    			$strLiveAddress		= $this->getAddress( $fltLatestLatitude, $fltLatestLongitude );
    
    			if ( true == empty( $strLiveAddress )) {
    				$strLiveAddress	= "Address not available " . $fltLatestLatitude . "," . $fltLatestLongitude;
    			}
    			$strVehBearing	=	 "";
    			$strIgnition			= "";
    			$strVhGps				= "";
    			$strAc						= "";
    			$strVhStIm			= "";
    				
    			$intSpeedVal	= $objLiveLocationData->speed;
    				
    			echo '<marker lat="' . $objLiveLocationData->latitude . '" lng="' . $objLiveLocationData->longitude. '" ';
    			echo ' vehicle_dir="' . $strVehBearing . '"';
    			echo ' titleb="Speed : ' . $intSpeedVal . ' Km/hr | Ignition : ' . ucfirst( $strIgnition ).' | AC : ' . ucfirst( $strAc ) . ' | GPS : ' . strtoupper( $strVhGps ) . ' | Date-Time : ' . $objLiveLocationData->rawdate . ' | Address : ' . $strLiveAddress .'"';
    			// 					echo ' html="&lt;b&gt;Vehicle&lt;/b&gt; : '.ucfirst($result_usr['name']).' &lt;br&gt;&lt;b&gt;Latitude&lt;/b&gt; : '.$res_latest['latitude'].' &lt;br&gt;&lt;b&gt;Longitude&lt;/b&gt; : '.$res_latest['longitude'].' &lt;br&gt;&lt;b&gt;Speed&lt;/b&gt; : '.$intSpeedVal.' Km/hr &lt;br&gt;&lt;b&gt;Ignition&lt;/b&gt; : '.$strIgnition.'&lt;br&gt;&lt;b&gt;AC&lt;/b&gt; : '.$strAc.'&lt;br&gt;&lt;b&gt;GPS&lt;/b&gt; : '.strtoupper($strVhGps).' &lt;br&gt;&lt;b&gt;Date-Time&lt;/b&gt; : '.$res_latest['rawdate'].' &lt;br&gt;&lt;b&gt;Address&lt;/b&gt; : '.$vh_adr.'"';
    				
    			echo ' html="&lt;b&gt;Name&lt;/b&gt; : '.$objLiveLocationData->name.
    			' &lt;br&gt;&lt;b&gt;Speed&lt;/b&gt; : '.$intSpeedVal.' Km/hr &lt;br
    			&gt;&lt;b&gt;Date-Time&lt;/b&gt; : '.$objLiveLocationData->dated.' &lt;br
    			&gt;&lt;b&gt;Address&lt;/b&gt; : '.$strLiveAddress.'"';
    				
    			//echo ' token="' . $objLiveLocationData->token . '"';
    			echo ' marker_icon="' . $objLiveLocationData->marker_icon . '"';
    			echo ' phoneno="' . $objLiveLocationData->phoneno . '"';
    			echo ' vehicle_icon=""';
    			echo ' label="'.$strVhStIm.'  '.$objLiveLocationData->name.'" />';
    
    		}
    	}
    	 
    	echo '</markers>';
    }
    
    
    function sendMail( $strEmailFrom, $strEmailTo, $strSubject, $arrstrEmailFormat ) {
    
    	$this->load->library( 'email' );
    	$this->load->library( 'parser' );
    	$this->email->mailtype	= 'html';
    
    	$this->email->from( $strEmailFrom, 'Rectus Energy pvt ltd' );
    	$this->email->to( $strEmailTo );
    	$this->email->subject( $strSubject );
    
    	$strEmailContent = $this->parser->parse( 'email_template',$arrstrEmailFormat, TRUE );
    	$this->email->message( $strEmailContent );
    
    	return $this->email->send();
    }	
	
    public function autosuggetionUserForLocation()
    {
    	
    	$arrmixAutoSuggest = Tracker_model::getAutoSuggestionById( $this->input->post('name') );
    	if(count($arrmixAutoSuggest) > 0)
    	{
    		foreach($arrmixAutoSuggest as $result)
    		{
    			echo "<li>".$result->first_name."(".$result->account_number.")<input type='hidden' name='gniss_id[]' value=".$result->account_number."></li>";
    		}
    	}
    	else
    	{
    		echo '<li>Not Found</li>';
    	}
    }
}
