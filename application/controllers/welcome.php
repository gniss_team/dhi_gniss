<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

		const C_LINE	= 'Line';
		const C_BAR		= 'Bar';
		const C_PIE		= 'Pie';
	
	public function __construct() {
		
			parent::__construct();

	}
	
	public function action( $strAction = '' ) {
	
		switch( $strAction ) {
						
						case 'displayGraphFilters':
		    							$this->displayGraphFilters( $strAction );
                              			break;
                              						
                              	case 'viewGraph':
                        		      $this->viewGraph( $strAction );
                              		  break;
                              		  
                              	case 'ajaxgetEnergyStats':
                              		  $this->ajaxgetEnergyStats();
                              		  break;
                              		  
                              	 case 'viewVoltageVsCurrent':
                              		  	$this->viewVoltageVsCurrent();
                              		  	break;
                              		  	
                              		 case 'viewLiveGraph':
                              		 $this->viewLiveGraph( $strAction );
                              		break;
                              		
                              		case 'displayGraph':
                              		$this->displayGraph();
                              		break;
                              		case 'getparalist':
                              			$this->getparalist();
                              			break;
                              		
                              		case 'viewWattHourGraph':
                              			$this->viewWattHourGraph( $strAction );
                              			break;
                              			
                              	default:
                              	show_404();
		}
	
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://localhost/gnissv3/users/action/login
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 * 
	 * This index method is mainly used to check if remember me is enabled or not.
	 * If cookie is available, it will redirect to dashboard directy else go for normal login page (if user is not in session).
	 */
	
	public function index()	{
	
// 			$arrmixDashboardInfo	= array();
// 			$intUserId		= $this->session->userdata('user_id');
		
// 			$arrmixDashboardInfo['device_type_id']	=  ( false == empty( $this->uri->segment(4) ) ) ? $this->uri->segment(4) : NULL;
			
// 			$arrmixDashboardInfo['devices_types']	= Devices_model::fetchDeviceTypes();
			
// 			if( false == is_null( $arrmixDashboardInfo['device_type_id'] ) ) {
// 					$arrmixDashboardInfo['devices']					= Devices_model::fetchDevicesByDeviceTypeIdByUserId( $arrmixDashboardInfo['device_type_id'], $intUserId	 );
// 			}
// 			$arrmixDashboardInfo['msg']		= $this->session->userdata('sucmsg');
// 			$this->session->unset_userdata('sucmsg');
			
// 			$this->load->view( 'dashboard', $arrmixDashboardInfo );

		$intUserId					= $this->session->userdata('user_id');
		$arrmixAppDetails['devices']	= Devices_model::fetchDeviceTypeByUserId( $intUserId );
		$isVtsRegister =  Tracker_model::getVtsTotalRow( $intUserId );
		$arrmixAppDetails['isVtsRegister'] = $isVtsRegister;
			
		$this->load->view( 'apps', $arrmixAppDetails );
			
	}
	
	public function displayGraph() {
		
		$arrmixDashboardInfo	= array();
		$intUserId		= $this->session->userdata('user_id');
		$arrmixDashboardInfo['device_id']				=  ( false == empty( $this->uri->segment(5) ) ) ? $this->uri->segment(5) : NULL;
		
		$arrmixDashboardInfo['device_type_id']	=  ( false == empty( $this->uri->segment(4) ) ) ? $this->uri->segment(4) : NULL;
	
		$arrmixDashboardInfo['devices_types']	= Devices_model::fetchDeviceTypes();
		$arrmixDashboardInfo['devices']					= Devices_model::fetchDevicesByDeviceTypeIdByUserId( $arrmixDashboardInfo['device_type_id'], $intUserId	 );
		
		$arrmixDashboardInfo['msg']		= $this->session->userdata('sucmsg');
		$this->session->unset_userdata('sucmsg');
		
		$objDevieType		= Devices_model::fetchDeviceTypeById( $arrmixDashboardInfo['device_type_id'] );
				
		switch( $objDevieType->unit_type ) {
			case '1':
				$this->load->view('dashboard_aces', $arrmixDashboardInfo );
				break;
		
			case '2':
				$this->load->view('dashboard', $arrmixDashboardInfo );
				break;
		
			default:
							$this->load->view('dashboard', $arrmixDashboardInfo );
		}
			
// 		$this->load->view( 'dashboard_aces', $arrmixDashboardInfo );
		//$this->load->view( 'dashboard', $arrmixDashboardInfo );
		
	}
	
	/**
	 * This method is used to log out of session and cookie.
	 */
	function logout() {
		
			$this->session->sess_destroy();
			redirect( base_url() );
		
	}
	
	/**
	 * This method is used to unset cookie.
	 */
	function unsetcookie() {
		
			$this->session->sess_destroy();
			$this->input->set_cookie( 'gnisscookie', '' );
			redirect( base_url() );
		
	}
	
	public function displayGraphFilters() {
			$this->load->view( 'energy_meter_graph' );
	}

	public function ajaxgetEnergyStats() {
		
			$intDeviceId		=  ( false == empty( $this->uri->segment(4) ) ) ? $this->uri->segment(4) : NULL;
			$fieldsarr 			= array( 'ambient_temperature', 'set_temperature','grid_voltage', 'cyclic_period', 'ac_switch_duration', 'cum_kwh' , 'cum_watt_hour', 'ac1_stat', 'ac2_stat', 'compressor_status','humidity' );
			$getStatData = energy_meter_model::getAcesAtmStatsByDeviceId( $fieldsarr, $intDeviceId );
			
			if( true == is_object( $getStatData ) ) {
			
				$intAc1Fault		= ( (1 == $getStatData->ac1_stat) && ( 0 == $getStatData->compressor_status ||  $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10 )) ? 0 : 1;
				$intAc2Fault		= ( ( 1 == $getStatData->ac2_stat )  && ( 0 == $getStatData->compressor_status ||  $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10) ) ? 0 : 1;
				$intAcFault			= ( 0 == $getStatData->ac1_stat && 0 == $getStatData->ac1_stat &&  0 == $getStatData->compressor_status ) ? 0 : 1;
				$intDeviceFault		= ( 0 == $getStatData->ac1_stat && 0 == $getStatData->ac2_stat && $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10 ) ? 0 : 1;

		//$currentvoltage  = energy_meter_model::getlatestStats('voltage');
				$stats = array(
					"temperature"=>round($getStatData->ambient_temperature/10),  "set_temperature"=>round($getStatData->set_temperature/10), "cyclic_period"=>$getStatData->cyclic_period ,
					"ac_switch_duration"=>$getStatData->ac_switch_duration/60, 'kwh'=>round( $getStatData->cum_kwh . '.'. $getStatData->cum_watt_hour, 2), 'ac1_stat'=>$getStatData->ac1_stat, 
					'ac2_stat'=>$getStatData->ac2_stat, 'compressor_status'=>$getStatData->compressor_status, "ac1_fault"=> $intAc1Fault, "ac2_fault"=> $intAc2Fault, "device_fault"=> $intDeviceFault,"voltage"=> $getStatData->grid_voltage/2,"humitity"=> $getStatData->humidity  );
				
			} else { 
				$stats = array(
						"temperature"=>0, "set_temperature"=>0, "voltage"=>0, "cyclic_period"=>0,
						"ac_switch_duration"=>0, "kwh"=>0, 'ac1_stat'=>0, 'ac2_stat'=>0, 'compressor_status'=>0,
						"ac1_fault"=> 0, "ac2_fault"=> 0, "device_fault"=> 0,"humitity"=> 0
				);
			}
			echo json_encode($stats);
	}
	
	public function viewLiveGraph() {
	
		$strUnitType		= $this->uri->segment(4);
		$intDeviceId		= $this->uri->segment(7);
		$intDeviceTypeId	= ( int ) $this->uri->segment(6);
		
		$arrmixEnergymeterData		= array();
		
		if( 'cum_watt_hour' == $strUnitType) {
				$strUnitType .= " * 10 AS cum_watt_hour";
		} 
		
		if( 'line' == $this->uri->segment(5) ) {
				$objDevieType		= Devices_model::fetchDeviceTypeById( $intDeviceTypeId );

				switch( $objDevieType->unit_type ) {
					case '1':
							$arrmixEnergymeterData['energy_meter_data']		= energy_meter_model::getLastHourAcesAtmByUnitTypeByDeviceId( $strUnitType, $intDeviceId );
						break;
				
					case '2':
							$arrmixEnergymeterData['energy_meter_data']		= energy_meter_model::getLastHourEnergyMeterDataByUnitTypeByDeviceId( $strUnitType, $intDeviceId );
							break;
				
					default:
							$arrmixEnergymeterData['energy_meter_data']		= energy_meter_model::getLastHourEnergyMeterDataByUnitTypeByDeviceId( $strUnitType, $intDeviceId );
				}
				$strUnitType		= $this->uri->segment(4);
				$arrmixEnergymeterData['cnt_graph_parameter']		=	1;
				$arrmixEnergymeterData['type']						=$this->uri->segment(4);
				
				if('ambient_temperature' == $strUnitType) {
						
						$arrmixEnergymeterData['type'] 		= 'temperature';
						$arrmixEnergymeterData['plot']		= 'livedata';
						
				} else	if( 'cum_watt_hour' == $strUnitType) {
						
						$arrmixEnergymeterData['type'] 	= 'watt';
						$arrmixEnergymeterData['plot']		= 'voltagedata';
						
				} else if( 'humidity' == $strUnitType) {
						
						$arrmixEnergymeterData['type'] 	= 'humidity';
						$arrmixEnergymeterData['plot']		= 'humiditydata';
						
				} else {
						$arrmixEnergymeterData['plot']		= 'livedata';
				}
							
				if( true == empty( $arrmixEnergymeterData['energy_meter_data'] ) ) {
						$arrmixEnergymeterData['energy_meter_data']		= array( );
				} 
				$arrmixEnergymeterData['unit_type']	= $strUnitType;
				if( 'humidity' == $strUnitType) {
					echo json_encode($arrmixEnergymeterData['energy_meter_data']);
				}else {
				$this->load->view( 'ajax_linegraph_data', $arrmixEnergymeterData );
				}
			}  
			
			if(  'bar' == $this->uri->segment(5) || 'pie' ==  $this->uri->segment(5)  ) { 
			 	$strFromDate	= date( 'Y-m-d 00:00:00',  time() );
			 	$strToDate			= date( 'Y-m-d 23:59:59',  time() );
				$strField	=	'hour';
				$arrmixEnergymeterData['energy_meter_data']		= energy_meter_model::fetchEnergyMeterDataByUnitTypeByFromDateByToDateByField( $strUnitType, $strFromDate , $strToDate, $strField );
				
				if( false == empty( $arrmixEnergymeterData['energy_meter_data'] ) ) {
					$arrmixEnergymeterData['type']					= ( array )$this->uri->segment(4);
					$arrmixEnergymeterData['unit_type'] 			= $strUnitType;
					$arrmixEnergymeterData['cnt_graph_parameter']	=	1;
				
					$this->load->view( 'view_piechart_graph', $arrmixEnergymeterData );
				} else {
					echo "<span style='color:red;font-size:13px;text-align:center;';>No records Found </span>";exit;
				}
			}
	}
	
	public function viewVoltageVsCurrent() {
		
			$intDeviceId		= ( int ) $this->uri->segment(4);
			$arrmixEnergymeterData['vcdata']		= energy_meter_model::getVoltageVesesCurrentDataByDeviceId( $intDeviceId );
			// 	print_r($arrmixEnergymeterData);
			if(count($arrmixEnergymeterData['vcdata']) > 0) {
				$this->load->view( 'view_meter_gauge' , $arrmixEnergymeterData );
			}else {
				echo "<span style='color:red;font-size:13px;text-align:center;';>No records Found </span>";exit;
			}
		 
	}
	
	public function viewWattHourGraph()
	{
		$intDeviceId		= $this->uri->segment(4);
		$arrmixEnergymeterData['energy_meter_data']		= energy_meter_model::getHourlyWattBydeviceId( $intDeviceId );
		// 	print_r($arrmixEnergymeterData);
		if(count($arrmixEnergymeterData['energy_meter_data']) > 0) {
			$this->load->view( 'watt_hour_graph' , $arrmixEnergymeterData );
		}else {
			echo "<span style='color:red;font-size:13px;text-align:center;';>No records Found </span>";exit;
		}
	
	}
	
	public function getparalist()
	{
		$data_fields = array('temprature','ac_switch','low_cut','high_cut','temp_offset','main_offset','load_offset','load_gain');
		$intDeviceId		= $this->uri->segment(4);
		$para = array();
		$para= Devices_model::getparalist($intDeviceId);
		$strtempData 			= str_replace( ":", "", $para->parastr );
		$substring 			= trim( substr($strtempData , 6 ) );
		$str_final_data		= substr( $substring, 0, -2 );
		$arrstrvalues		= str_split( $str_final_data, 4 );
	
		for( $i=0;$i<sizeof($arrstrvalues);$i++ ) {
			$parrwithvalue[$data_fields[$i]] = hexdec($arrstrvalues[$i]);
		}
	
		echo json_encode($parrwithvalue);
	}

}
