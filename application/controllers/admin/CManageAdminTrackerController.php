<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageAdminTrackerController extends CI_Controller {
	 
          	public function __construct() {
          		parent::__construct();
          		//$this->load->helper( array('common_function_helper') );
         }
	
          	public function action( $strAction = '' ) { 
          	      
                    	switch( $strAction ) {
                               
                    	          case 'add':
                              	$this->addTracker( $strAction );
                              		        break;
                              		        
                              	case 'edit':
                                 $this->editTracker( $strAction );
                              	            break;
                              	            
          	                     case 'delete':
          	                      $this->deleteTracker( $strAction );
          	                              break;
          	                              
                             	            
                              	case 'manageMobileTracker':     
                              	$this->manageMobileTracker( $strAction );
                              	         	break;
                              	         	
                          
                              	default:
                              	show_404();
                    			
                  }
                  
          	}
	
            public function addTracker( $strAction ) { 
                     
                   $arrmixAssetInfo	     = Array();
                   $arrmixInputData     =  $this->input->post();
                   
                        if(  false == empty($arrmixInputData) &&  true == is_array( $arrmixInputData ) ) {
                                
                              $this->form_validation->set_rules('name', 'Name',  'trim|required|alpha_numeric|min_length[2]|max_length[20]|xss_clean');
                             // $this->form_validation->set_rules('asset_type_id', 'Asset type',  'trim|required|xss_clean');
                              $this->form_validation->set_rules('sim_number', 'Sim number', 'trim|is_digit|exact_length[20]|is_unique[assets.sim_number]|xss_clean');
                              $this->form_validation->set_rules('asset_number', 'Asset number', 'trim|required|alpha_numeric|min_length[2]|max_length[15]|is_unique[assets.asset_number]|xss_clean');
                              $this->form_validation->set_rules('imei_number', 'IMEI number', 'trim|required|is_digit|exact_length[15]|is_unique[assets.imei_number]|xss_clean');
                              $this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|is_digit|exact_length[10]|xss_clean');
                              $this->form_validation->set_rules('model_number', 'Model number', 'trim|required|alpha_numeric|min_length[1]|max_length[20]|xss_clean');
                              $this->form_validation->set_rules('mode', ' Mode', 'required');
                              
                                     if( true == $this->form_validation->run() ) { 
                                        $arrmixAssetInfo['assets'] = Asset_model::addAsset();
                                        $this->session->set_userdata( array( 'successmsg' =>"Asset  added successfully" ) );
                                        redirect( base_url().'admin/tracker/action/manageMobileTracker' );
                            } else {
                                      $arrmixAssetInfo['assets'][0] = new stdClass;
                                      $arrmixAssetInfo['assets'][0]->status = $this->input->post( 'status' );
                                      $arrmixAssetInfo['assets'][0]->mode = $this->input->post( 'mode' );
                                      $arrmixAssetInfo['assets'][0]->company_id = $this->input->post( 'company_id' );

                                                   if( 'Individual' == $this->session->userdata('login_type') ) {
                                                $arrmixAssetInfo['companies']          = Company_model::fetchAllCompaniesByUserId( $this->session->userdata('user_id') );
                                      }
                            }
                    }
                    
                            if( 'Individual' == $this->session->userdata('login_type') ) {
                              $arrmixAssetInfo['companies']          = Company_model::fetchAllCompaniesByUserId( $this->session->userdata('user_id') );
     
                    } 
                   // $arrmixAssetInfo['asset_types']          = Asset_types_model::fetchAssetTypes();
                    
                    $this->load->view( 'admin/add_assets', $arrmixAssetInfo );
         }
	
        	public function manageMobileTracker( $strAction ) {
         	      
        			$arrmixAssetsInfo		=	array();
               $intAdminUserId      = $this->session->userdata( 'admin_user_id' );
                 
                     
                    $arrmixAssetsInfo['assets']	= Asset_model::fetchAssetsByUserTypeIdByOwnerId( $intAdminUserId  );
						
//                   	   if( true == $this->session->userdata( 'successmsg' ) ) {
//           	                    $arrmixAssetsInfo['msg'] = $this->session->userdata( 'successmsg' );
//           	                    $this->session->unset_userdata( 'successmsg' );
//           	          }
          	           
//           	          if( true == $this->session->userdata( 'errmsg' ) ) {
//           	                    $arrmixUserInfo['errmsg'] = $this->session->userdata( 'errmsg' );
//           	                    $this->session->unset_userdata( 'errmsg' );
//           	          }
                    
                 $this->load->view( 'admin/manage_tracker', $arrmixAssetsInfo );
                              	
          	} 
     
         	public function editTracker( $strAction ) {
     
          		$intTrackerId			= ( int ) $this->uri->segment( 5 );
          		$inAdminUserId		= $this->session->userdata('admin_user_id');
//           		if( 'Individual' == $this->session->userdata('login_type') ) {
//           			$intUserTypeId        = 1;
//           			$intOwnerId            = $this->session->userdata('user_id');
//           		} else {
//           			$intUserTypeId     = 2;
//           			$intOwnerId         = $this->session->userdata('company_id');
//           		}
//           		$arrmixAssetCount =	asset_model::fetchAssetCountByUserTypeIdByOwnerIdByAssetId( $intTrackerId );
          	
//           		 	 if( 1  != count($arrmixAssetCount)) {
//           		  				redirect( base_url().'admin/tracker/action/manageMobileTracker' );
//           			}
 
          		
                    $arrmixInputData     =  $this->input->post();
                    
                    $arrmixAssetInfo   = array();

                           if( false == empty($arrmixInputData) &&  true == is_array( $arrmixInputData ) ) {
                  
                          		
                              $this->form_validation->set_rules('name', 'Name',  'trim|required|alpha_numeric|min_length[2]|max_length[20]|xss_clean');
                      //        $this->form_validation->set_rules('asset_type_id', 'Asset type',  'trim|required|xss_clean');
                              $this->form_validation->set_rules('sim_number', 'Sim number', 'trim|is_digit|exact_length[20]|unique_check[assets.sim_number.' . $intTrackerId .']|xss_clean');
                              $this->form_validation->set_rules('asset_number', 'Asset number', 'trim|required|alpha_numeric|min_length[2]|max_length[15]|unique_check[assets.asset_number.' . $intTrackerId .']|xss_clean');
                              $this->form_validation->set_rules('imei_number', 'IMEI number', 'trim|required|is_digit|exact_length[15]|unique_check[assets.imei_number.' . $intTrackerId .']|xss_clean');
                              $this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|is_digit|exact_length[10]|xss_clean');
                              $this->form_validation->set_rules('model_number', 'Model number', 'trim|required|alpha_numeric|min_length[1]|max_length[20]|xss_clean');
                              $this->form_validation->set_rules('mode', ' Mode', 'required');
                              
                                        if(  true == $this->form_validation->run() ) { 
                                                   if(  true == Asset_model::updateAsset( $intTrackerId ) ) { 
                                                   $this->session->set_userdata( array( 'successmsg' =>"Asset updated successfully." ) );
                                                   redirect( base_url().'admin/tracker/action/manageMobileTracker' );
                                       } else {
                                                 $this->session->set_userdata( array( 'errmsg' =>"Unable to delete record." ) );
                                       }
                              } else {
                                        $arrmixAssetInfo['assets']                                = Asset_model::fetchAssetById( $intTrackerId );
                                        $arrmixAssetInfo['assets']->asset_type_id      =  1; //$this->input->post('asset_type_id');
                                        $arrmixAssetInfo['assets']->mode                   =  $this->input->post('mode');
                                        $arrmixAssetInfo['assets']->status                  =  $this->input->post('status');
                                        $arrmixAssetInfo['assets']->name                   =  $this->input->post('name');
                                        $arrmixAssetInfo['assets']->sim_number        =  $this->input->post('sim_number');
                                        $arrmixAssetInfo['assets']->asset_number     =  $this->input->post('asset_number');
                                        $arrmixAssetInfo['assets']->phone_number    =  $this->input->post('phone_number');
                                        $arrmixAssetInfo['assets']->model_number    =  $this->input->post('model_number');
                                        $arrmixAssetInfo['assets']->imei_number       =  $this->input->post('imei_number');
                                        			
                                        			if( true == isset( $arrmixInputData['company_id'])) {
                                        			$arrmixAssetInfo['assets']->owner_id       =  $this->input->post('company_id');
                                        }
                                     
                                        $arrmixAssetInfo['companies']            = Company_model::fetchAllCompaniesByUserId( $this->session->userdata('user_id') );
                              }
                    } else {
               
                              $arrmixAssetInfo['assets']              = Asset_model::fetchAssetById( $intTrackerId );
									
                              //$arrmixAssetInfo['companies']       = Company_model::fetchAllCompaniesByUserId( $this->session->userdata('admin_user_id') );
                    }
                    
               //    $arrmixAssetInfo['asset_types']          = Asset_types_model::fetchAssetTypes();
                    $arrmixAssetInfo['msg'] = $this->session->userdata( 'sucmsg' );
                    $this->session->unset_userdata( 'sucmsg' );
                    
                    $this->load->view( 'admin/edit_assets', $arrmixAssetInfo );
          	           
          	} 
          	
       		 public function deleteTracker( $strAction ) { 
          	  
          	 $arrmixAssetsInfo         = array();         
	          $intTrackerId	                = ( int ) 	$this->uri->segment(4);
	          
	          //$intTrackerId	=  ( false == empty( $this->uri->segment(4) ) ) ? ( int ) $this->uri->segment(4) : NULL;
	           
          	if( false == isset($intTrackerId) || empty($intTrackerId) || 0 >= $intTrackerId ) { 
                    	    show_404();
           }
	          
	           if( true == Asset_model::delete( $intTrackerId ) ) {
	                  $this->session->set_userdata( array( 'successmsg' =>"Tracker deleted successfully." ) );
          	         
	         } else {
	                   $this->session->set_userdata( array( 'errmsg' =>"Unable to delete record." ) );
	         }
	         redirect( base_url().'admin/tracker/action/manageMobileTracker' );
          	
      } 


}