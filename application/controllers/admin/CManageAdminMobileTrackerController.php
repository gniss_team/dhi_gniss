<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageAdminMobileTrackerController extends CI_Controller {
	 
          	public function __construct() {
	          		parent::__construct();
        }
	
          	public function action( $strAction = '' ) { 
          	      
                    	switch( $strAction ) {
                               
                    	          case 'add':
                              	$this->add( $strAction );
                              		        break;
                              		        
                              	case 'edit':
                                 $this->edit( $strAction );
                              	            break;
                              	            
          	                     case 'delete':
          	                      $this->delete( $strAction );
          	                              break;
          	                              
                             	            
                              	case 'manage':     
                              	$this->manage( $strAction );
                              	         	break;
                              	         	
                          
                              	default:
                              	show_404();
                    			
                  }
                  
          	}
	
            public function add( $strAction ) { 
                     
                   $arrmixAssetInfo	     = Array();
                                    
                        if( true == is_array(  $this->input->post() ) ) {
                                
                              $this->form_validation->set_rules('name', 'Name',  'trim|required|alpha_numeric|min_length[2]|max_length[20]|xss_clean');
                              $this->form_validation->set_rules('sim_number', 'Sim number', 'trim|is_digit|exact_length[20]|is_unique[assets.sim_number]|xss_clean');
                              $this->form_validation->set_rules('asset_number', 'Asset number', 'trim|required|alpha_numeric|min_length[2]|max_length[15]|is_unique[assets.asset_number]|xss_clean');
                              $this->form_validation->set_rules('imei_number', 'IMEI number', 'trim|required|is_digit|exact_length[15]|is_unique[assets.imei_number]|xss_clean');
                              $this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|is_digit|exact_length[10]|xss_clean');
                              $this->form_validation->set_rules('model_number', 'Model number', 'trim|required|alpha_numeric|min_length[1]|max_length[20]|xss_clean');
                              $this->form_validation->set_rules('mode', ' Mode', 'required');
                              
                                     if( true == $this->form_validation->run() ) { 
                                        $arrmixAssetInfo['assets'] = Asset_model::insert( $this->input->post() );
                                        $this->session->set_userdata( array( 'successmsg' =>"Mobile Tracker added successfully" ) );
                                        redirect( base_url().'admin/tracker/action/manage' );
                            }
                    }
                    
                    $this->load->view( 'admin/add_assets', $arrmixAssetInfo );
         }
	
        	public function manage( $strAction ) {
         	      
        			$arrmixAssetsInfo		=	 array();
        			$arrmixAssetsInfo['msg']		= $this->session->userdata( 'successmsg' );
        			$this->session->unset_userdata( 'successmsg' );
        			$arrmixUserInfo['errmsg']	= $this->session->userdata( 'errmsg' );
        			$this->session->unset_userdata( 'errmsg' );
        			
               $arrmixAssetsInfo['assets']		= Asset_model::fetchAssets();
			
               $this->load->view( 'admin/manage_tracker', $arrmixAssetsInfo );
                              	
       	} 
     
         	public function edit( $strAction ) {
     
          		$intTrackerId			= ( int ) $this->uri->segment( 5 );
          		$inAdminUserId		= ( int ) $this->session->userdata('admin_user_id');
          		$arrmixAssetInfo   = array();
          		
          		$arrmixAssetInfo['assets']		= Asset_model::fetchAssetById( $intTrackerId );
          		
                           if(  true == is_array( $this->input->post() ) ) {

                              $this->form_validation->set_rules('name', 'Name',  'trim|required|alpha_numeric|min_length[2]|max_length[20]|xss_clean');
                              $this->form_validation->set_rules('sim_number', 'Sim number', 'trim|is_digit|exact_length[20]|unique_check[assets.sim_number.' . $intTrackerId .']|xss_clean');
                              $this->form_validation->set_rules('asset_number', 'Asset number', 'trim|required|alpha_numeric|min_length[2]|max_length[15]|unique_check[assets.asset_number.' . $intTrackerId .']|xss_clean');
                              $this->form_validation->set_rules('imei_number', 'IMEI number', 'trim|required|is_digit|exact_length[15]|unique_check[assets.imei_number.' . $intTrackerId .']|xss_clean');
                              $this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|is_digit|exact_length[10]|xss_clean');
                              $this->form_validation->set_rules('model_number', 'Model number', 'trim|required|alpha_numeric|min_length[1]|max_length[20]|xss_clean');
                              $this->form_validation->set_rules('mode', ' Mode', 'required');
                              
                                        if(  true == $this->form_validation->run() ) { 
                                                   if(  true == Asset_model::update( $intTrackerId, $this->input->post() ) ) { 
                                                   $this->session->set_userdata( array( 'successmsg' =>"Mobile tracker updated successfully." ) );
                                                   redirect( base_url() . 'admin/tracker/action/manage' );
                                       } else {
                                                 $this->session->set_userdata( array( 'errmsg' =>"Unable to delete record." ) );
                                       }
                              } else {
                                        			foreach( $this->input->post() as $strKey => $strValue ) {
                                        					if( $strKey == $arrmixAssetInfo['assets']->$strkey ) {
                                        						$arrmixAssetInfo['assets']->$strKey	= $strValue;	
                               					}
                                       }
                              }
                    } 
                    
                    $arrmixAssetInfo['msg'] = $this->session->userdata( 'successmsg' );
                    $this->session->unset_userdata( 'successmsg' );
                    
                    $this->load->view( 'admin/edit_assets', $arrmixAssetInfo );
          	           
          	} 
          	
       		 public function delete( $strAction ) { 
          	  
          	 $arrmixAssetsInfo         = array();         
	          $intTrackerId	                = ( int ) 	$this->uri->segment(5);
              
          		if( false == isset( $intTrackerId ) || true == empty( $intTrackerId ) || 0 >= $intTrackerId ) { 
                 show_404();
           }
	          
	           if( true == Asset_model::delete( $intTrackerId ) ) {
	                  $this->session->set_userdata( array( 'successmsg' =>"Tracker deleted successfully." ) );
          	         
	         } else {
	                   $this->session->set_userdata( array( 'errmsg' =>"Unable to delete record." ) );
	         }
	         redirect( base_url().'admin/tracker/action/manage' );
      } 
}