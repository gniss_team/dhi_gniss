<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CManageAdminController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
	}
	
	public function action( $strAction = '' ) {
		
		switch( $strAction ) {
			
    	   		case 'login':
    				$this->login();
    				break;
    			
    			case 'logout':
    				$this->logout();
    				break; 
    			
    			case	'forgotPass':
    					$this->forgotPassword();
    					break;
    					
    			case	'resetPass':
    						$this->resetPassword();
    						break;
    						
    	    	case 'viewdashboard':
    				$this->viewdashboard();
    				break;
    				case 'dashboardstats':
    					$this->dashboardstats();
    					break;
    			
    			case 'changepassword':
    				$this->changePassword();
    				break;
    			
    			
    				
    		    default:
    			  show_404();			
    	}
	}
	
	public function login() {
	
			$arrLoginErrorData = array();
			
			$arrLoginErrorData['msg']			= $this->session->userdata('successmsg');
			$this->session->unset_userdata('successmsg');
				
			$arrLoginErrorData['errmsg']			= $this->session->userdata('errmsg');
			$this->session->unset_userdata('errmsg');
		
			if( true == is_array( $this->input->post( ) ) ) {	
					$this->form_validation->set_rules( 'username', 'Username', 'trim|required|max_length[50]|xss_clean' );
					$this->form_validation->set_rules( 'password', 'Password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
					
					if( true == $this->form_validation->run( ) ) {				
				
							$strUsername = $this->input->post('username');
							$strPassword = $this->input->post('password');
							
							$strLoginType = "admin";
							
							$objUserInfo = Users_model::fetchUserByUsernameByPasswordByUserType( $strUsername, $strPassword, user_type_model::ADMIN );
							
							if(  true == is_object( $objUserInfo ) ) {
								
										$arrUserSessionData = array(
												'admin_user_id'		=> $objUserInfo->id,
												'user_name'				=> $objUserInfo->email_address,
												'username'				=> $objUserInfo->first_name,						
												'name'							=> $objUserInfo->first_name .' '. $objUserInfo->last_name,
												'status'						=> $objUserInfo->status,
												'role'								=> $objUserInfo->role,
												'user_type_id'			=> $objUserInfo->user_type_id,
												'account_number'	=> $objUserInfo->account_number,
												'is_logged_in'			=> true
										);	
										
										if( user_type_model::ADMIN == $this->session->userdata('user_type_id') && '0'	!=  $this->session->userdata('role') ) {
												$arrUserSessionData['user_permission'] = Permission_model::fetchModuleIdsByUserId( $arrUserSessionData['user_id'] );
										} 
										$this->session->set_userdata( $arrUserSessionData );
										redirect( base_url()."admin/action/viewdashboard");
									
							} else {
										$arrUserSessionData['errmsg'] = "Invalid user details";
							}
							
					}
			}
		
			$this->load->view( 'admin/admin_login', $arrLoginErrorData );
	}
	
	public function logout() {
		$this->session->sess_destroy();
		$this->input->set_cookie('cookiesample', '');
		redirect(base_url()."admin/action/login");
	}
	
	public function viewdashboard() {
		
		$this->load->view('admin/admin-dashboard');
	}
	
	public function checkOldPassword( $strPassword ) {
		$objPassword	= Users_model::checkPasswordAlreadyExists( $strPassword, 1  );
			
		if ( false == is_object( $objPassword ) ) {
	
			$this->form_validation->set_message( 'checkOldPassword', ' %s  not found in system' );
			return FALSE;
	
		} else {
			return TRUE;
		}
	}
	
	public function changePassword() {
			
		$arrmixUserInfo = array();
		$arrmixUserInfo['msg']	= $this->session->userdata( 'msg' );
	
		$this->session->unset_userdata( 'msg' );
		$this->form_validation->set_rules( 'old_password', 'Old password', 'trim|required|callback_checkOldPassword|xss_clean' );
		$this->form_validation->set_rules( 'new_password', 'New password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
		$this->form_validation->set_rules( 'confirm_password', 'Confirm password', 'trim|required|xss_clean|matches[new_password]' );
		$arrmixFormInputData = $this->input->post();
		if( true == is_array( $arrmixFormInputData ) && true == $this->form_validation->run() ) {
	
			if( true == Users_model::changePassword( user_type_model::ADMIN ) ) {
					
				$this->session->set_userdata( array( 'msg' =>"Password changed sucessfully" ) );
				redirect(  base_url().'admin/action/changepassword' );
			}
			
		}
		$this->load->view( 'admin/changepassword' , $arrmixUserInfo);
	}
		
	public function forgotPassword() {
		
			$arrmixUserInfo    = array();
			$arrmixUserInfo['msg']			= $this->session->userdata('successmsg');
			$this->session->unset_userdata('successmsg');
			
			$arrmixUserInfo['errmsg']			= $this->session->userdata('errmsg');
			$this->session->unset_userdata('errmsg');
		
			
	
			if( true == is_array( $this->input->post() ) ) {
			
					$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]|xss_clean');
		
					if(  true == $this->form_validation->run() ) {
							$objUser					= Users_model::fetchEmailAddressByForgotPasswordFieldByUserType( $this->input->post( 'username' ), user_type_model::ADMIN );
					
							if( true  == is_object( $objUser ) ) {
				
									$strRandomTempPass = md5( time() );
					
									if( true == Users_model::updateResetPasswordCode( $objUser->email_address,  $strRandomTempPass ) ) {
										
											$strUrl					= base_url()."admin/action/resetPass/" . $strRandomTempPass;
										 	$strMessage		= 'Please click on the following link for reset your password.'.
																					'<br><br>Username : '.  $objUser->username .
																					'<br><br>'.'<a href='.$strUrl.'>Reset Password ';
											
											$arrstrEmailFormat = array(
													'header' 	=> 'Dear Recipient,',
													'content'	=> $strMessage
											);
										
											if( true == $this->sendMail( 'no_reply@rectusenergy.com',  $objUser->email_address,  'Reset Password', $arrstrEmailFormat ) ) {
													$this->session->set_userdata( array( 'successmsg' => "Reset password link sent to  your email address. <br/> Please check the email to reset password." ) );
													redirect( base_url() . 'admin/action/login');
											} else {
												
													$this->session->set_userdata( array( 'errmsg' => "Could not send mail to user." ) );
													redirect( base_url() . 'admin/action/login');
											}
										
									} else {
											$arrmixUserInfo['errmsg']	= 'Unable to update reset password token.';
											$this->load->view( 'admin/forgotpassword', $arrmixUserInfo );
									}
				
							} else {
									$arrmixUserInfo['errmsg']	= 'Usename not found.';
								
							}
					}
			}
			
			$this->load->view( 'admin/forgotpassword', $arrmixUserInfo );
		}
		
		
	public function dashboardstats() {  
			 $userMixarr = array();
			$userMixarr['usercnt'] 	= Users_model::getTotalUserCnt ( );
			$userMixarr['customers'] 	= Users_model::fetchLatestUsers( );
			$userMixarr['productcnt'] 	= Devices_model::getProductcnt( );
			
			
		echo 	json_encode($userMixarr);
			
		}
		
	public function resetPassword() {
		
			$strToken	= $this->uri->segment(4);
			$objUser 	= Users_model::fetchTokenExpirationTimeByResetPasswordCode( $strToken );
		
			if( true == is_array( $this->input->post() ) ) {
				 
				$this->form_validation->set_rules( 'password', 'New Password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
				$this->form_validation->set_rules( 'passconf', 'Confirm password', 'trim|required|matches[password]|min_length[6]|max_length[20]|xss_clean' );
				 
				if( true == $this->form_validation->run() ) {
					$objUser = Users_model::fetchTokenExpirationTimeByResetPasswordCode( $strToken );
		
					if( true == is_object( $objUser ) ) {
		
						if( true == Users_model::updatePassword( $strToken  ) ) {
								$this->session->set_userdata( array( 'successmsg' => "Password has been reset successfully." ) );
								redirect( base_url() . 'admin/action/login');
						} else {
								$this->session->set_userdata( array( 'successmsg' => "Unable to update password" ) );
								redirect( base_url() . 'admin/action/login');
						}
					} else {
						$this->session->set_userdata( array( 'errmsg' => "Token not found." ) );
					}
				} 
			} else {
					if( true == is_object( $objUser ) ) {
							if( time() > $objUser->token_expiration_time ) {
								$this->session->set_userdata( array( 'errmsg' => "Your reset password link is expired" ) );
								redirect( base_url() . 'admin/action/forgotPass');
							}
					} else {
							$this->session->set_userdata( array( 'errmsg' => "Your reset password link is expired" ) );
							redirect(  base_url() . 'admin/action/forgotPass');
					}
			}
		
			$arrmixUserInfo['ver_code']	=$strToken;
			$this->load->view( 'admin/resetpassword', $arrmixUserInfo );
		}
		
	function sendMail( $strEmailFrom, $strEmailTo, $strSubject, $arrstrEmailFormat ) {
		
			$this->load->library( 'email' );
			$this->load->library( 'parser' );
			$this->email->mailtype	= 'html';
		
			$this->email->from( $strEmailFrom, 'Rectus Energy pvt ltd' );
			$this->email->to( $strEmailTo );
			$this->email->subject( $strSubject );
		
			$strEmailContent = $this->parser->parse( 'email_template',$arrstrEmailFormat, TRUE );
			$this->email->message( $strEmailContent );
		
			return $this->email->send();
		}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */