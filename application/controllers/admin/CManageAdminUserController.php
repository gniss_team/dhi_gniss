<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ini_set('display_errors', 1);

class CManageAdminUserController extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function action( $strAction = '' ) {
		
		switch ( $strAction ) {
		
				case 'addUser' :
					$this->addUser();
					break;
	
				case 'view' :
					$this->view();
					break;
					
// 				case 'viewUser' :
// 						$this->view();
// 						break;
					
				case 'editUser' :
					$this->editUser();
					break;
				
				case 'deleteUser' :
					$this->deleteUser();
					break;
	
				case 'viewCustomers' :
					$this->viewCustomers();
					break;
					
					case 'viewPlaces' :
						$this->viewPlaces();
						break;
					
	
				case 'editCustomer' :
					$this->editCustomer();
					break;
			
				case 'activate' :
					$this->activateUser();
					break;
					
				case 'activateDevice' :
						$this->activateDevice();
						break;
					
				case 'assign' :
					$this->loadModule( $strAction );
					break;
				
				case 'show' :
					$this->show( $strAction );
					break;
						 
				default:
					show_404();
				
		}

	}

	public function addUser() {
		$this->form_validation->set_rules( 'first_name', 'First name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
		$this->form_validation->set_rules( 'last_name', 'Last name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
		$this->form_validation->set_rules( 'email_address', 'Email address', 'trim|required|valid_email|is_unique[users.email_address]|max_length[50]|xss_clean' );
		$this->form_validation->set_rules( 'mobile_number', 'Mobile number', 'trim|required|numeric|exact_length[10]|mobile_zero|xss_clean' );
		$this->form_validation->set_rules( 'username', 'Username', 'trim|required|min_length[3]|max_length[15]|validate_username|non_numeric_name|non_character_name|consicutive_periods_underscore|is_unique[users.username]|xss_clean' );
		$this->form_validation->set_rules( 'role', 'Role', 'trim|required|xss_clean' );
		
		if( true == $this->form_validation->run() ) {
					
			$strToken = md5( time() );
			$strAccountNumber = Users_model::checkforaccountNumber();
						
			$randomPassword = $this->generateRandomPassowrd();
			
			
			if( true == Users_model::insert( $strToken , $strAccountNumber, $randomPassword) ) {
					$intUserId = $this->db->insert_id();
					$arrmixUserInfo['msg'] = "User added successfully.";
					$arrobjModules		= Permission_model::fetchModulesByRoleId( $this->input->post('role') );
				
					$boolIsInsert	=	 false;
					foreach( $arrobjModules as $intKey => $objModule ) {
								if( true == Permission_model::insert( $objModule->module_id, $intUserId )) {
									$boolIsInsert	= true;
								}
					}
					
					$arrmixUserInfo['msg'] .= ( true == $boolIsInsert ) ? "<br/>Module permisions assigned to user successfully" : "";
				 	$strMessage		= 'You have chosen:<br><br>'.
														'Gniss Id :' . $strAccountNumber. '<br><br>'.
														'Password :' . $randomPassword. '<br><br>'.
														'Email Id :' . $this->input->post( 'email_address' ). '<br><br>'.
														'A few more steps will connect you to the world of GNISS. Click <a href=' . base_url().'users/action/verify/' . $strToken . '>here </a>to complete your registration and get started.'. '<br><br>'.
														'Should you experience any trouble setting up, please contact us at info@rectusenergy.com' .'<br><br>'.
														'If you did not create this ID on GNISS and think that someone else has used your email ID, please click <a href=' . base_url().'>here </a>to remove your email ID from this account' ;
					
				 	$strHeaderMsg	= '<div style="font-size:1.5em;">Welcome to GNISS!</div><br><br/>'.
						 										'Dear Recipient,<br><br>';
				 	$arrstrEmailFormat = array(
		 					'header' 	=> $strHeaderMsg,
		 					'content'	=> $strMessage
				 	);
				 	
					if( true == $this->sendMail( 'no_reply@rectusenergy.com', $this->input->post('email_address'),  'Thank you for Your Registration', $arrstrEmailFormat ) ) {
							$this->session->set_userdata( array( 'successmsg' => "You have registered successfully, Please check your email." ) );
					} else {
							$this->session->set_userdata( array( 'msg' => "Could not send mail to user." ) );
					}
					redirect('admin/user/action/view');
			} else{
					$arrmixUserInfo['errmsg']		= "User cannot be added.";
			}
			
			redirect('admin/user/action/view');
		} else {
				//redirect('admin/user/action/view');
		}
		$data['roles'] = Users_model::fetchRoles();
		$this->load->view('admin/add_user', $data);

	}

	public function editUser() {
		$intUserId			= ( int ) $this->uri->segment( 5 );
		$data['user'] = Users_model::fetchUserById($intUserId);
		if( true == is_array(  $this->input->post() ) ) {

			$this->form_validation->set_rules( 'first_name', 'First name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
			$this->form_validation->set_rules( 'last_name', 'Last name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
			$this->form_validation->set_rules( 'email_address', 'Email address', 'trim|required|valid_email|max_length[50]|xss_clean' );
			$this->form_validation->set_rules( 'mobile_number', 'Mobile number', 'trim|required|numeric|exact_length[10]|mobile_zero|xss_clean' );
			$this->form_validation->set_rules( 'username', 'Username', 'trim|required|min_length[3]|max_length[15]|validate_username|non_numeric_name|non_character_name|consicutive_periods_underscore|xss_clean' );
			$this->form_validation->set_rules( 'role', 'Role', 'trim|required|xss_clean' );
			
			if( true == $this->form_validation->run() ) {
				if( true ==  Users_model::update_user( $intUserId, $this->input->post() ) ) {
					$this->session->set_userdata( array( 'msg' =>"Record updated successfully" ) );
					redirect('admin/user/action/view');
    			}
			} else {
				$this->session->set_userdata( array( 'msg' =>"Record not updated successfully!" ) );		
			}
		}
		$data['roles'] = Users_model::fetchRoles();
		$this->load->view('admin/update_user', $data);
	}

	public function deleteUser() {
		$intUserId			= ( int ) $this->uri->segment( 5 );
		$userType			= $this->uri->segment( 6 );
		$res = Users_model::deleteUserById( $intUserId );

		if( true == Users_model::deleteUserById( $intUserId ) ) {
					
	    	$this->session->set_userdata( array('msg' =>"Record deleted successfully.") );

		} else {

			$this->session->set_userdata( array('msg' =>"Record cannot be deleted.") );

		}
		if( $userType ) {
			redirect('admin/user/action/viewCustomers');
		} else {
			redirect('admin/user/action/view');	
		}
		
	}

	public function generateRandomPassowrd() {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$password = substr( str_shuffle( $chars ), 0, 8 );
    	return $password;
	}

	public function sendMail( $strEmailFrom, $strEmailTo, $strSubject, $arrstrEmailFormat ) {

			$this->load->library( 'email' );
			$this->load->library( 'parser' );
			$this->email->mailtype	= 'html';
			
			$this->email->from( $strEmailFrom, 'Rectus Energy pvt ltd' );
			$this->email->to( $strEmailTo );
			$this->email->subject( $strSubject );
			
			$strEmailContent = $this->parser->parse( 'email_template',$arrstrEmailFormat, TRUE );
			$this->email->message( $strEmailContent );
			
			return $this->email->send();
	}

// 	public function viewUser() {
		
// 			$result	= array();
// 			$intRoleId = NULL;
// 			if( true == is_array( $this->input->post()) ) {
		
// 					$filter_data = $this->input->post();
				
// 					if( true == is_array( $filter_data ) ) {
// 							$this->session->set_userdata( array('role_id' => $filter_data['select_role']) );
// 							$intRoleId	=	$this->session->userdata( 'role_id' );
// 					} else {
// 							$this->session->unset_userdata('role_id');
							
// 					}
// 			}
// 			//$result['role_id']	= $intRoleId;
// 			$result['users'] = Users_model::fetchUsersByUserIdByRoleIdByUserTypeId( $this->session->userdata( 'admin_user_id' ), $intRoleId  );
// 			$roles = Users_model::fetchRoles();
// 			$tmp_arr = array();
// 			foreach ($roles as $key => $role) {
// 					$tmp_arr[ $role->id ] = $role->name;
// 			}
// 			$result['roles'] = $tmp_arr;
// 			$this->load->view('admin/view_users', $result);
// 	}
	
	public function view() {
	
	
		if( $this->input->post() ) {
				
			$filter_data = $this->input->post();
				
			if( isset($filter_data) ) {
					$this->session->set_userdata( array('role_id' => $filter_data['select_role']) );
			} else {
					$this->session->unset_userdata('role_id');
			}
				
		}
	
		$result['users'] = Users_model::fetchUsers();
		$roles = Users_model::fetchRoles();
		$tmp_arr = array();
		foreach ($roles as $key => $role) {
				$tmp_arr[ $role->id ] = $role->name;
		}
		$result['roles'] = $tmp_arr;
	
		$this->load->view('admin/view_users', $result);
	}
	
	public function editCustomer() {
		$intUserId			= ( int ) $this->uri->segment( 5 );
		$data['user'] = Users_model::fetchUserById($intUserId);
		if( true == is_array(  $this->input->post() ) ) {

			$this->form_validation->set_rules( 'first_name', 'First name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
			$this->form_validation->set_rules( 'last_name', 'Last name', 'trim|required|min_length[2]|max_length[15]|alpha|xss_clean' );
			$this->form_validation->set_rules( 'email_address', 'Email address', 'trim|required|valid_email|max_length[50]|xss_clean' );
			$this->form_validation->set_rules( 'mobile_number', 'Mobile number', 'trim|required|numeric|exact_length[10]|mobile_zero|xss_clean' );
			$this->form_validation->set_rules( 'username', 'Username', 'trim|required|min_length[3]|max_length[15]|validate_username|non_numeric_name|non_character_name|consicutive_periods_underscore|xss_clean' );			
			
			if( true == $this->form_validation->run() ) {
				if( true ==  Users_model::update_user( $intUserId, $this->input->post() ) ) {
					$this->session->set_userdata( array( 'msg' =>"Record updated successfully" ) );
					redirect('admin/user/action/viewCustomers');
    			}
			} else {
				$this->session->set_userdata( array( 'msg' =>"Record not updated successfully!" ) );		
			}
		}
		$this->load->view('admin/update_customer', $data);
	}

	public function viewCustomers() {
		
			$result['users'] = Users_model::fetchCustomers();

			$this->load->view('admin/view_customers', $result);
	}
	
	public function viewPlaces() {
	
		$result['users'] = Users_model::fetchCustomers();
	
		$this->load->view('admin/view_places', $result);
	}
	
	
	public function activateUser() {
	
			$intUserId			= ( int ) $this->uri->segment( 5 );
			$intStatus			= ( int ) $this->uri->segment( 6 );
			
			$intActiveStatus	=( '0' == $intStatus ) ?  '1' : '0' ;
			
			if( true ==  Users_model::updateActiveStatus( $intUserId, $intActiveStatus ) ) {
					If( 1 == $intActiveStatus ) {
							$this->session->set_userdata( array( 'msg' =>"User deactivated successfully" ) );
					} else {
							$this->session->set_userdata( array( 'msg' =>"User activated successfully" ) );
					}
					redirect('admin/user/action/view');
    		}
	
	}
	
	public function activateDevice() {
	
			$intCustomerId		= ( int ) $this->uri->segment( 5 );
			$intDeviceId				= ( int ) $this->uri->segment( 6 );
			$intStatus					= ( int ) $this->uri->segment( 7 );
				
			$intActiveStatus		=( '1' == $intStatus ) ?  '0' : '1' ;
				
			if( true ==  Devices_model::updateActiveStatus( $intDeviceId, $intActiveStatus ) ) {
				
					If( 0 == $intActiveStatus ) {
						$this->session->set_userdata( array( 'msg' =>"Device deactivated successfully" ) );
					} else {
						$this->session->set_userdata( array( 'msg' =>"Device activated successfully" ) );
					}
					redirect('admin/user/action/show/'. $intCustomerId );
			}
	
	}

	public function show() {
	
			$intCustomerId 		= $this->uri->segment(5);
			$arrmixCustomerDevices['devices'] = Devices_model::fetchDeviceTypeByUserId( $intCustomerId );
				
			if( true == $this->session->userdata('sucmsg') ) {
				$arrmixCustomerDevices['msg']	= $this->session->userdata('sucmsg');
				$this->session->unset_userdata('sucmsg');
			} else {
				$arrmixCustomerDevices['errmsg']	= $this->session->userdata('errmsg');
				$this->session->unset_userdata('sucmsg');
			}
			
			$this->load->view( 'admin/view_customer_devices', $arrmixCustomerDevices );
	}
	
	public function loadModule() {

			$intUserId 					 			= $this->uri->segment(5);
			//$arrmixModuleInfo['permission_info'] 	= Permission_model::fetchPermissionById( $intUserId );
			//$boolIsAdmin	= ( true == is_bool( $this->session->userdata('customer_id') ) ) ? '0' : '1';
			$arrmixModuleInfo['user_modules']		= Permission_model::fetchPermissionById( $intUserId, '0' );
			$arrmixModuleInfo['user_id'] 					= $intUserId;
		
			$this->session->unset_userdata('sucmsg');
			if( true == is_array( $this->input->post() ) ) {	//print_r($this->input->post('permission'));die;
		
					if( true == is_array($this->input->post('permission'))) {
							if( 0 < Permission_model::fetchAllpermissionByUserId( $intUserId )) {
									Permission_model::delete( $intUserId );
							}
							
							foreach( $this->input->post('permission') as $strKey => $intModuleId) {
									Permission_model::insert($intModuleId,$intUserId);
							}
					}
					$strUserName = $arrmixModuleInfo['user_info'][0]->first_name . " " . $arrmixModuleInfo['user_info'][0]->last_name;
					$this->session->set_userdata( array( 'msg' => "Module permision assigned to user successfully." ) );
					redirect('admin/user/action/assign/'. $intUserId );
			}
		
			$this->load->view( 'admin/assign-permission', $arrmixModuleInfo );
	}
	
}