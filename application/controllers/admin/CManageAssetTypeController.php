<?php 
if ( ! defined( 'BASEPATH' )) exit( 'No direct script access allowed' );

class CManageAssetTypeController extends CI_Controller {

	public function __construct() {
        parent::__construct();       
     }
	
	public function action( $strAction = '' ) {
		 
		switch ( $strAction ) {
			case 'addassettype' :
				$this->addAssetType( $strAction );
				break;
				 
			case 'editassettype' :
				$this->editAssetTypes( $strAction );
				break;
	
			case 'deleteassettype' :
				$this->delete( $strAction );
				break;
	
			case 'manageassettype' :
				$this->manageAssetTypes( $strAction );
				break;
	
			default :
				$this->createAssetType();
		}
	}
		
    public function manageAssetTypes() {	
		
		$arrmixAssetTypeData['count_assettype']	= Asset_types_model::getCountAssetTypes();
		$arrmixAssetTypeData['assettype'] 		= Asset_types_model::getAssetTypes();
		$arrmixAssetTypeData['msg']				= $this->session->userdata( 'successmsg' );
		
		$this->session->unset_userdata( 'successmsg' );
		$this->load->view( 'admin/view_asset_type', $arrmixAssetTypeData );	
	}
	
	public function applyValidation( $strAction ) {
		 
		if( 'add' == $strAction ) {
			$this->form_validation->set_rules( 'typename', 'Name', 'required|alpha_numeric|is_unique[asset_types.name]|min_length[2]|max_length[15]' );
			$this->form_validation->set_rules( 'typedescription', 'Description', 'required|min_length[2]|max_length[255]' );
			 
		} else {
			$intAssetTypeId 		= $this->uri->segment(4);
			$this->form_validation->set_rules( 'typename', 'Name', 'required|callback_edit_unique[asset_types.name.' . $intAssetTypeId .']|min_length[2]|max_length[15]|alpha_numeric|trim' );
			$this->form_validation->set_rules( 'typedescription', 'Description', 'required|min_length[2]|max_length[255]|trim' ); 
		}
		
		if( TRUE == $this->form_validation->run() ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function addAssetType( $strAction = "" ) {
		
		if('addassettype' == $strAction && $this->applyValidation( $strAction )){
			
			if( true == Asset_types_model::insert() ) {	
				$this->session->set_userdata( array( 'successmsg' =>"Asset type added successfully." ) );
				redirect( 'assettype/action/manageassettype' );
			} else {
			   	$this->session->set_userdata( array( 'successmsg' =>"Asset type cannot be added." ) );
			   	redirect( 'assettype/action/manageassettype' );
		 	}
		 	
		}
	    $this->load->view( 'admin/add_asset_type' );
	}
	
	public function editAssetTypes( $strAction ) {
		
		$intAssetTypeId 					= $this->uri->segment(4);
		$arrMixAssetData['assettype_info'] 	= Asset_types_model::getAssetTypes( $intAssetTypeId );
	
		if ( true == $this->input->post() ) {
			 if( true == $this->applyValidation( $strAction ) ) { 
				if( true == Asset_types_model::update( $intAssetTypeId )) {
					$arrMsg	= array( 'successmsg' =>"Asset type updated successfully." );
				} else {
			   		$arrMsg	= array( 'successmsg' =>"Asset type cannot be updated." );
				}
				$this->session->set_userdata( $arrMsg );
				redirect( 'assettype/action/manageassettype' );
			 }
			else {
				$arrMixAssetData['assettype_info'][0]->name = $this->input->post('typename');
				$arrMixAssetData['assettype_info'][0]->description = $this->input->post('typedescription');
			} 
		}
		$this->load->view( 'admin/edit_asset_type', $arrMixAssetData );
	}
	
	public function delete( $strAction ) {
    	
    	$intAssetTypeId 		= $this->uri->segment(4);
    
        if( true == Asset_types_model::delete( $intAssetTypeId ) ) {
        	$this->session->set_userdata( array( 'successmsg' => "Asset type deleted successfully." ) );    
	   	} else {
	   		$this->session->set_userdata( array( 'successmsg' => "Asset type cannot be deleted." ) );
	   	}
	   	redirect( 'assettype/action/manageassettype' );
	}
	
	public function createAssetType() {
	
		$arrmixAssetInfo = array();
		$this->load->view( 'admin/add_asset_type', $arrmixAssetInfo );
	}
	
	public function edit_unique ( $strFieldValue, $strFieldParameter ) {
	
		$this->form_validation->set_message( 'edit_unique', '%s already exists.' );
	
		list( $strTableName, $strFieldName, $intId ) = explode( ".", $strFieldParameter, 3 );
	
		$objDbResult = $this->db->select( $strFieldName )->from( $strTableName )->where( $strFieldName, $strFieldValue )->where( 'id !=', $intId )->limit(1)->get();
	
		if ( true == ( boolean ) $objDbResult->row() ) {
			return false;
		} else {
			return true;
		}
	}
}