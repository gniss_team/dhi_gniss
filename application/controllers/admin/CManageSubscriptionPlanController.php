<?php if ( ! defined('BASEPATH') ) exit( 'No direct script access allowed' );

class CManageSubscriptionPlanController extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function action( $strAction = '' ) {
		
		switch ( $strAction ) {
			
			case 'add' :
					$this->add( $strAction );
					break;
					
				case 'edit' :
					$this->edit( $strAction );
					break;
	
				case 'delete' :
					$this->delete( $strAction );
					break;
				
				case 'view' :
					$this->view( $strAction );
					break;
					
			
				default :
					$this->create();
		}
	}
	
	public function applyValidation( $strAction ) {
		
			if( 'add' == $strAction ) {
					$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[2]|max_length[20]|alpha_dot_space|xss_clean');
			} 
			if( 'edit' == $strAction ) {
					$intModuleId 	= $this->uri->segment( 5 );
					$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[2]|max_length[20]|alpha_dot_space|xss_clean');
			}
			$this->form_validation->set_rules( 'price', 'Price', 'trim|is_decimal|xss_clean' );
			$this->form_validation->set_rules( 'subscription_length', 'Subscription length', 'trim|required|is_digit|xss_clean' );
			$this->form_validation->set_rules( 'description', 'Module description', 'alpha_space_dot|min_length[2]|max_length[100]trim|max_length[100]|xss_clean');
			
			if( TRUE == $this->form_validation->run() ) {
					return true;
			} else {
					return false;
			}
	}
	
	public function view() {

			$arrmixSubscriptionPlans['subscription_plan'] = Subscription_plan_Model::fetchSubscriptionPlans();
			
			if( true == $this->session->userdata('sucmsg') ) {
					$arrmixSubscriptionPlans['msg']	= $this->session->userdata('sucmsg');
					$this->session->unset_userdata('sucmsg');
			} else {
					$arrmixSubscriptionPlans['errmsg']	= $this->session->userdata('errmsg');
					$this->session->unset_userdata('sucmsg');
			}
			$this->load->view( 'admin/view_subscription_plan', $arrmixSubscriptionPlans );
	}
	
	public function add( $strAction = "" ) {
			
			$arrmixSubscriptionPlans		= array();
			
			if( true == is_array( $this->input->post() ) && "add" == $strAction ) {
				
					if( true  == $this->applyValidation( $strAction ) ) {
						
								$config['upload_path'] = 'uploads/';
							
								$this->load->library('upload', $config);
								$this->upload->initialize($config);
								$this->upload->set_allowed_types('gif|jpg|png|jpeg');
								
								if( true == $this->upload->do_upload('thumbnail_image') ) {
									
											$data = array('msg' => "Subscription plan uploaded successfully.");
											$data['upload_data'] 	= $this->upload->data();
											$strPlanImg			= $this->upload->file_name;
										
								} else {
									
											$strPlanImg	= NULL;
											$arrmixUpoadfile =  $_FILES['thumbnail_image'];
											
											if( false == empty( $arrmixUpoadfile['name'] )) {
													$data = array('upload_error' => $this->upload->display_errors());
													//$this->session->set_userdata( array( 'msg' =>$this->upload->display_errors() ) );
											}
								}
													
								if( true == Subscription_plan_Model::insert( $strPlanImg ) ) {
										$this->session->set_userdata( array('sucmsg' =>"Subscription plan added successfully") );
									
								} else {
										$this->session->set_userdata( array('sucmsg' =>"Subscription plan cannot be added") );
								}
								redirect('admin/plan/action/view');
						} 
						
				} 
			
				$this->load->view( 'admin/add_subscription_plan', $arrmixSubscriptionPlans );
	}
	
	public function edit( $strAction ) {
		
		$intSubscriptionPlanId 		= $this->uri->segment(5);
		$arrmixPlanInfo['subscription_plan'] = Subscription_plan_Model::fetchSubscriptionPlanById( $intSubscriptionPlanId );

		if( true == is_array( $this->input->post() ) ) {
					
					if( true  == $this->applyValidation( $strAction ) ) {
							
									$config['upload_path'] = 'uploads/';
									$this->load->library('upload', $config);
									$this->upload->initialize($config);
									$this->upload->set_allowed_types('gif|jpg|png|jpeg');
									$msg	= '';
										
									if( true == $this->upload->do_upload('thumbnail_image') ) {
										
												if( $this->input->post('existing_plan_image') != "" && false == @unlink( 'uploads/'. $arrmixModuleInfo['subscription_plan'][0]->thumbnail_image ) ) {
														$msg	.= "Error deleting file, ";
									   		}
									   			
												$msg	.= "Subscription plan image uploaded successfully.";
												$data = array( 'msg' => $msg );
												$data['upload_data'] 	= $this->upload->data();
												$strPlanImg				= $this->upload->file_name;
									} else {
												$data = array('upload_error' => $this->upload->display_errors());
												$strPlanImg	= $this->input->post('existing_plan_image'); 
												//$this->session->set_userdata( array( 'msg' =>$this->upload->display_errors() ) );
									}
							
									if( true == Subscription_plan_Model::update( $intSubscriptionPlanId, $strPlanImg ) ) {
												$this->session->set_userdata( array( 'sucmsg' =>"Subscription plan updated successfully.") );
									} else {
												$this->session->set_userdata( array( 'sucmsg' =>"Subscription plan cannot be updated.") );
									}
									redirect('admin/plan/action/view');
					} 
		   } 
		   $this->load->view( 'admin/add_subscription_plan', $arrmixPlanInfo );
	}
	
	public function delete( $strAction ) {
			
		 $intSubscriptionPlanId 	= $this->uri->segment(5);
		
			if( true == Subscription_plan_Model::delete( $intSubscriptionPlanId ) ) {
					//$objPlan = Modules_Model::fetchModuleById( $intModuleId );
					$this->session->set_userdata( array( 'sucmsg' => "Subscription plan deleted successfully." ) );
			} else {
					$this->session->set_userdata( array( 'errmsg' => "Subscription plan cannot be deleted." ) );
			}
			redirect('admin/plan/action/view');
   }
		
	public function create() {
	
			$arrmixSubscriptionPlans = array();
			$this->load->view( 'admin/add_subscription_plan', $arrmixSubscriptionPlans );
			
	}
	
}