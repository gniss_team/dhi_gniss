<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageAdminEnergyMeterController extends CI_Controller {
	 
      public function __construct() {
			parent::__construct();
			
			$this->load->dbforge();
     }
	
      public function action( $strAction = '' ) { 
          	     
                    	switch( $strAction ) {
                               
                             	case 'viewTableColumns': 
                              	$this->viewTableColumns( );
                              				break;
                              	
                              	case 'addTableColumn':	
                              	$this->addTableColumn( );
                              				break;
                              	
                              	default:
                              	show_404();
                    			
                  }
                  
          	}
      
     public function viewTableColumns( ) {
    
     		$arrmixTableInfo['table_fields']		= energy_meter_model::fetchTableFieldsByTableName( 'energy_meter_data' );
      	$this->displayView( $arrmixTableInfo );
    }
       
      public function addTableColumn() {
      
			$arrmixTableInfo	=	 array(); 

     		if( true == is_array( $this->input->post() ) ) {	
	
	       		$this->form_validation->set_rules( 'field_name', 'Field name', 'trim|required|min_length[2]|max_length[20]|alpha_hyphen|xss_clean' );
	      	
	       		 	if( true == $this->form_validation->run( ) ) {	
	       		 		
	       		 		$arrmixTableInfo['table_fields']		= energy_meter_model::fetchTableFieldsByTableName( 'energy_meter_data' );
	       		 		
	       		 		   if( true == in_array( $this->input->post( 'field_name' ), $arrmixTableInfo['table_fields']  )  ) {
	       		 		   	$arrmixTableInfo['errmsg']	= " Column is already exists in a table.";
	       		 				$this->displayView( $arrmixTableInfo );
			      					return;
		       		 	} 
	       		 			if( 0 != energy_meter_model::getColumnCountByColumnName( strtoupper( $this->input->post( 'field_name' ) ) ) ) {
	       		 				$arrmixTableInfo['errmsg']	= " You cannot add column name as Mysql  keyword, Please choose another name.";
	       		 				
	       		 				$this->displayView( $arrmixTableInfo );
			      					 return;
	       		 		} 
	     		 			if( true == energy_meter_model::alterTableColumnByColumnName( $this->input->post( 'field_name' ) ) ) {
	     		 					$arrmixTableInfo['msg']	= "Column added successfully.";
	     		 					unset( $arrmixTableInfo['table_fields'] );
	     		 					redirect( base_url() . 'admin/energymeter/action/viewTableColumns' );

	      		 		} else {
	      		 				$arrmixTableInfo['errmsg']	= "Unable to add column.";
	      		 				$this->displayView( $arrmixTableInfo );
			      					 return;
	 	      			}
			      
		       	} else {
			   			
			      		$this->load->view( 'admin/add_table_column', $arrmixTableInfo );
		       	}
     		} else {
			   			
			      		$this->load->view( 'admin/add_table_column', $arrmixTableInfo );
		    }
     
     }
     
     public function displayView( $arrmixTableInfo = array() ) {
     		
     			$arrmixTableInfo['table_fields']		= energy_meter_model::fetchTableFieldsByTableName( 'energy_meter_data' );
     		
      		$this->load->view( 'admin/view_table', $arrmixTableInfo );
     }
     
}