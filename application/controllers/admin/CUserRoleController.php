<?php 
if ( ! defined( 'BASEPATH' )) exit( 'No direct script access allowed' );

class CUserRoleController extends CI_Controller {

	public function __construct() {
        parent::__construct();
      $this->load->model('admin/user_role_model');
    
	}
	
	public function action( $strAction = '' ) {
		 
		switch ( $strAction ) {
			
			case 'add' :
				$this->add( $strAction );
				break;
				 
			case 'edit' :
				$this->edit( $strAction );
				break;
	
			case 'update' :
				$this->update( $strAction );
				break;
				
			case 'delete' :
				$this->delete( $strAction );
				break;
	
			case 'view' :
				$this->view( $strAction );
				break;
			
			case 'assign' :
					$this->loadModule( $strAction );
					break;
		
			default :
				$this->view();
		}
	}
		
	 public function displayView( $strViewName, $arrmixRoles = array() ) {
		 	
			$arrmixRoles['msg']	= $this->session->userdata('sucmsg');
		 	$this->session->unset_userdata('sucmsg');
		 		
		 	$arrmixRoles['errmsg']	= $this->session->userdata('errmsg');
		 	$this->session->unset_userdata('errmsg');
	 		$this->load->view( $strViewName, $arrmixRoles );
	}
	 
    public function view() {	

			$arrmixRoles	= array();
			$arrmixRoles['roles']			= User_role_model::fetchUserRoles();
			$this->displayView( 'admin/view_user_role', $arrmixRoles );
			
	}
	
	public function applyValidation( $strAction ) {
		 
			if( 'add' == $strAction ) {
		
					$this->form_validation->set_rules( 'name', 'Name', 'trim|required|min_length[2]|max_length[15]|alpha_numeric|is_unique[user_role.name]xss_clean|' );
					$this->form_validation->set_rules( 'description', 'Description', 'trim|min_length[2]|max_length[255]' );
				 
			} 
			
			if( 'edit' == $strAction ){
				
					$intUserRoleId 		= $this->uri->segment(4);
					$this->form_validation->set_rules( 'name', 'Name', 'trim|required|callback_validate_unique_check[user_role.name.' . $intUserRoleId .']|min_length[2]|max_length[15]|alpha_numeric|xss_clean' );
					$this->form_validation->set_rules( 'description', 'Description', 'trim|min_length[2]|max_length[255]' ); 
			}
			
			if( TRUE == $this->form_validation->run() ) {
					return true;
			} else {
					return false;
			}
		
	}
	
	public function add( $strAction = "" ) {
	
		if( 'add' == $strAction && $this->applyValidation( $strAction )){
			
					if( true == User_role_model::insert() ) {	
							$this->session->set_userdata( array( 'sucmsg' =>"User role added successfully." ) );
						
					} else {
					   	$this->session->set_userdata( array( 'errmsg' =>"User role cannot be added." ) );
				 	}
				 	redirect( 'admin/roles/action/view' );
		}
	    $this->load->view( 'admin/add_user_role' );
	}
	
	public function edit( $strAction ) {
		
			$intUserRoleId 		= $this->uri->segment(5);
	
			$arrmixUserRoleInfo['role'] 		= User_role_model::fetchUserRoleById( $intUserRoleId );
			
			if( true == is_array( $this->input->post() ) ) {  
						
					 if( true == $this->applyValidation( $strAction ) ) { 
					 			
								if( true == User_role_model::update( $intUserRoleId )) {
										$this->session->set_userdata( array( 'sucmsg' =>"User role updated successfully."  ) );
								} else {
							   		$this->session->set_userdata( array( 'errmsg' =>"User role cannot be updated." ) );
								}
								redirect( 'admin/roles/action/view' );
					}	
			}
			$this->load->view( 'admin/edit_user_role', $arrmixUserRoleInfo );
	}
	
	//remove this function
	public function duplicatetypename_check( $strFieldname ) {
		
		$intDeviceTypeId	= $this->uri->segment(3);
		$intRowCount	 	= Device_types_model::checkDuplicateDeviceType( $strFieldname, $intDeviceTypeId );
		if( 0 < $intRowCount ) {
			$this->form_validation->set_message('duplicatetypename_check', 'Device type already exists.');
			return false;
		} else {
			return true;
		}
	}

    public function delete( $strAction ) {
    	
	    	$intUserRoleId 		= $this->uri->segment( 5 );
	    
	       	 if( true == User_role_model::delete( $intUserRoleId ) ) {
	       	 	$this->session->set_userdata( array( 'sucmsg' => "User role deleted successfully." ) );    
		   } else {
		   		$this->session->set_userdata( array( 'errmsg' => "User role cannot be deleted." ) );
		   }
	   	redirect( 'admin/roles/action/view' );
	}
	
	public function loadModule() {
	
		$intRoleId 					 			= $this->uri->segment(5);
	
		$arrmixModuleInfo['modules']		= Permission_model::fetchPermissionByRoleId( $intRoleId, '0' );
		
		$arrmixModuleInfo['role_id'] 			= $intRoleId;
	
		$this->session->unset_userdata('sucmsg');
		if( true == is_array( $this->input->post() ) ) {	//print_r($this->input->post('permission'));die;
	
				if( true == is_array($this->input->post('permission'))) {
					
						if( 0 < Permission_model::fetchAllpermissionByRole( $intRoleId )) {
								Permission_model::deleteRole( $intRoleId );
						}
							
						foreach( $this->input->post('permission') as $strKey => $intModuleId) {
								Permission_model::insertRole( $intModuleId, $intRoleId );
						}
				}
		
				$this->session->set_userdata( array( 'msg' => "Module access assigned to role successfully." ) );
				redirect('admin/roles/action/assign/'. $intRoleId );
		}
	
		$this->load->view( 'admin/assign_role_permission', $arrmixModuleInfo );
	}
	
	
	public function edit_unique ( $strFieldValue, $strFieldParameter ) {
	
		$this->form_validation->set_message( 'edit_unique', '%s already exists.' );
	
		list( $strTableName, $strFieldName, $intId ) = explode( ".", $strFieldParameter, 3 );
	
		$objDbResult = $this->db->select( $strFieldName )->from( $strTableName )->where( $strFieldName, $strFieldValue )->where( 'id !=', $intId )->limit(1)->get();
	
		if ( true == ( boolean ) $objDbResult->row() ) {
			return false;
		} else {
			return true;
		}
	}
}