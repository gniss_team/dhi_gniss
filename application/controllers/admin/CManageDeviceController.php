<?php 
if ( ! defined( 'BASEPATH' )) exit( 'No direct script access allowed' );

class CManageDeviceController extends CI_Controller {

	public function __construct() {
        parent::__construct();       
        $this->load->model('devices_model');
        
        $this->load->dbforge();
     }
	
   public function action( $strAction = '' ) {
    
    	switch ( $strAction ) {
    		case 'add' :
    			$this->addDevices( $strAction );
    			break;
    			  	
    		case 'edit' :
    			$this->editDevices( $strAction );
    			break;
    			
    		case 'deletecolumn' :
    			$this->deletecolumn( $strAction );
    			break;
    		    
    		case 'delete' :
    			$this->deleteDevices( $strAction );
    			break;
    		
    		case 'changestatus' :
    			$this->changestatus( $strAction );
    			break; 
    
    		case 'view' :
    			$this->viewDevices( $strAction );
    			break;
    			
    		case 'viewdevices' :
    			$this->viewDeviceTypes( $strAction );
    			break;
    			
   			case 'viewdevice' :
   				$this->editDevices( $strAction );
   				break;
   
    		default :
    			$this->createDevices();
    	}
    }
     public function applyValidation( $strAction ) {
    	
    	if( 'add' == $strAction ) {
    		
    		$this->form_validation->set_rules('device_name', 'Device name', 'required|alpha_space|is_unique[device_types.name]');
    	
    		
    	
    	} elseif( 'edit' == $strAction ) {
   	
    		foreach($this->input->post() as $key => $value) {
    			if($key != "addassettype"  && $key != "para") {
    				
    				$this->form_validation->set_rules($key, $key, 'required|alpha');
    			}else{
    				return true;
    			}
    		}
    			 
    	}    	
    	if( TRUE == $this->form_validation->run() ) {
			return true;
		} else {
			return false;
		}
    }
    
    public function viewDevices() {
		$arrmixDeviceInfo['devices']	= $this->devices_model->fetchAllDevices();
		$arrmixDeviceInfo['msg']		= $this->session->userdata('sucmsg');
		$this->session->unset_userdata('sucmsg');
		$this->load->view('view_devices', $arrmixDeviceInfo );
    }
    
    public function viewDeviceTypes() {
    	$arrmixDeviceInfo['device_types']	= $this->devices_model->fetchDeviceTypes();
    	$arrmixDeviceInfo['msg']			= $this->session->userdata('sucmsg');
    	$this->session->unset_userdata('sucmsg');
    	$this->load->view('admin/view_devices', $arrmixDeviceInfo );
    }
	
    public function addDevices( $strAction = "" ) {
    	
	    	if( 'add' == $strAction ) {
	    		
	    		if( true == $this->applyValidation( $strAction )) {
	    		
		    		if( true == $this->devices_model->createbleByDeviceName() ) {
		    			$this->session->set_userdata( array('sucmsg' =>"Device added successfully.") );
		    			redirect('admin/devices/action/viewdevices');
		    		} else {
		    			$this->session->set_userdata( array('sucmsg' =>"Device cannot be added.") );
		    		}
	    		}	
	    	}
    	//$arrstrDeviceTypes['device_types'] = $this->devices_model->fetchDeviceTypes();
    	$this->load->view( 'admin/add_devices' );
    }
    
    public function editDevices( $strAction ) {
    	$intDeviceTypeId 		= $this->uri->segment(5);
    	
    	// get device name/table name from device type id 
    	$device_type_name = $this->devices_model->fetchDeviceTypeName( $intDeviceTypeId );
    	// end here 
    	$arrmixDeviceInfo['devicename']=$device_type_name;
    	$arrmixDeviceInfo['deviceid']=$intDeviceTypeId;
    	$arrmixDeviceInfo['device_info'] = $this->devices_model->fetchDeviceByName( $device_type_name );
    	//$arrmixDeviceInfo['device_type'] = $this->devices_model->fetchDeviceTypes();
    //	print_r($arrmixDeviceInfo);
    	if(TRUE==is_array($this->input->post())) {
    		if( true == $this->applyValidation( $strAction )) {
    		
    			if(true == $this->devices_model->updateColumnName( $device_type_name )) {
	    		
	    			$this->session->set_userdata( array('sucmsg' =>"Device updated successfully.") );
	    		} else
	    		 {
	    			$this->session->set_userdata( array('sucmsg' =>"Device cannot be updated.") );
	    		}
	    		redirect('admin/devices/action/viewdevices');
    		}
    	}
    	$this->load->view('admin/edit_devices',$arrmixDeviceInfo);
    }
    
    public function deleteDevices( $strAction ) {
    	$intDeviceTypeId 		= $this->uri->segment(5);
    	// get device name/table name from device type id
    	$device_type_name = $this->devices_model->fetchDeviceTypeName( $intDeviceTypeId );
    	 $strDeviceName 		= $device_type_name;
		if( true == $this->devices_model->delete( $strDeviceName ) ) {
			
	    	$this->session->set_userdata( array('sucmsg' =>"Device ".$strDeviceName." deleted successfully.") );
	    		redirect('admin/devices/action/viewdevices');
		} else {
				$this->session->set_userdata( array('sucmsg' =>"Device ".$strDeviceName." cannot be deleted.") );
				redirect('admin/devices/action/viewdevices');
		}
    }
    
    
    public function changestatus( $strAction ) {
    	$intDeviceId 		= $this->uri->segment(4);
    	if( true == $this->devices_model->updtestatus( $intDeviceId ) ) {
    		redirect('devices/action/view');
    	} 
    }
    
    public function deletecolumn()
    {
    	$strColumnName = $this->uri->segment(5);
    	$strtableName  = $this->uri->segment(6);
    	
    	if($strColumnName!=""){
    
    		 $this->dbforge->drop_column( $strtableName, $strColumnName );
    	}
    	$this->session->set_userdata( array('sucmsg' =>"Device column deleted successfully.") );
    	redirect('admin/devices/action/viewdevices/');
    }
    public function createDevices() {
    
    	$arrmixPlanInfo = array();
    	$this->load->view( 'add_devices', $arrmixPlanInfo );
    }
    
    public function edit_unique ( $strFieldValue, $strFieldParameter ) {
    
    	$this->form_validation->set_message( 'edit_unique', 'The %s is already exists.' );
    
    	list( $strTableName, $strFieldName, $intId ) = explode( ".", $strFieldParameter, 3 );
    
    	$objDbResult = $this->db->select( $strFieldName )->from( $strTableName )->where( $strFieldName, $strFieldValue )->where( 'id !=', $intId )->limit(1)->get();
    
    	if ( true == ( boolean ) $objDbResult->row() ) {
    		return false;
    	} else {
    		return true;
    	}
    }
}