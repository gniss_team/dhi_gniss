<?php if ( ! defined('BASEPATH') ) exit( 'No direct script access allowed' );

class CManageModulesController extends CI_Controller {

    public function __construct() {
    	parent::__construct();
    }

    public function action( $strAction = '' ) {
    	
    	switch ( $strAction ) {
	    		case 'add' :
	    			$this->add( $strAction );
	    			break;
	    			
	    		case 'edit' :
	    			$this->edit( $strAction );
	    			break;
	
	    		case 'delete' :
	    			$this->delete( $strAction );
	    			break;
	    		
	    		case 'view' :
	    			$this->view( $strAction );
	    			break;
	    			
	    		case 'sort' :
	    			$this->setOrderNum( $strAction );
	    			break;
	    	   			
	    		default :
	    			$this->create();
    	}
    }
    
    public function applyValidation( $strAction ) {
    	
	    	if( 'add' == $strAction ) {
	    			$this->form_validation->set_rules('name', 'Module name', 'trim|required|min_length[2]|max_length[20]|alpha_dot_space|xss_clean');
	    	} elseif( 'edit' == $strAction ) {
	    			$intModuleId 	= $this->uri->segment( 5 );
	    			$this->form_validation->set_rules('name', 'Module name', 'trim|required|min_length[2]|max_length[20]|alpha_dot_space|xss_clean');
	    	}
	    	
// 	    	$this->form_validation->set_rules( 'is_default', 'Default Module', 'trim|required|xss_clean' );
	    	$this->form_validation->set_rules( 'position', 'Position', 'trim|required|xss_clean' );
	    	$this->form_validation->set_rules( 'internal_url', 'Module Url', 'trim|required|xss_clean' );
// 	     	$this->form_validation->set_rules( 'is_admin', 'Module Type', 'trim|required|xss_clean' );
	    	$this->form_validation->set_rules( 'description', 'Module description', 'alpha_space_dot|min_length[2]|max_length[100]trim|max_length[100]|xss_clean');
	    	
	    	if( TRUE == $this->form_validation->run() ) {
	    			return true;
	    	} else {
	    			return false;
	    	}
    }
    
    public function view() {
   
			$arrmixModuleInfo['modules'] = Modules_Model::fetchModules();
			if( true == $this->session->userdata('sucmsg') ) {
					$arrmixModuleInfo['msg']	= $this->session->userdata('sucmsg');
					$this->session->unset_userdata('sucmsg');
			} else {
					$arrmixModuleInfo['errmsg']	= $this->session->userdata('errmsg');
					$this->session->unset_userdata('sucmsg');
			}
			$this->load->view( 'admin/view_modules', $arrmixModuleInfo );
    }
	
    public function add( $strAction = "" ) {
    		
    		$arrmixModuleInfo		= array();
    		
			if( true == is_array( $this->input->post() ) && "add" == $strAction ) {
				
					if( true  == $this->applyValidation( $strAction ) ) {
						
								$config['upload_path'] = 'uploads/';
							
								$this->load->library('upload', $config);
								$this->upload->initialize($config);
								$this->upload->set_allowed_types('gif|jpg|png|jpeg');
								
								if( true == $this->upload->do_upload('module_img') ) {
									
											$data = array('msg' => "Module image uploaded successfully.");
											$data['upload_data'] 	= $this->upload->data();
											$strModuleImg			= $this->upload->file_name;
										
								} else {
									
											$strModuleImg	= NULL;
											$arrmixUpoadfile =  $_FILES['module_img'];
											
											if( false == empty( $arrmixUpoadfile['name'] )) {
													$data = array('upload_error' => $this->upload->display_errors());
													//$this->session->set_userdata( array( 'msg' =>$this->upload->display_errors() ) );
											}
										
								}
						
								$arrobjModule	= Modules_Model::getOrderNum();
													
								if( true == Modules_Model::insert( $arrobjModule[0]->order_num + 1, $strModuleImg ) ) {
									
// 											$intModuleId 	= $this->db->insert_id(); 
// 											if( '0' == $this->input->post('is_admin') ) {
// 												$this->load->model('permission_model');
// 												$intUserId		=  $this->session->userdata('user_id');
// 												$this->permission_model->insert( $intModuleId, $intUserId );
// 											}
									    	$this->session->set_userdata( array('sucmsg' =>"Module added successfully") );
									
								} else {
											$this->session->set_userdata( array('sucmsg' =>"Module cannot be added") );
								}
								redirect('admin/module/action/view');
						} 
						
				} 
				$arrmixModuleInfo['modules_list'] = Modules_Model::fetchModules();
		
				$this->load->view( 'admin/add_modules', $arrmixModuleInfo );
    }
    
    public function edit( $strAction ) {
    	
    	$intModuleId 		= $this->uri->segment(5);
    	$arrmixModuleInfo['modules'] = Modules_Model::fetchModuleById( $intModuleId );
    	$arrmixModuleInfo['modules_list'] = Modules_Model::fetchModules();
    	if( true == is_array( $this->input->post() ) ) {
		    		
    				if( true  == $this->applyValidation( $strAction ) ) {
    						
						    		$config['upload_path'] = 'uploads/';
						    		$this->load->library('upload', $config);
						    		$this->upload->initialize($config);
						    		$this->upload->set_allowed_types('gif|jpg|png|jpeg');
						    		$msg	= '';
							    		
						    		if( true == $this->upload->do_upload('module_img') ) {
						    			
								    			if( $this->input->post('existing_module_image') != "" && false == @unlink( 'uploads/'. $arrmixModuleInfo['modules'][0]->image ) ) {
								    					$msg	.= "Error deleting file, ";
									   		}
									   			
								    			$msg	.= "Module image uploaded successfully.";
								    			$data = array( 'msg' => $msg );
								    			$data['upload_data'] 	= $this->upload->data();
								    			$strModuleImg			= $this->upload->file_name;
						    		} else {
							    				$data = array('upload_error' => $this->upload->display_errors());
							    				$strModuleImg	= $this->input->post('existing_module_img'); 
						    					//$this->session->set_userdata( array( 'msg' =>$this->upload->display_errors() ) );
						    		}
				    		
						    		if( true == Modules_Model::update( $intModuleId, $strModuleImg ) ) {
						    					$this->session->set_userdata( array( 'sucmsg' =>"Module updated successfully.") );
						    		} else {
						    					$this->session->set_userdata( array( 'sucmsg' =>"Module cannot be updated.") );
						    		}
						    		redirect('admin/module/action/view');
		    		} 
		   } 
		   $this->load->view( 'admin/add_modules', $arrmixModuleInfo );
    }
	
    public function delete( $strAction ) {
	    	
	     $intModuleId 	= $this->uri->segment(5);
		
			if( true == Modules_Model::delete( $intModuleId ) ) {
					//$objPlan = Modules_Model::fetchModuleById( $intModuleId );
					$this->session->set_userdata( array( 'sucmsg' => "Module deleted successfully." ) );
			} else {
					$this->session->set_userdata( array( 'errmsg' => "Module cannot be deleted." ) );
			}
			redirect('admin/module/action/view');
   }
        
    public function setOrderNum(  $strAction ) {
    
    		$arrmixModuleInfo['modules'] = Modules_Model::fetchModulesCount();
    }
    
    public function create() {
    
    	$arrmixModuleInfo = array();
    	$this->load->view( 'admin/add_module', $arrmixModuleInfo );
    }
      
  	public function edit_unique ( $strFieldValue, $strFieldParameter ) {
		
		$this->form_validation->set_message( 'edit_unique', 'The %s is already exists.' );
		
		list( $strTableName, $strFieldName, $intId ) = explode( ".", $strFieldParameter, 3 );
	
		$objDbResult = $this->db->select( $strFieldName )->from( $strTableName )->where( $strFieldName, $strFieldValue )->where( 'id !=', $intId )->limit(1)->get();
		
		if ( true == ( boolean ) $objDbResult->row() ) {
			return false;
		} else {
			return true;
		}
	}
}