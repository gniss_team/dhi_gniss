<?php 
if ( ! defined( 'BASEPATH' )) exit( 'No direct script access allowed' );

class CDeviceTypeController extends CI_Controller {

	public function __construct() {
        parent::__construct();       
        $this->lang->load( "message", $this->session->userdata('site_lang') );
       // $this->load->model( 'Device_types_model' );
	}
	
	public function action( $strAction = '' ) {
		 
		switch ( $strAction ) {
			case 'add' :
				$this->addDeviceType( $strAction );
				break;
				 
			case 'edit' :
				$this->editDeviceTypes( $strAction );
				break;
	
			case 'delete' :
				$this->delete( $strAction );
				break;
	
			case 'view' :
				$this->viewDeviceTypes( $strAction );
				break;
	
			default :
				$this->createDeviceType();
		}
	}
		
    public function viewDeviceTypes() {	
		
		$arrmixDeviceTypeData['count_devicetype']	= Device_types_model::getCountDeviceTypes();
		//$config['total_rows'] 					= $arrmixDeviceTypeData['count_devicetype'];
		$arrmixDeviceTypeData['devicetype'] 		= Device_types_model::getDeviceTypes();
		$arrmixDeviceTypeData['msg']				= $this->session->userdata( 'sucmsg' );
		
		$this->session->unset_userdata( 'sucmsg' );
		$this->load->view( 'devicetype',$arrmixDeviceTypeData );	
	}
	
	public function applyValidation( $strAction ) {
		 
		if( 'add' == $strAction ) {
	
			$this->form_validation->set_rules( 'typename', 'Name', 'required|callback_duplicatetypename_check|min_length[2]|max_length[15]|alpha_numeric' );
			$this->form_validation->set_rules( 'typedescription', 'Description', 'required|min_length[2]|max_length[255]' );
			 
		} else {
			$intDeviceTypeId 		= $this->uri->segment(4);
			$this->form_validation->set_rules( 'typename', 'Name', 'required|callback_edit_unique[device_types.name.' . $intDeviceTypeId .']|min_length[2]|max_length[15]|alpha_numeric|trim' );
		$this->form_validation->set_rules( 'typedescription', 'Description', 'required|min_length[2]|max_length[255]|trim' ); 
		}
		if( TRUE == $this->form_validation->run() ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function addDeviceType( $strAction = "" ) {
		
		if('add' == $strAction && $this->applyValidation( $strAction )){
			
			if( true == Device_types_model::insert() ) {	
				$this->session->set_userdata( array( 'sucmsg' =>"Device type added successfully." ) );
				redirect( 'devicetype/action/view' );
			} else {
			   	$this->session->set_userdata( array( 'sucmsg' =>"Device type cannot be added." ) );
			   	redirect( 'devicetype/action/view' );
		 	}
		 	
		}
	    $this->load->view( 'add_device_type' );
	}
	
	public function editDeviceTypes( $strAction ) {
		$intDeviceTypeId 		= $this->uri->segment(4);
	
		$arrMixDeviceData['devicetype_info'] = Device_types_model::getDeviceTypes( $intDeviceTypeId );
	
		if ( true == $this->input->post() ) {
			 if( true == $this->applyValidation( $strAction ) ) { 
				if( true == Device_types_model::update( $intDeviceTypeId )) {
					$arrMsg	= array( 'sucmsg' =>"Device type updated successfully." );
				} else {
			   		$arrMsg	= array( 'sucmsg' =>"Device type cannot be updated." );
				}
				$this->session->set_userdata( $arrMsg );
				redirect( 'devicetype/action/view' );
			 }
			else {
				$arrMixDeviceData['devicetype_info'][0]->name = $this->input->post('typename');
				$arrMixDeviceData['devicetype_info'][0]->description = $this->input->post('typedescription');
			} 
		}
		$this->load->view( 'edit_device_type', $arrMixDeviceData );
	}
	
	//remove this function
	public function duplicatetypename_check( $strFieldname ) {
		
		$intDeviceTypeId	= $this->uri->segment(3);
		$intRowCount	 	= Device_types_model::checkDuplicateDeviceType( $strFieldname, $intDeviceTypeId );
		if( 0 < $intRowCount ) {
			$this->form_validation->set_message('duplicatetypename_check', 'Device type already exists.');
			return false;
		} else {
			return true;
		}
	}

    public function delete( $strAction ) {
    	
    	$intDeviceTypeId 		= $this->uri->segment(4);
    
        if( true == Device_types_model::delete( $intDeviceTypeId ) ) {
        	$this->session->set_userdata( array( 'sucmsg' => "Device type deleted successfully." ) );    
	   	} else {
	   		$this->session->set_userdata( array( 'sucmsg' => "Device type cannot be deleted." ) );
	   	}
	   	redirect( 'devicetype/action/view' );
	}
	
	public function createDeviceType() {
	
		$arrmixDeviceInfo = array();
		$this->load->view( 'add_device_type', $arrmixDeviceInfo );
	}
	
	public function edit_unique ( $strFieldValue, $strFieldParameter ) {
	
		$this->form_validation->set_message( 'edit_unique', '%s already exists.' );
	
		list( $strTableName, $strFieldName, $intId ) = explode( ".", $strFieldParameter, 3 );
	
		$objDbResult = $this->db->select( $strFieldName )->from( $strTableName )->where( $strFieldName, $strFieldValue )->where( 'id !=', $intId )->limit(1)->get();
	
		if ( true == ( boolean ) $objDbResult->row() ) {
			return false;
		} else {
			return true;
		}
	}
}