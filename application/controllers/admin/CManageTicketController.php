	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageTicketController extends CI_Controller { 

	public function __construct() {
		parent::__construct();
		$this->load->model( 'Ticket_model', 'ticket' );
		$this->load->model( 'users_model' );
		 
	}
	
	public function action( $strAction = '' ) {
	
		switch( $strAction ) {
			case 'new':
				$this->addTicket( $strAction );
				break;
				
			case 'send_invite':
			$this->send_invite( $strAction );
				break;

			case 'view':
				$this->viewTickets( $strAction );
				break;
				
			case 'close':
				$this->closeTickets( $strAction );
				break;
				
			case 'viewfull':
				$this->fullTickets( $strAction );
				break;
				
			case 'reply':
				$this->replyTicket( $strAction );
				break;
			 
			default:
				$this->viewTickets();
	
		}
	}
	

	
	public function viewTickets() {
		
		//$intCustomerId = $this->session->userdata( 'user_id' );
		$arrmixTicketsInfo[ 'ticket' ] = $this->ticket->fetchAlltickets(  );
		$arrmixTicketsInfo[ 'msg' ]	= $this->session->userdata( 'sucmsg' );
		$this->session->unset_userdata( 'sucmsg' );
		$this->load->view( 'admin/view_tickets',$arrmixTicketsInfo );
	}
	
	public function addticket() {
		
		$this->form_validation->set_rules( 'subject', 'Category', 'trim|required|min_length[2]|max_length[50]|xss_clean' );
		$this->form_validation->set_rules( 'message', 'Message', 'trim|required|min_length[1]|max_length[1000]|xss_clean' );
		
		if( true == is_array( $this->input->post() ) ) {
			
			if( true == $this->form_validation->run() ) {

				$strSubject = $this->input->post( 'subject' );
				$txtMessage = $this->input->post( 'message' );
				$intUserId = $this->session->userdata( 'user_id' );
				$booleanResult = $this->ticket->addNewTicket( $strSubject,$txtMessage,$intUserId );
				redirect( base_url().'ticket/action/view' );
			}
		}
		$arrmixDeviceInfo[ 'subjectlist' ] = $this->ticket->fetchAllSubjects();
		$this->load->view( 'add_ticket', $arrmixDeviceInfo );
	}
	
	public function closeTickets( ) {
		
		$intTicketId	= $this->uri->segment(5);
		if( true == $this->ticket->delete( $intTicketId ) ) {
			
			$this->session->set_userdata( array( 'msg' =>"User has been deleted successfully." ) );
		} else {
			
			$this->session->set_userdata( array( 'msg' =>"User can not be deleted." ) );
		}
		redirect( base_url().'admin/ticket/action/view' );
	}
	
	public function fullTickets() {
		
		$intTicketId	= $this->uri->segment(5);
		if( true == is_array( $this->input->post() ) ) {
			
			$this->form_validation->set_rules( 'reply-message', 'Reply', 'trim|required|min_length[1]|max_length[1000]|xss_clean' );
			
			if( true == $this->form_validation->run() ) {
				
				$arrmixTicketsInfo[ 'ticket' ] = $this->ticket->replyTicketnow();
			}
		}
		
		//$intCustomerId = $this->session->userdata( 'user_id' );
		
		$arrmixTicketsInfo[ 'ticket' ] = $this->ticket->fetchFullticket( $intTicketId );
		$arrmixTicketsInfo[ 'replies' ] = $this->ticket->fetchTicketReplies( $intTicketId );
		
		$createdBy = $arrmixTicketsInfo[ 'ticket' ]->created_by;
		$arrmixTicketsInfo[ 'userdet' ] = $this->users_model->fetchUserById( $createdBy );
		
		$arrmixTicketsInfo[ 'msg' ]	= $this->session->userdata( 'sucmsg' );
		$this->session->unset_userdata( 'sucmsg' );
		
		//print_r($arrmixTicketsInfo);
		
		$is_Ticket = $arrmixTicketsInfo[ 'ticket' ];
		if( $is_Ticket ) {
			
			$this->load->view( 'admin/view_tickets_full',$arrmixTicketsInfo );
		} else {
			
			redirect( base_url().'admin/ticket/action/view' );
		}
		
		//$this->load->view( 'view_tickets_full',$arrmixTicketsInfo );
	}
	
	public function replyTicket( ) {
		
		$intTicketId = $this->uri->segment(4);
		
		$this->form_validation->set_rules( 'reply-message', 'Message', 'trim|required|min_length[1]|max_length[1000]|xss_clean' );
			
		if( true == is_array( $this->input->post() ) ) {
	
			if( true == $this->form_validation->run() ) {
				
				$arrmixTicketsInfo[ 'ticket' ] = $this->ticket->replyTicketnow();
				$this->load->view( 'admin/view_tickets_full',$arrmixTicketsInfo );
			}
		}
	
		$this->load->view( 'admin/view_tickets_full' );
	}
	
	public function applyValidation( $strAction ) {
			
	
		$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[2]|max_length[20]|alpha_dot_space|xss_clean');
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required|min_length[2]|max_length[80]|alpha_dot_space|xss_clean');
		$this->form_validation->set_rules( 'email', 'Email address', 'trim|required|valid_email|max_length[50]|xss_clean' );
		$this->form_validation->set_rules( 'message', 'Module description', 'trim|required|xss_clean');
	
		if( TRUE == $this->form_validation->run() ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function send_invite( $strAction )
	{
		$arrmixModuleInfo		= array();
		
		if( true == is_array( $this->input->post() ) && "send_invite" == $strAction ) {
		
			if( true  == $this->applyValidation( $strAction ) ) {
		
				$strHeaderMsg	= '<div style="font-size:1.5em;">Invitation From  GNISS!</div><br><br/>'.
						'Dear Recipient,<br><br>';
				$arrstrEmailFormat = array(
						'header' 	=> $strHeaderMsg,
						'content'	=> $this->input->post('message')
				);
				
				if( true == $this->sendMail( 'no_reply@rectusenergy.com', $this->input->post('email'), $this->input->post('subject'), $arrstrEmailFormat ) ) {
					$this->session->set_userdata( array( 'successmsg' => "Your invitation sent." ) );
				} else {
					$this->session->set_userdata( array( 'errmsg' => "Could not send mail to user." ) );
				}
				redirect('admin/ticket/action/send_invite');
			}
		
		}
		
		
		$this->load->view( 'admin/send_invite.php' );
	}
	
	// Need to move in common functions.
	public function sendMail( $strEmailFrom, $strEmailTo, $strSubject, $arrstrEmailFormat ) {
	
		$this->load->library( 'email' );
		$this->load->library( 'parser' );
		$this->email->mailtype	= 'html';
			
		$this->email->from( $strEmailFrom, 'Rectus Energy pvt ltd' );
		$this->email->to( $strEmailTo );
		$this->email->subject( $strSubject );
			
		$strEmailContent = $this->parser->parse( 'email_template',$arrstrEmailFormat, TRUE );
		$this->email->message( $strEmailContent );
			
		return $this->email->send();
	}
	 
}
