<?php
/*
 This Model is developed for  Live Tracking
 Here we mnaintained all databse related queries 
Last Updated : 23 Oct 2015
Updated By : Bhatu Jadhav .
*/
class Tracker_model extends CI_Model {
        
    public static function getAllTrackers() {
    	// fetch all live location of logged in users from Traker table 
        $CI = get_instance();
        
        $boolQueryResult = $CI->db->query("SELECT tt.phoneno,tt.deviceid,tt.latitude,tt.longitude,tt.speed,tt.time,
        		tt.stop_flag,tt.token,tt.rawdate,tt.dated,a.name,a.phone_number,a.imei_number,a.marker_icon
        FROM devices as a JOIN (select * from tracker order by id desc  ) as tt on(tt.phoneno=a.phone_number)
        WHERE a.created_by='".$CI->session->userdata('user_id')."'  group by a.phone_number");
        //echo $CI->db->last_query();
        return $boolQueryResult->result();
    }
    
    public static function getAllLatLangByDeviceID( $intPhoneNumber ) {
    	// fetch all live location of logged in users from Traker table
    	$CI = get_instance();
    
    	$boolQueryResult = $CI->db->query("SELECT tt.phoneno,tt.deviceid,tt.latitude,tt.longitude,tt.speed,tt.time,
    			tt.stop_flag,tt.token,tt.rawdate,tt.dated,a.name,a.phone_number,a.imei_number,a.marker_icon
    			FROM devices as a JOIN tracker as tt on(tt.phoneno=a.phone_number)
    			WHERE tt.phoneno='".$intPhoneNumber."' order by tt.id DESC LIMIT 0,1 ");
    	//echo $CI->db->last_query();
    	return $boolQueryResult->result();
    }
    public static function getAllVTSDevices() {
    	$CI = get_instance();
    	$intUserId	= ( int ) $CI->session->userdata('user_id');
    	$CI->db->select( 'name , id , phone_number' );
    	$CI->db->from( 'devices ' );
    	$CI->db->where( 'device_type_id', '3' );
    	$CI->db->where( 'created_by', $intUserId );
    	$objQueryResult = $CI->db->get();
    	$arrmixDevices = array();
    		
    	foreach( $objQueryResult->result() as $arrmixDevice ) {
    
    		$arrmixDevices[] = $arrmixDevice;
    	}
    	return $arrmixDevices;
    
    }

    public static function getTrackerByPhoneNumber( $intPhoneNumber ) {
    	$CI = get_instance();
      $boolQueryResult = $CI->db->query("SELECT tt.*,a.name,a.phone_number,a.imei_number,a.marker_icon
        FROM devices as a JOIN (select * from tracker order by id desc  ) as tt on(tt.phoneno=a.phone_number)
        WHERE a.created_by='".$CI->session->userdata('user_id')."' AND a.phone_number='".$intPhoneNumber."'  group by a.phone_number");
        //echo $CI->db->last_query();
        return $boolQueryResult->result();
    }
    
    public static function getShareTrackerByPhoneNumber( $intPhoneNumber ) {
    	$CI = get_instance();
    	$boolQueryResult = $CI->db->query("SELECT tt.*,a.name,a.phone_number,a.imei_number,a.marker_icon
    			FROM devices as a JOIN (select * from tracker order by id desc  ) as tt on(tt.phoneno=a.phone_number)
    			WHERE  a.phone_number='".$intPhoneNumber."'  group by a.phone_number");
    	//echo $CI->db->last_query();
    	return $boolQueryResult->result();
    }
    
     public static function getLatestLatLongByPhoneNumber( $intPhoneNumber ) {
     	// fetch Latest lat and long  from Tracker table by phone number
        $CI = get_instance();
        $boolQueryResult = $CI->db->query("SELECT tracker.*,devices.name from tracker 
                left join devices on devices.phone_number=tracker.phoneno WHERE `phoneno`= '".$intPhoneNumber."' and latitude!='' and latitude!='0' and longitude!='0' and longitude!='0' order by id DESC limit 1");
        return $boolQueryResult->result();  
    }
    
     public static function getImeiNumberCountByImeiNumberByPhoneNumber( $intImeiNumber , $intPhoneNumber ) {   
           // return count of all records by imei number and phone number from tracket table . 
            $CI = get_instance();
            $CI->db->where( 'imei_number',$intImeiNumber );
            $CI->db->where( 'phone_number', $intPhoneNumber );          
            $arrobjUser = $CI->db->get('devices');
            $sql = $CI->db->last_query();
            return  $arrobjUser->num_rows;
    }
    
      public static function updateApiKey( $strApiKey ,$intPhoneNumber ) {
      	// This model for web service to updated API Key . 
            $CI = get_instance();
            $arrmixApiData = array(
                    'api_key'       => $strApiKey
            );
            $CI->db->where( 'phone_number', $intPhoneNumber );
            
            return $CI->db->update( 'devices', $arrmixApiData );
    }
    
    public static function UpdateShareStatus( $status , $DeviceID , $SharedGnissId ) {
    	if($status==1)
    	{
    		$sts=0;
    	}else{
    		$sts=1;
    	}
    	$CI = get_instance();
    	$arrmixApiData = array(
    			'is_active'       => $sts
    	);
    	$CI->db->where( 'shared_device', $DeviceID );
    	$CI->db->where( 'shared_gnissid', $SharedGnissId );
  return  $CI->db->update( 'share_location', $arrmixApiData );
  //   echo $CI->db->last_query();
    }
   
    
     public static function insetEnergyMeterData( $arrmixEnergyMeterData ) {
        // This function used for save energy meter data into energy meter device  taable 
        // This data is send by energy meter device 
        $CI = get_instance();
        return $CI->db->insert( 'energy_meter_data', $arrmixEnergyMeterData );
    }
    
      public static function getroute( $intPhoneNumber , $intTokenNumber="" ) {
      	// This function return all travelled route by phone number and token number 
        $CI = get_instance();
        if($intTokenNumber!=""){
        $objResult = $CI->db->query(' SELECT tr.stop_flag,tr.longitude,tr.latitude FROM tracker tr
                WHERE  tr.token='.$intTokenNumber.' AND tr.stop_flag="1" or tr.dated = (SELECT MAX(dated) FROM tracker tr2 where token='.$intTokenNumber.' ) or 
                tr.dated = (SELECT MIN(dated) FROM tracker tr2 where token='.$intTokenNumber.') 
                ORDER BY tr.dated ASC');
        }else {
        	
        	$objResult = $CI->db->query(
        					'select 
								* 
        					from 
        						( 
									SELECT 
										(@row_number:=@row_number + 1) AS num, tr.stop_flag, tr.longitude, tr.latitude 
									FROM 
										tracker tr, 
        								(SELECT @row_number:=0) AS t 
									WHERE 
										tr.phoneno='.$intPhoneNumber.' 
										AND ( tr.dated >= ( SELECT MAX(dated) FROM tracker tr2 where phoneno='.$intPhoneNumber.' ) 
										AND tr.dated <= ( SELECT MIN(dated) FROM tracker tr2 where phoneno='.$intPhoneNumber.' ) ) 
								) temp 
        						JOIN ( 
										select 
											COALESCE(
        									case
												when 1 = t.id then t.id
												when ((t.id - 1) * (p.total DIV 80 ) ) && t.id != 80 then ((t.id -1) * (p.total DIV 80 ) ) 
												when 80 = t.id and 80 < p.total then p.total
												WHEN 80 = t.id and 80 > p.total then t.id
											END, t.id ) as record_id,
        									p.total 
										from ( 
												select 
        											*
        										from 
        											incr
        									 ) as t, 
											(
												SELECT 
													count(*) as total 
												from 
													tracker 
												WHERE 
													phoneno='.$intPhoneNumber.' 
													AND( dated <= ( SELECT MAX(dated) as total FROM tracker where phoneno='.$intPhoneNumber.' ) 
													AND dated >= ( SELECT MIN(dated) FROM tracker where phoneno='.$intPhoneNumber.' ) )
											 ) as p
										) cr 
								ON ( cr.record_id = temp.num )');
        	
//         	$objResult = $CI->db->query(' SELECT tr.stop_flag,tr.longitude,tr.latitude FROM tracker tr
//         			WHERE  tr.phoneno='.$intPhoneNumber.' or tr.dated = (SELECT MAX(dated) FROM tracker tr2 where phoneno='.$intPhoneNumber.') or
//         			tr.dated = (SELECT MIN(dated) FROM tracker tr2 where phoneno='.$intPhoneNumber.')
//         			ORDER BY tr.dated ASC');
        }

        return $objResult ->result();
    }
    public static function fetchAssetByPhoneNumber( $intPhoneNumber ) {
    	 
    	$arrmixAssetsInfo         = array();
    	 
    	$CI = get_instance();
    	 
    	$CI->db->select( '*' );
    	$CI->db->from( 'devices') ;
    	 
    	$CI->db->where( 'phone_number', $intPhoneNumber );
    	//$CI->db->where( 'deleted_by', NULL );
    	//echo  $CI->db->last_query();
    	$objDbResult = $CI->db->get();
    	 
    	return $objDbResult->row() ;
    }
    
    public static function getrouteByFromDateByToDate( $intPhoneNumber , $strFromDate, $strToDate ) {
    	// This function return all travelled route by phone number and token number
    	$CI = get_instance();
    	
//    		$objResult = $CI->db->query('SELECT tr.stop_flag,tr.longitude,tr.latitude FROM tracker tr
//                 WHERE tr.phoneno='.$intPhoneNumber.' AND ( tr.dated >= "'. $strFromDate .'" and 
//                 tr.dated <= "'. $strToDate .'" )
//                 ORDER BY tr.dated ASC');
    
    	
    	$objResult = $CI->db->query(
    			'select
								*
        					from
        						(
									SELECT
										(@row_number:=@row_number + 1) AS num, tr.stop_flag, tr.longitude, tr.latitude
									FROM
										tracker tr,
        								(SELECT @row_number:=0) AS t
									WHERE
										tr.phoneno='.$intPhoneNumber.'
										AND ( tr.dated >= "'. $strFromDate .'" and tr.dated <= "'. $strToDate .'" )
								) temp
        						JOIN (
										select
											COALESCE(
    										case
												when 1 = t.id then t.id
												when ((t.id - 1) * (p.total DIV 80 ) ) && t.id != 80 then ((t.id -1) * (p.total DIV 80 ) )
												when 80 = t.id and 80 < p.total then p.total
												WHEN 80 = t.id and 80 > p.total then t.id
											END, t.id ) as record_id,
        									p.total
										from (
												select
        											*
        										from
        											incr
        									 ) as t,
											(
												SELECT
													count(*) as total
												from
													tracker
												WHERE
													phoneno='.$intPhoneNumber.'
													AND ( dated >= "'. $strFromDate .'" and dated <= "'. $strToDate .'" )
											 ) as p
										) cr
								ON ( cr.record_id = temp.num )');
    
    	return $objResult->result();
    }
    
    public static function getLatestRouteByPhoneNumer( $intPhoneNumber ) {
    	// This function return all travelled route by phone number and token number
    	$CI = get_instance();
    	  	 
    	$objResult = $CI->db->query( '
    				select 
						* 
					from (
						 SELECT 
							(@row_number:=@row_number + 1) AS num,
							tr.stop_flag,
							tr.longitude,
							tr.latitude,
    						tr.dated
						FROM 
							tracker tr,
							(SELECT @row_number:=0) AS t 
						WHERE 
							( date(tr.dated) <= ( 
										SELECT 
											Date( MAX(dated) ) as latest_date 
										FROM 
											tracker 
										where 
											phoneno = '. $intPhoneNumber .'
										)
							and date(tr.dated) >= ( 
										SELECT 
											Date( MAX(dated) ) as latest_date 
										FROM 
											tracker 
										where 
											phoneno = '. $intPhoneNumber .'
										)
							)
					
					) temp 
					JOIN (
						 select 
							COALESCE(
    							case 
								when 1 = t.id then t.id 
								when ((t.id - 1) * (p.total DIV 80 ) ) && t.id != 80 then ((t.id -1) * (p.total DIV 80 ) ) 
								when 80 = t.id and 80 < p.total then p.total
								WHEN 80 = t.id and 80 > p.total then t.id 
							END, t.id ) as record_id,
							p.total 
						from
							( 
								select
									*
								from 
									incr 
							) as t,
							( 
								SELECT 
									count(*) as total 
								from 
									tracker 
								WHERE 
									( date(dated) <= ( 
										SELECT 
											Date( MAX(dated) ) as latest_date 
										FROM 
											tracker 
										where 
											phoneno = '. $intPhoneNumber .'
										)
							and date(dated) >= ( 
										SELECT 
											Date( MAX(dated) ) as latest_date 
										FROM 
											tracker 
										where 
											phoneno = '. $intPhoneNumber .'
										)
							)
						) as p 
					
						) cr ON ( cr.record_id = temp.num )');
    
    
    	return $objResult->result();
    }
    
    public static function fetchDistanceReports( $intPhoneNumber ) {
        $CI = get_instance();
        $CI->db->distinct();
        $CI->db->select('token,phoneno');
        $CI->db->from( 'tracker' );
        $CI->db->where("phoneno",$intPhoneNumber);
        $strSql = $CI->db->get();
        $arrDevicesInfo = array();
        $arrTrackerInfo = array();
        
        foreach( $strSql->result() as $row ) {
            
            $tokenNumber = $row->token;
            $phonenumber = $row->phoneno;
            if($tokenNumber) {
            $objResult = $CI->db->query(' SELECT * FROM tracker tr
                    WHERE   tr.dated = (SELECT MAX(dated) FROM tracker tr2 where token='.$tokenNumber.') or
                    tr.dated = (SELECT MIN(dated) FROM tracker tr2 where token='.$tokenNumber.')
                    ORDER BY dated desc');
            }else {
            	$objResult = $CI->db->query(' SELECT * FROM tracker tr
            			WHERE   tr.dated = (SELECT MAX(dated) FROM tracker tr2 where phoneno='.$phonenumber.') or
            			tr.dated = (SELECT MIN(dated) FROM tracker tr2 where phoneno='.$phonenumber.')
            			ORDER BY dated desc');
            	
            }
            $rowTrackerDet = $objResult->result(); 
            if($rowTrackerDet){
            
            if(isset($rowTrackerDet[1]))
            {
                $rowx = $rowTrackerDet[1];
            }
            else
            {
                $rowx = $rowTrackerDet[0];
            }
            $rowy = $rowTrackerDet[0];
    
            $arrTrackerInfo['token'] = $tokenNumber;
            $arrTrackerInfo['phoneno'] = $rowx->phoneno;
            $arrTrackerInfo['deviceid'] = $rowx->deviceid;
            $arrTrackerInfo['datefrom'] = $rowx->dated;
            $arrTrackerInfo['dateto'] = $rowy->dated;
            
    
            $startLat = $rowx->latitude;
            $startLong = $rowx->longitude;
            $endLat = $rowy->latitude;
            $endLong = $rowy->longitude;
    
            $arrTrackerInfo['trip_from'] = self::getAddress( $startLat,$startLong );
            $arrTrackerInfo['trip_end'] = self::getAddress( $endLat,$endLong );
    
            $arrTrackerInfo['dist'] = self::distance( $startLat, $startLong, $endLat, $endLong, 'K' );
            $arrTrackerInfo['exptdist'] = Tracker_model::getexpecteddist( $tokenNumber );
            $arrMixTrackerData[] = $arrTrackerInfo;
            }
           
        }
      
        return $arrMixTrackerData;
    }
      public static function insertDriversGprs( $arrMixTrackerData ) {
      	// This is web service for store data into tracker table data send by android app .
        $CI = get_instance();
        if($arrMixTrackerData) {
        $CI->db->insert( 'vahak_gprs', $arrMixTrackerData );
        $intUserId = $CI->db->insert_id();
        return $intUserId;
        }
    }
    

public static function insert( $arrMixTrackerData ) {
      	// This is web service for store data into tracker table data send by android app .
        $CI = get_instance();
        if($arrMixTrackerData) {
        $CI->db->insert( 'tracker', $arrMixTrackerData );
        $intUserId = $CI->db->insert_id();
        return $intUserId;
        }
    }
    // code review changes are remaining.
    public static function getexpecteddist($tokenId ) {
        $CI = get_instance();
        $CI->db->select('*');
        $CI->db->from( 'tracker' );
        $CI->db->where( 'token',$tokenId );
        $CI->db->order_by( 'dated', 'asc' );
        $strSqltrasc = $CI->db->get();
        $arrDistances = $strSqltrasc->result();
        $totalDist=0;
        $j=0;
        $cntDistancearr = count($arrDistances);
    
        for($i=0;$i<$cntDistancearr;$i++)
        {
        $j=$i+1;
        if($j<$cntDistancearr)
        {
        $lat1 = $arrDistances[$i]->latitude;
        $long1 = $arrDistances[$i]->longitude;
        $lat2 = $arrDistances[$j]->latitude;
        $long2 = $arrDistances[$j]->longitude;
    
        $currDist = self::distance($lat1, $long1, $lat2, $long2, 'K');
        $totalDist = $currDist + $totalDist;
        }
        }
    
        return $totalDist;
    
    }
     public static function checkforapikey( $apy_key ) {
     	// check for API key is right or not in device tabl
        $CI = get_instance();
        $CI->db->select('api_key,frequency_rate');
        $CI->db->where( 'api_key', $apy_key );
        $CI->db->where( 'api_key !=', "" );
        $arrobjTracker = $CI->db->get( 'devices' );
       
       // return ( 1 == $arrobjTracker->num_rows ) ? $arrobjTracker->num_rows: NULL;
        return ( 1 == $arrobjTracker->num_rows ) ? $arrobjTracker->row: false;
    }
    
    public static function getVtsTotalRow( $intUserID )
    { 
    	// return the count of total VTS Devices for the provided device . 
        $CI = get_instance();
        $CI->db->select('id');
        $CI->db->where( 'created_by', $intUserID );
        $arrobjTracker = $CI->db->get( 'devices' );
        return (  $arrobjTracker->num_rows > 0 ) ? $arrobjTracker->num_rows: 0;
        
    }
    public static function getTrackercount()
    {
        $CI = get_instance();
        $boolQueryResult = $CI->db->query("SELECT a.id
                FROM devices as a JOIN (select * from tracker order by id desc  ) as tt on(tt.phoneno=a.phone_number)
                WHERE a.created_by='".$CI->session->userdata('user_id')."'  group by a.phone_number");
        //echo $CI->db->last_query();
        return $boolQueryResult->num_rows();exit;
    }

    public static function getAddress( $dblLat, $dblLong ) {
    
        $strUrl   = "https://maps.googleapis.com/maps/api/geocode/json?latlng=". $dblLat.",". $dblLong ."&sensor=false";

        $strJsonData    = @file_get_contents( $strUrl );
        $objJson        = json_decode( $strJsonData );
  
        $strStatus      = $objJson->status;

        $strFormatedAddress = '';

        if( 'OK' == $strStatus ) {
        $strFormatedAddress = $objJson->results[0]->formatted_address;
        }

        return $strFormatedAddress;
    }

    public static function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
    
        if ($unit == "K") {
            $retval = ($miles * 1.609344);
            return number_format($retval, 2, '.', '');
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
    
    public static  function getlastlatlong( $intPhoneNumber )
    {

		 $CI = get_instance();
		 $CI->db->select( 'latitude,longitude ' );
		 $CI->db->where("phoneno",$intPhoneNumber);
		 $CI->db->order_by("id", "desc");
		 $CI->db->limit( 1);
		 $CI->db->from( 'tracker' );
		 $arrobjUserInfo = $CI->db->get();
		 	
		 return $arrobjUserInfo->row();
		  

    }
    public static function geofench( $intPhoneNumber)
    {
    	$CI = get_instance(); 
    	$CI->db->select( 'geofench.*,t.latitude,t.longitude' );
    	$CI->db->from( 'geofench' );
    	$CI->db->join('tracker t', 't.phoneno = geofench.device_id');
    	 $CI->db->limit('1');
    	 $CI->db->order_by("t.id", "desc");
    	$CI->db->where("device_id",$intPhoneNumber);
    	
    	$arrobjUserInfo = $CI->db->get();
    	//echo $CI->db->last_query();
    	return $arrobjUserInfo->row();
    	
    }
    public static function insertFench( $intPhoneNumber,$lat ,$lang , $radious ) {
    	// This is web service for store data into tracker table data send by android app .
    	$CI = get_instance();
    	
    	$CI->db->select('id');
    	$CI->db->where( 'device_id', $intPhoneNumber );
    	$arrobjTracker = $CI->db->get( 'geofench' );
    	
    	$arrMixFenchData = array(
    			"device_id"=> $intPhoneNumber ,
    			"lat" => $lat,
    			"lang" => $lang,
    			"radious"=>$radious
    			
    			);
    	if($arrobjTracker->num_rows > 0 ) {
    		$CI->db->where( 'device_id', $intPhoneNumber );
    		
    		return $CI->db->update( 'geofench', $arrMixFenchData );
    		
    	}else {
    		$CI->db->insert( 'geofench', $arrMixFenchData );
    		$intUserId = $CI->db->insert_id();
    		return $intUserId;
    		
    	}
    }

    public static function deleteTracker( $intPhoneNumber , $intTokenNumber) {
        $CI = get_instance();
  
        $CI->db->where('phoneno', $intPhoneNumber );
        $CI->db->where('token', $intTokenNumber );

        return $CI->db->delete( 'tracker' );
    } 
    
    
    public static function deleteVtsTracker( $intPhoneNumber , $intTokenNumber) {
    	$CI = get_instance();
    
    	$CI->db->where('phoneno', $intPhoneNumber );
    	$CI->db->where('token', $intTokenNumber );
    
    	return $CI->db->delete( 'tracker' );
    }
    public static function checkforshare( $intPhoneNumber )
    {
    	$CI = get_instance();
    	$CI->db->select( '*' );
    	$CI->db->where( 'shared_device',$intPhoneNumber );
    	$CI->db->where( 'shared_gnissid',$CI->session->userdata('account_number') );
    	$CI->db->or_where( 'shared_by',$CI->session->userdata('user_id') );
    	$arrobjUser = $CI->db->get('share_location');
    //	echo $sql = $CI->db->last_query();
		return $arrobjUser->row();
    	
    }
    
    public static function setMarkRead($intPhoneNumber)
    {
    	$CI = get_instance();
    	$arrUpdata = array(
    			"is_read"=> '1'
    	);
    		$CI->db->where( 'shared_device', $intPhoneNumber );
    		$CI->db->where( 'shared_gnissid', $CI->session->userdata('account_number') );
    	
    		return $CI->db->update( 'share_location', $arrUpdata );
    	
    }
   
    public static function countShareByGnissId()
    {
    	$CI = get_instance();
    	$CI->db->select( 's.id,s.shared_device,s.is_active,u.first_name,u.last_name,s.is_read' );
    	$CI->db->from( 'share_location s' );
    	$CI->db->join('users u', 's.shared_by = u.id');
    	$CI->db->where( 's.shared_gnissid',$CI->session->userdata('account_number') );
    	$CI->db->where( 's.is_active','1' );
    	$CI->db->where( 's.is_read','0' );
    	$arrobjUser = $CI->db->get();
   // 	echo $sql = $CI->db->last_query();
    	return $arrobjUser->result();
    	
    	 
    }
    public static function SharedLocationByuser()
    {
    	$CI = get_instance(); 
    	$CI->db->select( 's.*,u.first_name,u.last_name' );
    	$CI->db->from( 'share_location s' );
    	$CI->db->join('users u', 's.shared_gnissid = u.account_number');
    	
    	$CI->db->where("shared_by",$CI->session->userdata('user_id'));
    	
    	$arrobjUserInfo = $CI->db->get();
    	//echo $CI->db->last_query();
    	return $arrobjUserInfo->result();
    	
    }
    public static function SharedLocationByuserId( $intUserId )
    {
    	
    	$CI = get_instance();
    	$CI->db->select( 's.*, d.name' );
    	$CI->db->from( 'share_location s', 'LEFT' );
    	$CI->db->join( 'devices d', 'd.id = s.device_id' );
    	$CI->db->join('users u', 's.shared_gnissid = u.account_number');
    	$CI->db->where("s.shared_by", $intUserId );
    	 
    	$CI->db->where("s.is_active", 1 );

    	$arrobjUserInfo = $CI->db->get();
    	//echo $CI->db->last_query();
    	return $arrobjUserInfo->result();
    	 
       }
       
       public static function SharedDevicesById( $intGnissId ){
       	 
       		$CI = get_instance();
	       	$CI->db->select( 's.*, d.name, u.first_name, u.last_name' );
	       	$CI->db->from( 'share_location s' );
	       	$CI->db->join( 'devices d', 'd.id = s.device_id' );
	       	$CI->db->join('users u', 's.shared_by = u.id');
	       	$CI->db->where("s.shared_gnissid", $intGnissId );
	       
	       	$CI->db->where("s.is_active", 1 );
	       
	       	$arrobjUserInfo = $CI->db->get();
	       	//echo $CI->db->last_query();
	       	return $arrobjUserInfo->result();
	       
       }
       
       public static function fetchUsersSharedByGnissId( $intGnissId ){
       	 
	       	$CI = get_instance();
	       	$CI->db->select( 's.*,d.*, u.first_name, u.last_name' );
	       	$CI->db->from( 'share_location s' );
	       	$CI->db->join( 'devices d', 'd.phone_number = s.shared_device' );
	       	$CI->db->join('users u', 's.shared_by = u.id');
	       	$CI->db->where("s.shared_gnissid", $intGnissId );
	       
	       	$CI->db->where("s.is_active", '1' );
	       
       		return $CI->db->get();
       
       }
       
       public static function sharedLocationDeviceIdsByuserId( $intUserId, $arrmixDeviceIds ) {
       	 
	       	$CI = get_instance();
	       	$CI->db->select( 'device_id' );
	       	$CI->db->where("shared_by", ( int ) $intUserId );
	       
	       	$arrobjDevices = $CI->db->get( 'share_location' );
	       
	      	$arrintDeviceIds = array();
			foreach(  $arrobjDevices->result() as $intIndex => $objDevice ) {
				if( true == in_array($objDevice->device_id, $arrmixDeviceIds )) {
					$arrintDeviceIds[]	= $objDevice->device_id;
				}
			}
		
       		if( true == is_array( $arrintDeviceIds ) && false == empty( $arrintDeviceIds ) ) {	
       			$data['is_active']	= 0;
       			$data['shared_by']	= ( int ) $intUserId;
       			$CI->db->where_in('device_id' ,$arrintDeviceIds);
       			
       			if( true == $CI->db->update('share_location', $data ) ) {
       				return true;
       			} else {
       				return false;
       			}
			} else {
				return false;
			}
       
       }
    
    public static function getuseremailbygnissId( $intShareToGnissId ) {		
    	$CI = get_instance();
    	$CI->db->select( 'email_address' );
    	$CI->db->where( 'account_number',$intShareToGnissId );
       
       	$objDbResult = $CI->db->get('users');
       	return $objDbResult->row() ;
    }
    
    public static function saveshareloc(  ) {
    	$CI = get_instance();
    	// return count of all records by imei number and phone number from tracket table .
    	 $intShareMobile = $CI->input->post('sharemobile');
    	$intShareToGnissId =  $CI->input->post('gniss_id');
    	
    	if(!empty($intShareToGnissId)){
    	for($i=0;$i<sizeof($intShareToGnissId);$i++ ) {
	        	$CI->db->select( 'id' );
	        	$CI->db->where( 'shared_device',$intShareMobile );
	        	$CI->db->where( 'shared_gnissid	',$intShareToGnissId[$i] );
		        	$checkforExists = $CI->db->get('share_location');
		        	$shareflag=0;
	        	if($checkforExists->num_rows > 0)
	        	{ 
	        			//echo "<div style='color:red;'>You already Shared this gniss id</div>";
	        	}else
        		 {
        		 		$shareflag=1;
		        	 	$arrMixShareData = array ("shared_device"=>$intShareMobile,"shared_gnissid"=>$intShareToGnissId[$i],"is_active"=> "1","shared_by"=> $CI->session->userdata('user_id'));
		        	 	$CI->db->insert( 'share_location', $arrMixShareData );
		        	 	$intUserId = $CI->db->insert_id();
		        	 	
		        	 	$email = Tracker_model::getuseremailbygnissId($intShareToGnissId[$i]);
		        	 	$link=base_url().'livetracking/share/'.$intShareMobile;
		        	 	$strMessage		= 'Your Freind Shared Location With You , Please click on following link :<br><br>'.'<a href=' . base_url().'livetracking/share/'.$intShareMobile.'>'.$link.'</a>' ;
		        	 	 
		        	 	$strHeaderMsg	= '<div style="font-size:1.5em;">Welcome to GNISS!</div><br><br/>'.
		        	 			'Dear Recipient,<br><br>';
		        	 	$arrstrEmailFormat = array(
		        	 			'header' 	=> $strHeaderMsg,
		        	 			'content'	=> $strMessage
		        	 	);
		        	 	
    $mail = Tracker_model::sendMail( 'no_reply@rectusenergy.com', $email->email_address,  'Your Freind Share Location With You', $arrstrEmailFormat );
		        	 	
		       
        		}
    	}
    	}else {
    		
    		$data = array(
    				'error' => 'true',
    				'msg' => "<div style='color:red;'>Please enter at leat one user. </div>"
    		);
    		return $data;
    		
    	
    	}
    	if($shareflag==1)
    	{
    		$data = array(
    				'error' => 'suc',
    				'msg' => "<div style='color:green;'>Sharing Completed! Shared URL : ".base_url()."livetracking/share/".$intShareMobile." </div>"
    		);
    	    return $data;
    		
    	}else{
    		
    		$data = array(
    				'error' => 'true',
    				'msg' => "<div style='color:red;'>User is already added. </div>"
    		);
    		return $data;
    		
    	}
      
    }
    
      public static function  sendMail( $strEmailFrom, $strEmailTo, $strSubject, $arrstrEmailFormat ) {
    	$CI = get_instance();
    	$CI->load->library( 'email' );
    	$CI->load->library( 'parser' );
    	$CI->email->mailtype	= 'html';
    
    	$CI->email->from( $strEmailFrom, 'Rectus Energy pvt ltd' );
    	$CI->email->to( $strEmailTo );
    	$CI->email->subject( $strSubject );
    
    	$strEmailContent = $CI->parser->parse( 'email_template',$arrstrEmailFormat, TRUE );
    	$CI->email->message( $strEmailContent );
    
    	return $CI->email->send();
    }
    
    public static function getAutoSuggestionById($username)
    {
    	$CI = get_instance();
    	$CI->db->select( 'first_name,account_number' );
    	$CI->db->where('id !=', $CI->session->userdata('user_id'));
    	$CI->db->like('first_name', $username, 'after');
    	$CI->db->from( 'users' );
    	 
    	$arrobjUserInfo = $CI->db->get();
    	//echo $CI->db->last_query();
    	return $arrobjUserInfo->result();
    }
        

}
