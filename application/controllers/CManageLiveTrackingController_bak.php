<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageLiveTrackingController extends CI_Controller { 
	 
	public function __construct() {
        parent::__construct();       
        //$this->lang->load("message", $this->session->userdata('site_lang'));
        $this->load->model('Tracker_model', 'livetrack');
    }
    
    public function index() {
		$data['deviceid']="";
		$this->load->view( 'livetracking',$data );
			 
	}
	
	
	
	public function deviceinfo(){
	
		$intSimNumber 		= (int) $this->uri->segment(3);
		$arrmixDeviceInfo	= $this->Devices_model->fetchDeviceBySim( $intSimNumber );
	
		if( false == is_array( $arrmixDeviceInfo ) || 0 == count( $arrmixDeviceInfo ) ) {
			$requiredData = array( 'device_info' => 'Error: This number is not registered' , 'error' => 1 );
		} else {
			$arrmixDeviceType = $this->Device_types_model->getDeviceTypes($arrmixDeviceInfo[0]->device_type_id);
			$requiredData = array( 'device_info' => array(	"device_type",$arrmixDeviceType[0]->name,
					"sim_number",$arrmixDeviceInfo[0]->sim_number,
					"device_number",$arrmixDeviceInfo[0]->device_number,
					"manufatured_date",$arrmixDeviceInfo[0]->manufactured_date ),
					'error' => 0 );
	
		}
		$data = json_encode( $requiredData );
		print_r($data);
		exit;
		//$this->load->view('api_devicedet', $data);
	}
	public function devicesettingsinfo() {
	
		$this->load->model('device_configuration_user_model');
	
		$intDeviceId 				= (int) $this->uri->segment(3);
		$arrmixDeviceSettingsInfo	= $this->device_configuration_user_model->fetchDeviceSettings( $intDeviceId );
	
		if( false == is_array( $arrmixDeviceSettingsInfo ) || 0 == count( $arrmixDeviceSettingsInfo ) ) {
			$requiredData = array( 'device_settings' => 'Error: This number is not registered' , 'error' => 1 );
		} else {
			$requiredData = array( 'device_settings' => $arrmixDeviceSettingsInfo , 'error' => 0 );
		}
		echo '<pre>';print_r($requiredData);
	
		$data = json_encode( $requiredData );
		print_r($data);
		exit;
		//$this->load->view('api_devicedet', $data);
	}
	
	public function viewmap($deviceid)
	{
		$data['deviceid']=$deviceid;
		$this->load->view( 'livetracking',$data );
	}
	public function getAddress($lat, $lon){ 
		$url  = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".
				$lat.",".$lon."&sensor=false";
		$json = @file_get_contents($url);
		$data = json_decode($json);
		$status = $data->status;
		$address = '';
		if($status == "OK"){
			$address = $data->results[0]->formatted_address;
		}
		return $address;
	}
	public function getDeviceMap()
	{
		$livetrack=$this->livetrack->getLiveTrackLocationByCustId( $this->intCustomerId );
		//print_r( $livetrack );
	}
	Public function getLatlong( $intdeviceid="" )
	{
		header("Content-type: text/xml");
	 echo '<markers>';
	   if($intdeviceid!="") {
	   	$getDevices=$this->livetrack->getDevicesBYid( $intdeviceid );

	   }else {
		$getDevices=$this->livetrack->getDevices();
	   }
	  ;
		//echo "<br> num:: ".$num_rows_usr."<br><br>";
		if(count($getDevices) > 0 )
		{
			foreach ($getDevices as $row) {
			{
				//echo $row->sim_number;
				$getLatestLatLong=$this->livetrack->getLatestLatLongByDeviceId( $row->phone_number );
				
				if(count($getLatestLatLong) > 0 )
				{
					$today_date=date("Y-m-d H:i:s");
					$today_date = strtotime($today_date);
						
					$today_date = $today_date;
					//	echo $res_latest['rawdate'];
		
					$last_date = strtotime( $getLatestLatLong[0]->rawdate );
					//echo "tarun";
					$date_diff=$last_date-$today_date;
					//echo "tarun";
					// here we are taking 20000 as the rawdate is in IST and server time GMT and there is a difference of 5.30 hrs = 19800 + 900 (900 = 15 mins)
					if ($date_diff>=18900 && $date_diff<=20400  && $date_diff>=0){
						$vh_dtc=1;
					}else {
						$vh_dtc=0;
					}
		
					if($getLatestLatLong[0]->avaliability=='a' || $getLatestLatLong[0]->avaliability=='A')
					{
						$vh_gps = 'on';
					} else {
						$vh_gps = 'off';
					}
					$latestlatitude = round($getLatestLatLong[0]->latitude,5);
					$latestlongitude = round($getLatestLatLong[0]->longitude,5);
						
						$vh_adr=$this->getAddress($latestlatitude,$latestlongitude);
					
						
						
					if ($vh_adr=="")
					{
						$vh_adr = "Address not available ".$latestlatitude.",".$latestlongitude;
					}
					$veh_bearing="";
					$speed_val=$getLatestLatLong[0]->speed; 
					$ignition="";
					$vh_gps="";
					$vh_st="";
					$ac="";
					$vh_st_im="";
		
					//$vh_adr = "Address not available ".$latestlatitude.",".$latestlongitude;
		
					 echo '<marker lat="' . $getLatestLatLong[0]->latitude . '" lng="' . $getLatestLatLong[0]->longitude. '" ';
					echo ' vehicle_dir="' . $veh_bearing . '"';
			
						echo ' titleb="Speed : '.$speed_val.' Km/hr | Ignition : '.ucfirst($ignition).' | AC : '.ucfirst($ac).' | GPS : '.strtoupper($vh_gps).' | Date-Time : '.$getLatestLatLong[0]->rawdate.' | Address : '.$vh_adr.'"';
						// 					echo ' html="&lt;b&gt;Vehicle&lt;/b&gt; : '.ucfirst($result_usr['name']).' &lt;br&gt;&lt;b&gt;Latitude&lt;/b&gt; : '.$res_latest['latitude'].' &lt;br&gt;&lt;b&gt;Longitude&lt;/b&gt; : '.$res_latest['longitude'].' &lt;br&gt;&lt;b&gt;Speed&lt;/b&gt; : '.$speed_val.' Km/hr &lt;br&gt;&lt;b&gt;Ignition&lt;/b&gt; : '.$ignition.'&lt;br&gt;&lt;b&gt;AC&lt;/b&gt; : '.$ac.'&lt;br&gt;&lt;b&gt;GPS&lt;/b&gt; : '.strtoupper($vh_gps).' &lt;br&gt;&lt;b&gt;Date-Time&lt;/b&gt; : '.$res_latest['rawdate'].' &lt;br&gt;&lt;b&gt;Address&lt;/b&gt; : '.$vh_adr.'"';
							
						echo ' html="&lt;b&gt;Name&lt;/b&gt; : '.$getLatestLatLong[0]->name.
						' &lt;br&gt;&lt;b&gt;Speed&lt;/b&gt; : '.$speed_val.' Km/hr &lt;br
						&gt;&lt;b&gt;Date-Time&lt;/b&gt; : '.$getLatestLatLong[0]->dated.' &lt;br
						&gt;&lt;b&gt;Address&lt;/b&gt; : '.$vh_adr.'"';
					
					echo ' token="' . $getLatestLatLong[0]->token . '"';
					echo ' phoneno="' . $getLatestLatLong[0]->phoneno . '"';
					echo ' vehicle_icon=""';
					echo ' label="'.$vh_st_im.'  '.$getLatestLatLong[0]->name.'" />';
				}
			}
			 
		}
	  }
	  echo '</markers>'; 
	}
	
	public function checkForimmiNumberApi( $intImmiNumber , $intPhoneNumber )
	{
		$immiNumberResult = $this->livetrack->checkForImmiNumber( $intImmiNumber , $intPhoneNumber );
		if($immiNumberResult > 0)
		{
			$api_key = md5(time());
			$this->livetrack->updteapikey($api_key,$intPhoneNumber);
			echo json_encode( array('imminumber' => $intImmiNumber , 'flag'=>'1','msg'=>'imei number is avilable' ,'api_key'=>$api_key) );
		}else 
		{
			echo json_encode( array('imminumber' => $intImmiNumber , 'flag'=>'0','msg'=>'imei number is not avilable' ) );
		}
	}
	public function  WebserviceLogin($email , $password)
	{
		$is_valid = $this->Users_model->fetchAdminUser( $email, md5($password) );

		if($is_valid)
		{
			echo json_encode( array('flag'=>'1','user_name' =>$is_valid[0]->email_address ,
									'user_id' =>$is_valid[0]->id ) );		
		}
		else // incorrect username or password
		{
			echo json_encode( array( 'flag'=>'0','msg'=>'Invalid credentials' ) );
				
		}
	}
	
	public function  fetchAllDeviceTypesWebservice()
	{
		$deviceTypes = $this->Device_types_model->getDeviceTypes();
	
		if(!empty($deviceTypes))
		{
			echo json_encode( array('flag'=>'1','devicetypes'=> $deviceTypes) );
	
		}
		else // incorrect username or password
		{
			echo json_encode( array( 'flag'=>'0','msg'=>'No data found' ) );
	
		}
	}
	
	public function  fetchAllDevicesByDeviceTypeWebservice($intDeviceTypeId, $intCustId)
	{
		$devices = $this->livetrack->fetchDevicesByDeviceTypeAndCustId($intDeviceTypeId,$intCustId);
	
		if(!empty($devices))
		{
			echo json_encode( array('flag'=>'1','devices'=> $devices) );

		}
		else // incorrect username or password
		{
			echo json_encode( array( 'flag'=>'0','msg'=>'No data found' ) );
	
		}
	}
	
	public function  getLatestLatLongByDeviceNoWebservice( $intPhoneNumber )
	{
		$devicelatlong = $getLatestLatLong=$this->livetrack->getLatestLatLongByDeviceId( $intPhoneNumber );
	
		if(!empty($devicelatlong))
		{
			echo json_encode( array('flag'=>'1','latlong'=> $devicelatlong) );

		}
		else // incorrect username or password
		{
			echo json_encode( array( 'flag'=>'0','msg'=>'No data found' ) );
	
		}
	}
	
	public function routeWiseLocations( $intPhoneNumber )
	{
		$arrmixRoutesInfo['asset_info']= $this->livetrack->getAassetInfo( $intPhoneNumber );
		$arrmixRoutesInfo['reports'] = $this->livetrack->fetchDistanceReports( $intPhoneNumber );
	//echo "<pre>";print_r($arrmixRoutesInfo);exit;
		$this->load->view('view_routes.php',$arrmixRoutesInfo);
	}
	
	public function viewLocationsmap( $intPhoneNumber , $intTokenNumber) {
			
		$arrmixReportsInfo['asset_info']= $this->livetrack->getAassetInfo( $intPhoneNumber );
		$arrmixReportsInfo['reports'] = $this->livetrack->getroute( $intPhoneNumber , $intTokenNumber );
		//	print_r($arrmixReportsInfo['reports'] );
		$this->load->view('routereport',$arrmixReportsInfo);
		//echo $this->uri->segment(4);
	}
	
	public function insertdata( $arrmixTrackerData="" )
	{
		$arrmixTrackerData = file_get_contents('php://input');
		$arrmixTrackerData = json_decode($arrmixTrackerData, true); 
		//echo "hi".print_r($array); exit;
		try
		{  
			if($this->livetrack->checkforapikey($arrmixTrackerData['api_key']) > 0) {
					$result= $this->livetrack->inserttrakerdata( $arrmixTrackerData );
					if($result){ 
						echo json_encode( array( 'flag'=>'1','msg'=>'Insert data sucesfully' ) );
					} 
			}else {
				echo json_encode( array( 'flag'=>'0','msg'=>'Invalid Api Key. Please login again' ) );
			}
			
		}
		catch(Exception $e) { //later
			echo json_encode( array( 'flag'=>'0','msg'=>'no data inserted' ) );
		}
	}
	
	
	
}