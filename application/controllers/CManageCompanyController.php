<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageCompanyController extends CI_Controller {

	public function action( $strAction = '' ) {

		switch( $strAction ) {    		
			
			case 'createcompany':
    			$this->createCompany();
    			break;
    			
    		case 'insertcompany':
    			$this->insertCompany();
    			break;
    
    		case 'viewcompanies':
    			$this->viewCompanies();
    			break;

    		case 'viewcompanyusers':
    			$this->viewCompanyUsers();
    			break;
    					
    		case 'viewcompanyAssets':
    			$this->viewCompanyAssets();
    			break;
    			
    		case 'createcompanyuser':
    			$this->createCompanyUser();
    			break;
    			
    		case 'insertcompanyuser':
    			$this->insertCompanyUser();
    			break;
    			
    		case 'approvecompanyrequest':
    			$this->approveCompanyRequest();
    			break;
   				 
   			case 'declinecompanyrequest':
   				$this->declineCompanyRequest();
   				break;
    				
    		default:
    			show_404();
    			break;    				
    	}
	}
	
	public function viewCompanyUsers() {
	
		$intCompanyId	= $this->uri->segment(4);
		if( 'Company' == $this->session->userdata('login_type') ) {
			$intCompanyId = $this->session->userdata( 'company_id' );
		}
		
		if( false == isset($intCompanyId) && 0 >= $intCompanyId ) {
			redirect( 'company/action/viewcompanies' );
		}
		$intUserId = $this->session->userdata( 'user_id' );
		
		$arrUserCompanies = Company_model::fetchAllUserCompanies( $intUserId );
		if( !in_array( $intCompanyId, $arrUserCompanies ) ) {
			redirect( 'company/action/viewcompanies' );
		}
		
		$arrobjCompanyUsersData['arrCompanyUsers'] 	= array();
		$arrobjCompanyUsersData['arrCompanyUsers']	= Company_user_model::fetchAllCompanyUsers( $intCompanyId );
		$arrobjCompanyUsersData['company_id']		= $intCompanyId;	
		$arrobjCompanyUsersData['msg']				= $this->session->userdata( 'successmsg' );
		$this->session->unset_userdata( 'successmsg' );
		
		$this->load->view( 'view_company_users', $arrobjCompanyUsersData );
	}
	
	public function viewCompanyAssets() {
	
		$intCompanyId	= $this->uri->segment(4);
		if( false == isset(	$intCompanyId) && 0 >= $intCompanyId ) {
			//set error message
			return false;
		}
		
		$arrobjCompanyAssets['company_assets']	= array();
		$arrobjCompanyAssets['company_assets']	= asset_model::fetchAllAssetsByCompanyId( $intCompanyId );
		
		$this->load->view( 'view_company_assets', $arrobjCompanyAssets );
	}
	
	public function createCompany() {

		$arrmixCompanyInfo = $this->registrationInfo();
		$this->load->view(	'add_company', $arrmixCompanyInfo	);
	}
	
	public function insertCompany() {
		
		if( false == is_array($this->input->post() ) ) {
			$this->session->set_userdata( array( 'successmsg' =>"Invalid Path" ) );
			redirect( base_url().'company/action/createcompany' );
		}	
		
		$this->form_validation->set_rules( 'company_name', 'Company name', 'trim|required|min_length[2]|max_length[50]|validate_company_name_new|non_numeric_name|xss_clean' );
		$this->form_validation->set_rules( 'username', 'Username', 'trim|required|min_length[2]|max_length[15]|validate_username|non_numeric_name|non_character_name|consicutive_periods_underscore|is_unique[companies.username]|is_unique[users.username]|xss_clean' );
		$this->form_validation->set_rules( 'email_address', 'Email address', 'trim|required|valid_email|is_unique[companies.email_address]|max_length[50]|xss_clean' );
		$this->form_validation->set_rules( 'password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[conf_password]|xss_clean' );
		$this->form_validation->set_rules( 'conf_password', 'Confirm password', 'trim|required|min_length[6]|max_length[20]|xss_clean' );
		$this->form_validation->set_rules( 'company_address', 'Company address', 'trim|required|min_length[2]|max_length[100]|validate_company_address|xss_clean' );
		$this->form_validation->set_rules( 'mobile_number', 'Mobile number', 'trim|required|numeric|exact_length[10]|mobile_zero|xss_clean' );
		$this->form_validation->set_rules( 'country_id', 'Country', 'required|xss_clean' );
		$this->form_validation->set_rules( 'state_id', 'State', 'required|xss_clean' );
		$this->form_validation->set_rules( 'city_id', 'City', 'required|xss_clean' );
		$this->form_validation->set_rules( 'zip_code', 'Zip code', 'trim|is_digit|callback_validate_zipcode|exact_length_digit[6]|xss_clean' );

		$this->form_validation->set_rules( 'question_id', 'Security question', 'required|xss_clean' );
		$this->form_validation->set_rules( 'answer', 'Answer', 'trim|required|min_length[2]|max_length[200]|xss_clean' );
		
		if( true == $this->form_validation->run() ) {				
			
			$intSecurityQuestionId 								= $this->input->post( 'question_id' );
			$strAnswer											= $this->input->post( 'answer' );
			$arrmixSecurityQuestionInfo['security_question'] 	= Security_questions_model::fetchSecurityQuestions();
			$arrmixCompanyInfo['companyinfo']					= Company_model::insertCompany();
			$intCompanyId										= $arrmixCompanyInfo['companyinfo'];
			$arrmixUserInfo['addanswer']						= Security_questions_model::insert( $intSecurityQuestionId, $intCompanyId, user_type_model::c_company, $strAnswer );
				
			$this->session->set_userdata( array( 'successmsg' =>"Company '".$this->input->post( 'company_name' )."' added successfully" ) );

			$this->load->library('email');
			$this->load->library('parser');
		   	$this->email->mailtype='html';
		   	$login_url = base_url().'users/action/login';
			$strEmailMsg='Thank you for registering company at Gniss!<br>'.'Following is account details<br><br>'.
					'Company Name : '.$this->input->post('company_name').'<br><br>'.
					'Username : '.$this->input->post('username').'<br><br>'.
					'Password : '.$this->input->post('password').'<br><br>'.
					'Email : '.$this->input->post('email_address').'<br><br>'.
			
					'<br>'.'<a href="'.$login_url.'">Click here to login </a>';
				
			$this->email->from('no_reply@rectusenergy.com', 'Rectus Energy pvt ltd');
			$this->email->to($this->input->post('email_address'));
			$this->email->cc($this->session->userdata('email_address'));
			$this->email->subject('Thank you for registering');
			
			$arrstrEmailData = array(
					'header' 	=> 'Dear Recipient,',
					'content'	=> $strEmailMsg
			);
			
			$strEmailContent = $this->parser->parse('email_template',$arrstrEmailData,TRUE);
			$this->email->subject('New company registered ');
			$this->email->message( $strEmailContent );
				
			if( true == $this->email->send() ) {
				redirect('company/action/viewcompanies');
			} else {
				$this->session->set_userdata( array( 'errormsg' =>' Could not send mail to user.' ) );
			}
			
			redirect( base_url().'company/action/viewcompanies' );
		} else {
			$arrmixCompanyInfo = $this->registrationInfo();
			$this->load->view('add_company', $arrmixCompanyInfo);
		}
	}
	
	public function viewCompanies() {
		
		if( 'Company' == $this->session->userdata('login_type') ) {
			show_404();
		}
		$intUserId = $this->session->userdata( 'user_id' );
		
		$arrmixCompanyData['companies']	= Company_user_model::fetchAllCompaniesByUserId( $intUserId );
		$arrmixCompanyData['msg']		= $this->session->userdata( 'successmsg' );
		$this->session->unset_userdata( 'successmsg' );
				
		$this->load->view( 'manage_companies',$arrmixCompanyData );
	}
	
	public function createCompanyUser() {
		
			$arrmixCompanyData['msg'] = $this->session->userdata( 'successmsg' );
			$this->session->unset_userdata( 'successmsg' );
			if( 'Individual' == $this->session->userdata('login_type') ) {
				$intUserId = $this->session->userdata( 'user_id' );
				$arrmixCompanyData['companies']		= Company_model::fetchAllCompaniesByUserId( $intUserId );
			}
			$this->load->view( 'add_company_user', $arrmixCompanyData );
	}
	
	public function insertCompanyUser() {
	
		if( false == is_array($this->input->post() ) ) {
			$this->session->set_userdata( array( 'successmsg' =>"Invalid Path" ) );
			redirect( base_url().'company/action/viewcompanies' );
		}
	
		//$intCompanyId		= $this->uri->segment(4);
		if( 'Individual' == $this->session->userdata('login_type') ) {
			$intCompanyId		= $this->input->post('company_id');
		}	else	{
			$intCompanyId		= $this->session->userdata( 'company_id' );
		}
		$intUserId			= Users_model::fetchIdByAccountNumber( $this->input->post( 'account_number' ) );
		$strCompanyName		= Company_model::fetchCompanyNameByCompanyId( $intCompanyId );

		if( true == isset( $intUserId ) ) {
			$this->form_validation->set_rules( 'account_number', 'Account Number', 'trim|required|callback_checkCompanyUserAlreadyExists[users.account_number]|callback_is_registered[company_user.'.$intUserId.'.'.$intCompanyId.']|max_length[50]|xss_clean' );
			if( 'Individual' == $this->session->userdata('login_type') ) {
				$this->form_validation->set_rules( 'company_id', 'Company', 'trim|required|xss_clean' );
			}
			if( true == $this->form_validation->run() ) {
				$arrmixCompanyInfo['companyuserinfo'] = Company_user_model::insert( $intUserId, $intCompanyId, '0', '0' );
					
				$this->session->set_userdata( array( 'successmsg' =>"User added under company '".$strCompanyName."' successfully" ) );
				
				$intUserEmailAddress	= Users_model::fetchOneFieldByUserId( $intUserId, 'email_address' );
				
				$this->load->library('email');
				$this->load->library('parser');
				$this->email->mailtype='html';
				$login_url = base_url().'users/action/login';
				$strEmailMsg='Hello User,<br>'.'Following company requested you to join their team on GNISS application. Please accept or cancel request from your dashboard.<br><br>'.
						'Company Name : '.$this->input->post('company_name').'<br><br>'.
						'<br>'.'<a href="'.$login_url.'">Click here to login </a>';
				
				$this->email->from('no_reply@rectusenergy.com', 'Rectus Energy pvt ltd');
				$this->email->to( $intUserEmailAddress );
				$this->email->subject('Request for joining comapny on GNISS');
					
				$arrstrEmailData = array(
						'header' 	=> 'Dear Recipient,',
						'content'	=> $strEmailMsg
				);
					
				$strEmailContent = $this->parser->parse('email_template',$arrstrEmailData,TRUE);
				$this->email->message( $strEmailContent );
				$this->email->send();
				
				
				if( 'Individual' == $this->session->userdata('login_type') ) {
					redirect( base_url().'company/action/viewcompanies' );
				} else {
					redirect( base_url().'company/action/viewcompanyusers/'. $intCompanyId );
				}	
			}
		} else {
			$this->session->set_userdata( array( 'successmsg' =>"Account Number is not valid" ) );
		}

		$strSuccessMessageData['msg'] = $this->session->userdata( 'successmsg' );
		$this->session->unset_userdata( 'successmsg' );
		
		if( 'Individual' == $this->session->userdata('login_type') ) {
			$intUserId = $this->session->userdata( 'user_id' );
			$strSuccessMessageData['companies']		= Company_model::fetchAllCompaniesByUserId( $intUserId );
		}
		$this->load->view( 'add_company_user', $strSuccessMessageData );
	}
	
	public function approveCompanyRequest() {
	
		$arrmixCompanyData['msg'] = '';
		if( 'Individual' == $this->session->userdata('login_type') ) {
			$intUserId = $this->session->userdata( 'user_id' );
			$intCompanyId	= $this->uri->segment(4);
			Company_user_model::changeUserRequest( $intUserId, $intCompanyId, '1' );
			$this->session->set_userdata( array( 'requestresult' =>'Accepted' ) );
		}
		redirect( base_url() );
	}
	
	public function declineCompanyRequest() {
	
		$arrmixCompanyData['msg'] = '';
		if( 'Individual' == $this->session->userdata('login_type') ) {
			$intUserId = $this->session->userdata( 'user_id' );
			$intCompanyId	= $this->uri->segment(4);
			Company_user_model::changeUserRequest( $intUserId, $intCompanyId, '2' );
			$this->session->set_userdata( array( 'requestresult' =>'Declined' ) );
		}
		redirect( base_url() );
	}
	
	public function checkCompanyUserAlreadyExists( $strAccountNumber ) {
		
		$queryResult	= Users_model::checkUserAvailable( $strAccountNumber  );
		if (0 >= $queryResult) {
			$this->form_validation->set_message( 'checkCompanyUserAlreadyExists', ' %s is not valid' );
			return FALSE;
			
		} else {
			return TRUE;
		}
	}
	
	public function is_registered( $strFieldValue, $strFieldParameter ) {

		$this->form_validation->set_message( 'is_registered', 'This %s is already added in your company.' );
	
		list( $strTableName, $intUserId, $intId ) = explode( ".", $strFieldParameter, 3 );
		$resUserRegistered = Company_user_model::checkUserRegisteredWithCompany( $strTableName, $intUserId, $intId );
		
		if ( true == ( boolean ) $resUserRegistered->row() ) {
			return false;
		} else {
			return true;
		}
	}
	
	public function validate_zipcode ( $strFieldValue ) {
	
		if( false == empty( $strFieldValue ) ) {
			if( '100' == $this->input->post('country_id') ) {
				if( '6' == strlen( $strFieldValue ) ) {
					if( true == preg_match( '/^[1-9]{1}[0-9]{5}+$/', $strFieldValue ) ) {
						return true;
					} else {
						$this->form_validation->set_message( 'validate_zipcode', '%s should not start with zero' );
						return false;
					}
				} else {
					$this->form_validation->set_message( 'validate_zipcode', '%s should contain 6 digits' );
					return false;
				}
			} else {
				if( true == preg_match( '/^[\-+]?[0-9]*\.?[0-9]+$/', $strFieldValue ) ) {
					return true;
				}
			}
		}
	}
	
	public function registrationInfo() {
		$arrmixCompanyInfo['states']			=	array();
		$arrmixCompanyInfo['cities']			=	array();
		$arrmixCompanyInfo['security_question']	=	array();
		$intCountryId							=	$this->input->post('country_id');
		$intStateId								=	$this->input->post('state_id');
			
		$arrmixCompanyInfo['countries']			= Locations_Model::fetchLocationsByType( 0 );
		$arrmixCompanyInfo['security_question'] = Security_questions_model::fetchSecurityQuestions();
			
		if( false == empty( $intCountryId ) ) {
			$arrmixCompanyInfo['states']		= Locations_Model::fetchLocationsByCountryIdByType( $intCountryId, '1' );
			if( false == empty( $intStateId ) ) {
				$arrmixCompanyInfo['cities']	= Locations_Model::fetchLocationsByCountryIdByStateIdByType( $intCountryId, $intStateId, '2' );
			}
		}
		
		return $arrmixCompanyInfo;
	}
	
}
