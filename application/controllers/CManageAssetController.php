<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageAssetController extends CI_Controller {
	 
		public function __construct() {
				parent::__construct();
		}
	
		public function action( $strAction = '' ) { 
				
			switch( $strAction ) {
								 
				case 'add':
							$this->add( $strAction );
							break;
							
				case 'edit':
							$this->edit( $strAction );
							break;
							
				 case 'delete':
							$this->delete( $strAction );
							break;

				case 'manage':	 
							$this->manage( $strAction );
						 	break;

				default:
							show_404();
					
				  }
				  
		}
	
		public function add( $strAction ) { 
					 
					$arrmixAssetInfo		 = Array();
									
						if( true == is_array(  $this->input->post() ) ) {
								
								$this->form_validation->set_rules('name', 'Name',  'trim|required|alpha_numeric|min_length[2]|max_length[20]|xss_clean');
								$this->form_validation->set_rules('sim_number', 'Sim number', 'trim|is_digit|exact_length[20]|is_unique[assets.sim_number]|xss_clean');
								$this->form_validation->set_rules('imei_number', 'IMEI number', 'trim|required|is_digit|exact_length[15]|is_unique_record[assets.imei_number]|xss_clean');
								$this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|is_digit|exact_length[10]|xss_clean');

								
									 if( true == $this->form_validation->run() ) { 
										$arrmixAssetInfo['assets'] = Asset_model::insert( $this->input->post() );
										$this->session->set_userdata( array( 'successmsg' =>"Record added successfully" ) );
										redirect( base_url().'assets/action/manage' );
							}
					}
					
					$this->load->view( 'add_assets', $arrmixAssetInfo );
		 }
	
		public function manage( $strAction ) {
		 			
			$intUserId					= ( int ) $this->session->userdata('user_id');

			$arrmixAssetsInfo		=	 array();
			$arrmixAssetsInfo['msg']		= $this->session->userdata( 'successmsg' );
			$this->session->unset_userdata( 'successmsg' );
			$arrmixUserInfo['errmsg']	= $this->session->userdata( 'errmsg' );
			$this->session->unset_userdata( 'errmsg' );
			
			$arrmixAssetsInfo['assets']		= Asset_model::fetchAssetsByUserId( $intUserId );
		
			$this->load->view( 'manage_assets', $arrmixAssetsInfo );
				
		 } 
	 
		public function edit( $strAction ) {
	 
			$intTrackerId			= ( int ) $this->uri->segment( 4 );
			$intUserId					= ( int ) $this->session->userdata('user_id');
			$arrmixAssetInfo	= array();
			
			$arrmixAssetInfo['assets']		= Devices_model::fetchDevicesById( $intTrackerId );
			
			if( 0 == count( $arrmixAssetInfo['assets'] ) ) {
					show_404();
			}
			if(  true == is_array( $this->input->post() ) ) {

					$this->form_validation->set_rules('name', 'Name',  'trim|required|alpha_numeric|min_length[2]|max_length[20]|xss_clean');
// 					$this->form_validation->set_rules('sim_number', 'Sim number', 'trim|is_digit|exact_length[19]|unique_check[assets.sim_number.' . $intTrackerId .']|xss_clean');
// 					$this->form_validation->set_rules('imei_number', 'IMEI number', 'trim|required|is_digit|exact_length[15]|unique_check[devices.imei_number.' . $intTrackerId .']|xss_clean');
// 					$this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|is_digit|exact_length[10]|xss_clean');
					$this->form_validation->set_rules('sim_number', 'Sim number', 'trim|is_digit|min_length[19]|max_length[20]|unique_check[devices.sim_number.' . $intTrackerId .']|xss_clean');
					$this->form_validation->set_rules('imei_number', 'IMEI number', 'trim|required|is_digit|exact_length[15]|unique_check[devices.imei_number.' . $intTrackerId .']|xss_clean');
					$this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|is_digit|exact_length[10]|unique_check[devices.phone_number.' . $intTrackerId .']|xss_clean');
					
					if(  true == $this->form_validation->run() ) { 
										if(  true == Asset_model::update( $intTrackerId, $this->input->post() ) ) { 
										$this->session->set_userdata( array( 'successmsg' =>"Record updated successfully." ) );
										redirect( base_url() . 'assets/action/manage' );
							 } else {
									 $this->session->set_userdata( array( 'errmsg' =>"Unable to delete record." ) );
							 }
					} else {
						
							foreach( $this->input->post() as $strKey => $strValue ) {
							
									if( $strKey == array_key_exists( $strKey, $arrmixAssetInfo['assets'] ) ) {
										 	$arrmixAssetInfo['assets']->$strKey	= $strValue;	
 									}
							 }
					}
				} 
				
				$arrmixAssetInfo['msg'] = $this->session->userdata( 'successmsg' );
				$this->session->unset_userdata( 'successmsg' );
				
				$arrmixUserInfo['errmsg']	= $this->session->userdata( 'errmsg' );
				$this->session->unset_userdata( 'errmsg' );
				
				$this->load->view( 'edit_assets', $arrmixAssetInfo );
					
		} 
		
		public function delete( $strAction ) { 
		  
		 		$arrmixAssetsInfo		 = array();		 
				$intTrackerId					= ( int ) 	$this->uri->segment(4);
				
				if( false == isset( $intTrackerId ) || true == empty( $intTrackerId ) || 0 >= $intTrackerId ) { 
					 show_404();
				}
			  
				if( true == Asset_model::delete( $intTrackerId ) ) {
						$this->session->set_userdata( array( 'successmsg' =>"Record deleted successfully." ) );
				 
				 } else {
							$this->session->set_userdata( array( 'errmsg' =>"Unable to delete record." ) );
				 }
			 	redirect( base_url().'assets/action/manage' );
		} 
}