<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CManageWebServicesController extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	
	}
	
	public function action( $strAction = '' ) {
	
		switch( $strAction ) {
	 
			case 'webserviceLogin':
					$this->login();
					break;
					
			case 'register':
			$this->register();
			break;

			case 'drivergprs':
			$this->insertDrivergprs();
			break;


			
			case 'viewprofile':
				$this->viewprofile();
				break;
			
					
			case 'webserviceDevicetypes' :
					$this->fetchDeviceTypesByUserId();
					break;
			
			case 'webserviceDevices' :
					$this->fetchDevicesByDeviceTypeIdWebservice();
					break;
					
			case 'webserviceDeviceData' :
					$this->fetchDeviceDataByDeviceNumberAndDeviceTypeIdWebservice();
					break;
	
			case 'webserviceDeviceTypesList' :
					$this->fetchDeviceTypesList();
					break;
			
			case 'webserviceDeviceTableColumnsByDeviceTypeId' :
					$this->fetchDeviceTableColumnsByDeviceTypeId();
					break;
				
			case 'webserviceAddDevice' :
					$this->insertDeviceInformation();
					break;
					
			case 'webserviceVTSDeviceList' :
					$this->fetchVTSDeviceList();
					break;
					
			case 'webserviceLatLongByPhoneNumberAndDeviceId' :
					$this->fetchLatLongByPhoneNumberAndDeviceId();
					break;
					
			case 'allcountries' :
					$this->allcountries();
					break;
			
			case 'webserviceChangePassword' :
					$this->changePassword();
					break;
				
			case 'webserviceUpdateGeneralProfile' :
					$this->updateGeneralProfile();
					break;
					
			case 'webserviceUpdateContactDetail' :
					$this->updateContactDetail();
					break;
					
			case 'webserviceUpdateAddress' :
					$this->updateAddress();
					break;
			
			case 'webserviceUpdateNotify' :
					$this->updateNotify();
					break;
					
			case 'webserviceUploadProfileImage' :
					$this->uploadProfileImage();
					break;
					
			case 'webserviceViewGraph' :
					$this->viewGraph();
					break;

			case 'webserviceRouteWiseLocation' :
					$this->routeWiseLocation();
					break;

			case 'webserviceGetRoute' :
					$this->getRoute();
					break;

			case 'webserviceUpdateSocial' :
					$this->updateSocial();
					break;

			case 'webserviceGetLocation' :
			 		$this->getLocation();
			 		break;

			case 'webserviceCreateOtp' :
					$this->createOtp();
					break;

			case 'webserviceVerifyOtp' :
					$this->verifyOtp();
					break;
			
			case 'webserviceGetLastVTSRecord' : 
					$this->getLastVTSRecord();
					break;

			case 'webserviceEditDevice' :
					$this->editDevice();
					break;

			case 'webserviceDeleteDevice' :
					$this->deleteDevice();
					break;

			case 'webserviceDeleteTracker' :
					$this->deleteTracker();
					break;

			case 'webserviceGetAcesParameterList':
					$this->getAcesParameterList();
					break;

			case 'webserviceUpdateAcesParameterValue' :
					$this->updateAcesParameterValue();
					break;

			case 'webserviceForgotPassword' :
					$this->forgotPassword();
					break;

			case 'webserviceResetPassword' :
					$this->resetPassword();
					break;
			case 'shareLocation' :
				$this->shareLocation();
				break;
				
			case 'fetchShareUsers' :
				$this->fetchShareUsers();
				break;
				
			case 'listSharedUsers':
				$this->listSharedUsers();
				break;
			
			case 'deactivateSharedLocations' :
				$this->deactivateSharedLocations();
				break;
				
			case 'notifications':
				$this->notifications();
				break;
			 case 'webserviceSaveplace' :
						$this->saveplace();
						break;
				
			case 'listSharedDevices':
				 $this->listSharedDevices();
				 break;

			default:
					show_404();
				 
		}
	}
	
	public function login() {
		
		$strUsername = $this->uri->segment(4);
		$strPassword = $this->uri->segment(5);
		$user_type =   $this->uri->segment(6);
                
		if($user_type!="")
		{
		$user_type =   $this->uri->segment(6);
		}else{
		$user_type =   0;
		}
		
	
		$is_valid = Users_model::fetchUserByUsernameByPasswordByUserType( $strUsername, $strPassword, $user_type );
		if( true == is_object($is_valid)) {
			
			if( '0' != $is_valid->status ) {
				
				echo json_encode( array('flag'=>'1', 'user_name' =>$is_valid->email_address , 'user_id' =>$is_valid->id, 'mobile_number' =>$is_valid->mobile_number, 'gniss_id'=> $is_valid->account_number, 'gcm_register_id'=> $is_valid->gcm_register_id ) );
				
			} else {
	
				echo json_encode( array( 'flag'=>'0','msg'=>'Your account is not verified.' ) );
	
			}
			 
		} else {
				
			echo json_encode( array( 'flag'=>'0','msg'=>'Invalid credentials' ) );
		}	
	}
	public function register() {
	
		$firstname = $this->uri->segment(4);
		$lastname = $this->uri->segment(5);
		$email = $this->uri->segment(6);
		$mobile = $this->uri->segment(7);
		$username = $this->uri->segment(8);
		$password = $this->uri->segment(9);
		$strAccountNumber = Users_model::checkforaccountNumber();
		$strToken = md5( time() );
		$intUsers = Users_model::checkEmailAlreadyExists( $email );
		if( $intUsers == 0 ) {
			$arrAddDevice = Users_model::webserviceinsert( $firstname, $lastname, $email, $mobile, $username, $password ,$strAccountNumber ,$strToken );
			if( $arrAddDevice != 0 ) {
							
			 	$strMessage		= 'You have chosen:<br><br>'.
													'Gniss Id :' . $strAccountNumber. '<br><br>'.
													'Password :' . $password. '<br><br>'.
													'Email Id :' . $email. '<br><br>'.
													'A few more steps will connect you to the world of GNISS. Click <a href=' . base_url().'users/action/verify/' . $strToken . '>here </a>to complete your registration and get started.'. '<br><br>'.
													'Should you experience any trouble setting up, please contact us at info@rectusenergy.com' .'<br><br>'.
													'If you did not create this ID on GNISS and think that someone else has used your email ID, please click <a href=' . base_url().'>here </a>to remove your email ID from this account' ;
				
			 	$strHeaderMsg	= '<div style="font-size:1.5em;">Welcome to GNISS!</div><br><br/>'.
					 										'Dear Recipient,<br><br>';
			 	$arrstrEmailFormat = array(
	 					'header' 	=> $strHeaderMsg,
	 					'content'	=> $strMessage
			 	);
			 	
				if( true == $this->sendMail( 'no_reply@rectusenergy.com', $email,  'Thank you for Your Registration', $arrstrEmailFormat ) ) {
						$response = array(
							'flag'=>'1', 
							'msg'=>'Insert data sucesfully' ,
							 'user_id'=>$arrAddDevice ,
							'username'=> $username,
							'account_number'=> $strAccountNumber
						);
				} else {
						$response = array(
							'flag'=>'1', 
							'msg'=>'Could not send mail to user' ,
							'user_id'=>$arrAddDevice ,
							'username'=> $username,
							'account_number'=> $strAccountNumber
						);
				}

				echo json_encode( $response );
			} else {
		
				echo json_encode( array( 'flag'=>'0','msg'=>'User not successfully registered! ' ) );
		
			}
		} else {
			echo json_encode( array( 'flag'=>'0','msg'=>'This email address already exist! ' ) );
		}
	}

	public function sendMail( $strEmailFrom, $strEmailTo, $strSubject, $arrstrEmailFormat ) {

			$this->load->library( 'email' );
			$this->load->library( 'parser' );
			$this->email->mailtype	= 'html';
			
			$this->email->from( $strEmailFrom, 'Rectus Energy pvt ltd' );
			$this->email->to( $strEmailTo );
			$this->email->subject( $strSubject );
			
			$strEmailContent = $this->parser->parse( 'email_template',$arrstrEmailFormat, TRUE );
			$this->email->message( $strEmailContent );
			
			return $this->email->send();
	}
	
	public function viewprofile()
	{
	    $intUserId = $this->uri->segment(4);
	    if($intUserId!="") 
	    {
		   $arrUserProfile = Users_model::ServiceProfile( $intUserId );
		   if($arrUserProfile)
		   {
		   	echo json_encode( array('flag'=>'1', 'userprofile' => $arrUserProfile ) );
		   }else
		   {
		   	echo json_encode( array( 'flag'=>'0','msg'=>'Invalid Userid' ) );
		   }
		   
	    }else
	    {
	    		echo json_encode( array( 'flag'=>'0','msg'=>'Invalid Userid' ) );
	    	
	    }
	
	}
	
	public function fetchDeviceTypesByUserId() {
		
		$intUserId = $this->uri->segment(4);
		$arrDeviceType = Devices_model::fetchDevicesByUserIdWebservice( $intUserId );
		
		if( $arrDeviceType ) {
			
			echo json_encode( array('flag'=>'1', 'device_type_array' =>$arrDeviceType ) );
			
		} else {
			
			echo json_encode( array( 'flag'=>'0','msg'=>'No records found' ) );
			
		}
	}
	
	public function fetchDeviceTypesList() {
		
		$arrDeviceTypesList = Devices_model::fetchDeviceTypesWebservice();
		
		if( $arrDeviceTypesList ) {
			
			echo json_encode( array('flag'=>'1', 'device_type_array' =>$arrDeviceTypesList ) );
			
		} else {
			
			echo json_encode( array( 'flag'=>'0','msg'=>'No records found' ) );
			
		}
	}
	
	public function fetchDevicesByDeviceTypeIdWebservice() {
	
		$intUserId = $this->uri->segment(4);
		$intDeviceTypeId = $this->uri->segment(5);
		$arrDevices = Devices_model::fetchDevicesByDeviceTypeIdWebservice( $intUserId, $intDeviceTypeId );
	
		if( $arrDevices ) {
				
			echo json_encode( array('flag'=>'1', 'device_type_array' =>$arrDevices ) );
				
		} else {
				
			echo json_encode( array( 'flag'=>'0','msg'=>'No records found' ) );
				
		}
	}
	
	public function fetchDeviceDataByDeviceNumberAndDeviceTypeIdWebservice() {
	
		$intDeviceNumber = $this->uri->segment(4);
		$intDeviceId = $this->uri->segment(5);
		$fromDate = $this->uri->segment(6);
		$toDate = $this->uri->segment(7);
		$arrDevices = Devices_model::fetchDeviceDataByUserIdAndDeviceTypeIdWebservice( $intDeviceNumber, $intDeviceId, $fromDate, $toDate );
	
		if( $arrDevices ) {
	
			echo json_encode( array('flag'=>'1', 'device_type_array' =>$arrDevices ) );
	
		} else {
	
			echo json_encode( array( 'flag'=>'0','msg'=>'No records found' ) );
	
		}
	}
	
	public function fetchDeviceTableColumnsByDeviceTypeId() {
		$intDeviceTypeId = $this->uri->segment(4);
		$arrDevices = Devices_model::fetchDeviceTableColumnsByDeviceTypeId( $intDeviceTypeId );
		
		if( $arrDevices ) {
		
			echo json_encode( array('flag'=>'1', 'device_type_array' =>$arrDevices ) );
		
		} else {
		
			echo json_encode( array( 'flag'=>'0','msg'=>'No records found' ) );
		
		}
	}
	
	public function insertDeviceInformation() {
		
		$arrmixDeviceData = file_get_contents('php://input');
		$arrmixDeviceData = json_decode($arrmixDeviceData, true);

		try{
			$arrAddDevice = Devices_model::insertDeviceInformation( $arrmixDeviceData );
	
			if( $arrAddDevice != 0 ) {
		
				echo json_encode( array('flag'=>'1', 'msg'=>'Insert data sucesfully' ) );
		
			} else {
		
				echo json_encode( array( 'flag'=>'0','msg'=>'No data inserted' ) );
		
			}
		} catch( Exception $e) {
			echo json_encode( array( 'flag'=>'0','msg'=>'No data inserted' ) );	
		}
		
	}
	
	public function fetchVTSDeviceList() {
		
		$intUserId = $this->uri->segment(4);
		$arrVTSDeviceList = Devices_model::fetchVTSDeviceList( $intUserId );
		
		//$arrVTSDeviceList['userName'] = Users_model::fetchUserNameById( $intUserId );
		
		if( $arrVTSDeviceList ) {
		
			echo json_encode( array('flag'=>'1', 'device_type_array' =>$arrVTSDeviceList ) );
		
		} else {
		
			echo json_encode( array( 'flag'=>'0','msg'=>'No records found' ) );
		
		}
	}
	
	public function fetchLatLongByPhoneNumberAndDeviceId() {
	
		$intPhoneNumber = $this->uri->segment(4);
		$intDeviceId = $this->uri->segment(5);
		$arrLatLong = Devices_model::fetchLatLongByPhoneNumberAndDeviceId( $intPhoneNumber, $intDeviceId );
		if( $arrLatLong ) {
	
			echo json_encode( array('flag'=>'1', 'device_type_array' =>$arrLatLong ) );
	
		} else {
	
			echo json_encode( array( 'flag'=>'0','msg'=>'No records found' ) );
	
		}
	}
	
	public function insertdata() 
	{
		$arrmixTrackerData = file_get_contents('php://input');
		$arrmixTrackerData = json_decode($arrmixTrackerData, true);
		//echo "hi".print_r($array); exit;
			
		try
		{
	
			if( Tracker_model::checkforapikey($arrmixTrackerData['api_key']) > 0 ) {
				$result= Tracker_model::insert( $arrmixTrackerData );
				if( $result ) {
					echo json_encode( array( 'flag'=>'1','msg'=>'Insert data sucesfully' ) );
				}
			}else {
				echo json_encode( array( 'flag'=>'0','msg'=>'Invalid Api Key. Please login again' ) );
			}
	
		}
		catch( Exception $e ) { //later
			echo json_encode( array( 'flag'=>'0','msg'=>'no data inserted' ) );
		}
	}

      public function insertDrivergprs() 
	{
		$lat = $this->uri->segment(4);
		$lang = $this->uri->segment(5);
		$driver_id = $this->uri->segment(6);
		try
		{

			$arrmixTrackerData = array(
    			"driver_id"=> $driver_id ,
    			"lat" => $lat,
    			"lang" => $lang
    			
    			);
				$result= Tracker_model::insertDriversGprs( $arrmixTrackerData );
				if( $result ) {
					echo json_encode( array( 'flag'=>'1','msg'=>'Insert data sucesfully' ) );
				}
			
	
		}
		catch( Exception $e ) { //later
			echo json_encode( array( 'flag'=>'0','msg'=>'no data inserted' ) );
		}
	}
	
	public function allcountries()
	{
		$type = $this->uri->segment(4);
		$parent_id = $this->uri->segment(5);
		$arrCountryList = Devices_model::fetchCountryWebservice( $type , $parent_id );
		
		if( $arrCountryList ) {
				
			echo json_encode( array('flag'=>'1', 'country_array' =>$arrCountryList ) );
				
		} else {
				
			echo json_encode( array( 'flag'=>'0','msg'=>'No records found' ) );
				
		}
	}
	
	public function changePassword()
	{	
		$accountNumber = $this->uri->segment(4);
		$oldPassword = $this->uri->segment(5);
		$newPassword = $this->uri->segment(6);
		
		$is_valid = Users_model::webserviceChangePasswrod( $accountNumber, $oldPassword, $newPassword );
		
		if( $is_valid ) {
			
			echo json_encode( array( 'flag' => '1', 'msg' => 'Password changed successfully') );
			
		} else {
			
			echo json_encode( array( 'flag' => '0', 'msg' => 'Password not updated successfully, Please enter correct current password!') );
		}
		
	}
	
	public function updateGeneralProfile() {
		$arrUserGeneralData = file_get_contents('php://input');
		$arrUserGeneralData = json_decode($arrUserGeneralData, true);
			
		try {
			
			//write code for security
			
			$is_valid = Users_model::webserviceUpdateGeneralProfile( $arrUserGeneralData );
			
			if( $is_valid ) {
				
				echo json_encode( array( 'flag' => '1', 'msg' => 'User general profile updated successfully') );
				
			} else {
				
				echo json_encode( array( 'flag' => '0', 'msg' => 'User Profile not updated successfully!') );
				
			}
			
		} catch( Exception $e ) {
			
			echo json_encode( array( 'flag' => '0', 'msg' => 'Profile not updated successfully!') );
			
		}
	}
	
	public function updateContactDetail() {
		
		$successMsg = json_encode( array( 'flag' => '1', 'msg' => 'User Contact detail updated successfully') );
		$errorMsg = json_encode( array( 'flag' => '0', 'msg' => 'User Contact detail not updated successfully!') );
		
		$arrUserContactData = file_get_contents('php://input');
		$arrUserContactData = json_decode( $arrUserContactData, true );
		
		try {
			if( !empty($arrUserContactData) ) {
				$account_number = $arrUserContactData['account_number'];
				$mobileNumbers = $arrUserContactData['mobile_number'];
				$countryCodes = $arrUserContactData['country_code'];
				$intUserID = $arrUserContactData['user_id'];
				$is_update = Users_model::webserviceUpdateContactDetail( $arrUserContactData );
				
				if( true == $is_update ) {
					
					if( count($mobileNumbers) == 1 && $is_update ) {
						
						$arrObjUserMobiles = Users_model::fetchMobileNumbersById( $intUserID );
						
						if( count( $arrObjUserMobiles ) > 0 ) {
							
							$is_deleted = Users_model::webserviceDeleteMobilesByUserID( $intUserID );
							
							if( false == $is_deleted ) {
								echo $successMsg;
							}
							
						}
							
						echo $successMsg;
						 
					}
				} else {
					echo $errorMsg;
				}
				
				if( count($mobileNumbers) > 1 && $is_update ) {
					
					array_shift( $mobileNumbers );
					array_shift( $countryCodes );
					
					$arrObjUserMobiles = Users_model::fetchMobileNumbersById( $intUserID );
					
					if( count( $arrObjUserMobiles ) > 0 ) {
						$is_deleted = Users_model::webserviceDeleteMobilesByUserID( $intUserID );
						
						if( false == $is_deleted ) {
							
							echo $errorMsg;
							
						}
						
						$is_added = Users_model::webserviceAddMobiles( $mobileNumbers, $countryCodes, $intUserID ); 
						
						if( $is_added ) {
							
							echo $successMsg;
							
						} else {
							
							echo $errorMsg;
							
						}
					} else {
						
						echo $errorMsg;
						
					}
				}
			} else {
				echo $errorMsg;
			}
		} catch( Exception $e) {
			
			echo $errorMsg;
			
		}
	}
	
	public function updateAddress() {
		
		$successMsg = json_encode( array( 'flag' => '1', 'msg' => 'User address updated successfully') );
		$errorMsg = json_encode( array( 'flag' => '0', 'msg' => 'User address not updated successfully!') );
		
		$arrUserAddressData = file_get_contents('php://input');
		$arrUserAddressData = json_decode( $arrUserAddressData, true );
		$account_number = $arrUserAddressData['account_number'];
		
		try {
	
				$is_updated = Users_model::webserviceUpdateAddress( $arrUserAddressData );
				
				if( $is_updated ) {
					echo $successMsg;
				} else {
					echo $errorMsg;
				}
			
		} catch( Exception $e ) {
			
			echo $errorMsg;
		}
		
	}
	
	public function updateNotify() {
		
		$successMsg = json_encode( array( 'flag' => '1', 'msg' => 'Preferences updated successfully') );
		$errorMsg = json_encode( array( 'flag' => '0', 'msg' => 'Preferences not updated successfully!') );
		
		$arrPreferencesData = file_get_contents('php://input');
		$arrPreferencesData = json_decode( $arrPreferencesData, true );
		
		try {
			
				$is_updated = Users_model::webserviceUpdatePreferences( $arrPreferencesData );
				
				if( $is_updated ) {
					echo $successMsg;
				} else {
					echo $errorMsg;
				}
			
		} catch ( Exception $e ) {
			echo $errorMsg;
		}
		
	}
	
	public function uploadProfileImage() {
	
		$successMsg = json_encode( array( 'flag' => '1', 'msg' => 'Profile image uploaded successfully') );
		$errorMsg = json_encode( array( 'flag' => '0', 'msg' => 'Profile image not uploaded successfully!') );
		
		try {
			if( !empty($_POST) && !empty($_FILES) ) { 
				$intUserID =  $_POST['user_id'];;
				$strProfileImg = $_FILES['profile_picture'];
				$boolImageUploadFlag = true;
				
				$config['upload_path'] = 'uploads/';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$this->upload->set_allowed_types('gif|jpg|png|jpeg');
				
				if( true == $this->upload->do_upload('profile_picture') ) {
					
					if( true == file_exists('uploads/'.$strProfileImg)) {
						
						if( false == unlink( 'uploads/'.$strProfileImg )) {
							$boolImageUploadFlag	= false;
						} 
							
					}
					if( true == $boolImageUploadFlag ) {
						$strProfileImg			= $this->upload->file_name;
					}
			 
				} else {
						$boolImageUploadFlag	= false;
						
				}
				
				if( true == $boolImageUploadFlag ) {
					
					$is_uploaded = Users_model::webserviceUploadProfilePicture( $strProfileImg, $intUserID );
					
					if( $is_uploaded ) {
						echo $successMsg;
					} else {
						echo $errorMsg;
					}
					
				} else {
					echo $errorMsg;
				}
				
			} else {
				echo $errorMsg;
			}
			
		} catch( Exception $e ) {
			echo $errorMsg;
		}
	
	}
	
	public function viewGraph() {
		
		$arrGraphData = file_get_contents('php://input');
		$arrGraphData = json_decode( $arrGraphData, true );
		
		try {
			if( !empty($arrGraphData) ) { 
				$intAcesId =  $arrGraphData['aces_id'];
				$intDeviceId =  $arrGraphData['device_id'];
				$intDeviceTypeId = $arrGraphData['device_type_id'];
				$strUnitType = array( 'id', 'ambient_temperature', 'cum_watt_hour');
				if( false == isset($intAcesId) ) {
					$intAcesId = 0;
				} 
				$arrmixAcesAtmData['flag'] = 1;
				$arrmixAcesAtmData['aces_atm_data']	= energy_meter_model::getLastHourAcesAtmByUnitTypeByDeviceId( $strUnitType, $intDeviceId, $intAcesId );
				
				if( true == empty( $arrmixAcesAtmData['aces_atm_data'] ) ) {
					
					$arrmixAcesAtmData['aces_atm_data']		= array( );
				
				} else {
					
					foreach( $arrmixAcesAtmData['aces_atm_data'] as $intIndex => $value ) {
						$arrmixAcesAtmData['aces_atm_data'][$intIndex]->ambient_temperature = $value->ambient_temperature/10;
					}
					
				}
				
				$stats = array();
				$fieldsarr 			= array( 'ambient_temperature', 'set_temperature','grid_voltage', 'cyclic_period', 'ac_switch_duration', 'cum_kwh' , 'cum_watt_hour', 'ac1_stat', 'ac2_stat', 'compressor_status','humidity' );
				$getStatData = energy_meter_model::getAcesAtmStatsByDeviceId( $fieldsarr, $intDeviceId );
					
				if( true == is_object( $getStatData ) ) {
						
					$intAc1Fault		= ( 1 == $getStatData->ac1_stat && 0 == $getStatData->compressor_status &&  $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10) ? 0 : 1;
					$intAc2Fault		= ( 1 == $getStatData->ac2_stat  && 0 == $getStatData->compressor_status &&  $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10) ? 0 : 1;
						
					$intDeviceFault	= ( 0 == $getStatData->ac1_stat && 0 == $getStatData->ac2_stat &&  $getStatData->ambient_temperature/10 > $getStatData->set_temperature/10) ? 0 : 1;
					$stats = array(
									"temperature"=>round($getStatData->ambient_temperature/10,0),  "set_temperature"=>round($getStatData->set_temperature/10, 2), "cyclic_period"=>$getStatData->cyclic_period ,
									"ac_switch_duration"=>$getStatData->ac_switch_duration/60, 'kwh'=>round( $getStatData->cum_kwh . '.'. $getStatData->cum_watt_hour, 2), 'ac1_stat'=>$getStatData->ac1_stat,
									'ac2_stat'=>$getStatData->ac2_stat, 'compressor_status'=>$getStatData->compressor_status, "ac1_fault"=> $intAc1Fault, "ac2_fault"=> $intAc2Fault, "device_fault"=> $intDeviceFault  );
				
				} else {
					$stats = array(
									"temperature"=>0, "set_temperature"=>0, "voltage"=>0, "cyclic_period"=>0,
									"ac_switch_duration"=>0, "kwh"=>0, 'ac1_stat'=>0, 'ac2_stat'=>0, 'compressor_status'=>0,
									"ac1_fault"=> 0, "ac2_fault"=> 0, "device_fault"=> 0
					);
				}
				$arrmixAcesAtmData['aces_stats'] = $stats;
				
				echo json_encode($arrmixAcesAtmData);
			
			} else {
				
				echo json_encode( array( 'flag' => '0', 'msg' => 'Please send required data!') );
				
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => '0', 'msg' => 'Please send required data!') );
		}

	}

	public function routeWiseLocation() {

		$arrRouteData = file_get_contents('php://input');
		$arrRouteData = json_decode( $arrRouteData, true );

		try {

			if( !empty($arrRouteData) ) {

				$intPhoneNumber = $arrRouteData['mobile_number'];
				$arrMixlocationData['flag'] = 1;
				//$arrMixlocationData['asset_info']		= 		asset_model::fetchAssetByPhoneNumber( $intPhoneNumber );
				$arrMixlocationData['reports'] 		= 		Tracker_model::fetchDistanceReports( $intPhoneNumber );

				if( !empty($arrMixlocationData['reports']) ) {

					echo json_encode($arrMixlocationData);

				} else {

					echo json_encode( array( 'flag' => '0', 'msg' => 'Record not found!') );
					
				}

			} else {
				echo json_encode( array( 'flag' => '0', 'msg' => 'Get or empty request not allowed!') );
			}

		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => '0', 'msg' => 'Get or empty request not allowed!') );
		}

	}

	public function getRoute() {
		$arrRouteInfo = file_get_contents('php://input');
		$arrRouteInfo = json_decode( $arrRouteInfo, true );

		try {

			if( !empty($arrRouteInfo) ) {

				$intPhoneNumber = $arrRouteInfo['mobile_number'];
				$intTokenNumber = $arrRouteInfo['token'];
				$arrMixlocationData['flag'] = 1;
				$arrMixlocationData['reports'] = Tracker_model::getroute( $intPhoneNumber , $intTokenNumber );

				if( !empty($arrMixlocationData['reports']) ) {

					echo json_encode($arrMixlocationData);

				} else {

					echo json_encode( array( 'flag' => '0', 'msg' => 'Record not found!') );
					
				}

			} else {
				echo json_encode( array( 'flag' => '0', 'msg' => 'Get or empty request not allowed!') );
			}

		} catch(Exception $e) {
			echo json_encode( array( 'flag' => '0', 'msg' => 'Get or empty request not allowed!') );
		}
	}

	public function updateSocial() {

		$successMsg = json_encode( array( 'flag' => '1', 'msg' => 'User social updated successfully') );
		$errorMsg = json_encode( array( 'flag' => '0', 'msg' => 'User social not updated successfully!') );
		
		$arrUserSocialData = file_get_contents('php://input');
		$arrUserSocialData = json_decode( $arrUserSocialData, true );
		$user_id = $arrUserSocialData['user_id'];
		
		try {
				if( !empty($user_id) ) {
					$arrmixUserSocialWebsites 	= Users_model::fetchUserSocialWebsitesByUserId( $user_id );
	    	
			    	$arrintUserWebsiteIds 	= array();
			    	$arrstrUserWebsites		= array();
			    	
			    	// Get website ids, name.
			    	foreach(  $arrmixUserSocialWebsites as $intIndex => $objUser ) {
				    		$arrintUserWebsiteIds[]		= $objUser->id;
				    		$arrstrUserWebsites[]			= $objUser->website;
			    	}

			    	$arrintDeletedIds		= ( false == empty( $arrUserSocialData['user_website_id'] ) ) ? array_diff( $arrintUserWebsiteIds, $arrUserSocialData['user_website_id'] ) : $arrintUserWebsiteIds;

			    	$boolIsUpdate	= false;
		    			 
	    			if( false == empty( $arrintDeletedIds ) ) {
    					// Remove deleted websites.
	    				if( true == Users_model::deleteWebsite( $arrintDeletedIds ) ) {
	    						$boolIsUpdate	= true;
	    				}
	    			}
	    			$arrUpdateData = array();
	    			$arrInsertData = array();
	    			foreach ($arrUserSocialData['website'] as $key => $value) {
	    				
	    				if( @$arrUpdateData[$arrUserSocialData['user_website_id'][$key]] ) {
	    					$arrUpdateData[$arrUserSocialData['user_website_id'][$key]] = $value;
	    				} else {
	    					$arrInsertData[$key] = $value;
	    				}
	    				
	    			}

	    			$is_updated = Users_model::serviceUpdateSocial( $arrUpdateData );

	    			if( false == $is_updated ) {
	    				echo $errorMsg;
	    			} 

					$is_inserted = Users_model::webserviceInsertSocial( $arrInsertData, $user_id );
					
					if( $is_inserted ) {
						echo $successMsg;
					} else {
						echo $errorMsg;
					}
				} else {
					echo $errorMsg;
				}
			
		} catch( Exception $e ) {
			
			echo $errorMsg;
		}

	}

	public function getLocation() {
		
		$location_data['flag'] = 1;
		$locations 	= Locations_model::getLocations();
		$location_data['location_data'] = $locations->result();
		echo json_encode($location_data);
	}

	public function createOtp() {

		$arrOtpData = file_get_contents('php://input');
		$arrOtpData = json_decode( $arrOtpData, true );
		$mobile_number = $arrOtpData['mobile_number'];
		$user_name = $arrOtpData['user_name'];
		
		$strToken = $this->generateOtp( 6, 'abcdefghijklmnopqrstuvwxyz1234567890' );
	
		$this->sendSms( $mobile_number, $strToken, $user_name );
	}

	public function generateOtp($length , $chars ) {
	
			$chars_length = (strlen($chars) - 1);
			$string = $chars{rand(0, $chars_length)};
			for ($i = 1; $i < $length; $i = strlen($string))
			{
				$r = $chars{rand(0, $chars_length)};
				if ($r != $string{$i - 1}) $string .=  $r;
			}
			return $string;
	}

	public function sendSms( $intMobileNumber, $strToken, $user_name ) {
	
		// Replace with your username
		$user = "rectusenergy";
			
		// Replace with your API KEY (We have sent API KEY on activation email, also available on panel)
		$apikey = "icv59VrXI2CWEDCHRfOw";
			
		// Replace if you have your own Sender ID, else donot change
		$senderid  =  "MYTEXT";
			
		// Replace with the destination mobile Number to which you want to send sms
		$mobile  =  $intMobileNumber;
			
		// Replace with your Message content
		$message   =  'Dear '.$user_name.'! Your One-Time password is: '. $strToken;
		$message = urlencode($message);
			
		// For Plain Text, use "txt" ; for Unicode symbols or regional Languages like hindi/tamil/kannada use "uni"
		$type   =  "txt";
			
 		$ch = curl_init();
 		// set URL and other appropriate options
  		curl_setopt($ch, CURLOPT_URL, "http://smshorizon.co.in/api/sendsms.php?user=".$user."&apikey=".$apikey."&mobile=".$intMobileNumber."&senderid=".$senderid."&message=".$message."&type=".$type."");
  		curl_setopt($ch, CURLOPT_HEADER, 0);
			
		// grab URL and pass it to the browser
 		$output = curl_exec($ch);

		// close cURL resource, and free up system resources
		curl_close($ch);

		// Display MSGID of the successful sms push
		if( $output ) {
			echo json_encode( array( 'flag' => '1','otp' => $strToken, 'msg' => 'Otp sent successfully!') );
		} else {
			echo json_encode( array( 'flag' => '0', 'msg' => 'Otp not sent successfully!') );
		}
			
	}


	public function verifyOtp() {

		$arrOtpData = file_get_contents('php://input');
		$arrOtpData = json_decode( $arrOtpData, true );
		
		
		try {

			if( !empty($arrOtpData) ) {
				$user_id = $arrOtpData['user_id'];
				$strMobileNumber = $arrOtpData['mobile_no'];
				$strField		= $arrOtpData['field'];
				$intMobileID	= $arrOtpData['mobileId'];
				if( false == empty( $intMobileID ) && true == Users_model::webserviceUpdateMobileVerificationStatus( $strField, $user_id, $strMobileNumber, $intMobileID ) ) {
						echo json_encode(  array( 'flag' => '1', 'msg' => 'successfully verified', 'number' =>$strMobileNumber ) );
				} else {
						echo json_encode(  array( 'flag' => '0', 'msg' => 'Not verified', 'number' => $strMobileNumber ) );
				}
			} else {
				echo json_encode(  array( 'flag' => '0', 'msg' => 'not verified' ) );	
			}

		} catch( Exception $e) {
			echo json_encode(  array( 'flag' => '0', 'msg' => 'not verified' ) );
		}
	}

	public function getLastVTSRecord() {
		$arrDeviceData = file_get_contents('php://input');
		$arrDeviceData = json_decode( $arrDeviceData, true );

		try {

			if( !empty( $arrDeviceData ) ) { 

				$intPhoneNumber = $arrDeviceData['phoneNumber'];
				$arrTrackerData = Tracker_model::getLastTrackerRecord( $intPhoneNumber );

				echo json_encode( array( 'flag' => 1, 'data' => $arrTrackerData ));
			} else {

				echo json_encode( array( 'flag' => 0, 'msg' => 'Please send required parameter'));

			}

		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'msg' => 'Please send required parameter'));			
		}
	}

	public function editDevice() {
		$arrDeviceData = file_get_contents('php://input');
		$arrDeviceData = json_decode( $arrDeviceData, true);
   
		try {

			if( !empty( $arrDeviceData ) ) {
			
				$intDeviceId = $arrDeviceData['deviceNumber'];
				$intUserID = $arrDeviceData['user_id'];
				unset($arrDeviceData['deviceNumber']);
				unset($arrDeviceData['user_id']);
				if( $intUserID && $intDeviceId ) {
					$update_device_res = Devices_model::update( $intDeviceId, $arrDeviceData, $intUserID);	
					if( $update_device_res ) {
						echo json_encode( array( 'flag' => 1, 'data' => 'Device data updated successfully' ));
					} else {
						echo json_encode( array( 'flag' => 0, 'data' => 'Device data not updated!' ));
					}
				} else {
					echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide Device data'));
				}
				
				
			} else {
				echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide Device data'));
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide Device data'));
		}
	}

	public function deleteDevice() {
		$arrDeviceData = array();
		$arrDeviceData = file_get_contents('php://input');
		$arrDeviceData = json_decode( $arrDeviceData );
	//	print_r($arrDeviceData);
	
		try {
	
			if( !empty($arrDeviceData) ) {
			//	print_r($arrDeviceData);
				//echo "dd";exit;
				$intDeviceId = $arrDeviceData->deviceNumber;
				$intUserID = $arrDeviceData->user_id;
				if( $intDeviceId && $intUserID ) {
				
					 $response = Devices_model::deleteDeviceById( $intDeviceId, $intUserID );	
					if( $response ) {
						echo json_encode( array( 'flag' => 1, 'data' => 'Device deleted successfully' ));
					} else {
						echo json_encode( array( 'flag' => 0, 'data' => 'Device not deleted successfully!' ));
					}
				} else {
					echo json_encode( array( 'flag' => 0, 'data' => 'Please provide device related information!' ));	
				}
				
			} else {
				echo json_encode( array( 'flag' => 0, 'data' => 'Please provide device related information!' ));
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'data' => 'Please provide device related information!' ));
		}
	}

	public function deleteTracker() {

		$arrTrackerData = file_get_contents('php://input');
		$arrTrackerData = json_decode( $arrTrackerData );

		try {
			if( !empty($arrTrackerData) ) {
				$intPhoneNumber = $arrTrackerData->phoneNumber;
				$intToken = $arrTrackerData->token;
				if( $intPhoneNumber && $intToken ) {
					$response = Tracker_model::deleteVtsTracker( $intPhoneNumber, $intToken );	
					if( $response ) {
						echo json_encode( array( 'flag' => 1, 'data' => 'Tracker data deleted successfully' ));
					} else {
						echo json_encode( array( 'flag' => 0, 'data' => 'Tracker data not deleted successfully!' ));
					}
				} else {
					echo json_encode( array( 'flag' => 0, 'data' => 'Please provide tracker related information!' ));	
				}
				
			} else {
				echo json_encode( array( 'flag' => 0, 'data' => 'Please provide tracker related information!' ));
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'data' => 'Please provide tracker related information!' ));
		}
	}

	public function getAcesParameterList() {

		$AcesParameters['flag'] = 1;
		$parameters 	= Devices_model::getAcesParameter();
		$AcesParameters['aces_parameters'] = $parameters->result();
		echo json_encode($AcesParameters);

	}

	public function updateAcesParameterValue() {
		$arrAcesParameterData = file_get_contents('php://input');
		$arrAcesParameterData = json_decode( $arrAcesParameterData, true);

		try {

			if( !empty( $arrAcesParameterData ) ) {
				$intUnitId = $arrAcesParameterData['unit_id'];
				
				if( $intUnitId ) {
					$aces_parameter_res = Devices_model::checkUpdateInsertParameters( $arrAcesParameterData );	
					if( $aces_parameter_res ) {
						echo json_encode( array( 'flag' => 1, 'data' => 'Record updated successfully' ));
					} else {
						echo json_encode( array( 'flag' => 0, 'data' => 'Record not updated!' ));
					}
				} else {
					echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide Aces parameter data'));
				}
				
				
			} else {
				echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide Aces parameter data'));
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide Aces parameter data'));
		}
	}

	public function forgotPassword() {
		$arrUserData = file_get_contents('php://input');
		$arrUserData = json_decode( $arrUserData, true);

		try {

			if( !empty( $arrUserData ) ) {

				$userName = $arrUserData['username'];

				$objUser					= Users_model::fetchEmailAddressByForgotPasswordFieldByUserType( $userName, user_type_model::USER );
				          
		        if( true  == is_object( $objUser ) ) {
		        			
					$strRandomTempPass = md5( time() );
										
					if( true == Users_model::updateResetPasswordCode( $objUser->email_address,  $strRandomTempPass ) ) {
						
							$strUrl			= base_url()."users/action/resetPass/" . $strRandomTempPass;
						 	$strMessage		= 'Please click on the following link for reset your password.'.
																			'<br><br>Username : '.  $objUser->username .
																			'<br><br>Account Number : '.  $objUser->account_number .
																			'<br><br>'.'<a href='.$strUrl.'>Reset Password ';
							
							$arrstrEmailFormat = array(
					 				'header' 	=> 'Dear Recipient,',
					 				'content'	=> $strMessage
							);
							
							if( true == $this->sendMail( 'no_reply@rectusenergy.com',  $objUser->email_address,  'Reset Password', $arrstrEmailFormat ) ) {
									echo json_encode( array( 'flag' => 1, 'msg' => 'Reset password link sent to  your email address. Please check the email to reset password'));
							} else {
								echo json_encode( array( 'flag' => 0, 'msg' => 'Password not reset successfully!'));
							}
							
					} else {
						echo json_encode( array( 'flag' => 0, 'msg' => 'Password not reset successfully!'));
					}
				} else {
					echo json_encode( array( 'flag' => 0, 'msg' => 'Password not reset successfully!'));
				}
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'msg' => 'Password not reset successfully!'));
		}
	}

	public function resetPassword() {
		$arrPwdData = file_get_contents('php://input');
		$arrPwdData = json_decode( $arrPwdData, true);

		try {

			if( !empty( $arrPwdData ) ) {

				$password = $arrPwdData['password'];
				$strToken = $arrPwdData['token'];

				if( true == Users_model::updatePassword( $strToken, $password ) ) {
				    echo json_encode( array( 'flag' => 1, 'msg' => 'Password reset successfully!'));
				} else {
				     echo json_encode( array( 'flag' => 0, 'msg' => 'Password not save successfully!'));
				}
			} else {
				echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide required data!'));	
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'msg' => 'Password not save successfully!'));
		}

	}
	public function shareLocation() {
		
		$strGnissIds 	= file_get_contents('php://input');
		$arrintGnissIds	= json_decode( $strGnissIds, true);
		
		try {
			$arrInvalidGnissIds	= array();
			$boolSentMailFlag	= false;
			$arrstrNotifications = array();
			if( true == is_array( $arrintGnissIds ) ) {
				
				$intShareById		= $arrintGnissIds['sharedBy'];
				$intCountDevices	= count( $arrintGnissIds['deviceid'] );
				$objShareUser		= Users_model::fetchUserById( $intShareById );
				
				foreach( $arrintGnissIds['gnissid'] as $intKey => $intGnissId ) {
					
					$objUser	= Users_model::fetchUsersByAccountNumber( $intGnissId );

					if( true == is_object( $objUser ) ) {
						
						foreach( $arrintGnissIds['deviceid'] as $intDeviceKey => $intDeviceId ) {
							
							$objDevice	= Devices_model::fetchDeviceById( $intDeviceId );
							
							if( true == is_object( $objDevice ) ) {
								$link	= base_url().'livetracking/share/'.$objUser->email_address;
								$strMessage	= 'Your Freind Shared Location With You , Please click on following link :<br><br>'.'<a href=' . base_url().'livetracking/share/' .$objDevice->phone_number.'></a>'.
											'<div style="color:green;"> URL : '. base_url().'livetracking/share/' . $objDevice->phone_number.' , Shared With Token : '.$objUser->account_number.'</div>';
								 
								$strHeaderMsg    = '<div style="font-size:1.5em;">Welcome to GNISS!</div><br><br/>'.
													'Dear Recipient,<br><br>';
								$arrstrEmailFormat = array(
									'header'     => $strHeaderMsg,
									'content'    => $strMessage
								);
								
						
								if( true == $this->sendMail( 'no_reply@rectusenergy.com', $objUser->email_address,  'Your friend share Location with you', $arrstrEmailFormat ) ) {
									
									$boolSentMailFlag	= true;
									$arrmixShareData = array ("shared_device"=> $objDevice->phone_number, "shared_gnissid"=> $intGnissId, "device_id"=> $intDeviceId , "is_active"=> 1, "shared_by"=> $intShareById );
									
									if( true == Devices_model::fetchSharedDeviceByMobileNumberByGnissIdByShareBy( $objDevice->phone_number, $intGnissId, $intShareById, $arrmixShareData ) ) {
										$boolSentMailFlag	= true;
									} 
// 									if( true == Devices_model::insertSharedDevices( $arrmixShareData ) ){
// 										$boolSentMailFlag	= true;
// 									}
								} else {
									$boolSentMailFlag	= false;
									$this->session->set_userdata( array( 'errmsg' => "Could not send mail to user." ) );
								}
							}
						}
						$msg = $intCountDevices .' devices shared by ' . $objShareUser->first_name . ' ' . $objShareUser->last_name ;
						$arrstrNotifications =  $this->notifications( $objUser->id , $msg );
					} else {
						$arrInvalidGnissIds[]	= $intGnissId;
					}
				}
				if( true == $boolSentMailFlag ) {
					echo json_encode( array( 'flag' => '1', 'success' => $arrstrNotifications['success'], 'failure' => $arrstrNotifications['failure'] ,'msg' => 'Email Sent successfully'));
				} else {
					echo json_encode( array( 'flag' => '0', 'gnissids' => $arrInvalidGnissIds ));
				}
			} else {
				echo json_encode( array( 'flag' => '0', 'msg' => 'Please provide required data!'));
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => '0', 'msg' => $e->getMessage()));
		}
	}
	
	
	public function fetchShareUsers() {

		$strName 			= file_get_contents('php://input');
		$arrstrUserSearch	= json_decode( $strName, true);
		
		try {
				
			if( true == is_array( $arrstrUserSearch ) ) {
			
				$strUserSearch	=	rtrim( $arrstrUserSearch['name'] );
				$objUsers	= Users_model::fetchShareUsersByName( $strUserSearch );
				
				if( false == empty( $objUsers ) && true == is_array( $objUsers ) ) {
	
					echo json_encode( array( 'flag' => 1, 'users' => $objUsers ));

				} else {
					
					echo json_encode( array( 'flag' => 0, 'msg' => 'No users available' ));
				}
				
			} else {
				echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide required data!'));
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'msg' => $e->getMessage()));
		}
	}
	
	public function listSharedUsers() {
		
		$strUser 		= file_get_contents('php://input');
		$arrstrUser		= json_decode( $strUser, true);
		
		try {
		
			if( true == is_array( $arrstrUser ) ) {
					
				$intUserId	= $arrstrUser['userid'];
				$objUsers	= Tracker_model::SharedLocationByuserId( $intUserId );
		
				if( false == empty( $objUsers ) && true == is_array( $objUsers ) ) {
		
					echo json_encode( array( 'flag' => 1, 'users' => $objUsers ));
		
				} else {
						
					echo json_encode( array( 'flag' => 0, 'msg' => 'No users available' ));
				}
		
			} else {
				echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide required data!'));
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'msg' => $e->getMessage()));
		}
	}
	
	public function listSharedDevices() {
		
		$strUser 		= file_get_contents('php://input');
		$arrstrUser		= json_decode( $strUser, true);
		
		try {
		
			if( true == is_array( $arrstrUser ) ) {
					
				$intGnissId		= $arrstrUser['gnissid'];
				$objSharedUsers	= Tracker_model::SharedDevicesById( $intGnissId );
		
				if( false == empty( $objSharedUsers ) && true == is_array( $objSharedUsers ) ) {
		
					echo json_encode( array( 'flag' => 1, 'users' => $objSharedUsers ));
		
				} else {
		
					echo json_encode( array( 'flag' => 0, 'msg' => 'No users available' ));
				}
		
			} else {
				echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide required data!'));
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'msg' => $e->getMessage()));
		}
	}
	
	public function deactivateSharedLocations() {
	
		$strShareLocation 		= file_get_contents('php://input');
		$arrmixShareLocation	= json_decode( $strShareLocation, true);
		
		try {
			$arrInvalidGnissIds	= array();
			$boolSentMailFlag	= false;
			
			if( true == is_array( $arrmixShareLocation ) ) {
				
				if( true == Tracker_model::sharedLocationDeviceIdsByuserId( $arrmixShareLocation['userid'], $arrmixShareLocation['deviceid'] ) ) {
					echo json_encode( array( 'flag' => 1, 'msg' => 'Successfully deactivated'));
				} else {
					echo json_encode( array( 'flag' => 0, 'msg' => 'Could not deactivate device' ));
				}
			} else {
				echo json_encode( array( 'flag' => 0, 'msg' => 'Please provide required data!'));
			}
		} catch( Exception $e ) {
			echo json_encode( array( 'flag' => 0, 'msg' => $e->getMessage()));
		}
		
	}
	
	public function notifications( $intUserId, $msg ) {
		
		
		//$strNotification	= file_get_contents('php://input');
		//$arrstrNotification	= json_decode( $strNotification, true);
		
		$objUser	= Users_model::fetchUserById( $intUserId );
		
		if( true == is_object( $objUser )) {
			
			//$arrstrNotification['msg']	= 'Hello';
			$url = 'https://android.googleapis.com/gcm/send';
			
// 			$fields = array(
// 					'data' => array( 'score'=> "5*1","time"=>'12', 'msg'=> 'Hello suril' ) ,
// 					'to' => $objUser->gcm_register_id,
// 			);
			
			$fields = array("data"=> array(
				"id"=>$msg, 'flag'=> '1' ), 
				'to' => $objUser->gcm_register_id
			);
			
			$headers = array(
				'Authorization: key=' . GOOGLE_API_KEY,
				'Content-Type: application/json'
			);
			// Open connection
			$ch = curl_init();
			
			// Set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
			
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
			// Execute post
			$result = curl_exec($ch);
			if ($result === FALSE) {
				die('Curl failed: ' . curl_error($ch));
			}
			
			// Close connection
			curl_close($ch);
		
			$arrstrResult = json_decode($result, true );
			
			return array( 'success' => $arrstrResult['success'], 'failure' => $arrstrResult['failure'] );
		}
	}

	public function saveplace() {
	
		$successMsg = json_encode( array( 'flag' => '1', 'msg' => 'Profile image uploaded successfully') );
		$errorMsg = json_encode( array( 'flag' => '0', 'msg' => 'Profile image not uploaded successfully!') );
	
		try {
			if( !empty($_POST) && !empty($_FILES) ) {
				$gniss_id =  $_POST['gniss_id'];
				 $place_name =  $_POST['place_name'];
				$lat =  $_POST['lat'];
				$lang =  $_POST['lang'];
				$address =  $_POST['address'];
				
				$date = date('Y-m-d H:i:s');
				
				 $strProfileImg = $_FILES['place_picture'];
				$boolImageUploadFlag = true;
	
				$config['upload_path'] = 'uploads/';
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$this->upload->set_allowed_types('gif|jpg|png|jpeg');
	
				if( true == $this->upload->do_upload('place_picture') ) {
						
					if( true == file_exists('uploads/'.$strProfileImg)) {
	
						if( false == unlink( 'uploads/'.$strProfileImg )) {
							$boolImageUploadFlag	= false;
						}
							
					}
					if( true == $boolImageUploadFlag ) {
						$strProfileImg	= $this->upload->file_name;
					}
	
				} else {
					
					$boolImageUploadFlag	= false;
	
				}
	
				if( true == $boolImageUploadFlag ) {

						
     $is_uploaded = Users_model::webserviceSavePlace($strProfileImg, $gniss_id ,$place_name,$lat,$lang ,$address  , $date);
						
					if( $is_uploaded ) {
						echo $successMsg;
					} else {
						echo $errorMsg;
					}
						
				} else {
					echo $errorMsg;
				
				}
	
			} else {
				
				echo $errorMsg;
			}
				
		} catch( Exception $e ) {
			echo $errorMsg;
		}
	
	}
	
}

?>
