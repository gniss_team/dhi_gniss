<?php 
/*
 This controller is developed for  Managing Devices information
for the user location.Here we used google map API
for showing location.
Last Updated : 24 Oct 2015
Updated By : Bhatu Jadhav .
*/
if ( ! defined( 'BASEPATH' )) exit( 'No direct script access allowed' );

class CManageDeviceController extends CI_Controller {
	
	
	const C_LINE	= 'Line';
	const C_BAR		= 'Bar';
	const C_PIE		= 'Pie';

	protected $m_strData;
	protected $m_strRawData;
	
	public function __construct() {
        parent::__construct();       
        //$this->load->model('devices_model');
      	
        $this->load->dbforge();
     }
	
  	public function action( $strAction = '' ) {
        // action function is written for redirecting to the perticular functions .
    	switch ( $strAction ) {
    		
    		case 'add' : //die( 'Here' );
	    			$this->addDevices();
	    			break;
    			  	
    		case 'edit' :
	    			$this->editDevices( $strAction );
	    			break;
	    	
	    	case 'update' :
	    			$this->updateDevice( $strAction );
	    			break;
	    				
    		case 'updateMarker' :
    				$this->updateMarker( $strAction );
    				break;
    				
    		case 'setting' :
    				$this->modifySetting( $strAction );
    				break;
	    			
    		case 'deletecolumn' :
    				$this->deletecolumn( $strAction );
    				break;
    			
    		case 'delete' :
	    			$this->deleteDevices( $strAction );
	    			break;
    		
    		case 'changestatus' :
	    			$this->changestatus( $strAction );
	    			break; 
    
    		case 'view' :
	    			$this->viewDevices();
	    			break;
	    			
	    	case 'savepara':
	    			$this->savepara();
	    			break;
	    			
    			
    		case 'viewdevices' :
	    			$this->viewDeviceTypes( $strAction );
	    			break;
    			
   			case 'viewdevice' :
   					$this->editDevices( $strAction );
   					break;
   		
   			case 'displayData':
   					$this->displayEnergyMeterData( $strAction );
   					break;
   					
   			case 'historicgraph':
   					$this->historicgraph( $strAction );
   					break;

   			case 'loadView':
   					$this->loadView( $strAction );
   					break;
   						
    		default :
    			$this->createDevices();
    	}
    }
    
    public function historicgraph(){
    	// this function display view of historic graphs , with date range parameters . 
    	$this->load->view( 'aces_atm_graphs' );
    }
  
  	public function applyValidation( $strAction ) {
    	// This function written for apply validation for historic graph form . 
    	if( 'add' == $strAction ) {
    		
    		$this->form_validation->set_rules('device_name', 'Device name', 'required|is_unique[device_types.name]|alpha');
    		$this->form_validation->set_rules('para1', 'Parameter 1', 'required|alpha');
    		$this->form_validation->set_rules('para2', 'parameter 2', 'required|alpha');

    	} elseif( 'edit' == $strAction ) {
   	
    		foreach($this->input->post() as $key => $value) {
    			if($key != "addassettype"  && $key != "para") {
    				
    				$this->form_validation->set_rules($key, $key, 'required|alpha');
    			} else {
    				return true;
    			}
    		}
    			 
    	}    	
    	if( TRUE == $this->form_validation->run() ) {
			return true;
		} else {
			return false;
		}
    }
    
    public function viewDevices() {
    	    // This function is written for to display all list of decice of logged user from devices table from databae . 
    		$intDeviceType 		= ( false == empty( $this->uri->segment(4) ) ) ? $this->uri->segment(4) : NULL;
    		$intUserId			= $this->session->userdata('user_id');
    		$arrmixDevices['devices_type_id']	= $intDeviceType;
			$arrmixDevices['devices']	= Devices_model::fetchDevicesByUserId( $intUserId, $intDeviceType, NULL );
            
			$objDevieType		= Devices_model::fetchDeviceTypeById( $intDeviceType );
			
			$arrmixDevices['msg']		= $this->session->userdata('sucmsg');
			$this->session->unset_userdata('sucmsg');
			
			switch( $objDevieType->unit_type ) {
				
				case '1':
						$arrmixDevices['device_name']		= $objDevieType->name;
						$this->load->view('view_aces_atm_device', $arrmixDevices );
						break;
					
				case '2':
						$this->load->view('view_energy_meter_device', $arrmixDevices );
						break;
					
				default:
						$arrmixDevices['device_name']		= $objDevieType->name;
						$this->load->view('view_devices', $arrmixDevices );
			}
			
    }
    public function viewDeviceTypes() {
    	
    	// This function written for load all devices type from device type master table . 
	    $arrmixDeviceInfo['device_types']	= Devices_model::fetchDeviceTypes();
	    $arrmixDeviceInfo['msg']			= $this->session->userdata('sucmsg');
	    $this->session->unset_userdata('sucmsg');
	    $this->load->view('admin/view_devices', $arrmixDeviceInfo );
    }
	
 	public function addDevices() {
    	$arrmixDevices	= array();
    		
    	$arrmixDevices['devices']	= Devices_model::fetchDeviceTypes();
    	$arrmixDevices['countries']	= Locations_Model::fetchLocationsByType( 0 );
 		   
 		if( true == is_array(  $this->input->post() ) ) {
 			
 			$this->form_validation->set_rules('device_type_id', 'Device type',  'trim|required|xss_clean');
			$this->form_validation->set_rules('name', 'Name',  'trim|required|alpha_numeric|min_length[3]|max_length[20]|xss_clean');
			$this->form_validation->set_rules('sim_number', 'Sim number', 'trim|is_digit|min_length[19]|max_length[20]|is_unique_record[devices.sim_number]|xss_clean');
			$this->form_validation->set_rules('imei_number', 'IMEI number', 'trim|required|is_digit|exact_length[15]|is_unique_record[devices.imei_number]|xss_clean');
			$this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|is_digit|min_length[6]|max_length[12]|is_unique_record[devices.phone_number]|xss_clean');
			$this->form_validation->set_rules('device_number', 'Device number', 'trim|required|is_digit|min_length[4]|is_unique_record[devices.device_number]|xss_clean');
			$this->form_validation->set_rules('country_id', 'Country', 'trim|required|is_digit|xss_clean' );
			$this->form_validation->set_rules('state_id', 'State', 'trim|required|is_digit|xss_clean' );
			$this->form_validation->set_rules('city_id', 'City', 'trim|required|is_digit|xss_clean' );
			$this->form_validation->set_rules('zip_code', 'Zip code', 'trim|required|is_digit|min_length[4]|max_length[6]|xss_clean' );
			$this->form_validation->set_rules('address_line1', 'Address Line 1', 'trim|required|validate_address|min_length[2]|max_length[100]|xss_clean');
			$this->form_validation->set_rules('address_line2', 'Address Line 2', 'trim|validate_address|min_length[2]|max_length[100]|xss_clean');
				
			if( true == $this->form_validation->run() ) { 
				
				if( true == Devices_model::insert( $this->input->post() ) ) {
					
					$strSuccMsg = "Record added successfully";
														
					$this->session->set_userdata( array( 'successmsg' => $strSuccMsg ) );
					if( $this->input->post( 'device_type_id' ) == '3' ) {
						redirect( base_url() . 'assets/action/manage' );
					} else{
						redirect( base_url() . 'devices/action/view/' . $this->input->post( 'device_type_id' ) );
					}
				}
					
			} else {
				
				$arrmixDevices['states']	 = Locations_Model::fetchLocationsByCountryIdByType( $this->input->post('country_id'), '1' );
				$arrmixDevices['cities']	 = Locations_Model::fetchLocationsByCountryIdByStateIdByType( $this->input->post('country_id'), $this->input->post('state_id'), '2' );
			}
		} 
    	//$arrstrDeviceTypes['device_types'] = $this->devices_model->fetchDeviceTypes();
    	$this->load->view( 'add_devices', $arrmixDevices);
    }
    
    public function updateDevice() {
    	
    	$intDeviceId			= ( int ) $this->uri->segment( 4 );
    	$intUserId				= ( int ) $this->session->userdata('user_id');
    	// This function written for add devices into device table .
    	$arrmixDevices	= array();
    	$errmsg	= "";
    	$arrmixDevices['devices_types']	= Devices_model::fetchDeviceTypes();
    	
    	$arrmixDevices['device']			= Devices_model::fetchDeviceByIdByUserId( $intDeviceId, $intUserId );
    	if( 0 == count( $arrmixDevices['device'] ) ) {
    		$this->load->view( 'page-not-found' );
    		return;
    	}
    	$arrmixDevices['countries']	 = Locations_Model::fetchLocationsByType( 0 );
    	$arrmixDevices['states']	 = Locations_Model::fetchLocationsByCountryIdByType( $arrmixDevices['device']->country_id, '1' );
    	$arrmixDevices['cities']	 = Locations_Model::fetchLocationsByCountryIdByStateIdByType( $arrmixDevices['device']->country_id, $arrmixDevices['device']->state_id, '2' );
    	
    	if( true == is_array(  $this->input->post() ) ) {
    		
    		$this->form_validation->set_rules('name', 'Name',  'trim|required|alpha_numeric|min_length[3]|max_length[20]|xss_clean');
     		$this->form_validation->set_rules('sim_number', 'Sim number', 'trim|is_digit|min_length[19]|max_length[20]|unique_check[devices.sim_number.' . $intDeviceId .']|xss_clean');
     		$this->form_validation->set_rules('device_number', 'Device number', 'trim|required|is_digit|min_length[4]|unique_check[devices.device_number.' . $intDeviceId .']|xss_clean');
    		$this->form_validation->set_rules('imei_number', 'IMEI number', 'trim|required|is_digit|exact_length[15]|unique_check[devices.imei_number.' . $intDeviceId .']|xss_clean');
    		$this->form_validation->set_rules('phone_number', 'Phone number', 'trim|required|is_digit|min_length[6]|max_length[12]|unique_check[devices.phone_number.' . $intDeviceId .']|xss_clean');
    		$this->form_validation->set_rules('country_id', 'Country', 'trim|required|is_digit|xss_clean' );
    		$this->form_validation->set_rules('state_id', 'State', 'trim|required|is_digit|xss_clean' );
    		$this->form_validation->set_rules('city_id', 'City', 'trim|required|is_digit|xss_clean' );
    		$this->form_validation->set_rules('zip_code', 'Zip code', 'trim|required|is_digit|min_length[4]|max_length[6]|xss_clean' );
    		$this->form_validation->set_rules('address_line1', 'Address Line 1', 'trim|required|validate_address|min_length[2]|max_length[100]|xss_clean');
    		$this->form_validation->set_rules('address_line2', 'Address Line 2', 'trim|validate_address|min_length[2]|max_length[100]|xss_clean');

    		if( true == $this->form_validation->run() ) { 
    			
	   			if( true ==  Devices_model::update( $intDeviceId, $this->input->post() ) ) { 
    				$this->session->set_userdata( array( 'successmsg' =>"Record updated successfully" ) );
    				$this->session->set_userdata( array( 'errmsg' =>$errmsg) );
    				
    				if( 3 ==  $arrmixDevices['device']->device_type_id ) {
    					redirect( base_url() . 'assets/action/manage' );
    				}
    				if( 1 ==  $arrmixDevices['device']->device_type_id ) {
    					redirect( base_url() . 'devices/action/view/' . $arrmixDevices['device']->device_type_id );
    				}
    			}
    		} else {
    			$arrmixDevices['countries']	 = Locations_Model::fetchLocationsByType( 0 );
    			$arrmixDevices['states']	 = Locations_Model::fetchLocationsByCountryIdByType( $this->input->post('country_id'), '1' );
    			$arrmixDevices['cities']	 = Locations_Model::fetchLocationsByCountryIdByStateIdByType( $this->input->post('country_id'), $this->input->post('state_id'), '2' );
    			
    		}
    		
    	}

    	$this->load->view( 'update_device', $arrmixDevices );
    }
    
    public function updateMarker() {
    	 
    	$intDeviceId	= ( int ) $this->uri->segment( 4 );
    	$intUserId		= ( int ) $this->session->userdata('user_id');
    	// This function written for add devices into device table .
    	
    	$strMsg	= "";
    	$arrmixDevices['device']	= Devices_model::fetchDeviceByIdByUserId( $intDeviceId, $intUserId );
    	
    	if( true == is_array(  $this->input->post() ) ) {
    		$boolIsError = true;
    		if( true == empty($this->input->post('marker_icon')) && true == empty( $_FILES['marker_img']['name'] ) ) {
    			$this->form_validation->set_rules( 'marker_icon', 'Marker icon', 'trim|callback_image_exist|xss_clean');
    			$boolIsError	= false;
    		} 
    		
    		
    		if( true == $this->form_validation->run() || true == $boolIsError ) {
    			
    			$boolIsUpdate		= false;
    			$arrstrMarkerIcon	= array('car-icon.png', 'truck-icon.png', 'map-marker-icon.png');
    			
    			$config['upload_path'] = 'images/map_icons/';
    			$this->load->library('upload', $config);
    			$this->upload->initialize($config);
    			$this->upload->set_allowed_types('gif|jpg|png|jpeg|jpe');
        			
    			// uplod profile image.
    			if( true == empty($this->input->post('marker_icon'))) { 
    					
    				if(  false == empty( $_FILES['marker_img']['name'] )) {
    					
    					if( true == $this->upload->do_upload('marker_img') ) {
    				
	    					$strMsg	.= "Marker icon uploaded successfully.";
	    					
	    					if( true == file_exists( base_url() . 'images/map_icons/'.$arrmixDevices['device']->marker_icon )) {
	    
	    						if( false == unlink( base_url() . 'images/map_icons/'.$arrmixDevices['device']->marker_icon )) {
	    							$strMsg	.= "Error deleting old file, ";
	    						}
	    					};
	    					
	    					$arrobjUpload			= $this->upload->data();
	    					$arrstrUploadData		= $this->resize( $arrobjUpload );
	    					$strMarkerImg			= strtolower( $arrstrUploadData['upload_image_name'] );
	    					
	    				} else {
	    
	    					$strMarkerImg	= NULL;
	    					$strMsg .= $this->upload->display_errors();
	    				}
    				} 
    				
    			} else {
    				
    				if( false == in_array($this->input->post('marker_icon'), $arrstrMarkerIcon ) && false == empty( $_FILES['marker_img']['name'] )) {	
	    				if( true == $this->upload->do_upload('marker_img') ) {
	    					
	    					$strMsg	.= "Marker icon uploaded successfully.";
	    					if( true == file_exists( base_url() . 'images/map_icons/'.$arrmixDevices['device']->marker_icon )) {
	    						
	    						if( false == unlink( base_url() . 'images/map_icons/'.$arrmixDevices['device']->marker_icon )) {
	    							$strMsg	.= "Error deleting old file, ";
	    						}
	    					};
	    
	    					$arrobjUpload	 	= $this->upload->data();
	    					$arrstrUploadData	= $this->resize( $arrobjUpload );
	    
	    					//$strMarkerImg			= $this->upload->file_name;
	    					$strMarkerImg		= strtolower( $arrstrUploadData['upload_image_name'] );
	    				
	    				} else {
	    							
	    					$strMarkerImg	= NULL;
	    					$strMsg .= $this->upload->display_errors();
	    				}
    				} else {
    					$strMarkerImg = $this->input->post('marker_icon');
    					$strMsg .= "Marker icon updated successfully.";
    				}

    			}
    			
    			$_POST['marker_icon']	= $strMarkerImg;
    			if( true == Devices_model::update( $intDeviceId, $this->input->post() ) ) {
    				$boolIsUpdate	= true;
    			}
    			
    			if( true == $boolIsUpdate ) {
			       	$data = array(
			    		'error' => 'suc',
	                 	'msg' => $strMsg
	                 );
	                 
	            } else {
                	
					$data = array(
				    	'error' => 'true',
						'msg' => $strMsg
			        );
		       	}
		       	echo json_encode($data);
    		} else {
    			
    			$data = array(
					'error'	 => 'true',
					'msg' => form_error('marker_icon')
		        );
			      	
				echo json_encode($data);
    		}
    	} 
    }
    
    public function modifySetting( $strAction ) {
    	
    	$intDeviceId 	= ( int ) $this->uri->segment(4);
   		$arrmixInput	= $this->input->post();

    	$arrstrResult	= array();
    	
    	if( true == is_array( $this->input->post() ) ) {
    		
    		unset( $arrmixInput['device_id'] );  	
    				
	    	if( true == $this->devices_model->update( $intDeviceId, $arrmixInput ) ) {
	    		$arrstrResult = array( 'error'	 => 'false', 'msg' => 'Device settings updated successfully' );
	    	} else {
	    		$arrstrResult = array( 'error'	 => 'true', 'msg' => 'Device settings not updated' );
	    	}
	    	
    	}
    	echo json_encode( $arrstrResult );
    }
    
    public function editDevices( $strAction ) {
    	// This function written for Edit devices into device table .
    	$arrmixDeviceInfo	= array();
    	
    	$arrmixDeviceInfo['device_type_id'] = $this->uri->segment(4);
    	$arrmixDeviceInfo['device_id'] 		= $this->uri->segment(5);
    	$arrmixDeviceInfo['serrial_no'] 	= $this->uri->segment(6);
    	    	
 		$objDevice	= Devices_model::fetchDeviceByDeviceTypeIdByDeviceId( $arrmixDeviceInfo['device_type_id'], $arrmixDeviceInfo['device_id'] );
 		
    	if( true ==is_array( $this->input->post() ) ) {
    		$stringlength =  count($this->input->post());
    		
					$this->form_validation->set_rules('set_temperature', 'Set temperature',  'is_digit|trim|less_than[31]|greater_than[19]|xss_clean');
					$this->form_validation->set_rules('as_switch_dur', 'AC switch duration',  'trim|is_digit|greater_than[0]|less_than[13]|xss_clean');
					$this->form_validation->set_rules('high_cut', 'High cut', 'trim|is_digit|less_than[315]|callback_valhighcut|xss_clean');
					$this->form_validation->set_rules('low_cut', 'Low cut', 'trim|is_digit|greater_than[99]|less_than[200]|xss_clean');
					$this->form_validation->set_rules('temperature_offset', 'Temperature offset', 'trim|is_digit|is_digit|greater_than[0]|less_than[29]|xss_clean');
					$this->form_validation->set_rules('main_offset', 'Main offset', 'trim|is_digit|is_digit|greater_than[69]|less_than[131]|xss_clean');
					$this->form_validation->set_rules('load_offset', 'Load offset', 'trim|is_digit|is_digit|greater_than[479]|less_than[481]|xss_clean');
					$this->form_validation->set_rules('load_gain', 'Load gain', 'trim|is_digit|is_digit|greater_than[49]|less_than[151]|xss_clean');
				
					 if( true == $this->form_validation->run() ) { 
					 	
					 	$this->load->library( 'validatechecksum' );
					 	$stringlength = $stringlength *2;
					 	$this->m_strData	= '0215'.$stringlength;
					 	$this->m_strRawData	=	 '';
					 
					 //  $this->convertHextoFourDigit( $objDevice->unit_type );
					 	
					 //	$this->convertHextoFourDigit( $objDevice->device_number );
					 		
					 		foreach( $this->input->post() as $strKey =>$fltValue ) {
						 		
						 			if( false == empty( $fltValue ) ) {
						 					
						 	$this->m_strRawData	.=$this->convertHextoFourDigit( strtoupper($fltValue) );
						 			} else {
							 	$this->m_strRawData	.=	'0000';
							 		}
					 		}
					 			 $finslstring =  strtoupper($this->m_strData.$this->m_strRawData);
					 			
							 	
					 		$arrData = str_split( $finslstring, 2 );
					 		$arrintData = array_values( $arrData ); 
					 	//	print_r($arrintData);
					 //	echo $this->m_strData;
					 		$objvalidateChecksum = new Validatechecksum();
					 		$strComputedChecksum =  $objvalidateChecksum->validChecksum( $arrintData );

					 		// echo ':'.$finslstring	.= $strComputedChecksum;
							// check for avilability 
					 		$arrmixUserData = array('parastr' => $finslstring,
					 				'unit_id' => $this->input->post('serrial_no'),
					 				'status' => 'active'
					 				);
							 if(Devices_model::checkforunitid($this->input->post('serrial_no')) > 0)
							 {
							 	Devices_model::updatecomparameters($this->input->post('serrial_no'),$arrmixUserData);
							 }else {
							 	Devices_model::insertcomparameters($arrmixUserData);
							 }
					 		// end here 
							//	$arrmixDevices['devices'] = Devices_model::insert( $this->input->post() );
					
							
							    if($this->input->post('call_type')=='ajax') {
							    	$data = array(
							    			'error' => 'suc',
							    			'msg' => 'Social links updated successfully.'
							    	);
							    	echo json_encode($data);
							    	
								    }else {
								    	$this->session->set_userdata( array( 'successmsg' =>"parameter send successfully" ) );
								    	redirect( base_url() . 'devices/action/view/'.$this->input->post('device_type_id'));
								    		
							    }
					}
					else {
							if($this->input->post('call_type')=='ajax') {
							$data = array(
									'error' => 'true',
									'set_temperature_val' => form_error('set_temperature'),
									'as_switch_dur_val' => form_error('as_switch_dur'),
									'high_cut_val' => form_error('high_cut'),
									'low_cut_val' => form_error('low_cut'),
									'temperature_offset_val' => form_error('temperature_offset'),
									'main_offset_val' => form_error('main_offset'),
									'load_offset_val' => form_error('load_offset'),
									'load_gain_val' => form_error('load_gain')
						
							);
							echo json_encode($data);
							}
					}
    	}
    	
    //	$this->load->view('admin/edit_devices',$arrmixDeviceInfo);
    	if($this->input->post('call_type')!='ajax') {
    	$this->load->view( 'edit_aces_atm', $arrmixDeviceInfo );
    	}
    }
	
	public function convertHextoFourDigit( $strHexNumber ) {
		// This function written for convert hex number to decimal number for making the check sum string  .
	 	$strHexVal	= dechex( $strHexNumber );
	   	//$intTotal
	   	switch ( strlen( $strHexVal ) ) {
	   		
	   		case '1':
	   			$this->m_strRawData	.= '000' . $strHexVal;
	   			break;
	   	
	   		case '2':
	   			$this->m_strRawData	.= '00' . $strHexVal;
	   			break;
	   			 
	   		case '3':
	   			$this->m_strRawData	.= '0' . $strHexVal;
	   			break;
	   	
	   		default:
	   			$this->m_strRawData	.= $strHexVal;
	   }
		   
			//   return $this->m_strData;
	 }

	function strBytes($str){
	 	// STRINGS ARE EXPECTED TO BE IN ASCII OR UTF-8 FORMAT
	 
	 	// Number of characters in string
	 	$strlen_var = strlen($str);
	 
	 	// string bytes counter
	 	$d = 0;
	 
	 	/*
	 	 * Iterate over every character in the string,
	 	 * escaping with a slash or encoding to UTF-8 where necessary
	 	 */
	 	for($c = 0; $c < $strlen_var; ++$c){
	 		$ord_var_c = ord($str{$c});
	 		switch(true){
	 			case(($ord_var_c >= 0x20) && ($ord_var_c <= 0x7F)):
	 				// characters U-00000000 - U-0000007F (same as ASCII)
	 				$d++;
	 				break;
	 			case(($ord_var_c & 0xE0) == 0xC0):
	 				// characters U-00000080 - U-000007FF, mask 110XXXXX
	 				$d+=2;
	 				break;
	 			case(($ord_var_c & 0xF0) == 0xE0):
	 				// characters U-00000800 - U-0000FFFF, mask 1110XXXX
	 				$d+=3;
	 				break;
	 			case(($ord_var_c & 0xF8) == 0xF0):
	 				// characters U-00010000 - U-001FFFFF, mask 11110XXX
	 				$d+=4;
	 				break;
	 			case(($ord_var_c & 0xFC) == 0xF8):
	 				// characters U-00200000 - U-03FFFFFF, mask 111110XX
	 				$d+=5;
	 				break;
	 			case(($ord_var_c & 0xFE) == 0xFC):
	 				// characters U-04000000 - U-7FFFFFFF, mask 1111110X
	 				$d+=6;
	 				break;
	 			default:
	 				$d++;
	 		};
	 	};
	 	return $d;
	 }
	 
    public function deleteDevices( $strAction ) {
    	// This function written for Delete devices into device table .
    	$intDeviceId 		= $this->uri->segment(4);
    	$intDeviceTypeId 		= $this->uri->segment(5);
    	$intUserId				= $this->session->userdata('user_id');
    	 
		if( true == Devices_model::deleteDeviceByIdByUserId( $intDeviceId, $intUserId ) ) {
			
	    	$this->session->set_userdata( array('sucmsg' =>"Record deleted successfully.") );
		} else {
			$this->session->set_userdata( array('sucmsg' =>"Record cannot be deleted.") );
		}
		redirect('devices/action/view/' .$intDeviceTypeId);
    }
 
    public function changestatus( $strAction ) {
    	$intDeviceId 		= $this->uri->segment(4);
    	if( true == $this->devices_model->updtestatus( $intDeviceId ) ) {
    		redirect('devices/action/view');
    	} 
    }
   
    public function deletecolumn() {
    	$strColumnName = $this->uri->segment(5);
    	$strtableName  = $this->uri->segment(6);
    	
    	if($strColumnName!=""){
    
    		 $this->dbforge->drop_column( $strtableName, $strColumnName );
    	}
    	redirect('admin/devices/action/edit/'.$strtableName);
    }
   
    public function createDevices() {
    	
    	$arrmixPlanInfo = array();
    	$this->load->view( 'add_devices', $arrmixPlanInfo );
    }
    
    public function loadView() {
    	    	
    	$intDeviceId 	= ( int ) $this->uri->segment(4);
    	$intUserId		= ( int ) $this->session->userdata('user_id');
    	
    	$arrmixDevices['device']	= Devices_model::fetchDeviceByIdByUserId( $intDeviceId, $intUserId );
    	
    	if( 0 == count( $arrmixDevices['device'] ) ) {
    		$this->load->view( 'page-not-found' );
    		return;
    	}
    	
    	$this->load->view( 'create_dialog', $arrmixDevices );
    }
    
    
    public function displayEnergyMeterData( $strAction ) {
    	
    	$arrmixFormData	=	$this->input->post();
    
    	$intLimit 	= ( true == isset( $arrmixFormData['limit'] ) ) ? (  int ) $arrmixFormData['limit']  :10;
    	$strSearch = ( true == isset( $arrmixFormData['search'] ) ) ? $arrmixFormData['search']  :NULL;
    	$strStatus = ( true == isset( $arrmixFormData['status'] ) ) ? $arrmixFormData['search']  :NULL;
   
    	$intAdjacent = 2;
    
    	$objEnergyMeterData = new CManageDeviceController();
    	 
    	call_user_func( array( $objEnergyMeterData , 'showEnergyMeterData'), $arrmixFormData, $intLimit, $intAdjacent, $strSearch, $strStatus );
    }
    
    public function showEnergyMeterData($data, $intLimit, $intAdjacent, $strSearch, $strStatus ){
    	
    	$arrmixEnergymeterData		= array();
    	$intPageNo = $data['page'];
    	 
    	if( 1 == $intPageNo ) {
    		$intStartNo = 0;
    	} else {
    		$intStartNo = ( $intPageNo - 1) * $intLimit;
    	}
    	
    	$intDeviceType 		= ( false == empty( $this->uri->segment(4) ) ) ? $this->uri->segment(4) : NULL;
    	    
    	$intUserId				= $this->session->userdata('user_id');
    	$arrobjDevices	= Devices_model::fetchDevicesByUserId( $intUserId, $intDeviceType, $strSearch, $strStatus );
      	$intRows  = $arrobjDevices->num_rows;

		$arrmixDevices['devices']	= Devices_model::fetchDevicesByUserIdByLimit( $intUserId, $intDeviceType, $intStartNo, $intLimit,  $strSearch, $strStatus );
    	
		switch( $intDeviceType ) {
		
			case '1':
				$this->load->view('manage_energy_meter_device', $arrmixDevices );
				break;
		
			case '2':
				$arrmixDevices['msg']	= '';
				$this->load->view('manage_energy_meter_device', $arrmixDevices );
				//$this->load->view('manage_energydata', $arrmixDevices );
				break;
		
			default:
				show_error();
		}
	//	$this->load->view( 'manage_energy_meter_device', $arrmixDevices );
 
    	$this->pagination( $intLimit, $intAdjacent, $intRows, $intPageNo );
    	
    }
    
    public function pagination( $limit, $adjacents, $rows, $page ) {
    
    	$arrmixPaginationData['limit']		= $limit;
    	$arrmixPaginationData['adjacents']	= $adjacents;
    	$arrmixPaginationData['rows']		= $rows;
    	$arrmixPaginationData['page']		= $page;
    
    	$this->load->view( 'view_pagination', $arrmixPaginationData );
    }
    
    public function edit_unique ( $strFieldValue, $strFieldParameter ) {
    
    	$this->form_validation->set_message( 'edit_unique', 'The %s is already exists.' );
    
    	list( $strTableName, $strFieldName, $intId ) = explode( ".", $strFieldParameter, 3 );
    
    	$objDbResult = $this->db->select( $strFieldName )->from( $strTableName )->where( $strFieldName, $strFieldValue )->where( 'id !=', $intId )->limit(1)->get();
    
    	if ( true == ( boolean ) $objDbResult->row() ) {
    		return false;
    	} else {
    		return true;
    	}
    }
    public function image_exist( $strFieldValue ) {
    
    	if( true == empty( $strFieldValue ) ) {    
    		$this->form_validation->set_message('image_exist', 'Please select %s to upload.');
    		return false;
    	}
    	return true;
    }
    
    function valhighcut() {    
    
    	$high = $this->input->post('high_cut');
    	$low = $this->input->post('low_cut');
    	$cal = $high-$low;
    	if ($cal < 50 ) {
    		$this->form_validation->set_message( 'valhighcut', 'High cut should be 50+ greater than low cut');
    		return FALSE;
    	}
    }
    
    public function savepara(  ) {
    	$post_para = json_decode(file_get_contents("php://input"),true);
    	 
    	if( true ==is_array( $post_para ) ) {
    		$stringlength =  count($post_para)-1;
    		$this->load->library( 'validatechecksum' );
    		$stringlength = $stringlength *2;
    		$this->m_strData	= '0215'.$stringlength;
    		$this->m_strRawData	=	 '';
    		foreach( $post_para as $strKey =>$fltValue ) {
    				
    			if( false == empty( $fltValue ) ) {
    
    				$this->convertHextoFourDigit( strtoupper($fltValue) );
    			} else {
    				$this->m_strRawData	.=	'0000';
    			}
    		}
    		$finslstring =  strtoupper($this->m_strData.$this->m_strRawData);
    		$arrData = str_split( $finslstring, 2 );
    		$arrintData = array_values( $arrData );
    		$objvalidateChecksum = new Validatechecksum();
    		$strComputedChecksum =  $objvalidateChecksum->validChecksum( $arrintData );
    		$arrmixUserData = array('parastr' => $finslstring.$strComputedChecksum,
    				'unit_id' => $this->uri->segment(4),
    				'status' => 'active'
    		);
    		if(Devices_model::checkforunitid( $this->uri->segment(4)) > 0)
    		{
    			Devices_model::updatecomparameters( $this->uri->segment(4),$arrmixUserData);
    		}else {
    			Devices_model::insertcomparameters($arrmixUserData);
    		}
    
    	}
    
    }
	
    public function resize( $image_data ) {
    
    	//$img = substr($image_data['full_path'], 51);
    	$config['image_library'] = 'gd2';
    	$config['source_image'] = $image_data['full_path'];
    	$config['new_image'] = 'new_' . $image_data['file_name'];
    	$config['width']	= 39;
    	$config['height']	= 45;
    	$config['create_thumb'] = TRUE;
    	$config['maintain_ratio'] = TRUE;
    	
    	$this->load->library('image_lib', $config);
    	$data['upload_image_name']	= 'new_' . $image_data['raw_name'] . '_thumb.'. $image_data['image_type'];
    	   	
    	$data['img_src'] = base_url() . $this->image_lib->new_image;
    	// Call resize function in image library.
    	//print_r( $this->image_lib ); die( 'Here' );
    	$this->image_lib->resize();

    	// Return new image contains above properties and also store in "upload" folder.
    	return $data;
    }

}
