<?php
class SessionData{
	 
	function initializeData() {
		// This function will run after the constructor for the controller is ran
		// Set any initial values here
				$ci =& get_instance(); 
				if(!isset($ci->session))  //Check if session lib is loaded or not
				$ci->load->library('session');
				 $method=$ci->uri->segment(3);
				if( $method=='login' || $method=='forgotpass' || $method=='register'|| $method=='resetpass' || $method=='load' )
				{
					return;
				}else
				{
					if(!$ci->session->userdata('is_logged_in')){    //this is line 13
				    	
						redirect('users/action/login');
					
				      }
				}
		
			}
}
