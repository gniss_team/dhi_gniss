<?php
class CheckUserSession{
	 
	protected $ci;
	
	public function __construct(){
		
		if( false == isset( $ci->session ) ) {
			
			$this->ci =& get_instance();
			$this->ci->load->library('session');
		}
		 
	
	}
	
	public function checkSessionExist() {

			$strClassName 			= $this->ci->router->fetch_class();
			$strMethodName		= $this->ci->uri->segment(3);
			
			$live_track_method		= $this->ci->router->fetch_method();
			
			$arrstrMethods	= array( 'login','webserviceLogin','webserviceDevices','webserviceVTSDeviceList','webserviceLatLongByPhoneNumberAndDeviceId','webserviceAddDevice','webserviceDeviceTypesList','webserviceDeviceTableColumnsByDeviceTypeId','webserviceDevicetypes', 'webserviceDeviceData','forgotPass','register','resetPass','addUser','insertser','checkimminumber', 'viewmap','verify','signin', 'checkSecurityQuestion',  'showSecurityQuestion','about_us', 'contact_us', 'disclaimer', 'privacy_policy','terms-conditions','viewprofile','allcountries','webserviceChangePassword','webserviceUpdateGeneralProfile','webserviceUpdateContactDetail','webserviceUpdateAddress','webserviceUpdateNotify','webserviceUploadProfileImage', 'webserviceViewGraph', 'webserviceRouteWiseLocation', 'webserviceGetRoute', 'webserviceUpdateSocial','webserviceGetLocation', 'webserviceCreateOtp', 'webserviceVerifyOtp', 'webserviceGetLastVTSRecord' ,'drivergprs','insertDrivergprs','getnearByDrivers');
			$arrstrClass			= array( 'viewcaptcha' );
			$arrstrLiveTrackMethod	= array ( 'checkImeiNumber' , 'insertdata', 'viewLiveGraph', 'ajaxgetEnergyStats' );
			$arrAll = array('about_us', 'contact_us', 'disclaimer', 'privacy_policy','terms-conditions');
					
			if( true == in_array( $strMethodName, $arrstrMethods ) || true == in_array( strtolower($strClassName), $arrstrClass ) || true == in_array( $live_track_method, $arrstrLiveTrackMethod ) ) {
				
				if( 'admin' == $this->ci->uri->segment(1) ) {
					if( !$this->ci->session->userdata('admin_user_id') && !$this->ci->session->userdata( 'user_id' ) ) {
						return true;
						
					} else if( !$this->ci->session->userdata( 'admin_user_id' ) && $this->ci->session->userdata( 'user_id' ) ) {
						redirect( base_url() . 'users/action/login' );
						
					} else {  
						redirect( base_url() . 'admin/action/viewdashboard' );
					}
				} else {
					if( !$this->ci->session->userdata( 'user_id' ) && !$this->ci->session->userdata( 'admin_user_id' ) ) {
						return true;
						
					} else if( $this->ci->session->userdata( 'admin_user_id' ) && !$this->ci->session->userdata( 'user_id' ) ) {
						
						redirect( base_url() . 'admin/action/viewdashboard' );
					
						
					} else {  
						if(true == in_array( $strMethodName, $arrAll ))
						{
							return true;
								
						}else {
							redirect( base_url() );
						}
					}
				}

			} else {
				if( 'admin' == $this->ci->uri->segment(1) ) {
					if( !$this->ci->session->userdata('admin_user_id') ) {
						redirect( base_url() . 'admin/action/login' );
						
					} else {
						return true;
					}
				} else { 
					if( !$this->ci->session->userdata( 'user_id' ) ){
						redirect( 'users/action/login' );
					} else { 
						return true;
						
					}
				}
			}
	
	}

}
